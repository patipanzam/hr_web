FROM nginx:1.10-alpine
LABEL maintainer SiritasS <siritas@gmail.com>

COPY dist /usr/share/nginx/html

EXPOSE 80
