# IHR Web

The front-end project for iHR application.

## Prerequisite

* NodeJS and npm. To setup go to [nodejs.org](https://nodejs.org/en/)

## Build instruction

After the project was cloned. Change directory into the ihrweb.  
Update project's depedencies.
```
$ npm install
```

**Development run**
```
$ npm run start
```

Find the URL to access the application from the console
```
...
webpack result is served from http://localhost:8080/
...
```

If the application cannot access or cannot start, check the port 8080; is it available?.
Or change the port

`package.json`:
```
    "start": "webpack-dev-server --inline --progress --port 8080",
```
Change `8080` to `3000`

`webpack.dev.js`:
```
publicPath: 'http://localhost:3000/',
```
Change from `8080` to `3000`

## Deploy to Soft Square

1. In the `ihrweb` folder execute command `npm run build`.

2. Copy contents inside `dist` to the server

You can use following command line OR using your familiar file transfering tool.

```sh 
$ scp -r dist/* hrapp@172.16.0.43:/home/hrapp/apps/ihr-app/ihr-front
```

3. The app will serve at URL: `http://172.16.0.43:3000/`

## Deploy to Cloud

Normally source code that push to git repository will automatically build.

But if it is not succeed, developer must manually do it.

1. Install `netlify-cli` follow the instruction [here](https://www.netlify.com/docs/).

2. In the `ihrweb` folder execute command `npm run build`.

3. After successfully build execute command `netlify deploy`.  
If you're deploy for the 1st time you might need to authorize the CLI command in the Netlify website.

4. If there're no error the new codes should lived on this URL: `https://hrweb.ssgarage.ml`
