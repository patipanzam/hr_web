import { Injectable } from '@angular/core';

@Injectable()
export class RestServerService {

  constructor() { }

  public getAPI(postFix: string): string {
     return `http://localhost:4002/${postFix}`;
  }
}
