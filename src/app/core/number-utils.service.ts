import { Injectable } from '@angular/core';
declare var numeral: any;

export class NumberUtils {

  public static format(value : number | string, format: string = '0,0.00') : string {
    return numeral(value).format(format);
  }

  public static parse(value : string) : number {
    return numeral().unformat(value);
  }

}
