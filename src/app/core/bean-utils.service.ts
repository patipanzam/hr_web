export class BeanUtils {
    public static isEmpty(dataCheck: any): boolean{
        var result = false;
        if(dataCheck == undefined || dataCheck.toString() == "" || dataCheck.toString() == null){
            result = true;
        }
    return result;
    }

    public static isNotEmpty(dataCheck: any): boolean{
        return !this.isEmpty(dataCheck);
    }

    public static toArray(obj: Object){
        return Object.keys(obj).map((key)=>{return obj[key]});
    }

    public static lPad(data: number, digit: number){
      var pad = "";
      for(var i=0;i<digit;i++) {
        pad += "0";
      }

      var str = "" + data;
      return pad.substring(0,pad.length - str.length) + str;
    }
}
