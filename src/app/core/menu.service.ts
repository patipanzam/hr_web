import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs/Rx";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MenuService {
  private _menus: any;

  private dataStore: {
    menus: any[]
  };

  constructor() {
    this.dataStore = {
      menus: []
    };
    this._menus = <BehaviorSubject<any[]>>new BehaviorSubject([]);
  }

  clearMenu() {
     this.dataStore.menus = [];
    this._menus.next(Object.assign({}, this.dataStore).menus);
  }

  get menus() {
      return this._menus.asObservable();
  }

  set menus(menu: Observable<any>) {
    let dataObservable = Observable.from(menu);
    dataObservable.subscribe(data => {
      this.dataStore.menus = data;
      this._menus.next(Object.assign({}, this.dataStore).menus);
    }, error => console.log('Could not load menu'));
  }

  private pmMenus: Observable<any[]> = Observable.from([
    [ {
      title: 'Master',
      name: 'SIPMRT01',
      path: '/pm/sipmrt01',
      desc: 'เพิ่มข้อมูลพนักงาน'

    },{
      title: 'Master',
      name: 'SIPMRT02',
      path: '/pm/sipmrt02',
      desc: 'กำหนดวันลาพนักงาน'

    }]
  ]);



  private taMenus: Observable<any[]> = Observable.from([[{
    title: 'Master',
    name: 'SITART01',
    path: '/ta/sitart01',
    desc: 'กำหนดกลุ่มการลา'
  }, {
    title: 'Master',
    name: 'SITART02',
    path: '/ta/sitart02',
    desc: 'กำหนดประเภทการลา'
  }, {
    title: 'Master',
    name: 'SITART03',
    path: '/ta/sitart03',
    desc: 'กำหนดกลุ่มการทำงาน'
  }, {
    title: 'Master',
    name: 'SITART04',
    path: '/ta/sitart04',
    desc: 'กำหนดสถานะการทำงาน'
  }, {
    title: 'Master',
    name: 'SITART05',
    path: '/ta/sitart05',
    desc: 'กำหนดวันหยุดประจำปี'
  }, {
    title: 'Master',
    name: 'SITART06',
    path: '/ta/sitart06',
    desc: 'กำหนดงวดการทำงาน'
  }, {
    title: 'Master',
    name: 'SITART07',
    path: '/ta/sitart07',
    desc: 'กำหนดกลุ่มการทำงานตามแผนก'
  }, {
    title: 'Master',
    name: 'SITART08',
    path: '/ta/sitart08',
    desc: 'กำหนดกลุ่มการทำงานตามพนักงาน'
  }, {
    title: 'Transaction',
    name: 'SITADT01',
    path: '/ta/sitadt01',
    desc: 'บันทึกข้อมูลการลาของพนักงาน'
  }, {
    title: 'Transaction',
    name: 'SITADT02',
    path: '/ta/sitadt02',
    desc: 'บันทึกข้อมูลการทำล่วงเวลาของพนักงาน'
  }, {
    title: 'Transaction',
    name: 'SITADT03',
    path: '/ta/sitadt03',
    desc: 'ข้อมูลปฏิทินการทำงาน, ทำล่วงเวลา และการลาของพนักงาน'
  }, {
    title: 'Transaction',
    name: 'SITADT04',
    path: '/ta/sitadt04',
    desc: 'รับข้อมูลจากเครื่องบันทึกเวลาเข้าออก'
  }, {
    title: 'Transaction',
    name: 'SITADT05',
    path: '/ta/sitadt05',
    desc: 'ข้อมูลรายละเอียดการมาปฏิบัติงานประจำวัน'
  }, {
    title: 'Transaction',
    name: 'SITADT06',
    path: '/ta/sitadt06',
    desc: 'รวบรวมข้อมูลการมาปฏิบัติงาน และการลาประจำงวด'
  }, {
    title: 'ProcessEndOfPeriod',
    name: 'SITAPC01',
    path: '/ta/sitapc01',
    desc: 'โอนข้อมูลขาดงาน มาสาย ไประบบเงินเดือน'
  }, {
    title: 'ProcessEndOfPeriod',
    name: 'SITAPC02',
    path: '/ta/sitapc02',
    desc: 'โอนข้อมูลทำล่วงเวลา ไประบบเงินเดือน'
  }, {
    title: 'ProcessEndOfYear',
    name: 'SITAPC03',
    path: '/ta/sitapc03',
    desc: 'ปิดข้อมูลประจำปี'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITAIN01',
    path: '/ta/sitain01',
    desc: 'สอบถามการปฏิบัติงานผิดปกติของพนักงาน'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITAIN02',
    path: '/ta/sitain02',
    desc: 'สอบถามวันลาคงเหลือของพนักงาน'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITARP01',
    path: '/ta/sitarp01',
    desc: 'รายงานการลางานของพนักงาน'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITARP02',
    path: '/ta/sitarp02',
    desc: 'รายงานการขอทำล่วงเวลาของพนักงาน'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITARP03',
    path: '/ta/sitarp03',
    desc: 'รายงานรายละเอียดบันทึกเวลาทำงานของพนักงาน'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITARP04',
    path: '/ta/sitarp04',
    desc: 'รายงานตรวจสอบสถานะการปฏิบัติงานประจำวัน'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITARP05',
    path: '/ta/sitarp05',
    desc: 'รายงานการทำล่วงเวลาของพนักงาน'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITARP06',
    path: '/ta/sitarp06',
    desc: 'รายงานสรุปเวลาการมาปฏิบัติงาน ไม่มาปฏิบัติงาน และทำล่วงเวลา ตามวันที่'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITARP07',
    path: '/ta/sitarp07',
    desc: 'รายงานสรุปเวลาการปฏิบัติงาน มาปฏิบัติงานผิดปกติ และลาหยุด ประจำงวด'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITARP08',
    path: '/ta/sitarp08',
    desc: 'รายงานสรุปเวลาทำล่วงเวลาประจำงวด'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITARP09',
    path: '/ta/sitarp09',
    desc: 'รายงานตรวจสอบโอนข้อมูลขาดงาน มาสายเกิน ของพนักงาน'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITARP10',
    path: '/ta/sitarp10',
    desc: 'รายงานตรวจสอบโอนข้อมูลทำล่วงเวลา'
  }, {
    title: 'ReportEndOfYear',
    name: 'SITARP11',
    path: '/ta/sitarp11',
    desc: 'รายงานสรุปการลาพักผ่อนประจำปี'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITARP12',
    path: '/ta/sitarp12',
    desc: 'รายงานกลุ่มการทำงานตามแผนก'
  }, {
    title: 'ReportEndOfPeriod',
    name: 'SITARP13',
    path: '/ta/sitarp13',
    desc: 'รายงานกลุ่มการทำงานตามพนักงาน'
  }]]);

  

  public getMenus(path: string): Observable<any[]> {
    if (path == '/pm') {
      return this.pmMenus;
    } else if (path == '/ta') {
      return this.taMenus;
    }
    return Observable.empty();
  }
}
