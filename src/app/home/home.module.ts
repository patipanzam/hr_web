import {​NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';

import {HomeComponent} from './home.component';
import {Home1Component} from './home1.component';
import {Home2Component} from './home2.component';
import {HomeRoutingModule} from './home-routing.module';

@NgModule({
    imports: [SharedModule, HomeRoutingModule],
    declarations: [ HomeComponent, Home1Component, Home2Component],
    providers: []
})
export class HomeModule {
}
