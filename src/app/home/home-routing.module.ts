import { NgModule }       from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { Home1Component } from './home1.component';
import { Home2Component } from './home2.component';

@NgModule({
  imports: [
    RouterModule.forChild([{
      path: '',
      component: HomeComponent,
      children: [{
        path: ''
      },{
          path: 'home1',
          component: Home1Component
      }, {
          path: 'home2',
          component: Home2Component
      }]
    }])
  ],
  exports: [ RouterModule ],
  providers: [ ]
})
export class HomeRoutingModule {
}
