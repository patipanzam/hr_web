import { Pipe, PipeTransform } from '@angular/core';
const moment = require('moment');

@Pipe({ name: 'thaiDateFormat' })
export class ThaiDatePipe implements PipeTransform {
  constructor() {
  }

  transform(value: Date | string): string {
    var day = moment(value);
    var year = Number.parseInt(day.format('YYYY'))+543;
    return day.format('DD/MM/') + year;
  }

}
