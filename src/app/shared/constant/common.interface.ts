export class RowStatus {
  static addStatus: string = "A";
  static modifyStatus: string = "M";
  static deleteStatus: string = "D";
}

export class SearchStatus {
  static normalSearch: number = 0;
  static advanceSearch: number = 1;
}

export class PigingSetup {
  static start: number = 0;
  static limit: number = 10;
  static total: number = 0;
  static _activePage: number = 0;
  static _pages: number = 0;
  static _total: number = 0;
  static _cursor: number = 0;
  static _count: number = 0;
  static _pageSize: number = 10;
}
