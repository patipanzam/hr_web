import { BeanUtils } from '../core/bean-utils.service';
import { LoadMask } from '../load-mask.component';


export class ErrorMessage {

    private messageTitle : string =  "ผิดพลาด";
    private messageBody : string = "กรุณาติดต่อผู้ดูแลระบบ";

    constructor() {
    }


    public setMessageError(error: any) {
        if (error.indexOf("SqlErrorCode: 1451") != -1) {
            this.messageTitle = "แจ้งเตือน";
            this.messageBody = "มีการอ้างอิงข้อมูลไปใช้งานแล้ว ไม่สามารถลบรายการได้";
        }
    }

    public getMessageTitle() : string{
        return this.messageTitle;
    }

    public getMessageBody() : string{
        return this.messageBody;
    }

}
