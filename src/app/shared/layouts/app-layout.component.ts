import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { MenuService } from './../../core/menu.service';
import { AuthService } from './../../auth.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.scss']
})
export class AppLayoutComponent implements OnInit {

  public disabled: boolean = false;
  public status: { isopen: boolean } = { isopen: false };
  public menus: Observable<any[]>;
  public mainPath: string;
  // public masterMenus: Array<any>;
  // public transactionMenus: Array<any>;
  // public processMenus: Array<any>;
  // public reportMenus: Array<any>;

  constructor(private menuService: MenuService, private authService: AuthService, private location: Location) { }

  ngOnInit(): void {
    var selector = $('.sidebar li.nav-dropdown');
    this.mainPath = this.location.path().substring(0, 3);
    this.menus = this.menuService.getMenus(this.mainPath);


    $(selector[0].children[0]).on('click', (event) => {
      let menuFilter: any[] = [];
      this.menus.subscribe(data => {
        menuFilter = data.filter((menu: any) => {
          return menu.title === 'Master';
        });

        this.menuService.menus = Observable.from([menuFilter]);
      });
    });

    $(selector[1].children[0]).on('click', (event) => {
      let menuFilter: any[] = [];
      this.menus.subscribe(data => {
        menuFilter = data.filter((menu: any) => {
          return menu.title === 'Transaction';
        });

        this.menuService.menus = Observable.from([menuFilter]);
      });
    });

    $(selector[2].children[1].children[0].children[0]).on('click', (event) => {
      let menuFilter: any[] = [];
      this.menus.subscribe(data => {
        menuFilter = data.filter((menu: any) => {
          return menu.title === 'ProcessEndOfPeriod';
        });

        this.menuService.menus = Observable.from([menuFilter]);
      });
    });

    $(selector[2].children[1].children[1].children[0]).on('click', (event) => {
      let menuFilter: any[] = [];
      this.menus.subscribe(data => {
        menuFilter = data.filter((menu: any) => {
          return menu.title === 'ProcessEndOfYear';
        });

        this.menuService.menus = Observable.from([menuFilter]);
      });
    });

    $(selector[3].children[1].children[0].children[0]).on('click', (event) => {
      let menuFilter: any[] = [];
      this.menus.subscribe(data => {
        menuFilter = data.filter((menu: any) => {
          return menu.title === 'ReportEndOfPeriod';
        });

        this.menuService.menus = Observable.from([menuFilter]);
      });
    });

    $(selector[3].children[1].children[1].children[0]).on('click', (event) => {
      let menuFilter: any[] = [];
      this.menus.subscribe(data => {
        menuFilter = data.filter((menu: any) => {
          return menu.title === 'ReportEndOfYear';
        });

        this.menuService.menus = Observable.from([menuFilter]);
      });
    });
  }

  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  logout() {
    this.authService.logout();
  }

  show() {
    if ( $(".collapse").css('display') == 'none' ){
      $(".collapse").show();
    } else {
      $(".collapse").hide();
    }
  }

  slideBrand() {
    if ( $(".navbar-brand.header").css("height") == "60px" ){
      $(".navbar-brand.header").css({
        "background-size": "70px auto",
        "height":"100px"
      });
    } else {
      $(".navbar-brand.header").css({
        "background-size": "50px auto",
        "height":"60px"
      });
    }
  }

  onActivate(event: Event) {


    // this.menus = this.menuService.getMenus(this.mainPath);

    // this.masterMenus = this.menus.filter((menu) => {return menu.title == 'Master'});
    // this.transactionMenus = this.menus.filter((menu) => {return menu.title == 'Transaction'});
    // this.processMenus = this.menus.filter((menu) => {return menu.title == 'Process'});
    // this.reportMenus = this.menus.filter((menu) => {return menu.title == 'Report'});
  }

  onDeactivate(event: Event) {
  }
}
