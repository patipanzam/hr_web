import { Component, OnInit, ViewChild, Input  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonSelectService } from '../service/common-select.service';
import { ModalDirective } from 'ng2-bootstrap';
import { LoadMask } from '../../load-mask.component';
import { FileUploader, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { RestServerService } from '../../core/rest-server.service';


@Component({
  selector: 'upload-file',
  templateUrl: './upload-file.component.html'
})
export class UploadFileComponent implements OnInit {
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @Input('info') private info: any;
  @Input('empId') private empId: any;
  @Input('module') private module: any;

  constructor(
    private service: CommonSelectService,
    private router: Router,
    private routerActive: ActivatedRoute, 
    private restServer: RestServerService) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  ngOnInit() {
    this.onActive();
      this.uploader = new FileUploader({  
          url: this.restServer.getAPI(this.module+'/common/uploadFile') + '?employeeId=' + this.empId +'&module='+this.module,
          method: "POST",
          autoUpload: true,
          maxFileSize: 1024*1024*10 //10 MB
        });

    this.uploader.onAfterAddingAll = (fileItems: any) => {
      LoadMask.show();
    }
    this.uploader.onCompleteAll = () => {
        this.listFile();
        LoadMask.hide();
        this.alertModalToggle('สำเร็จ', 'อัพโหลดไฟล์สำเร็จสำเร็จ');
        $(".modal-backdrop").remove();
    }
    this.uploader.onWhenAddingFileFailed = (fileItem) => {
        if(fileItem.size > 1024*1024*10 ){
          this.alertModalToggle('ผิดพลาด', 'ขนาดไฟล์เกิน 10 MB');
          $(".modal-backdrop").remove();
        }
}
  }

  onActive() {
    this.listFile();
  }

  private fromLoadObject: any;
  private rows: any[] = [];
  private alert = { title: '', message: '' };
  public uploader:FileUploader = new FileUploader({});
  public hasBaseDropZoneOver:boolean = false;

  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }

  public listFile() {
    LoadMask.show();
    this.service.listFile(this.empId, this.module)
      .subscribe(response => {
        this.rows = response.data;
        LoadMask.hide();
      }, error => {
        console.error(error);
        LoadMask.hide();
      }
      );
  }
  public downloadFile(item: any) {
    var param = {
      fileName: item.empOtherDocumentName,
      employeeId: this.empId,
      module: this.module
    };
    var fileTypeSplit = item.empOtherDocumentName.split(".");
    var fileType = fileTypeSplit[fileTypeSplit.length-1];
    var mimeType : any;
    LoadMask.show();
    this.service.downloadFile(param, this.module).subscribe(response => {
    var a = document.createElement('a');
    var file = new Blob([response._body]
        , {
            type: mimeType
          }
        );
        var fileURL = URL.createObjectURL(file);
    a.href = fileURL;
    a.download = item.empOtherDocumentName;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    window.URL.revokeObjectURL(fileURL);    
    LoadMask.hide();
      }, error => {
        console.error(error);
        LoadMask.hide();
      });
  }

  public deleteFile(item: any ,flag: any) {
    var param = {
      fileName: item.empOtherDocumentName,
      employeeId: this.empId,
      empOtherDocumentId: item.empOtherDocumentId,
      module: this.module
    };
      LoadMask.show();
      this.service.deleteFile(param, this.module).subscribe(response => {
      this.listFile();
      LoadMask.hide();
      this.alertModalToggle('สำเร็จ', 'ลบไฟล์สำเร็จสำเร็จ');
      $(".modal-backdrop").remove();
        }, error => {
          console.error(error);
          LoadMask.hide();
        });
  }

  private alertModalToggle(title: string, message: string): void {
    this.alert.title = title;
    this.alert.message = message;
    this.alertModal.show();
  }
}
