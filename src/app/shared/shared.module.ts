import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Ng2BootstrapModule } from 'ng2-bootstrap';

// import { SIDEBAR_TOGGLE_DIRECTIVES } from './sidebar.directive';
// import { AsideToggleDirective } from './aside.directive';
// import { BreadcrumbsComponent } from './breadcrumb.component';

import { NumberPipe } from './number.pipe';
import { NumberField } from './number-field.directive';
import { NumberOnlyField } from './number-only-field.directive';
import { ThaiField } from './thai-field.directive';
import { ThaiOnlyField } from './thai-only-field.directive';
import { EngField } from './eng-field.directive';
import { EngOnlyField } from './eng-only-field.directive';
import { TextOnlyField } from './text-only-field.directive';
import { PaginationComponent } from './pagination.component';

import { NAV_DROPDOWN_DIRECTIVES } from './nav-dropdown.directive';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './sidebar.directive';
import { AsideToggleDirective } from './aside.directive';
import { BreadcrumbsComponent } from './breadcrumb.component';

import { AppLayoutComponent } from './layouts/app-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';

import { Select } from './ss-select.directive';
import { NKDatetime } from 'ng2-datetime/ng2-datetime';
import { SSDatePicker } from './ss-datepicker.component';
import { SSTimePicker } from './ss-timepicker.component';
import { SSToggle } from './ss-toggle.component';
import { PercentField } from './percent-field.directive';
import { ThaiDatePipe } from './thaidate.pipe';

@NgModule({
  imports: [CommonModule, FormsModule, RouterModule, Ng2BootstrapModule],
  declarations: [
    NumberPipe, NumberField, PaginationComponent, NumberOnlyField,ThaiField,EngField,
    ThaiOnlyField,EngOnlyField,TextOnlyField,AppLayoutComponent, SimpleLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES, BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    Select, NKDatetime, SSDatePicker, SSTimePicker, SSToggle, PercentField, ThaiDatePipe
  ],
  exports: [
    NumberPipe, NumberField, PaginationComponent, NumberOnlyField,ThaiField,EngField,
    ThaiOnlyField,EngOnlyField,TextOnlyField,CommonModule, FormsModule,
    AppLayoutComponent, SimpleLayoutComponent,
    Select, SSDatePicker, SSTimePicker, SSToggle, PercentField, ThaiDatePipe
  ]
})
export class SharedModule {
}
