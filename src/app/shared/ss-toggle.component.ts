import { Component, Output, Input, EventEmitter, ElementRef} from '@angular/core';

@Component({
  selector: 'ss-toggle',
  templateUrl: `
    <div class="form-actions">
      <button [class]="getStyleButton()" type="button" (click)="isRemoveRow = !isRemoveRow;">
        <span [class]="getIcon()"></span>
      </button>
    </div>
  `
})

export class SSToggle {
  isRemoveRow: false;

  constructor() {
  }

  getIcon() {
    if(this.isRemoveRow){
      return 'fa fa-undo';
    } else {
      return 'fa fa-remove';
    }
  }

  getStyleButton() {
    if(this.isRemoveRow){
      return 'btn btn-success btn-sm';
    } else {
      return 'btn btn-danger btn-sm';
    }
  }
}
