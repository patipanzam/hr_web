export enum PaymentType {
  Salary = 18,
  OvertimePay = 19,
  Bonus = 20,
  Commission = 59,
  Allowance = 60,
  TravelingExpenses = 61,
  OtherIncome = 62
}
export enum PaymentType {
  ProvidentFund = 23,
  OtherDeductions = 67
}
export enum TaxRefundType {
  taxPayer = 1,
  spouse = 2,
  childStudy = 3,
  childNotStudy = 4,
  taxPayerParentalCare = 5,
  // xxx = 6,
  spouseParentalCare = 7,
  // xxx = 8,
  incompetentPersonSupport = 9,
  healthInsurancePremiumForParent = 10,
  lifeInsurancePremiumPaid = 11,
  pensionInsurancePremiumPaid = 12,
  // xxx = 13,
  retirementMutualFundUnitPurchase = 14,
  longTermEquityFundUnitPurchase = 15,
  interestPaidOfResidence = 16,
  socialSecurityFundContribution = 17,
  travelingInCountry = 18,
  otopAllowances = 19,
  songkranFestivalAllowances = 20,
  firstHouse = 21,
  nationalSavingsFund = 22,
  helpGovermentShopping = 23,
  lessDonationSupportingEducation = 24,
  lessOtherDonation = 25
}


// 1 ค่าลดหย่อนส่วนตัว
// 2 ค่าลดหย่อนคู่สมรส
// 3 ค่าลดหย่อนบุตร
// 4 ค่าลดหย่อนบุตรกำลังศึกษา
// 5 อุปการะเลี้ยงดูบิดามารดาของผู้มีเงินได้
// 7 อุปการะเลี้ยงดูบิดามารดาของคู่สมรส
// 9 อุปการะเลี้ยงดูคนพิการหรือคนทุพพลภาพ
// 10 เบี้ยประกันสุขภาพบิดามารดา
// 11 เบี้ยประกันชีวิต
// 12 เบี้ยประกันชีวิตแบบบำนาญ
// 13 เงินสะสมกองทุนสำรองเลี้ยงชีพ  (ส่วนที่ไม่เกิน 10,000.00)
// 14 ค่าซื้อหน่วยลงทุนกองทุนรวมเพื่อการเลี้ยงชีพ (RTF)/กบข./กองทุนสำรองเลี้ยงชีพ/กองทุนสงเคราะห์ครูโรงเรียนเอกชน
// 15 "ค่าซื้อหน่วยลงทุนในกองรวมหุ้นระยะยาว (LTF) "
// 16 ดอกเบี้ยเงินกู้ยืมเพื่อซื้อ เช่าซื้อ หรือสร้างอาคาร
// 17 เงินสมทบกองทุนประกันสังคม
// 18 ค่าการเดินทางท่องเที่ยวในประเทศ
// 19 ค่าลดหย่อน OTOP
// 20 ค่าลดหย่อนกินเที่ยวช่วงสงกรานต์
// 21 โครงการบ้านหลังแรก
// 22 กองทุนการออมแห่งชาติ (กอช.)
// 23 ค่าลดหย่อนช้อปปิ้ง (ช้อปช่วยชาติ)
// 24 เงินบริจาคเพื่อการศึกษา การกีฬา การพัฒนาสังคม
// 25 เงินบริจาคทั่วไป
