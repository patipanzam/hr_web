import { Component, Output, Input, EventEmitter, ElementRef, OnInit, OnChanges, HostListener } from '@angular/core';
import { BeanUtils } from '../core/bean-utils.service';

declare var $: any;

@Component({
  selector: 'ss-datepicker',
  templateUrl: `
  <div *ngIf="this.readOnly == false">
    <datetime [(ngModel)]="date" (ngModelChange)="handleDateFromChange($event)" [timepicker]="false" [datepicker]="datepickerOpts"></datetime>
  </div>

  <div *ngIf="this.readOnly == true">
    <datetime [(ngModel)]="date" readonly="true" (ngModelChange)="handleDateFromChange($event)" [timepicker]="false" [datepicker]="datepickerOpts"></datetime>
  </div>
  `
  //   <datetime [(ngModel)]="date" (ngModelChange)="handleDateFromChange($event)" [timepicker]="false" [datepicker]="datepickerOpts"></datetime>
  // `
})

export class SSDatePicker implements OnInit {
  @Output('change') private dateChange: EventEmitter<Date> = new EventEmitter<Date>();
  @Input('dateFrom') private dateFrom: SSDatePicker;
  @Input('dateTo') private dateTo: SSDatePicker;

  @Input('readOnly') readOnly:boolean = false;

  @Input('setValueDate') private setValueDate: any;
  @Input('datepickerOpts') private datepickerOpts: any;

  private _date:Date = null;
  private get date():Date {
    return this._date;
  }

  private set date(value:Date) {
    this._date = value;
  }

  constructor(private el: ElementRef) {
  }

  ngOnInit() {
    this.datepickerOpts = {
      'startDate': '',
      'endDate': '',
      'icon': 'fa fa-calendar',
      'autoclose': true,
      'format': 'dd/mm/yyyy',
      'enableOnReadonly': false
    }

    if(this.setValueDate) {
      this.date = new Date(this.setValueDate) || null;
    }

    if(BeanUtils.isNotEmpty(this.setValueDate)){
     this.date = new Date(this.setValueDate) || null;
   }
  }

  private handleDateFromChange(value: any) {
    if (value) {
      if (this.dateFrom) {
        if (this.dateFrom.date == null) {
          this.dateFrom.date = value;
        }
      }

      if (this.dateTo) {
        if (value > this.dateTo.date) {
          this.dateTo.date = null;
        }

        this.dateTo.datepickerOpts = {
          'startDate': value,
          'endDate': '',
          'icon': 'fa fa-calendar',
          'autoclose': true,
          'format': 'dd/mm/yyyy',
          'enableOnReadonly': false
        }
      }
    }
    this.dateChange.emit(value);
  }

  setValue(value : number) {
    this.date = new Date(value) || null;
  }

  getValue() {
    return this.date ? new Date(this.date) : null;
  }

  getMilliSecond() {
    return this.date ? new Date(this.date).getTime() : null;
  }

  clearValue() {
    this.date = null;
  }
}
