import { Component, Output, Input, EventEmitter, ElementRef, OnInit, OnChanges, HostListener } from '@angular/core';
import { BeanUtils } from '../core/bean-utils.service';

declare var $: any;

@Component({
  selector: 'ss-timepicker',
  templateUrl: `
  <div *ngIf="this.readOnly == false">
    <datetime [(ngModel)]="time" (ngModelChange)="handleTimeChange($event)" [timepicker]="timepickerOpts" [datepicker]="false"></datetime>
  </div>

  <div *ngIf="this.readOnly == true">
    <datetime [(ngModel)]="time" readonly="true" (ngModelChange)="handleTimeChange($event)" [timepicker]="timepickerOpts" [datepicker]="false"></datetime>
  </div>
  `
})

export class SSTimePicker implements OnInit {
  @Output('change') private timeChange: EventEmitter<Date> = new EventEmitter<Date>();
  @Input('readOnly') readOnly:boolean = false;
  @Input('timepickerOpts') private timepickerOpts: any;

  private specifyDate: Date = null;

  private _time:Date = null;
  private get time():Date {
    return this._time;
  }

  private set time(value:Date) {
    this._time = value;
  }

  constructor(private el: ElementRef) {
  }

  ngOnInit() {
    this.timepickerOpts = {
      'showMeridian': false,
      'minuteStep': 1,
      'icon': 'fa fa-clock-o',
      'icons' : {
        'up':' fa fa-chevron-up',
        'down':'fa fa-chevron-down'
      }
    }

    this.setTimeToDate(0,0);
  }

  private handleTimeChange(value: Date) {
    this.timeChange.emit(value);
  }

  setValue(value : String) {
    value = value || "00.00";
    this.setTimeToDate((+value.substring(0,2)), (+value.substring(3)));
  }

  getValue() {
    return this.time ? this.pad(this.time.getHours(),2) + ":" + this.pad(this.time.getMinutes(),2) : null;
  }

  clearValue() {
    this.setTimeToDate(0,0);
  }

  private setTimeToDate(hrs:number, mins:number) {
    this.specifyDate = new Date();
    this.specifyDate.setHours(hrs);
    this.specifyDate.setMinutes(mins);

    this.time = new Date(this.specifyDate) || null;
  }

  private pad(num:number, size:number): string {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
  }
}
