import { Pipe, PipeTransform } from '@angular/core';
import { NumberUtils } from '../core/number-utils.service';

@Pipe({ name: 'number' })
export class NumberPipe implements PipeTransform {

  constructor() {
  }

  transform(value: number | string, format: string = '0,0.00'): string {
    return NumberUtils.format(value, format);
  }

}
