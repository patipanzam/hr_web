import { Component, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { NumberField } from './number-field.directive';
import { PigingSetup } from '../shared/constant/common.interface';

export class Page {
  start:number = PigingSetup.start;
  limit:number = PigingSetup.limit;
  total:number = PigingSetup.total;
}

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styles: [require('./pagination.component.scss')]
})
export class PaginationComponent {

  private _activePage: number = PigingSetup._activePage;
  private _pages: number = PigingSetup._pages;
  private _total: number = PigingSetup._total;
  private _cursor: number = PigingSetup._cursor;
  private _count: number = PigingSetup._count;
  private _pageSize: number = PigingSetup._pageSize;

  @ViewChild('activePage')
  private numberField : NumberField;

  @Output('changepage')
  private changeEvent = new EventEmitter<Page>();

  constructor() {
  }

  @Input() set page(page: Page){
    this._cursor = page.start;
    this._pageSize = page.limit;
    this._total = page.total;

    this._activePage = Math.ceil((page.start + page.limit) / page.limit);
    this._pages = page.total < page.limit ? 1 : Math.ceil(page.total/page.limit);
    this._count = this.calculateCount();
  }

  onFirstPage() : void {
    this.onChangePage(1);
  }

  onPreviousPage() : void {
    this.onChangePage(--this._activePage);
  }

  onNextPage() : void {
    this.onChangePage(++this._activePage);
  }

  onLastPage() : void {
    this.onChangePage(this._pages);
  }

  private calculateCount() : number {
    return (this._activePage == this._pages) ? (this._total % this._pageSize == 0 ? this._pageSize : this._total % this._pageSize) : this._pageSize;
  }

  onChangePage(page: number) {
    if(!page || page <= 0 || page > this._pages) {
      this.numberField.reset();
      return;
    }

    this._activePage = page;
    this._cursor = (this._activePage - 1) * this._pageSize;
    this._count = this.calculateCount();

    this.changeEvent.emit({
      start: this._cursor,
      limit: this._pageSize,
      total: this._total
    });
  }
}
