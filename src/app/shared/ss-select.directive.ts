import { Directive, ElementRef, EventEmitter, Renderer, Input, Output } from "@angular/core";
import { RestServerService } from '../core/rest-server.service';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import {Http} from "@angular/http";

declare var $: any;
const querystring = require('querystring');

export class IComboboxConfig {
  valueField?: string;
  labelField?: string;
  searchField?: Array<string>;
  sortField?: string;
  items?: any[];
  currentItem?: any;
  url?: string;
  placeholder?: string;
  renderer?: {
    item?: (data: any, escape: any) => void,
    option?: (data: any, escape: any) => void
  }
  defaultRenderer?: {
    item?: (data: any, escape: any) => void,
    option?: (data: any, escape: any) => void
  }

  constructor() {
    // default renderer combobox
    this.defaultRenderer = {
      option: function (data: any, escape: any) {
        return '<div class="option" data-selectable data-value="' + escape(data.id) + '">' + escape(data.text) + '</div>';
      },
      item: function (data: any, escape: any) {
        return '<div class="item" data-value="' + escape(data.id) + '">' + escape(data.text) + '</div>';
      }
    }
  }
}

@Directive({  // if use multiple select: add attr 'multiple' in html
  selector: 'select[ss-select]',
  exportAs: 'ss-select'
})
export class Select {

  limit: number = 20;
  $select: any
  selectize: any;

  constructor(public el: ElementRef, public renderer: Renderer, public restServer: RestServerService, public authHttp: AuthHttp,) {
  };

  public init(config: IComboboxConfig, changeEvent: EventEmitter<any>) {
    if (config.url) {
      this.renderer.setElementClass(this.el.nativeElement, 'remote', true);
    }

    this.$select = $(this.el.nativeElement).selectize({
      preload: true,
      create: false,
      placeholder: config.placeholder || 'ทั้งหมด',
      valueField: config.valueField || 'id',
      labelField: config.labelField || 'text',
      searchField: config.searchField || ['id', 'text'],
      sortField: config.sortField || 'text',
      options: config.items || [],
      render: config.renderer || config.defaultRenderer,
      load: (query: any, callback: any) => {
        if (!config.url) return callback();
        this.authHttpGet(this.restServer.getAPI('') + config.url, query, this.limit).subscribe(
          (response) => { return callback(response); },
          (error) => { console.log('error, ', error); return callback(); }
        );
      },
      onChange: (valueFields: any) => {
        if (!Array.isArray(valueFields)) {
          let objItems = $.grep(this.getOption(), (e: any) => {
            return e[config.valueField] == valueFields || e.id == valueFields;
          });
          changeEvent.emit(objItems[0] || {});
        } else {
          // multi-select
          let objItems: any[] = [];
          for (let valueField in valueFields) {
            objItems[valueField] = $.grep(this.getOption(), (e: any) => {
              return e[config.valueField] == valueFields[valueField] || e.id == valueFields[valueField];
            })[0];
          }
          changeEvent.emit(objItems || []);
        }
      }
    });

    this.selectize = this.$select[0].selectize;
    if(config.currentItem) {
      this.selectize.setValue(config.currentItem, true);
    }
  }

  public getOption(): any[] {
    var array = $.map(this.selectize.options, function (value: any, index: number) {
      return [value];
    });
    return array;
  }

  public load(dataObservable: Observable<any>) {
    this.selectize.clearOptions();
    this.selectize.load((callback: any) => {
      dataObservable.subscribe(
        (response) => { return callback(response); },
        (error) => { console.log('error, ', error); return callback(); }
      );
    });
  }

  public disabled(disabled: boolean){
     if(disabled == true) {
            this.selectize.disable();
        } else {
            this.selectize.enable();
        }
  }

  public setValue(valueField: any) {
    this.selectize.setValue(valueField, false);
  }

  public clearValue() {
    this.selectize.clear(false);
  }

  public getValue() {
    return this.selectize.getValue();
  }

  private authHttpGet(url: string, query: string, limit: number) {
    query = query || '';
    return this.authHttp.get(url + `?term=${encodeURIComponent(query)}&limit=${limit}`)
      .map(val => { return val.json(); });
  }
}
