import {Component} from '@angular/core';
import {Injectable} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";

@Injectable()
export class PageContext {

    private _search: any;
    get search(): any { return this._search; }
    set search(value: any) { this._search = value; }

    private _stores: any[] = [];
    get stores(): any { return this._stores.pop(); }
    set stores(value: any) { this._stores.push(value); }

    public constructor(private router:Router, private routerActive: ActivatedRoute) { }

    loadPageState(): any {
        let store = this.stores;
        if(store.url==this.router.url) {
            return store;
        }
        else {
            this.stores = [];
            this.search = {};
        }
    }

    setLastSearch(methodName: string, args: any): void {
        this.search = {
            url: this.router.url,
            method: methodName,
            args: args,
            hasSearch: true
        }
    }

    nextPage(routerArgs: any, pageContext: any): void {
        if(this.router.url!=this.search.url) {
            this.search = {};
        }
        this.stores = {
            url: this.router.url,
            pageContext: pageContext,
            search: this.search
        };
        this.search = {};
        this.router.navigate(routerArgs, { skipLocationChange: false });
    }

    backPage(): void {
        let store = this.stores;
        let url = '';
        if(store) {
            url = store.url;
            this.stores = store;
        }
        else {
            let currentUrl = this.router.url;
            url = currentUrl.substring(0,currentUrl.lastIndexOf('/')-1);
        }
        this.router.navigate([url]);
    }

    isLoad(): boolean {
        if(this._stores.length<=0) {
            this.search = {};
        } 
        return this._stores.length>0;
    }

}