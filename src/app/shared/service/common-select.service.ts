import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { RestServerService } from '../../core/rest-server.service';
import {ResponseContentType,Http} from "@angular/http";


const querystring = require('querystring');

@Injectable()
export class CommonSelectService {

  private _moduleName: string = '';

  constructor(public authHttp: AuthHttp, private restServer: RestServerService,private http: Http) { }

  private getRemoteSelect(url: string) {
    return this.http.get(this.restServer.getAPI(url)).map((response) => { return response.json(); });
  }

  private isObservableEmpty(value: any): boolean {
    return !(value.source && value.operator);
  }

  public setModuleName(moduleName: string) {
    this._moduleName = moduleName.replace(/\W|\s/g, '');
  }

  /* [ Remote Select ] use directive : `xxx-select` */

  /* Branch */
  // private _branch: Observable<any> = new Observable<any>();
  // get branch(): any { return this._branch }
  // set branch(value: any) { this._branch = value; }
  // public getBranch(params?: { ouCode?: string, ouCodeStart?: string, ouCodeEnd?: string }): Observable<any> {
  //   if (params && params.ouCode) {
  //     params.ouCodeStart = params.ouCode;
  //     params.ouCodeEnd = params.ouCode;
  //     delete params['ouCode'];
  //   }
  //   let url = (params) ? this._moduleName + '/combobox/branch?' + querystring.stringify(params) : this._moduleName + '/combobox/branch';
  //   return this.isObservableEmpty(this.branch) || params ? this.branch = this.getRemoteSelect(url) : this.branch;
  // }

   /*branch for ouCode only*/
  // private _branchs: Observable<any> = new Observable<any>();
  // get branchs(): any { return this._branchs; }
  // set branchs(value: any) { this._branchs = value; }
  // public getBranchs(params?: {
  //         ouId?: number}): Observable<any> {
  //           let url = (params) ? this._moduleName + '/combobox/branch?' + querystring.stringify(params) : this._moduleName + '/combobox/branch';
  //   return this.isObservableEmpty(this.branch) || params ? this.branch = this.getRemoteSelect(url) : this.branch;
  // }

  /* Country */
  private _country: Observable<any> = new Observable<any>();
  get country(): any { return this._country; }
  set country(value: any) { this._country = value; }
  public getCountry(): Observable<any> {
    return this.isObservableEmpty(this.country) ? this.country = this.getRemoteSelect(this._moduleName + '/combobox/country') : this.country;
  }

  /* Region */
  private _region: Observable<any> = new Observable<any>();
  get region(): any { return this._region; }
  set region(value: any) { this._region = value; }
  public getRegion(): Observable<any> {
    return this.isObservableEmpty(this.region) ? this.region = this.getRemoteSelect(this._moduleName + '/combobox/province/region') : this.region;
  }

  /* Department */
  private _department: Observable<any> = new Observable<any>();
  get department(): any { return this._department }
  set department(value: any) { this._department = value; }
  public getDepartment(params?: { ouCode?: string, ouCodeStart?: string, ouCodeEnd?: string }): Observable<any> {
    if (params && params.ouCode) {
      params.ouCodeStart = params.ouCode;
      params.ouCodeEnd = params.ouCode;
      delete params['ouCode'];
    }
    let url = (params) ? this._moduleName + '/combobox/department?' + querystring.stringify(params) : this._moduleName + '/combobox/department';
    return this.isObservableEmpty(this.department) || params ? this.department = this.getRemoteSelect(url) : this.department;
  }

  /*department for ouCode only*/
  private _departments: Observable<any> = new Observable<any>();
  get departments(): any { return this._departments; }
  set departments(value: any) { this._departments = value; }
  public getDepartments(params?: {
          branchCode?: string}): Observable<any> {
            let url = (params) ? this._moduleName + '/combobox/department?' + querystring.stringify(params) : this._moduleName + '/combobox/department';
    return this.isObservableEmpty(this.department) || params ? this.department = this.getRemoteSelect(url) : this.department;
  }

  /* District */
  private _district: Observable<any> = new Observable<any>();
  get district(): any { return this._district; }
  set district(value: any) { this._district = value; }
  public getDistrict(params?: {
          provinceId?: number}): Observable<any> {
            let url = (params) ? this._moduleName + '/combobox/district?' + querystring.stringify(params) : this._moduleName + '/combobox/district';
    return this.isObservableEmpty(this.district) || params ? this.district = this.getRemoteSelect(url) : this.district;
  }

  /* EmployeeNotFreeze */
  private _employeenotfreeze: Observable<any> = new Observable<any>();
  get employeenotfreeze(): any { return this._employeenotfreeze; }
  set employeenotfreeze(value: any) { this._employeenotfreeze = value; }
  public getEmployeenotfreeze(): Observable<any> {
    return this.isObservableEmpty(this.employeenotfreeze) ? this.employeenotfreeze = this.getRemoteSelect(this._moduleName + '/combobox/employeenotfreeze') : this.employeenotfreeze;
  }

  /* Education Place */
  private _educationPlace: Observable<any> = new Observable<any>();
  get educationPlace(): any { return this._educationPlace; }
  set educationPlace(value: any) { this._educationPlace = value; }
  public getEducationPlace(): Observable<any> {
    return this.isObservableEmpty(this.educationPlace) ? this.educationPlace = this.getRemoteSelect(this._moduleName + '/combobox/education/place') : this.educationPlace;
  }

   /* Education Level */
  private _educationLevel: Observable<any> = new Observable<any>();
  get educationLevel(): any { return this._educationLevel; }
  set educationLevel(value: any) { this._educationLevel = value; }
  public getEducationLevel(): Observable<any> {
    return this.isObservableEmpty(this.educationLevel) ? this.educationLevel = this.getRemoteSelect(this._moduleName + '/combobox/education/level') : this.educationLevel;
  }

  /* Education Id (คณะ) */
  private _educationId: Observable<any> = new Observable<any>();
  get educationId(): any { return this._educationId; }
  set educationId(value: any) { this._educationId = value; }
  public getEducationId(params?:  { educationPlaceId?: number}): Observable<any> {
    let url = (params) ? this._moduleName + '/combobox/education/id?' + querystring.stringify(params) : this._moduleName + '/combobox/education/id';
    return this.isObservableEmpty(this.educationId) || params ? this.educationId = this.getRemoteSelect(url) : this.educationId;
  }

  /* Employee */
  private _employee: Observable<any> = new Observable<any>();
  get employee(): any { return this._employee }
  set employee(value: any) { this._employee = value; }
  public getEmployee(params?: {
    ouCode?: string,
    ouCodeStart?: string,
    ouCodeEnd?: string,
    branchCodeStart?: string,
    branchCodeEnd?: string,
    departmentCodeStart?: string,
    departmentCodeEnd?: string,
    positionCodeStart?: string,
    positionCodeEnd?: string,
    empTypeCode?: string,
    employTypeCode?: string
  }): Observable<any> {
    if (params && params.ouCode) {
      params.ouCodeStart = params.ouCode;
      params.ouCodeEnd = params.ouCode;
      delete params['ouCode'];
    }
    let url = (params) ? this._moduleName + '/combobox/employee?' + querystring.stringify(params) : this._moduleName + '/combobox/employee';
    return this.isObservableEmpty(this.employee) || params ? this.employee = this.getRemoteSelect(url) : this.employee;
  }

  /* Keep Status */
  private _keepStatus: Observable<any> = new Observable<any>();
  get keepStatus(): any { return this._keepStatus; }
  set keepStatus(value: any) { this._keepStatus = value; }
  public getKeepStatus(): Observable<any> {
    return this.isObservableEmpty(this.keepStatus) ? this.keepStatus = this.getRemoteSelect(this._moduleName + '/combobox/keep/status') : this.keepStatus;
  }

  /* Language */
  private _language: Observable<any> = new Observable<any>();
  get language(): any { return this._language; }
  set language(value: any) { this._language = value; }
  public getLanguage(): Observable<any> {
    return this.isObservableEmpty(this.language) ? this.language = this.getRemoteSelect(this._moduleName + '/combobox/language') : this.language;
  }

  /* Number of Day */
  private _numberOfDay: Observable<any> = new Observable<any>();
  get numberOfDay(): any { return this._numberOfDay; }
  set numberOfDay(value: any) { this._numberOfDay = value; }
  public getNumberOfDay(): Observable<any> {
    return this.isObservableEmpty(this.numberOfDay) ? this.numberOfDay = this.getRemoteSelect(this._moduleName + '/combobox/number/of/day') : this.numberOfDay;
  }

  /* Obligation Type */
  private _obligationType: Observable<any> = new Observable<any>();
  get obligationType(): any { return this._obligationType; }
  set obligationType(value: any) { this._obligationType = value; }
  public getObligationType(): Observable<any> {
    return this.isObservableEmpty(this.obligationType) ? this.obligationType = this.getRemoteSelect(this._moduleName + '/combobox/obligation/type') : this.obligationType;
  }

  /* Ou */
  private _ou: Observable<any> = new Observable<any>();
  get ou(): any { return this._ou; }
  set ou(value: any) { this._ou = value; }
  public getOu(): Observable<any> {
    return this.isObservableEmpty(this.ou) ? this.ou = this.getRemoteSelect(this._moduleName + '/combobox/ou') : this.ou;
  }

  /* Position */
  private _position: Observable<any> = new Observable<any>();
  get position(): any { return this._position; }
  set position(value: any) { this._position = value; }
  public getPosition(params?: {
          positionCategoryId?: number}): Observable<any> {
            let url = (params) ? this._moduleName + '/combobox/position?' + querystring.stringify(params) : this._moduleName + '/combobox/position';
    return this.isObservableEmpty(this.position) || params ? this.position = this.getRemoteSelect(url) : this.position;
  }

  /* Position Category */
  private _positionCategory: Observable<any> = new Observable<any>();
  get positionCategory(): any { return this._positionCategory; }
  set positionCategory(value: any) { this._positionCategory = value; }
  public getPositionCategory(): Observable<any> {
    return this.isObservableEmpty(this.positionCategory) ? this.positionCategory = this.getRemoteSelect(this._moduleName + '/combobox/position/category') : this.positionCategory;
  }

  /* Province */
  private _province: Observable<any> = new Observable<any>();
  get province(): any { return this._province; }
  set province(value: any) { this._province = value; }
  public getProvince(): Observable<any> {
    console.log('==>province')
    return this.isObservableEmpty(this.province) ? this.province = this.getRemoteSelect(this._moduleName + '/getprovince') : this.province;
  }

/* Company */
private _company: Observable<any> = new Observable<any>();
get company(): any { return this._company; }
set company(value: any) { this._company = value; }
public getCompany(): Observable<any> {
  console.log('==>company')
  return this.isObservableEmpty(this.company) ? this.company = this.getRemoteSelect(this._moduleName + '/getcompany') : this.company;
}

  /* Branch */
  private _branch: Observable<any> = new Observable<any>();
  get branch(): any { return this._branch; }
  set branch(value: any) { this._branch = value; }
  public getBranch(): Observable<any> {
    console.log('==>branch')
    return this.isObservableEmpty(this.branch) ? this.branch = this.getRemoteSelect(this._moduleName + '/getbranch') : this.branch;
  }

  /* Resign */
  private _resign: Observable<any> = new Observable<any>();
  get resign(): any { return this._resign; }
  set resign(value: any) { this._resign = value; }
  public getResign(): Observable<any> {
    console.log('==>resign')
    return this.isObservableEmpty(this.resign) ? this.resign = this.getRemoteSelect(this._moduleName + '/getresign') : this.resign;
  }

  /* Resign Reason */
  private _resignReason: Observable<any> = new Observable<any>();
  get resignReason(): any { return this._resignReason; }
  set resignReason(value: any) { this._resignReason = value; }
  public getResignReason(): Observable<any> {
    return this.isObservableEmpty(this.resignReason) ? this.resignReason = this.getRemoteSelect(this._moduleName + '/combobox/resign/reason') : this.resignReason;
  }

  /* Resign Type */
  private _resignType: Observable<any> = new Observable<any>();
  get resignType(): any { return this._resignType; }
  set resignType(value: any) { this._resignType = value; }
  public getResignType(): Observable<any> {
    return this.isObservableEmpty(this.resignType) ? this.resignType = this.getRemoteSelect(this._moduleName + '/combobox/resign/type') : this.resignType;
  }

  /* Sub District */
  private _subDistrict: Observable<any> = new Observable<any>();
  get subDistrict(): any { return this._subDistrict; }
  set subDistrict(value: any) { this._subDistrict = value; }
  public getSubDistrict(params?: {
          districtId?: number}): Observable<any> {
            let url = (params) ? this._moduleName + '/combobox/sub/district?' + querystring.stringify(params) : this._moduleName + '/combobox/sub/district';
    return this.isObservableEmpty(this.subDistrict) || params ? this.subDistrict = this.getRemoteSelect(url) : this.subDistrict;
  }

  /* Training Type */
  private _trainingType: Observable<any> = new Observable<any>();
  get trainingType(): any { return this._trainingType; }
  set trainingType(value: any) { this._trainingType = value; }
  public getTrainingType(): Observable<any> {
    return this.isObservableEmpty(this.trainingType) ? this.trainingType = this.getRemoteSelect(this._moduleName + '/combobox/training/type') : this.trainingType;
  }

  /* [ Local Select ] use directive : `local-select` */

  /* Assessment */
  public getAssessment(): Observable<any> {
    return Observable.from([
      [
        { id: 0, code: '00', nameLocal: 'ยังไม่ได้รับการประเมิน', nameEn: '' },
        { id: 1, code: '01', nameLocal: 'รับการประเมินแล้ว', nameEn: '' }

      ]
    ]);
  }

  /* Assessment Status */
  public getAssessmentStatus(): Observable<any> {
    return Observable.from([
      [
        { id: 0, code: '0', nameLocal: 'ไม่ผ่าน', nameEn: '' },
        { id: 1, code: '1', nameLocal: 'ผ่าน', nameEn: '' }
      ]
    ]);
  }

  /* Child Type */
  public getChildType(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'บุตรชอบด้วยกฎหมาย', nameEn: '' },
        { id: 2, code: '2', nameLocal: 'บุตรบุญธรรม', nameEn: '' }
      ]
    ]);
  }

  /* Child Type */
  public getEducation(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'ประถม', nameEn: '' },
        { id: 2, code: '2', nameLocal: 'มัธยม', nameEn: '' },
        { id: 3, code: '3', nameLocal: 'ปริญญาตรี', nameEn: '' },
        { id: 4, code: '4', nameLocal: 'ปริญญาโท', nameEn: '' },
        { id: 5, code: '5', nameLocal: 'ปริญญาเอก', nameEn: '' }
      ]
    ]);
  }

 /* skill Level */
  private _skillLevel: Observable<any> = new Observable<any>();
  get skillLevel(): any { return this._skillLevel; }
  set skillLevel(value: any) { this._skillLevel = value; }
  public getSkillLevel(): Observable<any> {
    return this.isObservableEmpty(this.skillLevel) ? this.skillLevel = this.getRemoteSelect(this._moduleName + '/combobox/skill/level') : this.skillLevel;
  }

  /*com Skill Type */
  private _comSkillType: Observable<any> = new Observable<any>();
  get comSkillType(): any { return this._comSkillType; }
  set comSkillType(value: any) { this._comSkillType = value; }
  public getComSkillType(): Observable<any> {
    return this.isObservableEmpty(this.comSkillType) ? this.comSkillType = this.getRemoteSelect(this._moduleName + '/combobox/com/skill/type') : this.comSkillType;
  }

   /* exam Type from DB */
  private _examTypeDB: Observable<any> = new Observable<any>();
  get examTypeDB(): any { return this._examTypeDB; }
  set examTypeDB(value: any) { this._examTypeDB = value; }
  public getExamTypeDB(): Observable<any> {
    return this.isObservableEmpty(this.examTypeDB) ? this.examTypeDB = this.getRemoteSelect(this._moduleName + '/combobox/exam/type') : this.examTypeDB;
  }
  /* Education Status */
  public getEducationStatus(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'กำลังศึกษา', nameEn: 'Studying' },
        { id: 2, code: '0', nameLocal: 'ไม่ศึกษา', nameEn: 'No Study' }
      ]
    ]);
  }


  /* Employ Type */
  private _employtype: Observable<any> = new Observable<any>();
  get employtype(): any { return this._employtype; }
  set employtype(value: any) { this._employtype = value; }
  public getEmployType(): Observable<any> {
    return this.isObservableEmpty(this.employtype) ? this.employtype = this.getRemoteSelect(this._moduleName + '/combobox/employtype') : this.employtype;
  }

  /* Emp Status */
  private _empstatus: Observable<any> = new Observable<any>();
  get empstatus(): any { return this._empstatus; }
  set empstatus(value: any) { this._empstatus = value; }
  public getEmpStatus(): Observable<any> {
    return this.isObservableEmpty(this.empstatus) ? this.empstatus = this.getRemoteSelect(this._moduleName + '/combobox/empstatus') : this.empstatus;
  }

  /* Emp Type */
  private _emptype: Observable<any> = new Observable<any>();
  get emptype(): any { return this._emptype; }
  set emptype(value: any) { this._emptype = value; }
  public getEmpType(): Observable<any> {
    return this.isObservableEmpty(this.emptype) ? this.emptype = this.getRemoteSelect(this._moduleName + '/combobox/emptype') : this.emptype;
  }

  /* Emp And Employ Type */
  private _empAndEmployType: Observable<any> = new Observable<any>();
  get empAndEmployType(): any { return this._empAndEmployType; }
  set empAndEmployType(value: any) { this._empAndEmployType = value; }
  public getEmpAndEmployType(): Observable<any> {
    return this.isObservableEmpty(this.empAndEmployType) ? this.empAndEmployType = this.getRemoteSelect(this._moduleName + '/combobox/empAndEmployType') : this.empAndEmployType;
  }
 /* Emp And Employ Type */
  private _incomeExpenditure: Observable<any> = new Observable<any>();
  get incomeExpenditure(): any { return this._incomeExpenditure; }
  set incomeExpenditure(value: any) { this._incomeExpenditure = value; }
  public getincomeExpenditure(): Observable<any> {
    return this.isObservableEmpty(this.incomeExpenditure) ? this.incomeExpenditure = this.getRemoteSelect(this._moduleName + '/combobox/incomeExpenditure') : this.incomeExpenditure;
  }
  /* Exam Type */
  public getExamType(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '01', nameLocal: 'TOEIC', nameEn: 'TOEIC' },
        { id: 2, code: '02', nameLocal: 'IELTS', nameEn: 'IELTS' }
      ]
    ]);
  }

  /* Family Relation */
  public getFamilyRelation(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'บิดา', nameEn: '' },
        { id: 2, code: '2', nameLocal: 'มารดา', nameEn: '' },
        { id: 3, code: '3', nameLocal: 'พี่ชาย', nameEn: '' },
        { id: 4, code: '4', nameLocal: 'น้องชาย', nameEn: '' },
        { id: 5, code: '5', nameLocal: 'พี่สาว', nameEn: '' },
        { id: 6, code: '6', nameLocal: 'น้องสาว', nameEn: '' }
      ]
    ]);
  }

  /* Family Status */
  public getFamilyStatus(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'ยังมีชีวิต', nameEn: '' },
        { id: 2, code: '0', nameLocal: 'ถึงแก่กรรม', nameEn: '' }
      ]
    ]);
  }

  /* Gendar */
  public getGender(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: 'M', nameLocal: 'ชาย', nameEn: 'Male' },
        { id: 2, code: 'F', nameLocal: 'หญิง', nameEn: 'Female' }
      ]
    ]);
  }

  /* Marriage Status */
  public getMarriageStatus(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'โสด', nameEn: '' },
        { id: 2, code: '2', nameLocal: 'สมรส', nameEn: '' },
        { id: 3, code: '3', nameLocal: 'หย่า', nameEn: '' },
        { id: 4, code: '4', nameLocal: 'หม้าย', nameEn: '' }
      ]
    ]);
  }

  /* Movement Type */
  public getMovementType(): Observable<any> {
    return Observable.from([
      [
        { id: 24, code: '01', nameLocal: 'เริ่มปฏิบัติงาน', nameEn: '' },
        { id: 26, code: '02', nameLocal: 'บรรจุ/ผ่านการทดลองงาน', nameEn: '' },
        { id: 27, code: '03', nameLocal: 'โอนย้าย', nameEn: '' },
        { id: 28, code: '04', nameLocal: 'เลื่อนตำแหน่ง', nameEn: '' },
        { id: 29, code: '04', nameLocal: 'โอนย้ายและเลื่อนตำแหน่ง', nameEn: '' },
        { id: 30, code: '06', nameLocal: 'รักษาสภาพ', nameEn: '' },
        { id: 31, code: '07', nameLocal: 'พ้นสภาพ', nameEn: '' }
      ]
    ]);
  }

  /* Nationality */
  private _nationality: Observable<any> = new Observable<any>();
  get nationality(): any { return this._nationality; }
  set nationality(value: any) { this._nationality = value; }
  public getNationality(): Observable<any> {
    return this.isObservableEmpty(this.nationality) ? this.nationality = this.getRemoteSelect(this._moduleName + '/combobox/nationality') : this.nationality;
  }

  /* Obligation Return Status */
  public getObligationReturnStatus(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: 'Y', nameLocal: 'คืนแล้ว', nameEn: '' },
        { id: 1, code: 'N', nameLocal: 'ยังไม่คืน', nameEn: '' }
      ]
    ]);
  }

  /* Official Status */
  public getOfficialStatus(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '00', nameLocal: 'อนุมัติ', nameEn: '' },
        { id: 2, code: '01', nameLocal: 'รออนุมัติ', nameEn: '' },
        { id: 3, code: '02', nameLocal: 'ไม่อนุมัติ', nameEn: '' },
        { id: 4, code: '03', nameLocal: 'ประมวลผลเรียบร้อย', nameEn: '' }
      ]
    ]);
  }

  /* Pro Evaluate Expand Status */
  public getProEvaluateExpandStatus(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '0', nameLocal: 'ขยาย', nameEn: '' },
        { id: 1, code: '1', nameLocal: 'ยังไม่ขยาย', nameEn: '' }
      ]
    ]);
  }

  /* Race */
  private _race: Observable<any> = new Observable<any>();
  get race(): any { return this._race; }
  set race(value: any) { this._race = value; }
  public getRace(): Observable<any> {
    return this.isObservableEmpty(this.race) ? this.race = this.getRemoteSelect(this._moduleName + '/combobox/race') : this.race;
  }

  /* Religion */
  private _religion: Observable<any> = new Observable<any>();
  get religion(): any { return this._religion; }
  set religion(value: any) { this._religion = value; }
  public getReligion(): Observable<any> {
    return this.isObservableEmpty(this.religion) ? this.religion = this.getRemoteSelect(this._moduleName + '/combobox/religion') : this.religion;
  }

  /* Leave Group */
  private _leaveGroup: Observable<any> = new Observable<any>();
  get leaveGroup(): any { return this._leaveGroup; }
  set leaveGroup(value: any) { this._leaveGroup = value; }
  public getLeaveGroup(): Observable<any> {
    return this.isObservableEmpty(this.leaveGroup) ? this.leaveGroup = this.getRemoteSelect(this._moduleName + '/combobox/leave-group') : this.leaveGroup;
  }

  /* Leave Type */
  private _leaveType: Observable<any> = new Observable<any>();
  get leaveType(): any { return this._leaveType; }
  set leaveType(value: any) { this._leaveType = value; }
  public getLeaveType(): Observable<any> {
    return this.isObservableEmpty(this.leaveType) ? this.leaveType = this.getRemoteSelect(this._moduleName + '/combobox/leave-type') : this.leaveType;
  }

  /* Skill */
  public getSkill(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'ดีมาก', nameEn: 'Excellent' },
        { id: 2, code: '2', nameLocal: 'ดี', nameEn: 'Good' },
        { id: 3, code: '3', nameLocal: 'ปานกลาง', nameEn: 'Fair' },
        { id: 4, code: '4', nameLocal: 'ปรับปรุง', nameEn: 'Poor' }
      ]
    ]);
  }

  /* Time of Working Status */
  public getTimeOfWorkingStatus(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '0', nameLocal: 'ไม่ลงเวลา', nameEn: '' },
        { id: 2, code: '1', nameLocal: 'ลงเวลา', nameEn: '' }
      ]
    ]);
  }

  /* Title Name */
  public getTitleNameTH(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'นาย', nameEn: '' },
        { id: 2, code: '2', nameLocal: 'นาง', nameEn: '' },
        { id: 3, code: '3', nameLocal: 'นางสาว', nameEn: '' }
      ]
    ]);
  }

  /* Title Name */
  public getTitleNameEN(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'Mr.', nameEn: '' },
        { id: 2, code: '2', nameLocal: 'Mrs.', nameEn: '' },
        { id: 3, code: '3', nameLocal: 'Ms.', nameEn: '' }
      ]
    ]);
  }

   /* Title Name */
  public getTitleName(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'นาย', nameEn: 'Mr.' },
        { id: 2, code: '2', nameLocal: 'นาง', nameEn: 'Mrs.' },
        { id: 3, code: '3', nameLocal: 'นางสาว', nameEn: 'Ms.' }
      ]
    ]);
  }

  /* Training Place Status */
  public getTrainingPlaceStatus(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'ภายใน', nameEn: '' },
        { id: 1, code: '2', nameLocal: 'ภายนอก', nameEn: '' }
      ]
    ]);
  }

  /* Training Status */
  public getTrainingStatus(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'ก่อนเข้าทำงาน', nameEn: '' },
        { id: 1, code: '2', nameLocal: 'หลังเข้าทำงาน', nameEn: '' }
      ]
    ]);
  }

  /* Using Status */
  public getUsingStatus(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '0', nameLocal: 'ไม่ใช้งาน', nameEn: '' },
        { id: 2, code: '1', nameLocal: 'ใช้งาน', nameEn: '' }
      ]
    ]);
  }

  /* Work at same Company */
  public getWorkAtSameComp(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '0', nameLocal: 'ไม่ใช่', nameEn: '' },
        { id: 2, code: '1', nameLocal: 'ใช่', nameEn: '' }
      ]
    ]);
  }

  /* Year */
  public getYear(): Observable<any> {
    return Observable.from([
      [
        { year: '2565' },
        { year: '2564' },
        { year: '2563' },
        { year: '2562' },
        { year: '2561' },
        { year: '2560' },
        { year: '2559' },
        { year: '2558' },
        { year: '2557' },
        { year: '2556' },
        { year: '2555' }
      ]
    ]);
  }

   /* Month */
  public getMonth(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '01', nameLocal: 'มกราคม', nameEn: ''  },
        { id: 2, code: '02', nameLocal: 'กุมภาพันธ์', nameEn: ''  },
        { id: 3, code: '03', nameLocal: 'มีนาคม', nameEn: ''  },
        { id: 4, code: '04', nameLocal: 'เมษายน', nameEn: ''  },
        { id: 5, code: '05', nameLocal: 'พฤษภาคม', nameEn: ''  },
        { id: 6, code: '06', nameLocal: 'มิถุนายน', nameEn: ''  },
        { id: 7, code: '07', nameLocal: 'กรกฎาคม', nameEn: ''  },
        { id: 8, code: '08', nameLocal: 'สิงหาคม', nameEn: ''  },
        { id: 9, code: '09', nameLocal: 'กันยายน', nameEn: ''  },
        { id: 10, code: '10', nameLocal: 'ตุลาคม', nameEn: ''  },
        { id: 11, code: '11', nameLocal: 'พฤศจิกายน', nameEn: ''  },
        { id: 12, code: '12', nameLocal: 'ธันวาคม', nameEn: ''  },
      ]
    ]);
  }

  /* ActionOfPC02 */
  public getActionOfPC02(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'ปิดประจำงวด', nameEn: '' },
        { id: 2, code: '0', nameLocal: 'ยกเลิกปิดประจำงวด', nameEn: '' },
        { id: 3, code: '2', nameLocal: 'ปิดประจำปี', nameEn: '' }
      ]
    ]);
  }

  /* ActionOfPC03 */
  public getActionOfPC03(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: 'ปิดประจำงวด', nameEn: '' },
        { id: 2, code: '0', nameLocal: 'ยกเลิกปิดประจำงวด', nameEn: '' },
        { id: 3, code: '2', nameLocal: 'ปิดประจำปี', nameEn: '' }
      ]
    ]);
  }

  /* Bank */
  public getBank(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: 'BAY', nameLocal: 'ธนาคารกรุงศรีอยุธยา', nameEn: '' },
        { id: 2, code: 'BBK', nameLocal: 'ธนาคารกรุงเทพ', nameEn: '' },
        { id: 3, code: 'GHB', nameLocal: 'ธนาคารอาคารสงเคราะห์', nameEn: '' },
        { id: 4, code: 'GSB', nameLocal: 'ธนาคารออมสิน', nameEn: '' },
        { id: 5, code: 'KBANK', nameLocal: 'ธนาคารกสิกรไทย', nameEn: '' },
        { id: 6, code: 'KK', nameLocal: 'ธนาคารเกียรตินาคิน', nameEn: '' },
        { id: 7, code: 'KTB', nameLocal: 'ธนาคารกรุงไทย', nameEn: '' },
        { id: 8, code: 'LHBANK', nameLocal: 'ธนาคารแลนด์ แอนด์ เฮาส์', nameEn: '' },
        { id: 9, code: 'SCB', nameLocal: 'ธนาคารไทยพาณิชย์', nameEn: '' },
        { id: 10, code: 'SCIB', nameLocal: 'ธนาคารนครหลวงไทย', nameEn: '' },
        { id: 11, code: 'SCNB', nameLocal: 'ธนาคารสแตนดาร์ดชาร์เตอร์', nameEn: '' }
      ]
    ]);
  }

  /* Condition Deduct Tax */
  public getCondDeductTax(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '0', nameLocal: 'พนักงานเป็นผู้จ่าย', nameEn: '' },
        { id: 2, code: '1', nameLocal: 'นายจ้างเป็นผู้จ่าย', nameEn: '' }
      ]
    ]);
  }

  /* Condition Fund Type */
  public getFundType(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '02', nameLocal: 'ตราสารหนี้ 100%', nameEn: '' },
        { id: 2, code: '03', nameLocal: 'ตราสารหนี้ 90% + หุ้น 10%', nameEn: '' },
        { id: 3, code: '04', nameLocal: 'ตราสารหนี้ 80% + หุ้น 20%', nameEn: '' }
      ]
    ]);
  }

  /* MilitaryStudentYear */
  public getMilitaryStudentYear(): Observable<any> {
    return Observable.from([
      [
        { id: 1, code: '1', nameLocal: '1', nameEn: '' },
        { id: 2, code: '2', nameLocal: '2', nameEn: '' },
        { id: 2, code: '3', nameLocal: '3', nameEn: '' },
        { id: 2, code: '4', nameLocal: '4', nameEn: '' },
        { id: 2, code: '5', nameLocal: '5', nameEn: '' }
      ]
    ]);
  }

  public deleteFile(params: any, module: any): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI(module+'/common/deleteFile'), params)
      .map(value => {
        return value.json()
      });
  }

   public listFile(employeeId: any, module: any): Observable<any> {
    return this.authHttp.get(this.restServer.getAPI(module+'/common/listFile') + '?employeeId=' + employeeId)
      .map(value => {
        return value.json()
      });
  }

    public downloadFile(params : any, module: any): Observable<any> {
      return this.authHttp.post(this.restServer.getAPI(module+'/common/downloadFile') , params,{ responseType: ResponseContentType.Blob });
  }

    /* Shift */
  private _shift: Observable<any> = new Observable<any>();
  get shift(): any { return this._shift; }
  set shift(value: any) { this._shift = value; }
  public getShift(): Observable<any> {
    return this.isObservableEmpty(this.shift) ? this.shift = this.getRemoteSelect(this._moduleName + '/combobox/shift') : this.shift;
  }

      /* IncomeExpenditure */
  private _income: Observable<any> = new Observable<any>();
  get income(): any { return this._income; }
  set income(value: any) { this._income = value; }
  public getincome(): Observable<any> {
    return this.isObservableEmpty(this.income) ? this.income = this.getRemoteSelect(this._moduleName + '/combobox/income') : this.income;
  }

     /* Period */
  private _period: Observable<any> = new Observable<any>();
  get period(): any { return this._period; }
  set period(value: any) { this._period = value; }
  public getPeriod(): Observable<any> {
    return this.isObservableEmpty(this.period) ? this.period = this.getRemoteSelect(this._moduleName + '/combobox/period') : this.period;
  }

     /* PeriodYear */
  private _periodYear: Observable<any> = new Observable<any>();
  get periodYear(): any { return this._periodYear; }
  set periodYear(value: any) { this._periodYear = value; }
  public getPeriodYear(): Observable<any> {
    return this.isObservableEmpty(this.periodYear) ? this.periodYear = this.getRemoteSelect(this._moduleName + '/combobox/periodYear') : this.periodYear;
  }
}
