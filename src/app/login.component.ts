import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { AuthService } from './auth.service';
import { Subscription } from 'rxjs/Subscription';
import { ModalDirective } from 'ng2-bootstrap';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
  @ViewChild('alertModalSave') public alertModalSave: ModalDirective;
  @ViewChild('alertModal') public alertModal: ModalDirective;

  message: string;
  username: string = 'admin';
  password: string = 'admin';
  csrfToken: string;
  subscription: Subscription;
  private alert = { title: '', message: '', isRedirect: false };

  constructor(public authService: AuthService, public router: Router) {
    this.subscription = this.authService.isAuthened.subscribe(() => {
      console.log('username? ' + this.authService.username);
      console.log('is LoggedIn? ' + this.authService.isLoggedIn);
      if (this.authService.isLoggedIn) {
        this.redirectPage();
      }
    });
  }

  ngOnInit() {
    console.log('ngOnInit()');
    this.authService.checkAuthen();
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit()');
    this.authService.getCsrfToken();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  setMessage() {
    this.message = 'Logged ' + (this.authService.isLoggedIn ? 'in' : 'out')
  }

  login() {
    this.message = 'Trying to log in ...';

    if (!this.username && !this.password) {
      console.log('blank credentials');
      this.message = 'blank credentials';
      return;
    }

    this.authService.login(this.username, this.password).subscribe((result) => {
      this.setMessage();
      if (this.authService.isLoggedIn) {
        this.redirectPage()
      }
    }, err => {
      console.log(err);
      this.message = err.error;
      this.alertModalToggle('แจ้งเตือน', 'Username หรือ Password ของท่านผิด',false);
      this.redirectPage()
    });
  }

  logout() {
    this.authService.logout();
  }

  redirectPage() {
    // get the redirect URL from our auth service
    // if no redirect has been set, use the default
    let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/dashboard';

    // set our navigation extras object
    // that passes on our global query params and fragment
    let navigationExtras: NavigationExtras = {
      preserveQueryParams: true,
      preserveFragment: true
    };
    // redirect the user
    this.router.navigate([redirect], navigationExtras);
  }
  private alertModalToggle(title: string, message: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.message = message;
    this.alert.isRedirect = isRedirect;
    this.alertModal.show();
  }
  private back() {
    this.alertModal.hide();
    if (this.alert.isRedirect) {
      this.router.navigate(['login']);
    }
  }
}
