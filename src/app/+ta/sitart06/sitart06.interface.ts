export class Sitart06Criteria {
    public ouCode: string;
    public inputSearch: string;
    public period: number;
    public startDate: Date;
    public endDate : Date;
    public takeLeaveStartDate: Date;
    public takeLeaveEndDate: Date;
    public takeOtStartDate : Date;
    public takeOtEndDate : Date;
    periodYear: number;
    public flagForPaging: number = 0;

}

export class Sitart06SavePeriod {
    public ouCode: string;
    public periodId: number;
    public period: number;
    public periodYear: number;
    public startDate: Date;
    public endDate : Date;
    public takeLeaveStartDate: Date;
    public takeLeaveEndDate: Date;
    public takeOtStartDate : Date;
    public takeOtEndDate : Date;
    public startDateInputSub: any = '';
    public startDateInputShow: any = '';
    public endDateInputSub: any = '';
    public endDateInputShow: any = '';
    public takeLeaveStartDateInputSub: any = '';
    public takeLeaveStartDateInputShow: any = '';
    public takeLeaveEndDateInputSub: any = '';
    public takeLeaveEndDateInputShow: any = '';
    public takeOtStartDateInputSub: any = '';
    public takeOtStartDateInputShow: any = '';
    public takeOtEndDateInputSub: any = '';
    public takeOtEndDateInputShow: any = '';
    public success: boolean;
    public flagForPaging: number = 0;
}
