import { SSDatePicker } from '../../shared/ss-datepicker.component';
import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitart06Service } from './sitart06.service';
import { Sitart06Criteria, Sitart06SavePeriod } from '../sitart06/sitart06.interface';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { BeanUtils } from '../../core/bean-utils.service';
import { LoadMask } from '../../load-mask.component';
import { DateUtils } from '../../core/date-utils.service';
import { PageContext } from '../../shared/service/pageContext.service';
import { OuSelectDirective } from '../shared/ou-select.directive';

@Component({
  selector: 'sitart06a',
  template: require('./sitart06a.component.html'),
  providers: [Page, Sitart06Service]
})
export class Sitart06aComponent {

  @Output('closeModal') private closeEvent = new EventEmitter();
  @Input('info') private info: any;
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;
  @ViewChild('backModal') public backModal: ModalDirective;
  @ViewChild('startDateInput') public startDateInput: SSDatePicker;
  @ViewChild('endDateInput') public endDateInput: SSDatePicker;
  @ViewChild('takeLeaveStartDateInput') public takeLeaveStartDateInput: SSDatePicker;
  @ViewChild('takeLeaveEndDateInput') public takeLeaveEndDateInput: SSDatePicker;
  @ViewChild('takeOtStartDateInput') public takeOtStartDateInput: SSDatePicker;
  @ViewChild('takeOtEndDateInput') public takeOtEndDateInput: SSDatePicker;
  @ViewChild('ouCode') public ouCode: OuSelectDirective;

  private form: FormGroup = this.fb.group({
    periodId: this.fb.control(null)
    , ouCode: this.fb.control("", Validators.required)
    , period: this.fb.control(null, Validators.required)
    , periodYear: this.fb.control(null, Validators.required)
    , startDate: this.fb.control(null, Validators.required)
    , endDate: this.fb.control(null, Validators.required)
    , takeLeaveStartDate: this.fb.control(null, Validators.required)
    , takeLeaveEndDate: this.fb.control(null, Validators.required)
    , takeOtStartDate: this.fb.control(null, Validators.required)
    , takeOtEndDate: this.fb.control(null, Validators.required)
  });

  private periodId: number = null;
  private period: number = null;
  private periodYear: number = null;
  private fromLoadObject: any;
  private initMsg: string;
  private showMsg = false;
  private objCriteria: Sitart06SavePeriod = new Sitart06SavePeriod();
  private objDelete: Sitart06SavePeriod = new Sitart06SavePeriod();
  private alert = { title: '', msg: '' };
  private records: any[];
  private data: any;
  private dirty: boolean;
  private flagForLoad: boolean = false;

  constructor(private page: Page
    , private fb: FormBuilder
    , private service: Sitart06Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private pageContext: PageContext
  ) {

    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      this.setData(this.fromLoadObject.searchData);
    }
  }

  ngAfterViewInit() {
    if (BeanUtils.isNotEmpty(this.periodId)) {
      this.ouCode.setValue(this.form.controls["ouCode"].value);
      this.ouCode.disabled(true);
      this.form.controls["periodYear"].disable();
      this.form.controls["period"].disable();
    }
  }

  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  private changeOuCode(valueField: any) {
    this.form.controls["ouCode"].setValue(valueField.ouCode);
  }

  private alertModalClose() {
    if (this.records != null) {
      if (this.router.url.split("/")[2] == "sitart06a") {
        this.pageContext.backPage();
      }
    }
    this.alertModal.hide();
  }

  private startDateFunc(valueField: any) {
    if (valueField) {
      this.form.controls["startDate"].setValue(this.startDateInput.getMilliSecond());
      this.objCriteria.startDateInputSub = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.startDateInputShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
      if (this.flagForLoad == false) {
        this.form.controls["startDate"].markAsDirty(true);
      }
    }
  }

  private endDateFunc(valueField: any) {
    if (valueField) {
      this.form.controls["endDate"].setValue(this.endDateInput.getMilliSecond());
      this.objCriteria.endDateInputSub = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.endDateInputShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
      if (this.flagForLoad == false) {
        this.form.controls["endDate"].markAsDirty(true);
      }
    }
  }

  private takeLeaveStartDateFunc(valueField: any) {
    if (valueField) {
      this.form.controls["takeLeaveStartDate"].setValue(this.takeLeaveStartDateInput.getMilliSecond());
      this.objCriteria.takeLeaveStartDateInputSub = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.takeLeaveStartDateInputShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
      if (this.flagForLoad == false) {
        this.form.controls["takeLeaveStartDate"].markAsDirty(true);
      }
    }
  }

  private takeLeaveEndDateFunc(valueField: any) {
    if (valueField) {
      this.form.controls["takeLeaveEndDate"].setValue(this.takeLeaveEndDateInput.getMilliSecond());
      this.objCriteria.takeLeaveEndDateInputSub = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.takeLeaveEndDateInputShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
      if (this.flagForLoad == false) {
        this.form.controls["takeLeaveEndDate"].markAsDirty(true);
      }
    }
  }

  private takeOtStartDateFunc(valueField: any) {
    if (valueField) {
      this.form.controls["takeOtStartDate"].setValue(this.takeOtStartDateInput.getMilliSecond());
      this.objCriteria.takeOtStartDateInputSub = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.takeOtStartDateInputShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
      if (this.flagForLoad == false) {
        this.form.controls["takeOtStartDate"].markAsDirty(true);
      }
    }
  }

  private takeOtEndDateFunc(valueField: any) {
    if (valueField) {
      this.form.controls["takeOtEndDate"].setValue(this.takeOtEndDateInput.getMilliSecond());
      this.objCriteria.takeOtEndDateInputSub = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.takeOtEndDateInputShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
      if (this.flagForLoad == false) {
        this.form.controls["takeOtEndDate"].markAsDirty(true);
      }
    }
  }

  public onSave() {
    if (this.form.valid == true) {
      if (this.form.dirty) {
        LoadMask.show();
        this.objCriteria = this.form.value;
        if (BeanUtils.isNotEmpty(this.periodId)) {
          this.objCriteria.periodId = this.periodId;
          console.log(this.periodId);
        }
        this.objCriteria.periodYear = this.form.controls["periodYear"].value;
        this.objCriteria.ouCode = this.form.controls["ouCode"].value;
        this.objCriteria.period = this.form.controls["period"].value;
        this.objCriteria.startDate = this.form.controls["startDate"].value;
        this.objCriteria.endDate = this.form.controls["endDate"].value;
        this.objCriteria.takeLeaveStartDate = this.form.controls["takeLeaveStartDate"].value;
        this.objCriteria.takeLeaveEndDate = this.form.controls["takeLeaveEndDate"].value;
        this.objCriteria.takeOtStartDate = this.form.controls["takeOtStartDate"].value;
        this.objCriteria.takeOtEndDate = this.form.controls["takeOtEndDate"].value;
        this.showMsg = false;
        this.service.savePeriod(this.page, this.objCriteria).subscribe(response => {
          this.records = response.records.periodId;
          this.periodId = response.records.periodId;
          if (response.records.checkPeriod == 0) {
            if (response.records.success) {
              LoadMask.hide();
              this.alertModalToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ');
              this.ouCode.disabled(true);
              this.form.markAsPristine();
            }
            else {
              LoadMask.hide();
              this.alertModalToggle('ผิดพลาด', 'วันที่ผิดพลาด');
            }
          } else if (response.records.checkPeriod == 1) {
            LoadMask.hide();
            this.alertModalToggle('แจ้งเตือน', 'งวดซ้ำ');
          } else {
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้');
          }
        },
          error => {
            console.error(error);
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ');
          }
        );
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข');
      }
    } else {
      LoadMask.hide();
      this.alertModalToggle('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
    }
  }

  private setData(object: any) {
    if (BeanUtils.isNotEmpty(object)) {
      console.log(Object);
      this.form.controls["ouCode"].setValue(object.records.data[0].ouCode);
      this.form.controls["period"].setValue(object.records.data[0].period);
      this.form.controls["periodYear"].setValue(object.records.data[0].periodYear);
      this.form.controls["startDate"].setValue(object.records.data[0].startDate);
      this.objCriteria.startDate = this.fromLoadObject.searchData.records.data[0].startDate;
      this.form.controls["endDate"].setValue(object.records.data[0].endDate);
      this.objCriteria.endDate = this.fromLoadObject.searchData.records.data[0].endDate;
      this.form.controls["takeLeaveStartDate"].setValue(object.records.data[0].takeLeaveStartDate);
      this.objCriteria.takeLeaveStartDate = this.fromLoadObject.searchData.records.data[0].takeLeaveStartDate;
      this.form.controls["takeLeaveEndDate"].setValue(object.records.data[0].takeLeaveEndDate);
      this.objCriteria.takeLeaveEndDate = this.fromLoadObject.searchData.records.data[0].takeLeaveEndDate;
      this.form.controls["takeOtStartDate"].setValue(object.records.data[0].takeOtStartDate);
      this.objCriteria.takeOtStartDate = this.fromLoadObject.searchData.records.data[0].takeOtStartDate;
      this.form.controls["takeOtEndDate"].setValue(object.records.data[0].takeOtEndDate);
      this.objCriteria.takeOtEndDate = this.fromLoadObject.searchData.records.data[0].takeOtEndDate;
      this.periodId = object.records.data[0].periodId;
      this.periodYear = object.records.data[0].periodYear;
      this.period = object.records.data[0].period;

    }
  }

  public onBack() {
    if (this.form.dirty == true) {
      this.backModalToggle('แจ้งเตือน', 'ข้อมูลได้รับการแก้ไข ต้องการเปลี่ยนหน้าหรือไม่ ?');
    } else {
      this.pageContext.backPage();
    }
  }

  private backModalClose() {
    this.backModal.hide();
  }

  private backModalBack() {
    this.pageContext.backPage();
  }

  private backModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;

    this.backModal.toggle();
  }

  public onCancel() {
    this.closeEvent.emit();
  }

  private deleteModalClose() {
    this.deleteModal.hide();
  }

  private deleteModalConfirm() {
    this.onDelete();
    this.deleteModal.hide();
  }

  private alertModalDeleteToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.deleteModal.toggle();
  }

  public onBeforeDelete(record: any) {
    this.alertModalDeleteToggle('ยืนยันการลบ', 'คุณยืนยันการลบหรือไม่');
  }

  public onDelete() {
    LoadMask.show();
    this.objDelete.periodId = this.periodId
    this.service.deletePeriod(this.page, this.objDelete).subscribe(response => {
      this.alertModalToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ');
      LoadMask.hide();
      this.pageContext.backPage();
    },
      error => {
        console.error(error);
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ');
      }
    );
  }
}
