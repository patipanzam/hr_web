import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { Sitart06Criteria ,Sitart06SavePeriod} from '../sitart06/sitart06.interface';
import { BeanUtils } from '../../core/bean-utils.service';

const querystring = require('querystring');

@Injectable()
export class Sitart06Service {

   constructor(public authHttp: AuthHttp, private restServer: RestServerService) {}

  //Sitart03 search
  public getData(page: Page, searchValue: Sitart06Criteria): Observable<any> {
    let params = '?' + querystring.stringify(page) + "&" + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('/ta/sitart06/searchPeriod') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });
  }

    //savePeriod
    public savePeriod(page: Page, params: Sitart06SavePeriod): Observable<any> {
      return this.authHttp.post(this.restServer.getAPI('/ta/sitart06/savePeriod'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
    }

  //Sitart03a
  public searchData(periodId: number): Observable<any> {
    let params = '?periodId=' + periodId;
    return this.authHttp.get(this.restServer.getAPI('/ta/sitart06/selectPeriod') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

    //deleteShift
    public deletePeriod(page: Page, params: Sitart06SavePeriod): Observable<any> {
      return this.authHttp.post(this.restServer.getAPI('/ta/sitart06/deletePeriod'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
    }
}
