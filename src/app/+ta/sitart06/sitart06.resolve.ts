import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { LoadMask } from '../../load-mask.component';
import { Sitart06Service } from './sitart06.service';
import { BeanUtils } from '../../core/bean-utils.service';

@Injectable()
export class Sitart06Resolve implements Resolve<any> {

  constructor(private selectService: CommonSelectService, private Sitart06Service: Sitart06Service) {
    this.selectService.setModuleName('ta');
  }
  resolve(route: ActivatedRouteSnapshot): any {
    LoadMask.show();
    if (route.url[0].path == 'sitart06') {
      LoadMask.hide();
      return {
      };
    } else if (route.url[0].path == 'sitart06a') {
      if (BeanUtils.isNotEmpty(route.params['periodId'])) {
        var searchData = this.Sitart06Service.searchData(route.params['periodId']);
        var ouCode = this.selectService.getOu();
        return Observable.forkJoin([searchData, ouCode]).map((response) => {
          LoadMask.hide();
          return {
            searchData: response[0],
            ouCode: response[1]
          };
        }).first();
      } else {
        var ouCode = this.selectService.getOu();
        return Observable.forkJoin([ouCode]).map((response) => {
          LoadMask.hide();
          return {
            ouCode: response[0]
          };
        }).first();
      }
    }
  }
}
