import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitart06Service } from './sitart06.service';
import { Sitart06Criteria, Sitart06SavePeriod } from '../sitart06/sitart06.interface';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { DateUtils } from '../../core/date-utils.service';
import { LoadMask } from '../../load-mask.component';
import { AppLayoutComponent } from '../../shared/layouts/app-layout.component';
import { BeanUtils } from '../../core/bean-utils.service';
import { SearchStatus } from '../../shared/constant/common.interface';
import { PageContext } from '../../shared/service/pageContext.service';
import { SSDatePicker } from '../../shared/ss-datepicker.component';

@Component({
  selector: 'sitart06',
  template: require('./sitart06.component.html'),
  providers: [Page, Sitart06Service]
})
export class Sitart06Component {

  private form: FormGroup = this.formBuilder.group({
    inputSearch: this.formBuilder.control("")
    , period: this.formBuilder.control("")
    , periodYear: this.formBuilder.control("")
    , startDate: this.formBuilder.control("")
    , endDate: this.formBuilder.control("")
    , takeLeaveStartDate: this.formBuilder.control("")
    , takeLeaveEndDate: this.formBuilder.control("")
    , takeOtStartDate: this.formBuilder.control("")
    , takeOtEndDate: this.formBuilder.control("")
  });

  private initMsg: string;
  private showMsg = false;
  private records: any[];
  private fromLoadObject: any;
  private objCriteria: Sitart06Criteria = new Sitart06Criteria();
  private alert = { title: '', msg: '' };
  private inputSearch: string;
  private checkDelete: number = 0;
  private periodId: any;

  @ViewChild('alertModal') public alertModal: ModalDirective;

  public childModalShow(record: any) {
    this.pageContext.nextPage(['/ta/sitart06a', record.periodId], this.objCriteria);
  }

  public createPeriod() {
    let periodId: any = "";
    this.pageContext.nextPage(['/ta/sitart06a', periodId], this.objCriteria);
  }

  constructor(private page: Page
    , private service: Sitart06Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private formBuilder: FormBuilder
    , private pageContext: PageContext) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
  }
  // about search
  public onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.objCriteria.inputSearch = this.form.controls["inputSearch"].value;
    this.objCriteria.inputSearch = this.form.controls["periodYear"].value;
    this.showMsg = false;
    if (BeanUtils.isNotEmpty(changeEvent)) {
      this.page = changeEvent;
    }
    this.callServiceSearch(changeEvent, this.objCriteria);
  }

  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  private callServiceSearch(page: Page, mapping: Sitart06Criteria) {
    this.service.getData(page, mapping).subscribe(response => {
      this.page = response.page;
      this.records = response.records;
      if (BeanUtils.isNotEmpty(this.records)) {
        LoadMask.hide();
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
      }
    },
      error => {
        console.error(error);
        LoadMask.hide();
      }
    );
  }

  public onChangePage(changeEvent: Page) {
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private onSearch(modeSearch: number) {
    if (BeanUtils.isNotEmpty(modeSearch)) {
      this.objCriteria.flagForPaging = modeSearch
      this.page.start = 0;
      if (modeSearch == SearchStatus.normalSearch) {
        this.onNormalSearch(this.page);
      }
    }
  }

  public closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }
}
