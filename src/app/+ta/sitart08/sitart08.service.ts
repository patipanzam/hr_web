import { AuthHttp } from 'angular2-jwt';
import { BeanUtils } from '../../core/bean-utils.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { Sitart08Criteria, Sitart08SaveShiftEmp } from '../sitart08/sitart08.interface';

const querystring = require('querystring');

@Injectable()
export class Sitart08Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

  //searchShiftEmp
  public getData(page: Page, searchValue: Sitart08Criteria): Observable<any> {
    let params = '?' + querystring.stringify(page) + "&" + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('/ta/sitart08/searchShiftEmp') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });
  }

  //processShiftEmp
  public processShiftEmp(page: Page, params: Sitart08SaveShiftEmp): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitart08/processShiftEmp'), params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  //saveShiftEmp
  public saveShiftEmp(page: Page, params: Sitart08SaveShiftEmp): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitart08/saveShiftEmp'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  //selectShiftEmp
  public searchData(shiftEmpId: number): Observable<any> {
    let params = '?shiftEmpId=' + shiftEmpId;
    return this.authHttp.get(this.restServer.getAPI('/ta/sitart08/selectShiftEmp') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  //deleteShiftEmp
  public deleteShiftEmp(page: Page, params: Sitart08SaveShiftEmp): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitart08/deleteShiftEmp'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  public loadShift(shiftCode: string): Observable<any> {
    let params = '?shiftCode=' + shiftCode;
    return this.authHttp.get(this.restServer.getAPI('ta/sitart08/loadShift') + params)
      .map(val => { return val.json() });
  }
}
