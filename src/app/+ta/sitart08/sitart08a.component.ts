import { BeanUtils } from '../../core/bean-utils.service';
import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit, } from '@angular/core';
import { DateUtils } from '../../core/date-utils.service';
import { EmployeeSelectDirective } from '../shared/employee-select.directive';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { ModalDirective } from 'ng2-bootstrap';
import { OuSelectDirective } from '../shared/ou-select.directive';
import { Page } from '../../shared/pagination.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ShiftSelectDirective } from '../shared/shift-select.directive';
import { Sitart08SaveShiftEmp } from '../sitart08/sitart08.interface';
import { Sitart08Service } from './sitart08.service';
import { SSDatePicker } from '../../shared/ss-datepicker.component';

@Component({
  selector: 'sitart08a',
  template: require('./sitart08a.component.html'),
  providers: [Page, Sitart08Service, Sitart08SaveShiftEmp]
})
export class Sitart08aComponent implements AfterViewInit {

  @Input('info') private info: any;
  @Output('closeModal') private closeEvent = new EventEmitter();
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('backModal') public backModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;
  @ViewChild('successModal') public successModal: ModalDirective;
  @ViewChild('employeeAdd') public employeeAdd: EmployeeSelectDirective;
  @ViewChild('ouCode') public ouCode: OuSelectDirective;
  @ViewChild('shiftGroup') public shiftGroup: ShiftSelectDirective;
  @ViewChild('startDate') public startDate: SSDatePicker;
  @ViewChild('endDate') public endDate: SSDatePicker;

  private form: FormGroup = this.fb.group({
    shiftEmpId: this.fb.control(null)
    , ouCode: this.fb.control("", Validators.required)
    , empCode: this.fb.control("", Validators.required)
    , empThaiName: this.fb.control("", Validators.required)
    , shiftCode: this.fb.control("", Validators.required)
    , shiftIn: this.fb.control("", Validators.required)
    , shiftOut: this.fb.control("", Validators.required)
    , startDate: this.fb.control(null, Validators.required)
    , endDate: this.fb.control(null, Validators.required)
  });

  private shiftEmpId: number = null;
  private alert = { title: '', msg: '', isRedirect: false };
  private currentDate: Date = new Date();
  private data: any;
  private dirty: boolean;
  private flagForLoad: boolean = false;
  private fromLoadObject: any;
  private initMsg: string;
  private objCriteria: Sitart08SaveShiftEmp = new Sitart08SaveShiftEmp();
  private objDelete: Sitart08SaveShiftEmp = new Sitart08SaveShiftEmp();
  private records: any[];
  private showMsg = false;
  private shows: any[] = [];

  constructor(private fb: FormBuilder
    , private page: Page
    , private pageContext: PageContext
    , private router: Router
    , private routerActive: ActivatedRoute
    , private service: Sitart08Service) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      this.setData(this.fromLoadObject.searchData);
    }
  }

  ngAfterViewInit() {
    if (BeanUtils.isNotEmpty(this.shiftEmpId)) {
      this.flagForLoad = true;
      this.ouCode.setValue(this.form.controls["ouCode"].value);
      this.ouCode.disabled(true);
      this.shiftGroup.setValue(this.form.controls["shiftCode"].value);
      this.shiftGroup.disabled(true);
      this.employeeAdd.setValue(this.form.controls["empCode"].value);
      this.employeeAdd.disabled(true);
      this.startDate.setValue(this.fromLoadObject.searchData.records.data.startDate);
      this.endDate.setValue(this.fromLoadObject.searchData.records.data.endDate);
      this.flagForLoad = false;
      if (this.startDate.getValue() < this.currentDate && this.endDate.getValue() < this.currentDate) {
        this.startDate.readOnly = true;
        this.endDate.readOnly = true;
      }
      if (this.startDate.getValue() < this.currentDate && this.endDate.getValue() > this.currentDate) {
        this.startDate.readOnly = true;
      }
    }
  }

  private changeOuCode(valueField: any) {
    this.form.controls["ouCode"].setValue(valueField.ouCode);
    if (this.flagForLoad == false) {
      this.form.controls["ouCode"].markAsDirty(true);
    }
  }

  private changeShiftGroupA(valueField: any) {
    this.form.controls["shiftCode"].setValue(valueField.shiftCode);
    this.getShift();
    if (this.flagForLoad == false) {
      this.form.controls["shiftCode"].markAsDirty(true);
    }
  }

  private changeEmployeeAdd(valueField: any) {
    this.form.controls["empCode"].setValue(valueField.empCode);
    this.form.controls["empThaiName"].setValue(valueField.empThaiName);
    if (this.flagForLoad == false) {
      this.form.controls["empCode"].markAsDirty(true);
    }
  }

  private changeStartDateA(valueField: any) {
    if (valueField) {
      this.form.controls["startDate"].setValue(this.startDate.getMilliSecond());
      this.objCriteria.obligationsStartDateA = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsStartDateShowA = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
      if (this.flagForLoad == false) {
        this.form.controls["startDate"].markAsDirty(true);
      }
    }
  }

  private changeEndDateA(valueField: any) {
    if (valueField) {
      this.form.controls["endDate"].setValue(this.endDate.getMilliSecond());
      this.objCriteria.obligationsEndDateA = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsEndDateShowA = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
      if (this.flagForLoad == false) {
        this.form.controls["endDate"].markAsDirty(true);
      }
    }
  }

  private checkDirty() {
    if (this.form.dirty == true) {
      this.dirty = true;
    } else {
      this.dirty = false;
    }
    return this.dirty;
  }

  private alertModalToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.alertModal.show();
  }

  private alertModalSuccessToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.successModal.show();
  }

  private alertModalClose() {
    if (this.records == null) {
      (this.router.url.split("/")[2] == "sitart08a")
    }
    this.alertModal.toggle();
  }

  private alertModalSuccessClose() {
    if (this.records != null) {
      if (this.router.url.split("/")[2] == "sitart08a") {
      }
    }
    if (this.records == null) {
      this.pageContext.backPage();
    }
    this.successModal.toggle();
  }

  private onSave() {
    if (this.form.valid == true || this.shows.length != 0) {
      if (this.shows.length != 0 || this.shiftEmpId != null) {
        if (this.form.dirty) {
          LoadMask.show();
          this.objCriteria = this.form.value;
          if (BeanUtils.isNotEmpty(this.shiftEmpId)) {
            this.objCriteria.shiftEmpId = this.shiftEmpId;
          }
          this.objCriteria.grid = this.shows;
          this.showMsg = false;
          this.service.saveShiftEmp(this.page, this.objCriteria).subscribe(response => {
            if (response.records.data.success) {
              LoadMask.hide();
              this.alertModalSuccessToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ', true);
              this.form.markAsPristine();
            } else {
              LoadMask.hide();
              this.alertModalToggle('แจ้งเตือน', 'วันที่ผิดพลาด', false);
            }
          },
            error => {
              console.error(error);
              LoadMask.hide();
              this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
            }
          );
        } else {
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข', false);
        }
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'กรุณากดปุ่มประมวลผล', false);
      }
    } else {
      LoadMask.hide();
      this.alertModalToggle('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล', false);
    }
  }

  private onProcess(): void {
    let data = this.shows.filter(row => {
      return row.ouCode == this.form.controls["ouCode"].value
        && row.empCode == this.form.controls["empCode"].value
        && row.empThaiName == this.form.controls["empThaiName"].value
        && row.shiftCode == this.form.controls["shiftCode"].value
        && row.shiftIn == this.form.controls["shiftIn"].value
        && row.shiftOut == this.form.controls["shiftOut"].value
        && row.startDate == this.form.controls["startDate"].value
        && row.endDate == this.form.controls["endDate"].value;
    });
    if (data.length > 0) {
      this.alertModalToggle('แจ้งเตือน', 'ข้อมูลนี้ถูกประมวลผลแล้ว', false);
    } else {
      if ((this.form.controls["ouCode"].value == null) || (this.form.controls["shiftCode"].value == null) ||
        (this.form.controls["shiftIn"].value == null) || (this.form.controls["shiftOut"].value == null) ||
        (this.form.controls["empCode"].value == null) || (this.form.controls["startDate"].value == null) ||
        (this.form.controls["endDate"].value == null)) {
        this.alertModalToggle('ผิดพลาด', 'กรุณากรอกข้อมูลให้ครบถ้วน', false);
      } else {
        this.objCriteria.ouCode = this.form.controls["ouCode"].value;
        this.objCriteria.empCode = this.form.controls["empCode"].value;
        this.objCriteria.empThaiName = this.form.controls["empThaiName"].value;
        this.objCriteria.shiftCode = this.form.controls["shiftCode"].value;
        this.objCriteria.shiftIn = this.form.controls["shiftIn"].value;
        this.objCriteria.shiftOut = this.form.controls["shiftOut"].value;
        this.objCriteria.startDate = this.form.controls["startDate"].value;
        this.objCriteria.endDate = this.form.controls["endDate"].value;
        this.showMsg = false;
        this.service.processShiftEmp(this.page, this.objCriteria).subscribe(response => {
          if (response.records.data.success) {
            LoadMask.hide();
            let row: {};
            row = {
              ouCode: this.form.controls["ouCode"].value,
              empCode: this.form.controls["empCode"].value,
              empThaiName: this.form.controls["empThaiName"].value,
              shiftCode: this.form.controls["shiftCode"].value,
              shiftIn: this.form.controls["shiftIn"].value,
              shiftOut: this.form.controls["shiftOut"].value,
              startDate: this.form.controls["startDate"].value,
              endDate: this.form.controls["endDate"].value
            };
            this.shows.push(row);
            this.ouCode.clearValue();
            this.shiftGroup.clearValue();
            this.form.controls["shiftIn"].setValue(null);
            this.form.controls["shiftOut"].setValue(null);
            this.employeeAdd.clearValue();
            this.startDate.clearValue();
            this.endDate.clearValue();
          } else {
            LoadMask.hide();
            this.alertModalToggle('แจ้งเตือน', 'วันที่ผิดพลาด', false);
          }
        });
      }
    }
  }

  private setData(object: any) {
    if (BeanUtils.isNotEmpty(object)) {
      this.form.controls["ouCode"].setValue(object.records.data.ouCode);
      this.form.controls["empCode"].setValue(object.records.data.empCode);
      this.form.controls["empThaiName"].setValue(object.records.data.empThaiName);
      this.form.controls["shiftCode"].setValue(object.records.data.shiftCode);
      this.form.controls["shiftIn"].setValue(object.records.data.shiftIn);
      this.form.controls["shiftOut"].setValue(object.records.data.shiftOut);
      this.form.controls["startDate"].setValue(object.records.data.startDate);
      this.form.controls["endDate"].setValue(object.records.data.endDate);
      this.shiftEmpId = object.records.data.shiftEmpId;
      this.objCriteria.startDate = this.fromLoadObject.searchData.records.data.startDate;
      this.objCriteria.endDate = this.fromLoadObject.searchData.records.data.endDate;
    }
  }

  private onBack() {
    this.checkDirty();
    if (this.form.dirty == false) {
      this.backModalBack();
    } else {
      this.backModalToggle('แจ้งเตือน', 'ข้อมูลถูกแก้ไข ต้องการเปลี่ยนหน้าใช่หรือไม่', false);
    }
  }

  private backModalClose() {
    this.backModal.hide();
  }

  private backModalBack() {
    this.pageContext.backPage();
  }

  private backModalToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.backModal.show();
  }

  private onCancel() {
    this.closeEvent.emit();
  }

  private deleteModalClose() {
    this.deleteModal.hide();
  }

  private deleteModalConfirm() {
    this.onDelete();
    this.deleteModal.hide();
  }

  private alertModalDeleteToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.deleteModal.toggle();
  }

  private onBeforeDelete(record: any) {
    this.alertModalDeleteToggle('ยืนยันการลบ', 'ต้องการลบข้อมูลใช่หรือไม่', true);
  }

  private onDelete() {
    LoadMask.show();
    this.objDelete.shiftEmpId = this.shiftEmpId
    this.service.deleteShiftEmp(this.page, this.objDelete).subscribe(response => {
      this.alertModalSuccessToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ', true);
      LoadMask.hide();
      this.pageContext.backPage();
    },
      error => {
        console.error(error);
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
      }
    );
  }

  private getShift() {
    if (this.form.controls["shiftCode"].value != null) {
      LoadMask.show();
      this.service.loadShift(this.form.controls["shiftCode"].value)
        .subscribe(response => {
          this.form.controls["shiftIn"].setValue(response.shiftIn);
          this.form.controls["shiftOut"].setValue(response.shiftOut);
          LoadMask.hide();
        }, error => {
          this.form.controls["shiftIn"].setValue(null);
          this.form.controls["shiftOut"].setValue(null);
          LoadMask.hide();
          this.alertModalToggle('ผิดพลาด', 'ไม่พบข้อมูลที่ระบุ', false);
        });
    }
    else {
      this.form.controls["shiftIn"].setValue(null);
      this.form.controls["shiftOut"].setValue(null);
    }
  }
}
