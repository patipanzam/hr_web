import { BeanUtils } from '../../core/bean-utils.service';
import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { DepartmentSelectDirective } from '../shared/department-select.directive';
import { EmployeeSelectDirective } from '../shared/employee-select.directive';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchStatus } from '../../shared/constant/common.interface';
import { ShiftSelectDirective } from '../shared/shift-select.directive';
import { Sitart08Service } from './sitart08.service';
import { Sitart08Criteria } from '../sitart08/sitart08.interface';

@Component({
  selector: 'sitart08',
  template: require('./sitart08.component.html'),
  providers: [Page, Sitart08Service, Sitart08Criteria]
})

export class Sitart08Component implements OnInit {

  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('departmentStart') public departmentStart: DepartmentSelectDirective;
  @ViewChild('departmentEnd') public departmentEnd: DepartmentSelectDirective;
  @ViewChild('employeeStart') public employeeStart: EmployeeSelectDirective;
  @ViewChild('employeeEnd') public employeeEnd: EmployeeSelectDirective;
  @ViewChild('shiftGroup') public shiftGroup: ShiftSelectDirective;

  private form: FormGroup = this.fb.group({
    shiftEmpId: this.fb.control(null)
    , ouCode: this.fb.control("")
    , empCode: this.fb.control("")
    , empThaiName: this.fb.control("")
    , shiftCode: this.fb.control("")
    , shiftIn: this.fb.control("")
    , shiftOut: this.fb.control("")
    , startDate: this.fb.control(null)
    , endDate: this.fb.control(null)
  });

  private shiftEmpId: any;
  private alert = { title: '', msg: '' };
  private checkDelete: number = 0;
  private fromLoadObject: any;
  private initMsg: string;
  private objCriteria: Sitart08Criteria = new Sitart08Criteria();
  private showMsg = false;
  private records: any[];

  constructor(private fb: FormBuilder
    , private page: Page
    , private pageContext: PageContext
    , private service: Sitart08Service
    , private router: Router
    , private routerActive: ActivatedRoute) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
  }

  private childModalShow(record: any) {
    this.pageContext.nextPage(['/ta/sitart08a', record.shiftEmpId], this.objCriteria);
  }

  private createShiftEmp() {
    let shiftEmpId: any = "";
    this.pageContext.nextPage(['/ta/sitart08a', shiftEmpId], this.objCriteria);
  }

  private changeStartDepartment(valueField: any) {
    this.objCriteria.departmentIdStart = valueField.departmentId;
    this.objCriteria.departmentCodeStart = valueField.departmentCode;
    this.loadEmployee();
  }

  private changeEndDepartment(valueField: any) {
    this.objCriteria.departmentIdEnd = valueField.departmentId;
    this.objCriteria.departmentCodeEnd = valueField.departmentCode;
    this.loadEmployee();
  }

  private changeStartEmployee(valueField: any) {
    this.objCriteria.employeeCodeStart = valueField.empCode;
  }

  private changeEndEmployee(valueField: any) {
    this.objCriteria.employeeCodeEnd = valueField.empCode;
  }

  private changeShiftGroup(valueField: any) {
    this.objCriteria.shiftGroup = valueField.shiftCode;
  }

  // About Search
  private onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.showMsg = false;
    this.callServiceSearch(changeEvent, this.objCriteria);
  }

  private callServiceSearch(page: Page, mapping: Sitart08Criteria) {
    this.service.getData(page, mapping)
      .subscribe(response => {
        this.page = response.page;
        this.records = response.records;
        if (BeanUtils.isNotEmpty(this.records)) {
          LoadMask.hide();
        } else {
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
        }
      },
      error => {
        console.error(error);
        LoadMask.hide();
      }
      );
  }

  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  private onChangePage(changeEvent: Page) {
    this.page = changeEvent;
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private onSearch(modeSearch: number) {
    if (this.form.valid == true) {
      if (BeanUtils.isNotEmpty(modeSearch)) {
        this.objCriteria.flagForPaging = modeSearch
        this.page.start = 0;
        if (modeSearch == SearchStatus.normalSearch) {
          this.onNormalSearch(this.page);
        }
      }
    }
  }

  private closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }

  private loadEmployee() {
    let params = {
      departmentIdStart: this.objCriteria.departmentIdStart || '',
      departmentIdEnd: this.objCriteria.departmentIdEnd || '',
      departmentCodeStart: this.objCriteria.departmentCodeStart || '',
      departmentCodeEnd: this.objCriteria.departmentCodeEnd || ''
    }
    this.employeeStart.load(params);
    this.employeeEnd.load(params);
  }
}
