import { BeanUtils } from '../../core/bean-utils.service';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { Injectable } from '@angular/core';
import { LoadMask } from '../../load-mask.component';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Sitart08Service } from './sitart08.service';

@Injectable()
export class Sitart08Resolve implements Resolve<any> {

  constructor(private selectService: CommonSelectService, private sitart08Service: Sitart08Service) {
    this.selectService.setModuleName('ta');
  }

  resolve(route: ActivatedRouteSnapshot): any {
    LoadMask.show();
    if (route.url[0].path == 'sitart08') {
      var department = this.selectService.getDepartment();
      var employee = this.selectService.getEmployee();
      var shift = this.selectService.getShift();
      return Observable.forkJoin([department, employee, shift]).map((response) => {

        LoadMask.hide();
        return {
          department: response[0],
          employee: response[1],
          shift: response[2]
        };
      }).first();
    } else if (route.url[0].path == 'sitart08a') {

      if (BeanUtils.isNotEmpty(route.params['shiftEmpId'])) {
        var searchData = this.sitart08Service.searchData(route.params['shiftEmpId']);
        var ouCode = this.selectService.getOu();
        var shift = this.selectService.getShift();
        var employee = this.selectService.getEmployee();
        return Observable.forkJoin([searchData, ouCode, shift, employee]).map((response) => {
          LoadMask.hide();
          return {
            searchData: response[0],
            ouCode: response[1],
            shift: response[2],
            employee: response[3]
          };
        }).first();
      } else {
        var ouCode = this.selectService.getOu();
        var shift = this.selectService.getShift();
        var employee = this.selectService.getEmployee();
        return Observable.forkJoin([ouCode, shift, employee]).map((response) => {
          LoadMask.hide();
          return {
            ouCode: response[0],
            shift: response[1],
            employee: response[2]
          };
        }).first();
      }
    }
  }
}
