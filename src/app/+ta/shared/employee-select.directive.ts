import { Directive, ElementRef, Attribute, Renderer, EventEmitter, Output, Input, OnInit } from "@angular/core";
import { IComboboxConfig, Select } from '../../shared/ss-select.directive';
import { RestServerService } from '../../core/rest-server.service';
import { AuthHttp } from 'angular2-jwt';
import { CommonSelectService } from './../../shared/service/common-select.service';

@Directive({
  selector: '[employee-select]',
  exportAs: 'employee-select'
})
export class EmployeeSelectDirective extends Select implements OnInit {
  private items: any;
  private currentItem: any;
  private comboboxConfig: IComboboxConfig;
  private hideCode: boolean = false;
  private hideDesc: boolean = false;
  private placeholder: string = '';

  @Output('change') changeEvent: EventEmitter<any> = new EventEmitter<any>();
  @Input('items') set inputItems(inputItems: any) {
    this.items = inputItems;
  }
  @Input('currentItem') set inputCurrentItem(inputCurrentItem: any) {
    this.currentItem = inputCurrentItem;
  }

  constructor(
    public el: ElementRef,
    public renderer: Renderer,
    public restServer: RestServerService,
    public authHttp: AuthHttp,
    public commonSelectService: CommonSelectService,
    @Attribute('data-placeholder') placeholder: string,
    @Attribute('hide-code') hideCode: any,
    @Attribute('hide-desc') hideDesc: any
  ) {
    super(el, renderer, restServer, authHttp);
    this.placeholder = placeholder;
    this.hideCode = !(hideCode === null);
    this.hideDesc = !(hideDesc === null);
  }

  ngOnInit() {
    this.comboboxConfig = {
      placeholder: this.placeholder,
      valueField: 'empCode',
      searchField: ['empCode', 'empThaiName'],
      sortField: 'empCode',
      items: this.items,
      currentItem: this.currentItem,
      renderer: {
        option: (data: any, escape: any) => {
          let labelName = escape(data.empCode) + " : " + escape(data.empThaiName);
          return '<div class="option" data-selectable data-value="' + escape(data.empCode) + '">' + labelName + '</div>';
        },
        item: (data: any, escape: any) => {
          let labelName = ((!this.hideCode) ? escape(data.empCode) + ((this.hideDesc) ? '' : ' : ') : '') + ((!this.hideDesc) ? escape(data.empThaiName) : '');
          return '<div class="item" data-value="' + escape(data.empCode) + '">' + labelName + '</div>';
        }
      }
    };

    super.init(this.comboboxConfig, this.changeEvent);
  }

  public load(params?: {
    ouCode?: string,
    ouCodeStart?: string,
    ouCodeEnd?: string,
    branchCodeStart?: string,
    branchCodeEnd?: string,
    departmentCodeStart?: string,
    departmentCodeEnd?: string,
    positionCodeStart?: string,
    positionCodeEnd?: string,
    empTypeCode?: string,
    employTypeCode?: string
  }) {
    this.commonSelectService.setModuleName('ta');
    super.load(this.commonSelectService.getEmployee(params));
  }

  public setValue(valueField: any) {
    super.setValue(valueField);
  }
}
