import { Directive, ElementRef, Attribute, Renderer, EventEmitter, Output, Input, OnInit } from "@angular/core";
import { IComboboxConfig, Select } from '../../shared/ss-select.directive';
import { RestServerService } from '../../core/rest-server.service';
import { AuthHttp } from 'angular2-jwt';
import { CommonSelectService } from './../../shared/service/common-select.service';

@Directive({
  selector: '[department-select]',
  exportAs: 'department-select'
})
export class DepartmentSelectDirective extends Select implements OnInit {
  private items: any;
  private currentItem: any;
  private comboboxConfig: IComboboxConfig;
  private hideCode: boolean = false;
  private hideDesc: boolean = false;
  private placeholder: string = '';

  @Output('change') changeEvent: EventEmitter<any> = new EventEmitter<any>();
  @Input('items') set inputItems(inputItems: any) {
    this.items = inputItems;
  }
  @Input('currentItem') set inputCurrentItem(inputCurrentItem: any) {
    this.currentItem = inputCurrentItem;
  }

  constructor(
    public el: ElementRef,
    public renderer: Renderer,
    public restServer: RestServerService,
    public authHttp: AuthHttp,
    public commonSelectService: CommonSelectService,
    @Attribute('data-placeholder') placeholder: string,
    @Attribute('hide-code') hideCode: any,
    @Attribute('hide-desc') hideDesc: any
  ) {
    super(el, renderer, restServer, authHttp);
    this.placeholder = placeholder;
    this.hideCode = !(hideCode === null);
    this.hideDesc = !(hideDesc === null);
  }

  ngOnInit() {
    this.comboboxConfig = {
      placeholder: this.placeholder,
      valueField: 'departmentCode',
      searchField: ['departmentCode', 'departmentNameLocal'],
      sortField: 'departmentCode',
      items: this.items,
      currentItem: this.currentItem,
      renderer: {
        option: (data: any, escape: any) => {
          let labelName = escape(data.departmentCode) + " : " + escape(data.departmentNameLocal);
          return '<div class="option" data-selectable data-value="' + escape(data.departmentCode) + '">' + labelName + '</div>';
        },
        item: (data: any, escape: any) => {
          let labelName = ((!this.hideCode) ? escape(data.departmentCode) + ((this.hideDesc) ? '' : ' : ') : '') + ((!this.hideDesc) ? escape(data.departmentNameLocal) : '');
          return '<div class="item" data-value="' + escape(data.departmentCode) + '">' + labelName + '</div>';
        }
      }
    };

    super.init(this.comboboxConfig, this.changeEvent);
  }

  public load(params?: { ouCode?: string, ouCodeStart?: string, ouCodeEnd?: string }) {
    this.commonSelectService.setModuleName('ta');
    super.load(this.commonSelectService.getDepartment(params));
  }

  public setValue(valueField: any) {
    super.setValue(valueField);
  }
}
