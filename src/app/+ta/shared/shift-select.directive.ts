import { Directive, ElementRef, Attribute, Renderer, EventEmitter, Output, Input, OnInit } from "@angular/core";
import { IComboboxConfig, Select } from '../../shared/ss-select.directive';
import { RestServerService } from '../../core/rest-server.service';
import { AuthHttp } from 'angular2-jwt';

@Directive({
  selector: '[shift-select]',
  exportAs: 'shift-select'
})
export class ShiftSelectDirective extends Select implements OnInit {
  private items: any;
  private currentItem: any;
  private comboboxConfig: IComboboxConfig;
  private hideCode: boolean = false;
  private hideDesc: boolean = false;
  private placeholder: string = '';

  @Output('change') changeEvent: EventEmitter<any> = new EventEmitter<any>();
  @Input('items') set inputItems(inputItems: any) {
    this.items = inputItems;
  }
  @Input('currentItem') set inputCurrentItem(inputCurrentItem: any) {
    this.currentItem = inputCurrentItem;
  }

  constructor(
    public el: ElementRef,
    public renderer: Renderer,
    public restServer: RestServerService,
    public authHttp: AuthHttp,
    @Attribute('data-placeholder') placeholder: string,
    @Attribute('hide-code') hideCode: any,
    @Attribute('hide-desc') hideDesc: any
  ) {
    super(el, renderer, restServer, authHttp);
    this.placeholder = placeholder;
    this.hideCode = !(hideCode === null);
    this.hideDesc = !(hideDesc === null);
  }

  ngOnInit() {
    this.comboboxConfig = {
      placeholder: this.placeholder,
      valueField: 'shiftCode',
      searchField: ['shiftCode', 'shiftIn', 'shiftOut'],
      sortField: 'shiftCode',
      items: this.items,
      currentItem: this.currentItem,
      renderer: {
        option: (data: any, escape: any) => {
          let labelName = escape(data.shiftCode) + " : " + escape(data.shiftIn) + " - " + escape(data.shiftOut);
          return '<div class="option" data-selectable data-value="' + escape(data.shiftCode) + '">' + labelName + '</div>';
        },
        item: (data: any, escape: any) => {
          let labelName = ((!this.hideCode) ? escape(data.shiftCode) + ((this.hideDesc) ? '' : ' : ') : '') + ((!this.hideDesc) ? escape(data.shiftIn) + " - " + escape(data.shiftOut) : '') ;
          return '<div class="item" data-value="' + escape(data.shiftCode) + '">' + labelName + '</div>';
        }
      }
    };

    super.init(this.comboboxConfig, this.changeEvent);
  }

  public setValue(valueField: any) {
    super.setValue(valueField);
  }
}
