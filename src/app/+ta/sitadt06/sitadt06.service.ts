import { AuthHttp } from 'angular2-jwt';
import { BeanUtils } from '../../core/bean-utils.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { Sitadt06Criteria, Sitadt06SaveMonthly } from '../sitadt06/sitadt06.interface';

const querystring = require('querystring');

@Injectable()
export class Sitadt06Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

  //searchShiftEmp
  public getData(page: Page, searchValue: Sitadt06Criteria): Observable<any> {
    let params = '?' + querystring.stringify(page) + "&" + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('/ta/sitadt06/searchMonthly') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });
  }

  //collectDataMonthly
  public dataCollect(page: Page, params: Sitadt06Criteria): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('ta/sitadt06/collectMonthly'), params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  //processShiftEmp
  public processShiftEmp(page: Page, params: Sitadt06SaveMonthly): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitadt06/processShiftEmp'), params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  //saveShiftEmp
  public saveShiftEmp(page: Page, params: Sitadt06SaveMonthly): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitadt06/saveShiftEmp'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  //selectShiftEmp
  public searchData(shiftEmpId: number): Observable<any> {
    let params = '?shiftEmpId=' + shiftEmpId;
    return this.authHttp.get(this.restServer.getAPI('/ta/sitadt06/selectShiftEmp') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  //deleteMonthly
  public deleteMonthly(page: Page, monthlyId: number): Observable<any> {
    let params = '?monthlyId=' + monthlyId;
    return this.authHttp.get(this.restServer.getAPI('/ta/sitadt06/deleteMonthly') + params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  public loadDate(searchValue: any): Observable<any> {
    let params = '?' + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('ta/sitadt06/loadDate') + params)
      .map(val => { return val.json() });
  }
}
