import { BeanUtils } from '../../core/bean-utils.service';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { Injectable } from '@angular/core';
import { LoadMask } from '../../load-mask.component';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Sitadt06Service } from './sitadt06.service';

@Injectable()
export class Sitadt06Resolve implements Resolve<any> {

  constructor(private selectService: CommonSelectService, private sitadt06Service: Sitadt06Service) {
    this.selectService.setModuleName('ta');
  }

  resolve(route: ActivatedRouteSnapshot): any {
    LoadMask.show();
    if (route.url[0].path == 'sitadt06') {
      var department = this.selectService.getDepartment();
      var employee = this.selectService.getEmployee();
      var period = this.selectService.getPeriod();
      var periodYear = this.selectService.getPeriodYear();
      return Observable.forkJoin([department, employee, period, periodYear]).map((response) => {

        LoadMask.hide();
        return {
          department: response[0],
          employee: response[1],
          period: response[2],
          periodYear: response[3]
        };
      }).first();
    } else if (route.url[0].path == 'sitadt06a') {

      if (BeanUtils.isNotEmpty(route.params['monthlyId'])) {
        var searchData = this.sitadt06Service.searchData(route.params['monthlyId']);
        var ouCode = this.selectService.getOu();
        var shift = this.selectService.getShift();
        var employee = this.selectService.getEmployee();
        return Observable.forkJoin([searchData, ouCode, shift, employee]).map((response) => {
          LoadMask.hide();
          return {
            searchData: response[0],
            ouCode: response[1],
            shift: response[2],
            employee: response[3]
          };
        }).first();
      } else {
        var ouCode = this.selectService.getOu();
        var shift = this.selectService.getShift();
        var employee = this.selectService.getEmployee();
        return Observable.forkJoin([ouCode, shift, employee]).map((response) => {
          LoadMask.hide();
          return {
            ouCode: response[0],
            shift: response[1],
            employee: response[2]
          };
        }).first();
      }
    }
  }
}
