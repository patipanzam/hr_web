export class Sitadt06Criteria {
  public inputSearch: string = '';
  public monthlyId: number;
  public ouCode: string = '';
  public employeeId: number;
  // public empCode: string = '';
  // public empThaiName: string = '';
  // public shiftId: number;
  // public shiftCode: string = '';
  // public shiftIn: string = '';
  // public shiftOut: string = '';
  public workId: number;
  public leaveId: number;
  public showDate: string;
  public startDate: number;
  public endDate: number;
  public departmentIdStart: number;
  public departmentIdEnd: number;
  public departmentCodeStart: string = '';
  public departmentCodeEnd: string = '';
  public departmentCode: string = '';
  public departmentNameLocal: string = '';
  public employeeCodeStart: string = '';
  public employeeCodeEnd: string = '';
  // public shiftGroup: string = '';
  // public empAndEmployType: string = '';
  // public empTypeCode: string = '';
  // public employTypeCode: string = '';
  // public year: number = new Date().getFullYear() + 543;
  public period: number;
  public year: number;
  public periodGroup: string = '';
  public yearGroup: string = '';
  public times: number;
  public days: number;
  public hours: number;
  public mins: number;
  public departmentIdStartA: number;
  public departmentIdEndA: number;
  public departmentCodeStartA: string = '';
  public departmentCodeEndA: string = '';
  public employeeCodeStartA: string = '';
  public employeeCodeEndA: string = '';
  public periodA: number;
  public yearA: number;
  public flagForPaging: number = 0;
}

export class Sitadt06SaveMonthly {
  public monthlyId: number;
  public ouCode: string = '';
  public empId: number;
  public empCode: string = '';
  public empThaiName: string = '';
  public shiftId: number;
  public shiftCode: string = '';
  public shiftIn: string = '';
  public shiftOut: string = '';
  public startDate: number;
  public endDate: number;
  public obligationsStartDateA: any = '';
  public obligationsStartDateShowA: any = '';
  public obligationsEndDateA: any = '';
  public obligationsEndDateShowA: any = '';
  public shiftGroupA: string = '';
  public employeeAdd: string = '';
  public flagForPaging: number = 0;
  // public grid: Sitadt06ShiftEmp[];
}

// export class Sitadt06ShiftEmp {
//   public shiftEmpId: number;
//   public ouCode: string = '';
//   public empId: number;
//   public empCode: string = '';
//   public empThaiName: string = '';
//   public shiftId: number;
//   public shiftCode: string = '';
//   public shiftIn: string = '';
//   public shiftOut: string = '';
//   public startDate: Date;
//   public endDate: Date;
//   public departmentIdStart: number;
//   public departmentIdEnd: number;
//   public employeeCodeStart: string = '';
//   public employeeCodeEnd: string = '';
//   public shiftGroup: string = '';
// }
