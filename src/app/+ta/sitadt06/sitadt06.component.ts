import { BeanUtils } from '../../core/bean-utils.service';
import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { DepartmentSelectDirective } from '../shared/department-select.directive';
import { EmployeeSelectDirective } from '../shared/employee-select.directive';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchStatus } from '../../shared/constant/common.interface';
import { Sitadt06Service } from './sitadt06.service';
import { Sitadt06Criteria } from '../sitadt06/sitadt06.interface';
import { PeriodSelectDirective } from '../shared/period-select.directive';
import { PeriodYearSelectDirective } from '../shared/period-year-select.directive';

@Component({
  selector: 'sitadt06',
  template: require('./sitadt06.component.html'),
  providers: [Page, Sitadt06Service, Sitadt06Criteria]
})

export class Sitadt06Component implements OnInit {

  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('alertCollectModal') public alertCollectModal: ModalDirective;
  @ViewChild('collectModal') public collectModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;
  @ViewChild('successDeleteModal') public successDeleteModal: ModalDirective;
  @ViewChild('departmentStart') public departmentStart: DepartmentSelectDirective;
  @ViewChild('departmentEnd') public departmentEnd: DepartmentSelectDirective;
  @ViewChild('departmentStartA') public departmentStartA: DepartmentSelectDirective;
  @ViewChild('departmentEndA') public departmentEndA: DepartmentSelectDirective;
  @ViewChild('employeeStart') public employeeStart: EmployeeSelectDirective;
  @ViewChild('employeeEnd') public employeeEnd: EmployeeSelectDirective;
  @ViewChild('employeeStartA') public employeeStartA: EmployeeSelectDirective;
  @ViewChild('employeeEndA') public employeeEndA: EmployeeSelectDirective;
  @ViewChild('period') public period: PeriodSelectDirective;
  @ViewChild('periodYear') public periodYear: PeriodYearSelectDirective;
  @ViewChild('periodA') public periodA: PeriodSelectDirective;
  @ViewChild('periodYearA') public periodYearA: PeriodYearSelectDirective;

  private form: FormGroup = this.fb.group({
    monthlyId: this.fb.control(null)
    // , ouCode: this.fb.control("")
    , yearA: this.fb.control(null)
    , periodA: this.fb.control(null)
    , departmentIdStartA: this.fb.control(null)
    , departmentIdEndA: this.fb.control(null)
    , departmentCodeStartA: this.fb.control("")
    , departmentCodeEndA: this.fb.control("")
    , employeeCodeStartA: this.fb.control("")
    , employeeCodeEndA: this.fb.control("")
    , startDate: this.fb.control(null)
    , endDate: this.fb.control(null)
  });

  private monthlyId: number = null;
  private alert = { title: '', msg: '' };
  private checkDelete: number = 0;
  private fromLoadObject: any;
  private initMsg: string;
  private objCriteria: Sitadt06Criteria = new Sitadt06Criteria();
  private objDelete: Sitadt06Criteria = new Sitadt06Criteria();
  private showMsg = false;
  private records: any[];
  private startDate: any;
  private endDate: any;
  private monthlyIdDelete: any;

  constructor(private fb: FormBuilder
    , private page: Page
    , private pageContext: PageContext
    , private service: Sitadt06Service
    , private router: Router
    , private routerActive: ActivatedRoute) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      // this.setData(this.fromLoadObject.searchData);
      this.monthlyId = this.fromLoadObject.searchData.records.data.monthlyId;
    }
  }

  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
  }

  // public childModalShow(record: any) {
  //   this.pageContext.nextPage(['/ta/sitadt06a', record.monthlyId], this.objCriteria);
  // }

  // public createMonthly() {
  //   let monthlyId: any = "";
  //   this.pageContext.nextPage(['/ta/sitadt06a', monthlyId], this.objCriteria);
  // }

  private changeStartDepartment(valueField: any) {
    this.objCriteria.departmentIdStart = valueField.departmentId;
    this.objCriteria.departmentCodeStart = valueField.departmentCode;
    this.loadEmployee();
  }

  private changeEndDepartment(valueField: any) {
    this.objCriteria.departmentIdEnd = valueField.departmentId;
    this.objCriteria.departmentCodeEnd = valueField.departmentCode;
    this.loadEmployee();
  }

  private changeStartEmployee(valueField: any) {
    this.objCriteria.employeeCodeStart = valueField.empCode;
  }

  private changeEndEmployee(valueField: any) {
    this.objCriteria.employeeCodeEnd = valueField.empCode;
  }

  // private changeShiftGroup(valueField: any) {
  //   this.objCriteria.shiftGroup = valueField.shiftCode;
  // }

  private changeYearGroup(valueField: any) {
    this.objCriteria.year = valueField.periodYear;
    this.getDate();
  }

  private changePeriodGroup(valueField: any) {
    this.objCriteria.period = valueField.period;
    this.getDate();
  }

  private changeStartDepartmentA(valueField: any) {
    this.objCriteria.departmentIdStartA = valueField.departmentId;
    this.objCriteria.departmentCodeStartA = valueField.departmentCode;
    if (BeanUtils.isEmpty(this.monthlyId)) {
      this.loadEmployeeA();
    }
  }

  private changeEndDepartmentA(valueField: any) {
    this.objCriteria.departmentIdEndA = valueField.departmentId;
    this.objCriteria.departmentCodeEndA = valueField.departmentCode;
    if (BeanUtils.isEmpty(this.monthlyId)) {
      this.loadEmployeeA();
    }
  }

  private changeStartEmployeeA(valueField: any) {
    this.objCriteria.employeeCodeStartA = valueField.empCode;
  }

  private changeEndEmployeeA(valueField: any) {
    this.objCriteria.employeeCodeEndA = valueField.empCode;
  }

  private changeYearGroupA(valueField: any) {
    this.objCriteria.yearA = valueField.periodYear;
  }

  private changePeriodGroupA(valueField: any) {
    this.objCriteria.periodA = valueField.period;
  }

  // About Search
  private onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.showMsg = false;
    this.callServiceSearch(changeEvent, this.objCriteria);
  }

  private callServiceSearch(page: Page, mapping: Sitadt06Criteria) {
    this.service.getData(page, mapping)
      .subscribe(response => {
        this.page = response.page;
        this.records = response.records;
        if (BeanUtils.isNotEmpty(this.records)) {
          LoadMask.hide();
        } else {
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
        }
      },
      error => {
        console.error(error);
        LoadMask.hide();
      }
      );
  }

  // About Collect
  private dataCollect(): void {
    // if (BeanUtils.isNotEmpty(this.monthlyId)) {
    //   this.objCriteria.monthlyId = this.monthlyId;
    // }
    if ((this.objCriteria.yearA == null) || (this.objCriteria.periodA == null) ||
      (this.objCriteria.departmentIdStartA == null) || (this.objCriteria.departmentIdEndA == null) ||
      (this.objCriteria.employeeCodeStartA == null) || (this.objCriteria.employeeCodeEndA == null)) {
      this.alertCollectModalToggle('ผิดพลาด', 'กรุณากรอกข้อมูลให้ครบถ้วน');
    } else {
      // this.objCriteria.grid = this.shows;
      // this.objCriteria.yearA = this.form.controls["yearA"].value;
      // this.objCriteria.periodA = this.form.controls["periodA"].value;
      // this.objCriteria.departmentIdStartA = this.form.controls["departmentIdStartA"].value;
      // this.objCriteria.departmentIdEndA = this.form.controls["departmentIdEndA"].value;
      // this.objCriteria.departmentCodeStartA = this.form.controls["departmentCodeStartA"].value;
      // this.objCriteria.departmentCodeEndA = this.form.controls["departmentCodeEndA"].value;
      // this.objCriteria.employeeCodeStartA = this.form.controls["employeeCodeStartA"].value;
      // this.objCriteria.employeeCodeEndA = this.form.controls["employeeCodeEndA"].value;
      // this.showMsg = false;

      // let param = {
      //   periodYearA: this.objCriteria.yearA,
      //   periodA: this.objCriteria.periodA,
      //   departmentStartA: this.objCriteria.departmentIdStartA,
      //   departmentEndA: this.objCriteria.departmentIdEndA,
      //   employeeStartA: this.objCriteria.employeeCodeStartA,
      //   employeeEndA: this.objCriteria.employeeCodeEndA
      // };

      this.service.dataCollect(this.page, this.objCriteria).subscribe(response => {
        if (response.records.data.success) {
          LoadMask.hide();
          this.alertCollectModalToggle('สำเร็จ', 'รวบรวมข้อมูลสำเร็จ');
        }
      },
        error => {
          console.error(error);
          LoadMask.hide();
          this.alertCollectModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ');
        }
      );
      this.periodYearA.clearValue();
      this.periodA.clearValue();
      this.departmentStartA.clearValue();
      this.departmentEndA.clearValue();
      this.employeeStartA.clearValue();
      this.employeeEndA.clearValue();
    }
    // this.collectModal.hide();
  }

  // private setData(object: any) {
  //   if (BeanUtils.isNotEmpty(object)) {
  //     this.form.controls["yearA"].setValue(object.records.data.yearA);
  //     this.form.controls["periodA"].setValue(object.records.data.periodA);
  //     this.form.controls["departmentIdStartA"].setValue(object.records.data.departmentIdStartA);
  //     this.form.controls["departmentIdEndA"].setValue(object.records.data.departmentIdEndA);
  //     this.form.controls["departmentCodeStartA"].setValue(object.records.data.departmentCodeStartA);
  //     this.form.controls["departmentCodeEndA"].setValue(object.records.data.departmentCodeEndA);
  //     this.form.controls["employeeCodeStartA"].setValue(object.records.data.employeeCodeStartA);
  //     this.form.controls["employeeCodeEndA"].setValue(object.records.data.employeeCodeEndA);
  //     this.monthlyId = object.records.data.monthlyId;
  //   }
  // }

  private alertModalClose() {
    if (this.records == null) {
      (this.router.url.split("/")[2] == "sitadt06")
    }
    this.alertModal.toggle();
  }

  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.show();
  }

  private alertCollectModalClose() {
    if (this.records == null) {
      (this.router.url.split("/")[2] == "sitadt06")
    }
    this.alertCollectModal.toggle();
  }

  private alertCollectModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertCollectModal.show();
  }

  public onChangePage(changeEvent: Page) {
    this.page = changeEvent;
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private onSearch(modeSearch: number) {
    if (this.form.valid == true) {
      if (BeanUtils.isNotEmpty(modeSearch)) {
        this.objCriteria.flagForPaging = modeSearch
        this.page.start = 0;
        if (modeSearch == SearchStatus.normalSearch) {
          this.onNormalSearch(this.page);
        }
      }
    }
  }

  public closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }

  private deleteModalClose() {
    this.deleteModal.hide();
  }

  private deleteModalConfirm() {
    this.onDelete();
    this.deleteModal.hide();
  }

  private alertModalDeleteToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.deleteModal.toggle();
  }

  private onBeforeDelete(monthlyId: any) {
    this.monthlyIdDelete = monthlyId;
    this.alertModalDeleteToggle('ยืนยันการลบ', 'ต้องการลบข้อมูลใช่หรือไม่');
  }

  private onDelete() {
    LoadMask.show();
    // this.objDelete.monthlyId = this.monthlyId
    this.service.deleteMonthly(this.page, this.monthlyIdDelete).subscribe(response => {
      this.alertSuccessDeleteToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ');
      LoadMask.hide();
      // this.pageContext.backPage();
    },
      error => {
        console.error(error);
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ');
      }
    );
  }

  private alertSuccessDeleteToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.successDeleteModal.show();
  }

  private alertSuccessDeleteClose() {
    if (this.records == null || this.records != null) {
      (this.router.url.split("/")[2] == "sitadt06")
      this.onNormalSearch(this.page);
    }
    this.successDeleteModal.toggle();
  }

  private loadEmployee() {
    let params = {
      departmentIdStart: this.objCriteria.departmentIdStart || '',
      departmentIdEnd: this.objCriteria.departmentIdEnd || '',
      departmentCodeStart: this.objCriteria.departmentCodeStart || '',
      departmentCodeEnd: this.objCriteria.departmentCodeEnd || ''
    }
    this.employeeStart.load(params);
    this.employeeEnd.load(params);
  }

  private loadEmployeeA() {
    let params = {
      departmentIdStartA: this.objCriteria.departmentIdStartA || '',
      departmentIdEndA: this.objCriteria.departmentIdEndA || '',
      departmentCodeStartA: this.objCriteria.departmentCodeStartA || '',
      departmentCodeEndA: this.objCriteria.departmentCodeEndA || ''
    }
    this.employeeStartA.load(params);
    this.employeeEndA.load(params);
  }

  private getDate() {
    if (this.objCriteria.period != null && this.objCriteria.year != null) {
      LoadMask.show();

      let params = {
        year: this.objCriteria.year,
        period: this.objCriteria.period,
      }

      this.service.loadDate(params)
        .subscribe(response => {
          this.startDate = response.startDate;
          this.endDate = response.endDate;
          LoadMask.hide();
        }, error => {
          this.objCriteria.showDate = null;
          LoadMask.hide();
          this.alertModalToggle('ผิดพลาด', 'ไม่พบข้อมูลที่ระบุ');
        });
    }
    else {
      this.objCriteria.showDate = null;
    }
  }
}
