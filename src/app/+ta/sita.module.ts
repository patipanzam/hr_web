import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SitaRoutingModule } from './sita-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownModule, ModalModule, TabsModule, TypeaheadModule, AlertModule, TooltipModule } from 'ng2-bootstrap';



import { SitaComponent } from './sita.component';
import { Sitart01aComponent } from "./sitart01/sitart01a.component";
import { Sitart01Component } from "./sitart01/sitart01.component";

import { Sitart02Component } from "./sitart02/sitart02.component";
import { Sitart02aComponent } from "./sitart02/sitart02a.component";

import { Sitart03Component } from "./sitart03/sitart03.component";
import { Sitart03aComponent } from "./sitart03/sitart03a.component";

import { Sitart04Component } from './sitart04/sitart04.component';
import { Sitart04aComponent } from "./sitart04/sitart04a.component";

import { Sitart05Component } from "./sitart05/sitart05.component";
import { Sitart05aComponent } from "./sitart05/sitart05a.component";

import { Sitart06Component } from "./sitart06/sitart06.component";
import { Sitart06aComponent } from "./sitart06/sitart06a.component";

import { Sitart07Component } from "./sitart07/sitart07.component";
import { Sitart07aComponent } from "./sitart07/sitart07a.component";

import { Sitart08Component } from "./sitart08/sitart08.component";
import { Sitart08aComponent } from "./sitart08/sitart08a.component";


import { Sitadt01Component } from "./sitadt01/sitadt01.component";
import { Sitadt01aComponent } from "./sitadt01/sitadt01a.component";

import { Sitadt02Component } from "./sitadt02/sitadt02.component";
import { Sitadt02aComponent } from "./sitadt02/sitadt02a.component";

import { Sitadt04Component } from "./sitadt04/sitadt04.component";
import { Sitadt04aComponent } from "./sitadt04/sitadt04a.component";


import { Sitadt05Component } from "./sitadt05/sitadt05.component";

import { Sitadt06Component } from "./sitadt06/sitadt06.component";


import { LocalSelectDirective } from './shared/local-select.directive';

import { EmpTypeSelectDirective } from './shared/emp-type-select.directive';
import { LeaveGroupSelectDirective } from './shared/leave-group-select.directive';
import { LeaveTypeSelectDirective } from './shared/leave-type-select.directive';
import { OuSelectDirective } from './shared/ou-select.directive';
import { EmployeeSelectDirective } from "./shared/employee-select.directive";
import { DepartmentSelectDirective } from "./shared/department-select.directive";
import { ShiftSelectDirective } from "./shared/shift-select.directive";
import { IncomeTypeSelectDirective } from './shared/income-type-select.directive';
import { FileUploadModule } from 'ng2-file-upload';
import { UploadFileComponent } from './../shared/layouts/upload-file.component';
import { PeriodSelectDirective } from './shared/period-select.directive';
import { PeriodYearSelectDirective } from './shared/period-year-select.directive';

@NgModule({
  imports: [SharedModule, ReactiveFormsModule, SitaRoutingModule, DropdownModule, ModalModule, TabsModule, TypeaheadModule, AlertModule, TooltipModule, FileUploadModule],
  declarations: [
    SitaComponent,
    Sitart01Component, Sitart01aComponent,
    Sitart02Component, Sitart02aComponent,
    Sitart03Component, Sitart03aComponent,
    Sitart04Component, Sitart04aComponent,
    Sitart05Component, Sitart05aComponent,
    Sitart06Component, Sitart06aComponent,
    Sitart07Component, Sitart07aComponent,
    Sitart08Component, Sitart08aComponent,
    Sitadt01Component, Sitadt01aComponent,
    Sitadt02Component, Sitadt02aComponent,
    Sitadt04Component, Sitadt04aComponent,
    Sitadt05Component,
    Sitadt06Component,
    LocalSelectDirective,
    EmpTypeSelectDirective,
    LeaveGroupSelectDirective,
    LeaveTypeSelectDirective,
    OuSelectDirective,
    EmployeeSelectDirective,
    DepartmentSelectDirective,
    ShiftSelectDirective,
    IncomeTypeSelectDirective,
    UploadFileComponent,
    PeriodSelectDirective,
    PeriodYearSelectDirective

  ],
  providers: []
})
export class SitaModule {
}
