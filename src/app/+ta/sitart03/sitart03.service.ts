import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { Sitart03Criteria, Sitart03SaveShift } from '../sitart03/sitart03.interface';
import { BeanUtils } from '../../core/bean-utils.service';

const querystring = require('querystring');

@Injectable()
export class Sitart03Service {
  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

  public getData(page: Page, searchValue: Sitart03Criteria): Observable<any> {
    let params = '?' + querystring.stringify(page) + "&" + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('ta/sitart03/searchShift') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });
  }

  public saveShift(page: Page, params: Sitart03SaveShift): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('ta/sitart03/saveShift'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  public searchData(shiftId: number): Observable<any> {
    let params = '?shiftId=' + shiftId;
    return this.authHttp.get(this.restServer.getAPI('ta/sitart03/selectShift') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  public deleteShift(page: Page, params: Sitart03SaveShift): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('ta/sitart03/deleteShift'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }
}
