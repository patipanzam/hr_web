import { Component, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitart03Service } from './sitart03.service';
import { Sitart03Criteria, Sitart03SaveShift } from '../sitart03/sitart03.interface';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BeanUtils } from '../../core/bean-utils.service';
import { LoadMask } from '../../load-mask.component';
import { SSTimePicker } from '../../shared/ss-timepicker.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { OuSelectDirective } from '../shared/ou-select.directive';

@Component({
  selector: 'sitart03a',
  template: require('./sitart03a.component.html'),
  providers: [Page, Sitart03Service]
})
export class Sitart03aComponent implements AfterViewInit {

  @Output('closeModal') private closeEvent = new EventEmitter();
  @Input('info') private info: any;
  @ViewChild('alertModal') private alertModal: ModalDirective;
  @ViewChild('deleteModal') private deleteModal: ModalDirective;
  @ViewChild('backModal') private backModal: ModalDirective;
  @ViewChild('timetime') private timetime: SSTimePicker;
  @ViewChild('shiftIn') private shiftInComp: SSTimePicker;
  @ViewChild('shiftOut') private shiftOutComp: SSTimePicker;
  @ViewChild('breakIn') private breakInComp: SSTimePicker;
  @ViewChild('breakOut') private breakOutComp: SSTimePicker;
  @ViewChild('otBeforeIn') private otBeforeInComp: SSTimePicker;
  @ViewChild('otBeforeOut') private otBeforeOutComp: SSTimePicker;
  @ViewChild('otAfterIn') private otAfterInComp: SSTimePicker;
  @ViewChild('otAfterOut') private otAfterOutComp: SSTimePicker;
  @ViewChild('ouCode') private ouCode: OuSelectDirective;

  private form: FormGroup = this.fb.group({
    shiftId: this.fb.control("")
    , shiftCode: this.fb.control("", Validators.required)
    , shiftDesc: this.fb.control("")
    , status: this.fb.control("")
    , workSunday: this.fb.control("")
    , workMonday: this.fb.control("")
    , workTuesday: this.fb.control("")
    , workWednesday: this.fb.control("")
    , workThursday: this.fb.control("")
    , workFriday: this.fb.control("")
    , workSaturday: this.fb.control("")
    , workallday: this.fb.control("")
    , workHour: this.fb.control("", Validators.required)
    , minAbsence: this.fb.control("")
    , minBackHome: this.fb.control("")
    , minLate: this.fb.control("")
    , shiftIn: this.fb.control("", Validators.required)
    , shiftOut: this.fb.control("", Validators.required)
    , breakIn: this.fb.control("", Validators.required)
    , breakOut: this.fb.control("", Validators.required)
    , otBeforeIn: this.fb.control("")
    , otBeforeOut: this.fb.control("")
    , otAfterIn: this.fb.control("")
    , otAfterOut: this.fb.control("")
    , ouCode: this.fb.control("", Validators.required)

  });
  private shiftId: number = null;
  private shiftCode: string = "";
  private shiftDesc: string = "";
  private status: number = null;
  private workSunday: number = null;
  private workMonday: number = null;
  private workTuesday: number = null;
  private workWednesday: number = null;
  private workThursday: number = null;
  private workFriday: number = null;
  private workSaturday: number = null;
  private workallday: number = null;
  private workHour: number = null;
  private minAbsence: number = null;
  private minBackHome: number = null;
  private minLate: number = null;
  private shiftIn: string = null;
  private shiftOut: string = null;
  private breakIn: string = null;
  private breakOut: string = null;
  private otBeforeIn: string = null;
  private otBeforeOut: string = null;
  private otAfterIn: string = null;
  private otAfterOut: string = null;
  private fromLoadObject: any;
  private initMsg: string;
  private showMsg = false;
  private objCriteria: Sitart03SaveShift = new Sitart03SaveShift();
  private objDelete: Sitart03SaveShift = new Sitart03SaveShift();
  private alert = { title: '', msg: '', isRedirect: false };
  private records: any[];
  private flagForLoad: boolean = false;
  private dirty: boolean;

  constructor(private page: Page
    , private fb: FormBuilder
    , private service: Sitart03Service
    , private router: Router
    , private pageContext: PageContext
    , private routerActive: ActivatedRoute
  ) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      this.setData(this.fromLoadObject.searchData);
    }
  }
  ngOnInit() {

  }
  ngAfterViewInit() {
    if (BeanUtils.isNotEmpty(this.shiftId)) {
      this.form.controls["shiftCode"].disable();
      this.form.controls["workHour"].disable();
      this.ouCode.setValue(this.form.controls["ouCode"].value);
      this.ouCode.disabled(true);
      this.shiftInComp.setValue(this.form.controls["shiftIn"].value);
      this.shiftOutComp.setValue(this.form.controls["shiftOut"].value);
      this.breakInComp.setValue(this.form.controls["breakIn"].value);
      this.breakOutComp.setValue(this.form.controls["breakOut"].value);
      this.otBeforeInComp.setValue(this.form.controls["otBeforeIn"].value);
      this.otBeforeOutComp.setValue(this.form.controls["otBeforeOut"].value);
      this.otAfterInComp.setValue(this.form.controls["otAfterIn"].value);
      this.otAfterOutComp.setValue(this.form.controls["otAfterOut"].value);
    }
    else {
      this.form.controls["status"].setValue(1);
      this.form.controls["workallday"].setValue(0);
      this.form.controls["workMonday"].setValue(1);
      this.form.controls["workTuesday"].setValue(1);
      this.form.controls["workWednesday"].setValue(1);
      this.form.controls["workThursday"].setValue(1);
      this.form.controls["workFriday"].setValue(1);
      this.form.controls["workSaturday"].setValue(0);
      this.form.controls["workSunday"].setValue(0);
    }
  }

  private alertModalToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.alertModal.show();
  }

  private alertModalClose() {
    if (this.records != null) {
      if (this.router.url.split("/")[2] == "sitart03a") {
        this.pageContext.backPage();
      }
    }
    this.alertModal.toggle();
  }

  private onSave() {
  if (this.form.valid == true) {
    if (this.form.dirty) {
      LoadMask.show();
      if (BeanUtils.isNotEmpty(this.shiftId)) {
        this.objCriteria.shiftId = this.shiftId;
      }
      this.objCriteria.ouCode = this.form.controls["ouCode"].value;
      this.objCriteria.shiftCode = this.form.controls["shiftCode"].value;
      this.objCriteria.shiftDesc = this.form.controls["shiftDesc"].value;
      this.objCriteria.status = this.form.controls["status"].value;
      this.objCriteria.workallday = this.form.controls["workallday"].value;
      this.objCriteria.workMonday = this.form.controls["workMonday"].value;
      this.objCriteria.workTuesday = this.form.controls["workTuesday"].value;
      this.objCriteria.workWednesday = this.form.controls["workWednesday"].value;
      this.objCriteria.workThursday = this.form.controls["workThursday"].value;
      this.objCriteria.workFriday = this.form.controls["workFriday"].value;
      this.objCriteria.workSaturday = this.form.controls["workSaturday"].value;
      this.objCriteria.workSunday = this.form.controls["workSunday"].value;
      this.objCriteria.workHour = this.form.controls["workHour"].value;
      this.objCriteria.minAbsence = this.form.controls["minAbsence"].value;
      this.objCriteria.minBackHome = this.form.controls["minBackHome"].value;
      this.objCriteria.minLate = this.form.controls["minLate"].value;
      this.objCriteria.shiftIn = this.form.controls["shiftIn"].value;
      this.objCriteria.shiftOut = this.form.controls["shiftOut"].value;
      this.objCriteria.breakIn = this.form.controls["breakIn"].value;
      this.objCriteria.breakOut = this.form.controls["breakOut"].value;
      this.objCriteria.otBeforeIn = this.form.controls["otBeforeIn"].value;
      this.objCriteria.otBeforeOut = this.form.controls["otBeforeOut"].value;
      this.objCriteria.otAfterIn = this.form.controls["otAfterIn"].value;
      this.objCriteria.otAfterOut = this.form.controls["otAfterOut"].value;
      this.showMsg = false;
      this.service.saveShift(this.page, this.objCriteria).subscribe(response => {
        this.records = response.records.data.shiftId;
        this.shiftId = response.records.data.shiftId;
        if (response.records.data.shiftCode == 0) {
          LoadMask.hide();
          this.alertModalToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ', true);
          this.ouCode.disabled(true);
          this.form.controls["shiftCode"].disable();
          this.form.controls["workHour"].disable();
          this.form.markAsPristine();
        } else if (response.records.data.shiftCode == 1) {
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'รหัสกะการทำงานซ้ำ', false);
        } else {
          LoadMask.hide();
          this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้', false);
        }
      },
        error => {
          LoadMask.hide();
          this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
        }
      );
    } else {
      LoadMask.hide();
      this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข', false);
    }
  } else {
    LoadMask.hide();
    this.alertModalToggle('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล', false);
  }
}

  private setData(object: any) {
  if (BeanUtils.isNotEmpty(object)) {
    this.form.controls["ouCode"].setValue(object.records.data[0].ouCode);
    this.form.controls["shiftCode"].setValue(object.records.data[0].shiftCode);
    this.form.controls["shiftDesc"].setValue(object.records.data[0].shiftDesc);
    this.form.controls["status"].setValue(object.records.data[0].status);
    this.form.controls["workMonday"].setValue(object.records.data[0].workMonday);
    this.form.controls["workTuesday"].setValue(object.records.data[0].workTuesday);
    this.form.controls["workWednesday"].setValue(object.records.data[0].workWednesday);
    this.form.controls["workThursday"].setValue(object.records.data[0].workThursday);
    this.form.controls["workFriday"].setValue(object.records.data[0].workFriday);
    this.form.controls["workSaturday"].setValue(object.records.data[0].workSaturday);
    this.form.controls["workSunday"].setValue(object.records.data[0].workSunday);
    this.checkAll();
    this.form.controls["workHour"].setValue(object.records.data[0].workHour);
    this.form.controls["minAbsence"].setValue(object.records.data[0].minAbsence);
    this.form.controls["minBackHome"].setValue(object.records.data[0].minBackHome);
    this.form.controls["minLate"].setValue(object.records.data[0].minLate);
    this.form.controls["shiftIn"].setValue(object.records.data[0].shiftIn);
    this.form.controls["shiftOut"].setValue(object.records.data[0].shiftOut);
    this.form.controls["breakIn"].setValue(object.records.data[0].breakIn);
    this.form.controls["breakOut"].setValue(object.records.data[0].breakOut);
    this.form.controls["otBeforeIn"].setValue(object.records.data[0].otBeforeIn);
    this.form.controls["otBeforeOut"].setValue(object.records.data[0].otBeforeOut);
    this.form.controls["otAfterIn"].setValue(object.records.data[0].otAfterIn);
    this.form.controls["otAfterOut"].setValue(object.records.data[0].otAfterOut);
    this.shiftId = object.records.data[0].shiftId;
    this.minLate = object.records.data[0].minLate;
    this.minAbsence = object.records.data[0].minAbsence;
    this.minBackHome = object.records.data[0].minBackHome;
  }
}

  private onBack() {
  if (this.form.dirty) {
    this.backModalToggle('แจ้งเตือน', 'ข้อมูลได้รับการแก้ไข ต้องการเปลี่ยนหน้าหรือไม่ ?');
  } else {
    this.pageContext.backPage();
  }
}

  private backModalClose(): void {
  this.backModal.hide();
}

  private backModalBack(): void {
  this.pageContext.backPage();
}

  private backModalToggle(title: string, msg: string): void {
  this.alert.title = title;
  this.alert.msg = msg;
  this.backModal.toggle();
}

  private onCancel() {
  this.closeEvent.emit();
}

  private deleteModalClose(): void {
  this.deleteModal.hide();
}

  private deleteModalConfirm(): void {
  this.onDelete();
  this.deleteModal.hide();
}

  private alertModalDeleteToggle(title: string, msg: string): void {
  this.alert.title = title;
  this.alert.msg = msg;
  this.deleteModal.toggle();
}

  private onBeforeDelete(record: any) {
  this.alertModalDeleteToggle('ยืนยันการลบ', 'คุณยืนยันการลบหรือไม่');
}

  private onDelete() {
  LoadMask.show();
  this.objDelete.shiftId = this.shiftId
  this.service.deleteShift(this.page, this.objDelete).subscribe(response => {
    this.alertModalToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ', true);
    LoadMask.hide();
    this.pageContext.backPage();
  },
    error => {
      console.error(error);
      LoadMask.hide();
      this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
    }
  );
}

  private changCheckboxstatus(event: any) {
  if (event.target.checked) {
    this.form.controls["status"].setValue(1);
  } else {
    this.form.controls["status"].setValue(0);
  }
}

  private changCheckboxallday(event: any) {
  if (event.target.checked) {
    this.form.controls["workallday"].setValue(1);
    this.form.controls["workMonday"].setValue(1);
    this.form.controls["workTuesday"].setValue(1);
    this.form.controls["workWednesday"].setValue(1);
    this.form.controls["workThursday"].setValue(1);
    this.form.controls["workFriday"].setValue(1);
    this.form.controls["workSaturday"].setValue(1);
    this.form.controls["workSunday"].setValue(1);
  } else {
    this.form.controls["workallday"].setValue(0);
    this.form.controls["workMonday"].setValue(0);
    this.form.controls["workTuesday"].setValue(0);
    this.form.controls["workWednesday"].setValue(0);
    this.form.controls["workThursday"].setValue(0);
    this.form.controls["workFriday"].setValue(0);
    this.form.controls["workSaturday"].setValue(0);
    this.form.controls["workSunday"].setValue(0);
  }
}

  private checkAll() {
  if (
    this.form.controls["workMonday"].value == 1 &&
    this.form.controls["workTuesday"].value == 1 &&
    this.form.controls["workWednesday"].value == 1 &&
    this.form.controls["workThursday"].value == 1 &&
    this.form.controls["workFriday"].value == 1 &&
    this.form.controls["workSaturday"].value == 1 &&
    this.form.controls["workSunday"].value == 1
  ) {
    this.form.controls["workallday"].setValue(1);
  }
}

  private changCheckboxmonday(event: any) {
  if (event.target.checked) {
    this.form.controls["workMonday"].setValue(1);
  } else {
    this.form.controls["workMonday"].setValue(0);
    this.form.controls["workallday"].setValue(0);
  }
  this.checkAll();
}

  private changCheckboxtuesday(event: any) {
  if (event.target.checked) {
    this.form.controls["workTuesday"].setValue(1);
  } else {
    this.form.controls["workTuesday"].setValue(0);
    this.form.controls["workallday"].setValue(0);
  }
  this.checkAll();
}

  private changCheckboxwednesday(event: any) {
  if (event.target.checked) {
    this.form.controls["workWednesday"].setValue(1);
  } else {
    this.form.controls["workWednesday"].setValue(0);
    this.form.controls["workallday"].setValue(0);
  }
  this.checkAll();
}

  private changCheckboxthursday(event: any) {
  if (event.target.checked) {
    this.form.controls["workThursday"].setValue(1);
  } else {
    this.form.controls["workThursday"].setValue(0);
    this.form.controls["workallday"].setValue(0);
  }
  this.checkAll();
}

  private changCheckboxfriday(event: any) {
  if (event.target.checked) {
    this.form.controls["workFriday"].setValue(1);
  } else {
    this.form.controls["workFriday"].setValue(0);
    this.form.controls["workallday"].setValue(0);
  }
  this.checkAll();
}

  private changCheckboxsaturday(event: any) {
  if (event.target.checked) {
    this.form.controls["workSaturday"].setValue(1);
  } else {
    this.form.controls["workSaturday"].setValue(0);
    this.form.controls["workallday"].setValue(0);
  }
  this.checkAll();
}

  private changCheckboxasunday(event: any) {
  if (event.target.checked) {
    this.form.controls["workSunday"].setValue(1);
  } else {
    this.form.controls["workSunday"].setValue(0);
    this.form.controls["workallday"].setValue(0);
  }
  this.checkAll();
}

  private changeShiftIn(valueField: any) {
  if (valueField) {
    this.form.controls["shiftIn"].setValue(this.shiftInComp.getValue());
    this.objCriteria.shiftIn = this.shiftInComp.getValue();
  }
}

  private changeShiftOut(valueField: any) {
  if (valueField) {
    this.form.controls["shiftOut"].setValue(this.shiftOutComp.getValue());
    this.objCriteria.shiftOut = this.shiftOutComp.getValue();
  }
}

  private changeBreakIn(valueField: any) {
  if (valueField) {
    this.form.controls["breakIn"].setValue(this.breakInComp.getValue());
    this.objCriteria.breakIn = this.breakInComp.getValue();
  }
}

  private changeBreakOut(valueField: any) {
  if (valueField) {
    this.form.controls["breakOut"].setValue(this.breakOutComp.getValue());
    this.objCriteria.breakOut = this.breakOutComp.getValue();
  }
}

  private changeOtBeforeIn(valueField: any) {
  if (valueField) {
    this.form.controls["otBeforeIn"].setValue(this.otBeforeInComp.getValue());
    this.objCriteria.otBeforeIn = this.otBeforeInComp.getValue();
  }
}

  private changeOtBeforeOut(valueField: any) {
  if (valueField) {
    this.form.controls["otBeforeOut"].setValue(this.otBeforeOutComp.getValue());
    this.objCriteria.otBeforeOut = this.otBeforeOutComp.getValue();
  }
}

  private changeOtAfterIn(valueField: any) {
  if (valueField) {
    this.form.controls["otAfterIn"].setValue(this.otAfterInComp.getValue());
    this.objCriteria.otAfterIn = this.otAfterInComp.getValue();
  }
}

  private changeOtAfterOut(valueField: any) {
  if (valueField) {
    this.form.controls["otAfterOut"].setValue(this.otAfterOutComp.getValue());
    this.objCriteria.otAfterOut = this.otAfterOutComp.getValue();
  }
}

  private changeOu(valueField: any) {
  this.form.controls["ouCode"].setValue(this.ouCode.getValue());
  this.objCriteria.ouCode = this.ouCode.getValue();
  this.fromLoadObject.ouCode = this.ouCode.getValue();
}
}






























