import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { LoadMask } from '../../load-mask.component';
import { Sitart03Service } from './sitart03.service';
import { BeanUtils } from '../../core/bean-utils.service';

@Injectable()
export class Sitart03Resolve implements Resolve<any> {
  constructor(private selectService: CommonSelectService, private Sitart03Service: Sitart03Service) {
    this.selectService.setModuleName('ta');
  }

  resolve(route: ActivatedRouteSnapshot): any {
    LoadMask.show();
    if (route.url[0].path == 'sitart03') {
      LoadMask.hide();
      return {
      };

    } else if (route.url[0].path == 'sitart03a') {
      if (BeanUtils.isNotEmpty(route.params['shiftId'])) {
        var searchData = this.Sitart03Service.searchData(route.params['shiftId']);
        var ouCode = this.selectService.getOu();
        return Observable.forkJoin([searchData,ouCode]).map((response) => {
          LoadMask.hide();
          return {
            searchData: response[0],
            ouCode: response[1]
          };
        }).first();

      } else {
        var ouCode = this.selectService.getOu();
        return Observable.forkJoin([ouCode]).map((response) => {
          LoadMask.hide();
          return {
            ouCode: response[0]
          };
        }).first();
      }
    }
  }
}
