import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitart03Service } from './sitart03.service';
import { Sitart03Criteria, Sitart03SaveShift } from '../sitart03/sitart03.interface';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DateUtils } from '../../core/date-utils.service';
import { LoadMask } from '../../load-mask.component';
import { BeanUtils } from '../../core/bean-utils.service';
import { SearchStatus } from '../../shared/constant/common.interface';
import { PageContext } from '../../shared/service/pageContext.service';

@Component({
  selector: 'sitart03',
  template: require('./sitart03.component.html'),
  providers: [Page, Sitart03Service]
})
export class Sitart03Component {

  @ViewChild('alertModal') private alertModal: ModalDirective;

  private form: FormGroup = this.formBuilder.group({
    shiftCode: this.formBuilder.control("")
    , shiftDesc: this.formBuilder.control("")
    , shiftIn: this.formBuilder.control("")
    , shiftOut: this.formBuilder.control("")
    , breakIn: this.formBuilder.control("")
    , breakOut: this.formBuilder.control("")
    , otBeforeIn: this.formBuilder.control("")
    , otBeforeOut: this.formBuilder.control("")
    , otAfterIn: this.formBuilder.control("")
    , otAfterOut: this.formBuilder.control("")
    , workHour: this.formBuilder.control("")
    , minAbsence: this.formBuilder.control("")
    , minBackHome: this.formBuilder.control("")
    , minLate: this.formBuilder.control("")
    , workSunday: this.formBuilder.control("")
    , workMonday: this.formBuilder.control("")
    , workTuesday: this.formBuilder.control("")
    , workWednesday: this.formBuilder.control("")
    , workThursday: this.formBuilder.control("")
    , workFriday: this.formBuilder.control("")
    , workSaturday: this.formBuilder.control("")
    , status: this.formBuilder.control("")
  });
  private initMsg: string;
  private showMsg = false;
  private records: any[];
  private fromLoadObject: any;
  private objCriteria: Sitart03Criteria = new Sitart03Criteria();
  private alert = { title: '', msg: '' };
  private checkDelete: number = 0;
  private shiftId: any;

  private childModalShow(record: any) {
    this.pageContext.nextPage(['/ta/sitart03a', record.shiftId], this.objCriteria);
  }

  private createShift() {
    let shiftId: any = "";
    this.pageContext.nextPage(['/ta/sitart03a', shiftId], this.objCriteria);
  }

  constructor(private page: Page
    , private service: Sitart03Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private pageContext: PageContext
    , private formBuilder: FormBuilder) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }
  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
  }

  private onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.showMsg = false;
    if (BeanUtils.isNotEmpty(changeEvent)) {
      this.page = changeEvent;
    }
    this.callServiceSearch(changeEvent, this.objCriteria);
  }

  private alertModalToggle(title: string, msg: string): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  private callServiceSearch(page: Page, mapping: Sitart03Criteria) {
    this.service.getData(page, mapping).subscribe(response => {
      this.page = response.page;
      this.records = response.records;
      if (BeanUtils.isNotEmpty(this.records)) {
        LoadMask.hide();
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
      }
    },
      error => {
        console.error(error);
        LoadMask.hide();
      }
    );
  }

  private onChangePage(changeEvent: Page) {
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private onSearch(modeSearch: number) {
    if (BeanUtils.isNotEmpty(modeSearch)) {
      this.objCriteria.flagForPaging = modeSearch
      this.page.start = 0;
      if (modeSearch == SearchStatus.normalSearch) {
        this.onNormalSearch(this.page);
      }
    }
  }

  private closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }
}
