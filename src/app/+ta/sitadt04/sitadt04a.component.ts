import { LocalSelectDirective } from '../shared/local-select.directive';
import { Component, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Sitadt04Criteria, Sitadt04SaveClock } from '../sitadt04/sitadt04.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitadt04Service } from './sitadt04.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BeanUtils } from '../../core/bean-utils.service';
import { LoadMask } from '../../load-mask.component';
import { EmployeeSelectDirective } from '../shared/employee-select.directive';
import { SSDatePicker } from '../../shared/ss-datepicker.component';
import { DateUtils } from '../../core/date-utils.service';
import { SSTimePicker } from '../../shared/ss-timepicker.component';
import { OuSelectDirective } from "../../+ta/shared/ou-select.directive";
import { PageContext } from '../../shared/service/pageContext.service';

@Component({
  selector: 'sitadt04a',
  template: require('./sitadt04a.component.html'),
  providers: [Page, Sitadt04Service]
})
export class Sitadt04aComponent {

  @Output('closeModal') private closeEvent = new EventEmitter();
  @ViewChild('deleteModal') public deleteModal: ModalDirective;
  @Input('info') private info: any;
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('backModal') public backModal: ModalDirective;
  @ViewChild('employeeAdd') public employeeAdd: EmployeeSelectDirective;
  @ViewChild('dateCard') public dateCard: SSDatePicker;
  @ViewChild('timeIn') public timeInClock: SSTimePicker;
  @ViewChild('timeOut') public timeOutClock: SSTimePicker;
  @ViewChild('calculateTimeIn') public calculateTimeIn: SSTimePicker;
  @ViewChild('calculateTimeOut') public calculateTimeOut: SSTimePicker;
  @ViewChild('ouCode') public ouCode: OuSelectDirective;

  private dateCardVal: number;

  private form: FormGroup = this.fb.group({
    clockId: this.fb.control("")
    , dateCard: this.fb.control("")
    , clockDate: this.fb.control("")
    , timeIn: this.fb.control("", Validators.required)
    , timeOut: this.fb.control("", Validators.required)
    , ouCode: this.fb.control("", Validators.required)
    , calculateTimeIn: this.fb.control("")
    , calculateTimeOut: this.fb.control("")
    , empId: this.fb.control("")
    , empCode: this.fb.control("")

  });

  private clockId: number = null;
  private empId: string;
  private timeIn: string;
  private timeOut: string;
  private status: number = null;
  private ou: string = "";
  private dirty: boolean;
  private routerActiveSubscribe: any;
  private flagForload = 0;

  private fromLoadObject: any;
  private initMsg: string;
  private showMsg = false;
  private alert = { title: '', msg: '' };
  private records: any[];
  private rows: any[] = [];
  private flagForLoad: boolean = false;
  private data: any;
  private objCriteria: Sitadt04SaveClock = new Sitadt04SaveClock();
  private objDelete: Sitadt04SaveClock = new Sitadt04SaveClock();

  constructor(private page: Page
    , private fb: FormBuilder
    , private service: Sitadt04Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private pageContext: PageContext) {

    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      this.setData(this.fromLoadObject.searchData);
      this.clockId = this.fromLoadObject.searchData.records.data.clockId;
    }
  }

  ngAfterViewInit() {
    if (!this.info) {
      this.flagForload = 1;
      if (BeanUtils.isNotEmpty(this.clockId)) {
        this.flagForLoad = true;
        this.ouCode.setValue(this.form.controls["ouCode"].value);
        this.ouCode.disabled(true);
        this.employeeAdd.setValue(this.form.controls["empCode"].value);
        this.employeeAdd.disabled(true);
        this.dateCard.setValue(this.dateCardVal);
        this.dateCard.readOnly = true;
        this.timeInClock.setValue(this.form.controls["timeIn"].value);
        this.timeInClock.readOnly = true;
        this.timeOutClock.setValue(this.form.controls["timeOut"].value);
        this.timeOutClock.readOnly = true;
        this.calculateTimeIn.setValue(this.form.controls["calculateTimeIn"].value);
        this.calculateTimeIn.readOnly = false;
        this.calculateTimeOut.setValue(this.form.controls["calculateTimeOut"].value);
        this.calculateTimeOut.readOnly = false;

        this.flagForLoad = false;
      }
      this.flagForload = 0;
    }
  }
  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  private alertModalClose() {
    if (null != this.records) {
      if (this.router.url.split("/")[2] == "sitadt04a") {
        this.pageContext.backPage();
      }
    }
    this.alertModal.toggle();
  }

  public onSave() {
    if (this.form.valid == true) {
      if (this.form.dirty) {
        LoadMask.show();
        if (BeanUtils.isNotEmpty(this.clockId)) {
          this.objCriteria.clockId = this.clockId;
        }
        this.objCriteria.ouCode = this.form.controls["ouCode"].value;
        this.objCriteria.empId = this.form.controls["empId"].value;
        this.objCriteria.timeIn = this.form.controls["timeIn"].value;
        this.objCriteria.timeOut = this.form.controls["timeOut"].value;
        this.objCriteria.calculateTimeIn = this.form.controls["calculateTimeIn"].value;
        this.objCriteria.calculateTimeOut = this.form.controls["calculateTimeOut"].value;
        this.objCriteria.date = this.form.controls["dateCard"].value;
        this.objCriteria.clockDate = this.form.controls["dateCard"].value;
        this.showMsg = false;

        this.service.saveClock(this.page, this.objCriteria).subscribe(response => {
          this.records = response.records.data.clockId;
          this.clockId = response.records.data.clockId;
          if (response.records.data.checkClockCode == 0) {
            LoadMask.hide();
            this.alertModalToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ');
            this.ouCode.disabled(true);
            this.form.markAsPristine();
          } else if (response.records.data.checkClockCode == 1) {
            LoadMask.hide();
            this.alertModalToggle('แจ้งเตือน', 'รหัสพนักงานซ้ำ');
          } else {
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้');
          }
        },
          error => {
            console.error(error);
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ');
          }
        );
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข');
      }
    } else {
      LoadMask.hide();
      this.alertModalToggle('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
    }
  }

  public onBack() {
    this.checkDirty();
    if (this.form.dirty == false) {
      this.backModalToggle('แจ้งเตือน', 'ข้อมูลได้รับการแก้ไข ต้องการเปลี่ยนหน้าหรือไม่ ?');
    } else {
      this.pageContext.backPage();
    }
  }

  private backModalClose() {
    this.backModal.hide();
  }

  private backModalBack() {
    this.pageContext.backPage();
  }

  private backModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;

    this.backModal.toggle();
  }
  public onCancel() {
    this.closeEvent.emit();
  }

  private deleteModalClose() {
    this.deleteModal.hide();
  }

  private deleteModalConfirm() {
    this.onDelete();
    this.deleteModal.hide();
  }

  private alertModalDeleteToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.deleteModal.toggle();
  }

  public onBeforeDelete(record: any) {
    this.alertModalDeleteToggle('ยืนยันการลบ', 'คุณยืนยันการลบหรือไม่');
  }

  public checkDirty() {
    if (this.form.dirty == true) {
      this.dirty = true;
    } else {
      this.dirty = false;
    }
    return this.dirty;
  }

  private setData(object: any) {
    if (BeanUtils.isNotEmpty(object)) {
      this.form.controls["ouCode"].setValue(object.records.data.ouCode);
      this.form.controls["empId"].setValue(object.records.data.empId);
      this.form.controls["empCode"].setValue(object.records.data.empCode);
      this.form.controls["dateCard"].setValue(object.records.data.date);
      this.form.controls["timeIn"].setValue(object.records.data.timeIn);
      this.form.controls["timeOut"].setValue(object.records.data.timeOut);
      this.form.controls["calculateTimeIn"].setValue(object.records.data.calculateTimeIn);
      this.form.controls["calculateTimeOut"].setValue(object.records.data.calculateTimeOut);
      this.clockId = object.records.data.clockId;
      this.dateCardVal = +object.records.data.date;

    }
  }

  public onDelete() {
    LoadMask.show();
    this.objDelete.clockId = this.clockId
    this.service.deleteClock(this.page, this.objDelete).subscribe(response => {
      this.alertModalToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ');
      LoadMask.hide();
      this.pageContext.backPage();
    },
      error => {
        console.error(error);
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ');
      }
    );
  }

  private changeOuCode(valueField: any) {
    this.form.controls["ouCode"].setValue(valueField.ouCode);
    this.objCriteria.ouCode = valueField.ouCode;
    if (BeanUtils.isEmpty(this.clockId)) {
      this.loadEmployee();
    }
  }
  private changCheckbox(event: any) {
    if (event.target.checked) {
      this.form.controls["status"].setValue(1);
      this.objCriteria.status = 1;
    } else {
      this.form.controls["status"].setValue(0);
      this.objCriteria.status = 0;
    }
  }

  private changeEmployeeAdd(valueField: any) {
    this.form.controls["empId"].setValue(valueField.empId);
    this.objCriteria.empCode = valueField.empCode;

    if (this.flagForLoad == false) {
      this.form.controls["empId"].markAsDirty(true);
    }
  }

  private changeDate(valueField: any) {
    if (valueField) {
      this.form.controls["dateCard"].setValue(this.dateCard.getMilliSecond());
      // this.objCriteria.date = this.dateCard.getMilliSecond();
      this.objCriteria.obligationsHolidayDate = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsHolidayDateShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);

    }
  }

  private changeTimeIn(valueField: any) {
    if (valueField) {
      this.form.controls["timeIn"].setValue(this.timeInClock.getValue());
      this.objCriteria.timeIn = this.timeInClock.getValue();
    }
  }

  private changeTimeOut(valueField: any) {
    if (valueField) {
      this.form.controls["timeOut"].setValue(this.timeOutClock.getValue());
      this.objCriteria.timeOut = this.timeOutClock.getValue();
    }
  }

  private changeCalculateIn(valueField: any) {
    if (valueField) {
      this.form.controls["calculateTimeIn"].setValue(this.calculateTimeIn.getValue());
      this.objCriteria.calculateTimeIn = this.calculateTimeIn.getValue();
      if (this.flagForLoad == false) {
        this.form.controls["calculateTimeIn"].markAsDirty(true);
      }
    }
  }

  private changeCalculateOut(valueField: any) {
    if (valueField) {
      this.form.controls["calculateTimeOut"].setValue(this.calculateTimeOut.getValue());
      this.objCriteria.calculateTimeOut = this.calculateTimeOut.getValue();
      if (this.flagForLoad == false) {
        this.form.controls["calculateTimeOut"].markAsDirty(true);
      }
    }
  }
  private loadEmployee() {
    let params = {
      ouCode: this.objCriteria.ouCode || ''
    }
    this.employeeAdd.load(params);
  }
}

