import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { Sitadt04Criteria, Sitadt04SaveClock } from '../sitadt04/sitadt04.interface';

const querystring = require('querystring');

@Injectable()
export class Sitadt04Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

  //Sitart02 search
  public getData(page: Page, searchValue: Sitadt04Criteria): Observable<any> {
    let params = '?' + querystring.stringify(page) + "&" + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('/ta/sitadt04/searchClock') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });
  }

  //saveClock
  public saveClock(page: Page, params: Sitadt04SaveClock): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitadt04/saveClock'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  //selectClock
  public searchData(clockId: number): Observable<any> {
    let params = '?clockId=' + clockId;
    return this.authHttp.get(this.restServer.getAPI('/ta/sitadt04/selectClock') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  // deleteClock
  public deleteClock(page: Page, params: Sitadt04SaveClock): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('ta/sitadt04/deleteClock'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  public getDataAdvSearch(page: Page, ouCode: string, empTypeCode: string, employTypeCode: string): Observable<any> {
    let params = '?' + querystring.stringify(page) + '&ouCode=' + ouCode + '&empTypeCode=' + empTypeCode + '&employTypeCode=' + employTypeCode;
    return this.authHttp.get(this.restServer.getAPI('py/sipyrt01/getSalaryPeriod') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }, records: val.json().data
        }
      });
  }
  public getAnalyzeData(page: Page, searchValue: Sitadt04Criteria): Observable<any> {
    let params = '?' + querystring.stringify(searchValue) + '&' + querystring.stringify(page);
    return this.authHttp.get(this.restServer.getAPI('ta/sitadt04/analyzeData') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }, records: val.json().data
        }
      });
  }

  public listFile(module: any): Observable<any> {
    return this.authHttp.get(this.restServer.getAPI(module + '/sitadt04/listFile')+ '?module=' + module)
      .map(value => {
        return value.json()
      });
  }

  public getEmployee(employeeId: any, addressTypeId: any): Observable<any> {
    return this.authHttp.get(this.restServer.getAPI('pm/sipmdt01/get/emp/detail/empCode') + '?employeeId=' + employeeId + '&addressTypeId=' + addressTypeId)
      .map(value => {
        return value.json()
      });
  }
}
