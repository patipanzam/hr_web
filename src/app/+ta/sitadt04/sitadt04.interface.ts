export class Sitadt04Criteria {
  public inputSearch: string = '';
  public clockId: number;
  public ouCode: string;
  public empId: number;
  public date: number;
  public timeIn: string;
  public timeOut: string;
  public calculateTimeIn: string;
  public calculateTimeOut: string;
  public clockDatas: string;
  public fileName: string;
  public flagForPaging: number = 0;
  public obligationsHolidayDate: any;
  public obligationsHolidayDateShow: any;
  public empCode: string;
  public empThaiName: string;
  public status: number;
  empTypeCode: string;
  employTypeCode: string;
  public departmentIdStart: number;
  public departmentCodeStart: string;
  public departmentIdEnd: number;
  public departmentCodeEnd: string;
  public employeeCodeStart: string;
  public employeeCodeEnd: string;
}
export class Sitadt04SaveClock {
  public clockId: number;
  public ouCode: string;
  public empId: number;
  public date: number;
  public timeIn: string;
  public timeOut: string;
  public calculateTimeIn: string;
  public calculateTimeOut: string;
  public obligationsHolidayDate: any;
  public obligationsHolidayDateShow: any;
  public empCode: string;
  public empThaiName: string;
  public clockDate: string;
  public fileName: string;
  public status: number;
  public flagForPaging: number = 0;


}

