import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { LoadMask } from '../../load-mask.component';
import { Sitadt04Service} from './sitadt04.service';
import { BeanUtils } from '../../core/bean-utils.service';

@Injectable()
export class Sitadt04Resolve implements Resolve<any> {

  constructor(private selectService: CommonSelectService, private sitadt04Service: Sitadt04Service) {
    this.selectService.setModuleName('ta');
  }

  resolve(route: ActivatedRouteSnapshot): any {
    LoadMask.show();
    if (route.url[0].path == 'sitadt04') {
      var department = this.selectService.getDepartment();
      var employee = this.selectService.getEmployee();

       return Observable.forkJoin([department, employee]).map((response) => {
      LoadMask.hide();
      return {
            department: response[0],
            employee: response[1],
      };
      }).first();
    } else if (route.url[0].path == 'sitadt04a') {

      if (BeanUtils.isNotEmpty(route.params['clockId'])) {
        let searchData = this.sitadt04Service.searchData(route.params['clockId']);
        let employee = this.selectService.getEmployee();
        let ouCode = this.selectService.getOu();


        return Observable.forkJoin([searchData, employee ,ouCode]).map((response) => {
          LoadMask.hide();
          return {
            searchData: response[0],
            employee: response[1],
            ouCode: response[2]
          };

        }).first();
      }
      else {
        let employee = this.selectService.getEmployee();
        let ouCode = this.selectService.getOu();

        return Observable.forkJoin([employee, ouCode]).map((response) => {
          LoadMask.hide();
          return {
            employee: response[0],
            ouCode: response[1],
          };
        }).first();
      }
    }
  }
}



