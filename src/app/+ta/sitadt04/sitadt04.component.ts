import { Component, OnInit, ViewChild, OnChanges, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitadt04Service } from './sitadt04.service';
import { Sitadt04Criteria } from '../sitadt04/sitadt04.interface';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { BeanUtils } from '../../core/bean-utils.service';
import { SearchStatus } from '../../shared/constant/common.interface';
import { PageContext } from '../../shared/service/pageContext.service';
import { SSDatePicker } from '../../shared/ss-datepicker.component';
import { DateUtils } from '../../core/date-utils.service';
import { FileUploader, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { DepartmentSelectDirective } from '../shared/department-select.directive';
import { RestServerService } from '../../core/rest-server.service';

@Component({
  selector: 'sitadt04',
  template: require('./sitadt04.component.html'),
  providers: [Page, Sitadt04Service]
})
export class Sitadt04Component {
  // implements OnInit
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('date') public date: SSDatePicker;
  @ViewChild('startDate') public startDate: SSDatePicker;
  @ViewChild('endDate') public endDate: SSDatePicker;
  @ViewChild('searchModal') public searchModal: ModalDirective;
  @ViewChild('departmentStart') private departmentStart: DepartmentSelectDirective;
  @ViewChild('departmentEnd') private departmentEnd: DepartmentSelectDirective;
  @ViewChild('searchModal2') public searchModal2: ModalDirective;
  @Input('info') private info: any;
  // @Input('clockId') private clockId: any;
  // @Input('module') private module: any;

  private form: FormGroup = this.formBuilder.group({

    ouCode: this.formBuilder.control("")
    , date: this.formBuilder.control("")
    , startDate: this.formBuilder.control("")
    , endDate: this.formBuilder.control("")

  });



  private initMsg: string;
  private showMsg = false;
  private records: any[];
  private fromLoadObject: any;
  private objCriteria: Sitadt04Criteria = new Sitadt04Criteria();
  private alert = { title: '', msg: '', isRedirect: false };
  public uploader: FileUploader = new FileUploader({});
  private flagForLoad = 0;
  public empId:any;
  private module = 'ta';
  // private imageEmployee: any;

  private checkDelete: number = 0;
  // private clockId: any;



  constructor(private page: Page
    , private service: Sitadt04Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private formBuilder: FormBuilder
    , private pageContext: PageContext
    , private restServer: RestServerService
  ) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  private onActive() {

  }

  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (this.objCriteria.date) {
        this.date.setValue(this.objCriteria.date);
      }
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
    // this.uploader = new FileUploader({
    //   url: this.restServer.getAPI(this.module + '/common/uploadFile'),
    //   method: "POST",
    //   autoUpload: true,
    //   // allowedMimeType: ['image/jpeg'],
    //   maxFileSize: 1024 * 1024 * 10 //10 MB
    // });
    // this.uploader.onAfterAddingAll = (fileItems: any) => {
    //   LoadMask.show();

    // }
    // this.uploader.onCompleteAll = () => {
    //   this.listFile();
    //   LoadMask.hide();
    //   this.alertModalToggle('สำเร็จ', 'อัพโหลดไฟล์สำเร็จ', true);
    //   $(".modal-backdrop").remove();
    // }
    // this.uploader.onWhenAddingFileFailed = (fileItem) => {
    //   if (fileItem.size > 1024 * 1024 * 10) {
    //     this.alertModalToggle('ผิดพลาด', 'ขนาดไฟล์เกิน 10 MB', false);
    //     $(".modal-backdrop").remove();
    //   }
    // }
    this.onUpload()
    // this.readURL(Input)
  }

  public childModalShow(record: any) {
    this.pageContext.nextPage(['/ta/sitadt04a', record.clockId], this.objCriteria);
  }

  public createClock() {
    let clockId: any = "";
    this.pageContext.nextPage(['/ta/sitadt04a', clockId], this.objCriteria);
  }

  public onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.objCriteria.date = this.date.getMilliSecond();
    this.showMsg = false;
    this.callServiceSearch(changeEvent, this.objCriteria);
  }
  private alertModalToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.alertModal.toggle();

  }
  private callServiceSearch(page: Page, mapping: Sitadt04Criteria) {
    this.service.getData(page, mapping).subscribe(response => {
      this.page = response.page;
      this.records = response.records;
      if (BeanUtils.isNotEmpty(this.records)) {
        LoadMask.hide();
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล', false);
      }
    },
      error => {
        console.error(error);
        LoadMask.hide();
      }
    );
  }

  public onChangePage(changeEvent: Page) {
    this.page = changeEvent;
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private onSearch(modeSearch: number) {
    if (this.form.valid == true) {
      if (BeanUtils.isNotEmpty(modeSearch)) {
        this.objCriteria.flagForPaging = modeSearch
        this.page.start = 0;
        if (modeSearch == SearchStatus.normalSearch) {
          this.onNormalSearch(this.page);
        }
      }
    }
  }

  private onAdvSearch(page: Page) {

    // this.pageContext.setLastSearch('onAdvSearch',this.page);
    this.pageContext.setLastSearch('onAdvSearch', [page]);
    console.log('adv search');
    LoadMask.show();
    this.service.getDataAdvSearch(page, this.objCriteria.ouCode, this.objCriteria.empTypeCode, this.objCriteria.employTypeCode)
      .subscribe(response => {
        this.page = response.page;
        this.records = response.records;
        LoadMask.hide();
      },
      error => {
        console.error(error);
        LoadMask.hide();
      }
      );
    this.searchModal.hide();
  }

  public onUpload() {
    this.uploader = new FileUploader({
      url: this.restServer.getAPI(this.module + '/sitadt04/uploadFile') + '?module=' + this.module,
      method: "POST",
      autoUpload: true,
      // allowedMimeType: ['image/jpeg'],
      maxFileSize: 1024 * 1024 * 10 //10 MB
    });
    this.uploader.onAfterAddingAll = (fileItems: any) => {
      LoadMask.show();

    }
    this.uploader.onCompleteAll = () => {
      this.listFile();
      LoadMask.hide();
      this.alertModalToggle('สำเร็จ', 'อัพโหลดไฟล์สำเร็จ', true);
      $(".modal-backdrop").remove();
    }
    this.uploader.onWhenAddingFileFailed = (fileItem) => {
      if (fileItem.size > 1024 * 1024 * 10) {
        this.alertModalToggle('ผิดพลาด', 'ขนาดไฟล์เกิน 10 MB', false);
        $(".modal-backdrop").remove();
      }
    }
  }


  public listFile() {
    LoadMask.show();
    this.service.listFile(this.module)
      .subscribe(response => {
        this.records = response.data;
        LoadMask.hide();
      }, error => {
        console.error(error);
        LoadMask.hide();
      }
      );
  }

  private analyzeData(page: Page) {

    this.pageContext.setLastSearch('analyzeData', [this.page]);

    LoadMask.show();
    this.service.getAnalyzeData(this.page, this.objCriteria)
      .subscribe(response => {
        this.page = response.page;
        this.records = response.records;

        this.alert.isRedirect = (this.records.length == 0) ? true : false;

        LoadMask.hide();
      },
      error => {
        console.error(error);
        LoadMask.hide();
      }
      );

    this.searchModal2.hide();
  }
  private changeDate(valueField: any) {
    if (valueField) {
      this.form.controls["date"].setValue(this.date.getMilliSecond());
      this.objCriteria.obligationsHolidayDate = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsHolidayDateShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
    }
  }

  private changeStartDate(valueField: any) {
    if (valueField) {
      this.form.controls["date"].setValue(this.date.getMilliSecond());
      this.objCriteria.obligationsHolidayDate = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsHolidayDateShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
    }
  }

  private changeEndDate(valueField: any) {
    if (valueField) {
      this.form.controls["date"].setValue(this.date.getMilliSecond());
      this.objCriteria.obligationsHolidayDate = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsHolidayDateShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
    }
  }

  public closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }
  public changeFile() {
    this.form.controls["empCode"].markAsDirty();
  }

  private changeStartDepartment(valueField: any) {
    this.objCriteria.departmentIdStart = valueField.departmentId;
    this.objCriteria.departmentCodeStart = valueField.departmentCode;
    // this.loadEmployee();
  }

  private changeEndDepartment(valueField: any) {
    this.objCriteria.departmentIdEnd = valueField.departmentId;
    this.objCriteria.departmentCodeEnd = valueField.departmentCode;
    // this.loadEmployee();
  }

  private changeStartEmployee(valueField: any) {
    this.objCriteria.employeeCodeStart = valueField.empCode;
  }

  private changeEndEmployee(valueField: any) {
    this.objCriteria.employeeCodeEnd = valueField.empCode;
  }
  private readURL(input: any): void {
    if (input.files && input.files[0]) {

        if (input.files[0].size > 1024 * 1024) {
          var reader = new FileReader();

          reader.onload = function (e) {
            $('#img-upload').attr('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADLCAYAAAArzNwwAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAADylJREFUeNrsnb1y20gWhRvwuso1ienM2VCRNxsqm0xUttGYfAJR2WYys8kkPYHIJxAZTW0karKNTGWTGcrWkaFM2XKSqZlysNtXurBhivjvBvrnnCoUbYmkgO7++tzb3WgIAUEQBEEQBClWgCJoR98NfurJlwH/t89HWi9Tv090s+OrYj5I0R/RrxuULgCxBYKk4Q/l8X0KhL7mPx3JY8Ovd/wKeABI544w5F7/gF97hp1mAg050RrQAJA2gHjLr31LLyViWG4kLCvULABpAsWAYTjakR+4ohU7zEoCE6PWAUgZKAiIkcUu0cRdloAFgOwKnyaOO0VVrVOwbACIn2Ak4dMEPOQm+hSGzSUoEQDxwy0ofDr1MIRS4ioSlAUAcROMd/I4EeYNx9omyk/OfQi/AoABNQy/5vKYuQpKADAgVaBISM4AiB1wnAGM7kIvl3KUwDEwKPm+QPJtRDJPoKwBiBlgEBCX4nHWGzJH5CRTm/OT0JFw6gPgMFITeXySdTSBg7QPxoBdAzPf9oRdx7YtYXlmsWv8Io/XaHfWiMLgyfPXb/76fP/xNziIvlzjCq5hvVbsJsbnJqFFcEw41wAc9mvEuYnxeeMzS+CgXIPCqhdoW87oBYdcgQy51gixEFJBFoZcgcFwEBTvBWbDfVHEkBi1pD40FI4k3wAc/uihQzQtLwkNhIOWilyivXipHkMyQZKenYz/E+3Ee41MSd4DQ8DosWuM0DaglBYyJzn22kEYDkrGh2gP0HZeIp2kL53k2ktAUnBgGBcyEpJngAMCJIYBAjggWyDpapgXcEB1NOGRTncdhC/wH6hrqIGT9KST/Ns5QBiOCeoYaqgfJSR3EhLty1LCFuF4BzgghbrkTTq0KmgJDrqQK9QppFi0+vdQ5wLHoAU4sCoX0qlYHvu6lsqHmuHosXMADkiX+jqjE905yKXAJm6Qfg15FbhyaRvF4p1HsDIXaks0snX7+f7jf4zPQfiml/eoM6iDpH1f5d5boQY4kqXrENS2lLc9HTkI8o78Hm4mj7E8DpND9njk5K/4/1PxuKdtjOKqnY+cGRliYb4jU9TYKz8WgHd1oXD1QNj9bPYutK9ifiRQCAfZ2yeBId1tx5iqel5G6rntJ4ClUJEs9/2mX6JsFOv56ze0Vy5W6H7VQyglK0nZPrSf7z/e07628pjL8qZl33+Kx/2J0Sk91WsV97UrcRCMWj1xjXGbD49hZ6HHWY/gLE+012RUK1BQOdR7fUDFPGjNcHS2QyBvmXMkcI//lzqR9XHYWYglbexngd1ISLQDx1ha+p9dngQtAZfHkpaDc8jre/jVbzKB2MhBeJQFOyAasD1Njrufisen/fosCrFqLWhsOg9yCjjMhINEDUIeNK9yyLmRty5St5Oo7SBIzM2GIyOR9/mRdRtO2Ct1FE0c5NRzOGgSamrLyfKk2SGft4+iSKfyit9aDgL30H8nm2Yn8fkGtkrDvnUdxHf3mNsIR8pJxh7XXaW2W9lB4B4ilo1sz/aL4BuMfB3dKu0ioW4CHdS5Q9fh68hW6TZcyUHgHm64R6o+zzzu8F6VGdGq6iBHnrvHyrHrmXlcl6XCy9KA8Kz5xHNAli5dDPegK0/r8kR1DnLiORwbW0euCnTtaX32yjwLsQogvruHqxNsa4/r9EQJIEya72uuYhcvioc7fR3NGvDEaWMH8T05J93BHf1zkUJAUhsHQJCLGjV1ENwM5b5uPL72Xt5jFMoAcoL2Azmuo1qAcALTR/lBrodZfPdlZQdBeAX5omEdQN6i3LzQDyiC3W09zAmvKLTCRnBfdeByoorq3R0thVUtBwIgrpYBr1YvDQjCq2/Vd/jaEClkmAIcxHNAipZa+J6HhDmFBtvdnZfBPRx20u3h3hDu4b2LYAQrp+1nAXKAcvKmt0VnmFPHcJBq6gF653WQCwjH2cg/ditGESDEQo+yWwtVj1IzTDNU7ROTGACQ6po6el3nqNon6ucBglGNp1p3+dQonXL1ulTlZXAQCMpJ1MM8e4G+CIMWntZ3mJWcQH64qqOrA7SFWOgpsxuSq5AAkJyOYxuQIYrGOxdBned0HCHKoXrihuvyFxAM8frX08JBKgCCHCSnwFxLaPP2g4J2J+lQvi4cux7seVYQeoaeJKKqNCqzZb4l7nGG8Kq6gyDE8sBF+K65U1QlQiwd6jmQiyBSACBoYDlCaFWyjABIgwQO5w8HgRzrgTn/gIMAEL0hlsV5COAAIGhoOcLj9ABIK7Juko3DK8yeAxCEWRl6h2oDIG3Kmsk2dg8sLQEgrSrz0V0GaiKwUqKKNgCkuXo2hC1YWlJLEQBRlKxbkIu8g3uoCbFiFEktFzG2d2Z44R4ApNv4ftfjuwzRJaoHOYgRDdG0hF2eD4VWQ1RNLd3CQdSKQpkLg+AYCPfugOw8xLpDkTQOtSYGwEFOdoXqaKSdo1jYyLi5LrrcZI7heC+wIZyWHCRCuTTWQwPtcOiXwircMajJQZCDqIPkqu2kXf49GrGaoPibK3ksRLj1QwCiToO2nIRAlMd7wKHWPXY5CMIs9ZB80JmTMIAExxDFrUxxHiBwET05yUQDHHRvxwfkHMp1mwfILcqntjY5kNBEopK8hFyDvks8DuVijVXLIdYa5VMbjkN57OeAQj3+J9rVsA4onGucsWvk3Rm4EBiyVwJIsKsS5Mt/UUbV4fgj+jXiMhxw794v+Aw15GXyuYJQ6m2JJJy+81h+34rP4RLhV/W6lOX3KhMQrpBPAhNNteDY6miuSibPMfda6fD2JTfussk3fX6cHonkc7gUuA+9ilayDMdFgGA8vZzW3GPHOb0/hUS6l5ufy3M4yzmHIZ/DEFVWrSyzVvPeoJxKhTKHRXNHXNh7mnI7+s69PDj4HOh91Cueo+pKlakocpABJ4LQUzDm8pglM60Vk2xVPfmKzoMbftVz6PM5IELY3ZkEhYAgD1ELRkYHdMS5QdkyphxjyTFyrOAcElDoHDBUzO5BUUFZQJCHPCbPS1Vg5MAyYFC+59dNKmEnl4h0/X0+B6rnt0jmxVSW86wsIBPh7+2a1Chp+HXh00Wndl488jSh398ejQwKCsun+ZA4FdvHwnNxCDZhWHwItWNZ73vbPwwKCsn1RXAbhuKaJtcQgudGE667ykK2gePtH/6t4EPXDhYKoKgoDjUXnC+dOJqbXu/6YZGDkLV+cqQAVpxXAAp1ucqJcGMpyzfLS0oDwoVh+3LqFY9OxGjaWmChCMP2Wfqd4RWpzL5Yc4uTbprpHgMOreFXMncwFfauIF5m/aIMIDaGJHTO+3VmmqHaoND8AS31t+2O1DivnYQlLjxZlm2TXY51TqxBmW3lwbUt61SXeb8su/XotS3OkRVLQq1B8rCQ0yInWTQGhEd+TI/jk4qBzIHEhg41VuEgNiTr5wirjIIksiA0Xxa9oQogC2HuKEW8vcgMMkJTw9vMShkg3DubmnzhRiBzQ62FzW0mdKAhxr6turVMJobmpTv7sGKPEBvYIyzRBo12ERPbzLxsvlrnCVOmuQjcw3yZ1IkRGKXz1dDyHmGFZSRWuMhamDNNMK8y2ln3GYWmuMg1mp81MmGAp5J71AbEEBfZIDlHmKXTPZo4iAkuskabsyrMijoOs+Kq7tEIEHaRLiFBeIUwq1KHXmelRdPnpM9EdzOluDPQPnXVqUV1w/FGgDCR044uGOuu7AuzugqLa7fRUMFFLzrIB5B/IMwqq0UTMMOuCa0pbK5tr9qsu8YRjhJAeISizYQdDmKv2ryR6rhpKB6oPJuWdkCh/GMf7cxeyXbyvzZCufSDcLoOsb4QC/eADKhDZXc0KgWkpVALT+FFmKU9tNLlIMkTlSKLCxfSL52d3ELl7pmhppMcC00TiEVPhIW8dpBYKB5RDTU14lhTPoL8wwFp7OSU74cWaiwEsrmZhh4CQqKelXcoBy/UWQLyhKeKC+IO7coZqezsFrpufQhbKIixwsJAiOWOVHV2kc7dNLUDwjGhqqQdCxQRYm270KHOk2zDQZKk7FjR90BuaKPg89o3KQ/bKg1O2ptAAjgckoLObtxGhxm2XCiUSNWdaUd4hUQ90XFb95aEbZcIz7TXGXGAgwCQBI5FWycYdlEqPOpQ9SJ/R3vyHpDjtneyCbsqmRqQwEHc053JcHQKSA1IkIO4p43JcHQOSEVIAIh7ikyGwwhAUpDMCt6DEMs/HXe9e2ZoSknwuq3MeZLvBj9N0F6c01FOtDA2YWvZwLQSYxAu5NHb8WsqsCn2xLJbso6pbq/kMcyA49CUiCEwtABp44f3GZBEQtPSZqiVuiUoLuXRz6jbsUmPtAhNLERu/HsZSdwDPAi5rITjjDu+XXCs2Dlik845MLxAexxuZcGwEgpv0Ie01WOfXWOY8ZZzXmFhnAJLCjgvL9kwJNjM2sy6eydfTnPqbtzhnr1uAJLKSy5F9sZ0K07gYzRLY+rrIsc11tyxGV1fz2wp8M/3H++fv37zL/nPF/L4ccdb/k6hmHzPX/K9v6GJdhcWyzr4Wf7zl4xcIwmpjmU9GR8aB5ZWwojdpJfxlojdZI0m23oofJoDhnUjkIHFldFjSEY5b7PCxh0AY8hgDHPeZmwi7iQgW5VzmdNrkRZcQQBFbdn3Rf7olJWu4RQgKTdJRksEQGkFDCrrSc7bNlzWM5uvNXCw4i4Kwi6AojeUIs1EzYdmAhCzKpJylDnmUEol3yei+NkvzuV8geMVO2JH6Re8lSqUHnS/gKt848YnHEb1SoBx7uKoYeBJZU9E/vBjWuQm9LjilW9LWDiXG5V0C6fB8AqQGqFXkmQSLDcuw5KC4m2J3C3dicx9mGfyCpBUoxikwgdRoVEksMQOXD91Ekei/DMlkw7Dq8ENLwHZ6j2TBLRf4aMxhxcETGT6GH8KiAN+7VX4OF3b3MeQ03tAdoRfRxxm9Cp+fMMN6YZf466g4esg2H9gdxjW+JqY3WLp+41pAGR3I0vH5L0GXxWl4Pk99X9RJ35nx0tCoj4fL/ln/YoumBVCXWPYG4BUheWAYelr/nOx+Lrb4KAhnGUBXsMpAIgqWPpbsXzfskuIUqHgCndiApA2gBnwcdBSr1/FjQiGW3aJCEAAEBOg6aWg6TE4ukKmTSqvuU1CNNwHA0Bsh2i49aM8eBIIvggAQBAEQRAEQRbr/wIMAB2nwEL3kwxHAAAAAElFTkSuQmCC');
          }

          reader.readAsDataURL(input.files[0]);
        } else {
          var reader = new FileReader();

          reader.onload = function (e) {
            $('#img-upload').attr('src', event.target["result"]);
          }

          reader.readAsDataURL(input.files[0]);
        }
        $("#imgInp").change(function () {
          this.readURL(this);
        });
      }
    }
  }
// }

