import { AuthHttp } from 'angular2-jwt';
import { BeanUtils } from '../../core/bean-utils.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { Sitadt05Criteria} from '../sitadt05/sitadt05.interface';

const querystring = require('querystring');

@Injectable()
export class Sitadt05Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

  //searchShiftEmp
  public getData(page: Page, searchValue: Sitadt05Criteria): Observable<any> {
    let params = '?' + querystring.stringify(page) + "&" + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('/ta/sitadt05/searchDaily') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });

  }

  // //processShiftEmp
  // public processShiftEmp(page: Page, params: Sitadt05SaveShiftEmp): Observable<any> {
  //   return this.authHttp.post(this.restServer.getAPI('/ta/sitadt05/processShiftEmp'), params)
  //     .map(val => {
  //       return {
  //         records: val.json()
  //       }
  //     });
  // }

  // //saveShiftEmp
  // public saveShiftEmp(page: Page, params: Sitart08SaveShiftEmp): Observable<any> {
  //   return this.authHttp.post(this.restServer.getAPI('/ta/sitadt05/saveShiftEmp'), params)
  //     .map(val => {
  //       return {
  //         page: {
  //           total: 100,
  //           start: page.start,
  //           limit: page.limit
  //         }
  //         , records: val.json()
  //       }
  //     });
  // }

  //selectShiftEmp
  public searchData(dailyId: number): Observable<any> {
    let params = '?dailyId=' + dailyId;
    return this.authHttp.get(this.restServer.getAPI('/ta/sitadt05/selectDaily') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  // //deleteShiftEmp
  // public deleteShiftEmp(page: Page, params: Sitart08SaveShiftEmp): Observable<any> {
  //   return this.authHttp.post(this.restServer.getAPI('/ta/sitadt05/deleteShiftEmp'), params)
  //     .map(val => {
  //       return {
  //         page: {
  //           total: 100,
  //           start: page.start,
  //           limit: page.limit
  //         }
  //         , records: val.json()
  //       }
  //     });
  // }

  public loadShift(shiftCode: string): Observable<any> {
    let params = '?shiftCode=' + shiftCode;
    return this.authHttp.get(this.restServer.getAPI('ta/sitadt05/loadShift') + params)
      .map(val => { return val.json() });
  }
}
