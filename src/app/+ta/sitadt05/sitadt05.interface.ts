export class Sitadt05Criteria {
  public inputSearch: string = '';
  public ouCode: string = '';
  public empId: number;
  public empCode: string = '';
  public empThaiName: string = '';
  public shiftId: number;
  public shiftCode: string = '';
  public shiftIn: string = '';
  public shiftOut: string = '';
  public startDate: number;
  public endDate: number;
  public departmentIdStart: number;
  public departmentIdEnd: number;
  public departmentCodeStart: string = '';
  public departmentCodeEnd: string = '';
  public employeeCodeStart: string = '';
  public employeeCodeEnd: string = '';
  public shiftGroup: string = '';
  public flagForPaging: number = 0;
  public dailyId: number;
  public timeIn: string = '';
  public timeOut: string = '';
}

// export class Sitadt05SaveShiftEmp {
//   public shiftEmpId: number;
//   public ouCode: string = '';
//   public empId: number;
//   public empCode: string = '';
//   public empThaiName: string = '';
//   public shiftId: number;
//   public shiftCode: string = '';
//   public shiftIn: string = '';
//   public shiftOut: string = '';
//   public startDate: number;
//   public endDate: number;
//   public obligationsStartDateA: any = '';
//   public obligationsStartDateShowA: any = '';
//   public obligationsEndDateA: any = '';
//   public obligationsEndDateShowA: any = '';
//   public shiftGroupA: string = '';
//   public employeeAdd: string = '';
//   public flagForPaging: number = 0;
//   public grid: Sitadt05ShiftEmp[];
// }

// export class Sitadt05ShiftEmp {
//   public shiftEmpId: number;
//   public ouCode: string = '';
//   public empId: number;
//   public empCode: string = '';
//   public empThaiName: string = '';
//   public shiftId: number;
//   public shiftCode: string = '';
//   public shiftIn: string = '';
//   public shiftOut: string = '';
//   public startDate: Date;
//   public endDate: Date;
//   public departmentIdStart: number;
//   public departmentIdEnd: number;
//   public employeeCodeStart: string = '';
//   public employeeCodeEnd: string = '';
//   public shiftGroup: string = '';
// }
