import { BeanUtils } from '../../core/bean-utils.service';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { Injectable } from '@angular/core';
import { LoadMask } from '../../load-mask.component';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Sitadt05Service } from './sitadt05.service';

@Injectable()
export class Sitadt05Resolve implements Resolve<any> {

  constructor(private selectService: CommonSelectService, private sitadt05Service: Sitadt05Service) {
    this.selectService.setModuleName('ta');
  }

  resolve(route: ActivatedRouteSnapshot): any {
    LoadMask.show();
    if (route.url[0].path == 'sitadt05') {
      var department = this.selectService.getDepartment();
      var employee = this.selectService.getEmployee();
      var shift = this.selectService.getShift();
      return Observable.forkJoin([department, employee, shift]).map((response) => {

        LoadMask.hide();
        return {
          department: response[0],
          employee: response[1],
          shift: response[2]
        };
      }).first();
    }
  }
}
