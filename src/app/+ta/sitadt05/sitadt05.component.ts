import { BeanUtils } from '../../core/bean-utils.service';
import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { DepartmentSelectDirective } from '../shared/department-select.directive';
import { EmployeeSelectDirective } from '../shared/employee-select.directive';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchStatus } from '../../shared/constant/common.interface';
import { ShiftSelectDirective } from '../shared/shift-select.directive';
import { Sitadt05Service } from './sitadt05.service';
import { Sitadt05Criteria } from '../sitadt05/sitadt05.interface';

@Component({
  selector: 'sitadt05',
  template: require('./sitadt05.component.html'),
  providers: [Page, Sitadt05Service, Sitadt05Criteria]
})

export class Sitadt05Component implements OnInit {

  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('departmentStart') public departmentStart: DepartmentSelectDirective;
  @ViewChild('departmentEnd') public departmentEnd: DepartmentSelectDirective;
  @ViewChild('employeeStart') public employeeStart: EmployeeSelectDirective;
  @ViewChild('employeeEnd') public employeeEnd: EmployeeSelectDirective;
  @ViewChild('shiftGroup') public shiftGroup: ShiftSelectDirective;

  private form: FormGroup = this.fb.group({
    dailyId: this.fb.control(null)
    , ouCode: this.fb.control("")
    , empCode: this.fb.control("")
    , empThaiName: this.fb.control("")
    , shiftCode: this.fb.control("")
    , shiftIn: this.fb.control("")
    , shiftOut: this.fb.control("")
    , timeIn: this.fb.control("")
    , timeOut: this.fb.control("")
    , leaveTypeCode: this.fb.control("")
    , leaveTypeDesc: this.fb.control("")

  });

  private dailyId: any;
  private alert = { title: '', msg: '' };
  private fromLoadObject: any;
  private initMsg: string;
  private objCriteria: Sitadt05Criteria = new Sitadt05Criteria();
  private showMsg = false;
  private records: any[];
  private shows: any[] = [];

  constructor(private fb: FormBuilder
    , private page: Page
    , private pageContext: PageContext
    , private service: Sitadt05Service
    , private router: Router
    , private routerActive: ActivatedRoute) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
  }

  public childModalShow(record: any) {
       this.service.searchData( record.dailyId)
      .subscribe(response => {
        // this.page = response.page;
        // this.records = response.records;
        if (BeanUtils.isNotEmpty(this.records)) {
          // this.records
          this.form.controls['shiftIn'].setValue(response.records.data.shiftIn);
          this.form.controls['shiftOut'].setValue(response.records.data.shiftOut);
          this.form.controls['timeIn'].setValue(response.records.data.timeIn);
          this.form.controls['timeOut'].setValue(response.records.data.timeOut);
          this.form.controls['leaveTypeCode'].setValue(response.records.data.leaveTypeCode);
          this.form.controls['leaveTypeDesc'].setValue(response.records.data.leaveTypeDesc);

          let row: {};
            row = {
              leaveTypeCode: this.form.controls["leaveTypeCode"].value,
              leaveTypeDesc: this.form.controls["leaveTypeDesc"].value,
            };
            this.shows.push(row);

        } else {
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
        }
      },
      error => {
        console.error(error);
        LoadMask.hide();
      }
       );


  }
  private changeStartDepartment(valueField: any) {
    this.objCriteria.departmentIdStart = valueField.departmentId;
    this.objCriteria.departmentCodeStart = valueField.departmentCode;
    this.loadEmployee();
  }

  private changeEndDepartment(valueField: any) {
    this.objCriteria.departmentIdEnd = valueField.departmentId;
    this.objCriteria.departmentCodeEnd = valueField.departmentCode;
    this.loadEmployee();
  }

  private changeStartEmployee(valueField: any) {
    this.objCriteria.employeeCodeStart = valueField.empCode;
  }

  private changeEndEmployee(valueField: any) {
    this.objCriteria.employeeCodeEnd = valueField.empCode;
  }

  // About Search
  public onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.showMsg = false;
    this.callServiceSearch(changeEvent, this.objCriteria);
  }

  private callServiceSearch(page: Page, mapping: Sitadt05Criteria) {
    this.service.getData(page, mapping)
      .subscribe(response => {
        this.page = response.page;
        this.records = response.records;
        if (BeanUtils.isNotEmpty(this.records)) {
          LoadMask.hide();
        } else {
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
        }
      },
      error => {
        console.error(error);
        LoadMask.hide();
      }
      );
  }

  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  public onChangePage(changeEvent: Page) {
    this.page = changeEvent;
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private onSearch(modeSearch: number) {
    if (this.form.valid == true) {
      if (BeanUtils.isNotEmpty(modeSearch)) {
        this.objCriteria.flagForPaging = modeSearch
        this.page.start = 0;
        if (modeSearch == SearchStatus.normalSearch) {
          this.onNormalSearch(this.page);
        }
      }
    }
  }

  public closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }

  private loadEmployee() {
    let params = {
      departmentIdStart: this.objCriteria.departmentIdStart || '',
      departmentIdEnd: this.objCriteria.departmentIdEnd || '',
      departmentCodeStart: this.objCriteria.departmentCodeStart || '',
      departmentCodeEnd: this.objCriteria.departmentCodeEnd || ''
    }
    this.employeeStart.load(params);
    this.employeeEnd.load(params);
  }
}
