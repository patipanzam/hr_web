export class Sitart07Criteria {
  public inputSearch: string = '';
  public ouCode: string = '';
	public shiftDepId: number;
  public departmentCode: string = '';
  public departmentId: number;
	public departmentNameLocal: string = '';
  public shiftCode: string = '';
  public shiftIn: string = '';
  public shiftOut: string = '';
  public departmentCodeStart: string = '';
  public departmentCodeEnd: string = '';
  public shiftGroup: string = '';
  public flagForPaging: number = 0;
}

export class Sitart07SaveShiftDepartment {
  public shiftDepId: number;
  public ouCode: string = '';
  public shiftId: number ;
	public departmentNameLocal: string = '';
  public shiftCode: string = '';
  public shiftIn: string = '';
  public shiftOut: string = '';
  public flagForPaging: number = 0;
  public departmentCode: string = '';
  public departmentId: number ;
  public shiftGroup: string = '';
  public grid: ShiftDepartment[];
}

  export class ShiftDepartment {
  public shiftDepId: number;
  public ouCode: string;
  public shiftId: number ;
	public departmentNameLocal: string;
  public shiftCode: string;
  public shiftIn: string;
  public shiftOut: string;
  public departmentCode: string = '';
  public departmentId: number ;
}


