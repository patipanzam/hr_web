import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { LoadMask } from '../../load-mask.component';
import { Sitart07Service } from './sitart07.service';
import { BeanUtils } from '../../core/bean-utils.service';

@Injectable()
export class Sitart07Resolve implements Resolve<any> {

  constructor(private selectService: CommonSelectService, private sitart07Service: Sitart07Service) {
    this.selectService.setModuleName('ta');
  }

  resolve(route: ActivatedRouteSnapshot): any {
    LoadMask.show();
    if (route.url[0].path == 'sitart07') {
      var ouCode = this.selectService.getOu();
      var department = this.selectService.getDepartment();
      var shift = this.selectService.getShift();
      return Observable.forkJoin([ouCode, department, shift]).map((response) => {

        LoadMask.hide();
        return {
          ouCode: response[0],
          department: response[1],
          shift: response[3]
        };
      }).first();
    } else if (route.url[0].path == 'sitart07a') {

      if (BeanUtils.isNotEmpty(route.params['shiftDepId'])) {
        var searchData = this.sitart07Service.searchData(route.params['shiftDepId']);
        var ouCode = this.selectService.getOu();
        var department = this.selectService.getDepartment();
        var shift = this.selectService.getShift();
        return Observable.forkJoin([searchData, ouCode, department, shift]).map((response) => {
          LoadMask.hide();
          return {
            searchData: response[0],
            ouCode: response[1],
            department: response[2],
            shift: response[3]
          };
        }).first();
      } else {
        var ouCode = this.selectService.getOu();
        var department = this.selectService.getDepartment();
        var shift = this.selectService.getShift();
        return Observable.forkJoin([ouCode, department, shift]).map((response) => {
          LoadMask.hide();
          return {
            ouCode: response[0],
            department: response[1],
            shift: response[2]
          };
        }).first();
      }
    }
  }
}
