import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitart07Service } from './sitart07.service';
import { Sitart07Criteria, Sitart07SaveShiftDepartment } from '../sitart07/sitart07.interface';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { DateUtils } from '../../core/date-utils.service';
import { LoadMask } from '../../load-mask.component';
import { AppLayoutComponent } from '../../shared/layouts/app-layout.component';
import { BeanUtils } from '../../core/bean-utils.service';
import { SearchStatus } from '../../shared/constant/common.interface';
import { PageContext } from '../../shared/service/pageContext.service';
import { DepartmentSelectDirective } from '../shared/department-select.directive';

@Component({
  selector: 'sitart07',
  template: require('./sitart07.component.html'),
  providers: [Page, Sitart07Service]
})
export class Sitart07Component {

  private form: FormGroup = this.formBuilder.group({
    ouCode: this.formBuilder.control("")
  });

  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('departmentStart') public departmentStart: DepartmentSelectDirective;
  @ViewChild('departmentEnd') public departmentEnd: DepartmentSelectDirective;

  private shiftDepId: any;
  private initMsg: string;
  private showMsg = false;
  private records: any[];
  private fromLoadObject: any;
  private objCriteria: Sitart07Criteria = new Sitart07Criteria();
  private objDelete: Sitart07SaveShiftDepartment = new Sitart07SaveShiftDepartment();
  private alert = { title: '', msg: '' };
  private checkDelete: number = 0;

  constructor(private page: Page
    , private service: Sitart07Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private formBuilder: FormBuilder
    , private pageContext: PageContext) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
  }

  public childModalShow(record: any) {
    this.pageContext.nextPage(['/ta/sitart07a', record.shiftDepId], this.objCriteria);
  }

  public createShiftDepartment() {
    let shiftDepId: any = "";
    this.pageContext.nextPage(['/ta/sitart07a', shiftDepId], this.objCriteria);
  }

  private changeStartDepartment(valueField: any) {
    this.objCriteria.departmentCodeStart = valueField.departmentCode;
  }

  private changeEndDepartment(valueField: any) {
    this.objCriteria.departmentCodeEnd = valueField.departmentCode;
  }

  public onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.showMsg = false;
    this.callServiceSearch(changeEvent, this.objCriteria);
  }

  private callServiceSearch(page: Page, mapping: Sitart07Criteria) {
    this.service.getData(page, mapping)
      .subscribe(response => {
        this.page = response.page;
        this.records = response.records;
        if (BeanUtils.isNotEmpty(this.records)) {
          LoadMask.hide();
        } else {
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
        }
      },
      error => {
        console.error(error);
        LoadMask.hide();
      }
      );
  }

  private alertModalToggle(title: string, msg: string): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  public onChangePage(changeEvent: Page) {
    this.page = changeEvent;
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private onSearch(modeSearch: number) {
    if (this.form.valid == true) {
      if (BeanUtils.isNotEmpty(modeSearch)) {
        this.objCriteria.flagForPaging = modeSearch
        this.page.start = 0;
        if (modeSearch == SearchStatus.normalSearch) {
          this.onNormalSearch(this.page);
        }
      }
    }
  }

  public closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }
}
