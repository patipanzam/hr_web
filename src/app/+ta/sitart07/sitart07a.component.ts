import { LocalSelectDirective } from '../shared/local-select.directive';
import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit, } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitart07Service } from './sitart07.service';
import { Sitart07Criteria, Sitart07SaveShiftDepartment } from '../sitart07/sitart07.interface';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { BeanUtils } from '../../core/bean-utils.service';
import { LoadMask } from '../../load-mask.component';
import { DateUtils } from '../../core/date-utils.service';
import { PageContext } from '../../shared/service/pageContext.service';
import { OuSelectDirective } from '../shared/ou-select.directive';
import { ShiftSelectDirective } from '../shared/shift-select.directive';
import { DepartmentSelectDirective } from '../shared/department-select.directive';

@Component({
  selector: 'sitart07a',
  template: require('./sitart07a.component.html'),
  providers: [Page, Sitart07Service]
})
export class Sitart07aComponent implements AfterViewInit {

  @Input('info') private info: any;
  @Output('closeModal') private closeEvent = new EventEmitter();
  @ViewChild('successModal') public successModal: ModalDirective;
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('saveModal') public saveModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;
  @ViewChild('backModal') public backModal: ModalDirective;
  @ViewChild('ouCode') public ouCode: OuSelectDirective;
  @ViewChild('shiftGroup') public shiftGroup: ShiftSelectDirective;
  @ViewChild('departmentCode') public departmentCode: DepartmentSelectDirective;

  private form: FormGroup = this.fb.group({
    shiftDepId: this.fb.control(null)
    , ouCode: this.fb.control(null, Validators.required)
    , departmentCode: this.fb.control(null, Validators.required)
    , departmentNameLocal: this.fb.control(null, Validators.required)
    , shiftCode: this.fb.control(null, Validators.required)
    , shiftIn: this.fb.control(null, Validators.required)
    , shiftOut: this.fb.control(null, Validators.required)
  });

  private shiftDepId: number = null;
  private fromLoadObject: any;
  private initMsg: string;
  private showMsg = false;
  private objCriteria: Sitart07SaveShiftDepartment = new Sitart07SaveShiftDepartment();
  private objDelete: Sitart07SaveShiftDepartment = new Sitart07SaveShiftDepartment();
  private alert = { title: '', msg: '', isRedirect: false };
  private records: any[];
  private data: any;
  private dirty: boolean;
  private flagForLoad: boolean = false;
  private shows: any[] = [];

  constructor(private page: Page
    , private fb: FormBuilder
    , private service: Sitart07Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private pageContext: PageContext) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      this.setData(this.fromLoadObject.searchData);
    }
  }

  ngAfterViewInit() {
    if (BeanUtils.isNotEmpty(this.shiftDepId)) {
      this.flagForLoad = true;
      this.ouCode.setValue(this.form.controls["ouCode"].value);
      this.ouCode.disabled(true);
      this.shiftGroup.setValue(this.form.controls["shiftCode"].value);
      this.departmentCode.setValue(this.form.controls["departmentCode"].value);
      this.departmentCode.disabled(true);
      this.flagForLoad = false;
    }
  }

  private changeOuCode(valueField: any) {
    if (valueField) {
      this.form.controls["ouCode"].setValue(valueField.ouCode);
      if (this.flagForLoad == false) {
        this.form.controls["ouCode"].markAsDirty(true);
      }
    }
  }

  private changeShiftGroup(valueField: any) {
    if (valueField) {
      this.form.controls["shiftCode"].setValue(valueField.shiftCode);
      this.objCriteria.shiftId = valueField.shiftId;
      this.getShift();
      if (this.flagForLoad == false) {
        this.form.controls["shiftCode"].markAsDirty(true);
      }
    }
  }

  private changeDepartmentCode(valueField: any) {
    if (valueField) {
      this.form.controls["departmentCode"].setValue(valueField.departmentCode);
      this.objCriteria.departmentId = valueField.departmentId;
      this.form.controls["departmentNameLocal"].setValue(valueField.departmentNameLocal);
      if (this.flagForLoad == false) {
        this.form.controls["departmentCode"].markAsDirty(true);
      }
    }
  }

  public checkDirty() {
    if (this.form.dirty == true) {
      this.dirty = true;
    } else {
      this.dirty = false;
    }
    return this.dirty;
  }

  private alertModalToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.alertModal.show();
  }

  private alertModalSuccessToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.successModal.show();
  }

  private alertModalClose(): void {
    if (null != this.records) {
      if (this.router.url.split("/")[2] == "sitart07a") {
      }
    }
    this.alertModal.toggle();
  }

  private alertModalSuccessClose() {
    if (this.records != null) {
      if (this.router.url.split("/")[2] == "sitart07a") {
      }
    }
    if (this.records == null) {
      this.pageContext.backPage();
    }
    this.successModal.toggle();
  }

  public onSave() {
    if (this.form.valid == true || this.shows.length != 0) {
      if (this.shows.length != 0 || this.shiftDepId != null) {
        if (this.form.dirty) {
          LoadMask.show();
          if (BeanUtils.isNotEmpty(this.shiftDepId)) {
            this.objCriteria.shiftDepId = this.shiftDepId;
          }
          this.objCriteria.grid = this.shows;
          this.showMsg = false;
          this.service.saveShiftDepartment(this.page, this.objCriteria).subscribe(response => {
            if (response.records.success) {
              LoadMask.hide();
              this.alertModalSuccessToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ', true);
              this.form.markAsPristine();
            } else {
              LoadMask.hide();
              this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้ แผนกซ้ำ', false);
            }
          },
            error => {
              console.error(error);
              LoadMask.hide();
              this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
            }
          );
        } else {
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข', false);
        }
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'กรุณากดปุ่มประมวลผล', false);
      }
    } else {
      LoadMask.hide();
      this.alertModalToggle('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล', false);
    }
  }

  private onProcess(): void {
    if (this.shows.find((obj) => {
      return obj.departmentCode == this.form.controls["departmentCode"].value &&
        obj.ouCode == this.form.controls["ouCode"].value;
    })) {
      LoadMask.hide();
      this.alertModalToggle('ผิดพลาด', 'ข้อมูลได้รับการประมวลผลแล้ว', false);
    } else {
      if ((this.form.controls["ouCode"].value == null) ||
        (this.form.controls["shiftCode"].value == null) ||
        (this.form.controls["departmentCode"].value == null) ||
        (this.form.controls["shiftIn"].value == null) ||
        (this.form.controls["shiftOut"].value == null)) {
        this.alertModalToggle('ผิดพลาด', 'กรุณากรอกข้อมูลให้ครบถ้วน', false);
      } else {
        this.objCriteria.ouCode = this.form.controls["ouCode"].value;
        this.objCriteria.departmentCode = this.form.controls["departmentCode"].value;
        this.objCriteria.departmentNameLocal = this.form.controls["departmentNameLocal"].value;
        this.objCriteria.shiftCode = this.form.controls["shiftCode"].value;
        this.objCriteria.shiftIn = this.form.controls["shiftIn"].value;
        this.objCriteria.shiftOut = this.form.controls["shiftOut"].value;
        this.objCriteria.shiftOut = this.form.controls["shiftOut"].value;
        this.objCriteria.departmentId;
        this.showMsg = false;
        this.service.processShiftDepartment(this.page, this.objCriteria).subscribe(response => {
          if (response.records.data.success) {
            LoadMask.hide();
            let row: {};
            row = {
              ouCode: this.form.controls["ouCode"].value,
              departmentCode: this.form.controls["departmentCode"].value,
              departmentNameLocal: this.form.controls["departmentNameLocal"].value,
              shiftCode: this.form.controls["shiftCode"].value,
              shiftIn: this.form.controls["shiftIn"].value,
              shiftOut: this.form.controls["shiftOut"].value,
              departmentId: this.objCriteria.departmentId,
              shiftId: this.objCriteria.shiftId
            };
            this.shows.push(row);
            this.ouCode.clearValue();
            this.shiftGroup.clearValue();
            this.form.controls["shiftIn"].setValue(null);
            this.form.controls["shiftOut"].setValue(null);
            this.departmentCode.clearValue();
          } else {
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้ แผนกซ้ำ', false);
          }
        });
      }
    }
  }

  private setData(object: any) {
    if (BeanUtils.isNotEmpty(object)) {;
      this.form.controls["ouCode"].setValue(object.records.data.ouCode);
      this.form.controls["departmentCode"].setValue(object.records.data.departmentCode);
      this.form.controls["departmentNameLocal"].setValue(object.records.data.departmentNameLocal);
      this.form.controls["shiftCode"].setValue(object.records.data.shiftCode);
      this.form.controls["shiftIn"].setValue(object.records.data.shiftIn);
      this.form.controls["shiftOut"].setValue(object.records.data.shiftOut);
      this.shiftDepId = object.records.data.shiftDepId;
    }
  }

  public onBack() {
    this.checkDirty();
    if (this.form.dirty == false) {
      this.backModalBack();
    } else {
      this.backModalToggle('แจ้งเตือน', 'ข้อมูลถูกแก้ไข ต้องการเปลี่ยนหน้าใช่หรือไม่', false);
    }
  }

  private backModalClose(): void {
    this.backModal.hide();
  }

  private backModalBack(): void {
    this.pageContext.backPage();
  }

  private backModalToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.backModal.show();
  }

  public onCancel() {
    this.closeEvent.emit();
  }

  private alertModalSaveToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.saveModal.show();
  }

  private deleteModalClose(): void {
    this.deleteModal.hide();
  }

  private deleteModalConfirm(): void {
    this.onDelete();
    this.deleteModal.hide();
  }

  private alertModalDeleteToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.deleteModal.toggle();
  }

  public onBeforeDelete(record: any) {
    this.alertModalDeleteToggle('ยืนยันการลบ', 'ต้องการลบข้อมูลใช่หรือไม่', true);
  }

  public onDelete() {
    LoadMask.show();
    this.objDelete.shiftDepId = this.shiftDepId
    this.service.deleteShiftDep(this.page, this.objDelete).subscribe(response => {
      this.alertModalSuccessToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ', true);
      LoadMask.hide();
      this.pageContext.backPage();
    },
      error => {
        console.error(error);
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
      }
    );
  }

  private getShift() {
    if (this.objCriteria.shiftId != null) {
      LoadMask.show();
      this.service.loadShift(this.form.controls["shiftCode"].value)
        .subscribe(response => {
          this.form.controls["shiftIn"].setValue(response.shiftIn);
          this.form.controls["shiftOut"].setValue(response.shiftOut);
          LoadMask.hide();
        }, error => {
          this.objCriteria.shiftId == null;
          LoadMask.hide();
          this.alertModalToggle('ผิดพลาด', 'ไม่พบข้อมูลที่ระบุ', false);
        });
    }
    else {
      this.objCriteria.shiftCode == null;
    }
  }
}
