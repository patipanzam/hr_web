import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { Sitart07Criteria ,Sitart07SaveShiftDepartment} from '../sitart07/sitart07.interface';
import { BeanUtils } from '../../core/bean-utils.service';

const querystring = require('querystring');

@Injectable()
export class Sitart07Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) {}

  public getData(page: Page, searchValue: Sitart07Criteria): Observable<any> {
    let params = '?' + querystring.stringify(page) + "&" + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('/ta/sitart07/searchShiftDepartment') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });
  }

    public saveShiftDepartment(page: Page, params: Sitart07SaveShiftDepartment): Observable<any> {
      return this.authHttp.post(this.restServer.getAPI('/ta/sitart07/saveShiftDepartment'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
    }

  public searchData(shiftDepId: number): Observable<any> {
    let params = '?shiftDepId=' + shiftDepId;
    return this.authHttp.get(this.restServer.getAPI('/ta/sitart07/selectShiftDepartment') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

    public deleteShiftDep(page: Page, params: Sitart07SaveShiftDepartment): Observable<any> {
      return this.authHttp.post(this.restServer.getAPI('/ta/sitart07/deleteShiftDepartment'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
    }

    public loadShift(shiftCode: string): Observable<any> {
    let params = '?shiftCode=' + shiftCode;
    return this.authHttp.get(this.restServer.getAPI('ta/sitart07/loadShift') + params)
      .map(val => { return val.json() });
  }

  public processShiftDepartment(page: Page, params: Sitart07SaveShiftDepartment): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitart07/processShiftDepartment'), params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }
}
