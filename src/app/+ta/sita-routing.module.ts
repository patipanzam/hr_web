import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../auth-guard.service';
import { CanDeactivateGuard } from '../can-deactivate-guard.service';
import { AppLayoutComponent } from './../shared/layouts/app-layout.component';
import { SimpleLayoutComponent } from './../shared/layouts/simple-layout.component';
import { CommonSelectService } from '../shared/service/common-select.service';

import { SitaComponent } from './sita.component';

import { Sitart01Resolve } from "./sitart01/sitart01.resolve";
import { Sitart01Service } from "./sitart01/sitart01.service";
import { Sitart01Component } from "./sitart01/sitart01.component";
import { Sitart01aComponent } from "./sitart01/sitart01a.component";

import { Sitart02Resolve } from "./sitart02/sitart02.resolve";
import { Sitart02Service } from "./sitart02/sitart02.service";
import { Sitart02Component } from "./sitart02/sitart02.component";
import { Sitart02aComponent } from "./sitart02/sitart02a.component";

import { Sitart03Resolve } from "./sitart03/sitart03.resolve";
import { Sitart03Service } from "./sitart03/sitart03.service";
import { Sitart03Component } from "./sitart03/sitart03.component";
import { Sitart03aComponent } from "./sitart03/sitart03a.component";

import { Sitart04Component } from './sitart04/sitart04.component';
import { Sitart04aComponent } from './sitart04/sitart04a.component';
import { Sitart04Resolve } from './sitart04/sitart04.resolve';
import { Sitart04Service } from './sitart04/sitart04.service';

import { Sitart05Resolve } from "./sitart05/sitart05.resolve";
import { Sitart05Service } from "./sitart05/sitart05.service";
import { Sitart05Component } from "./sitart05/sitart05.component";
import { Sitart05aComponent } from "./sitart05/sitart05a.component";

import { Sitart06Resolve } from "./sitart06/sitart06.resolve";
import { Sitart06Service } from "./sitart06/sitart06.service";
import { Sitart06Component } from "./sitart06/sitart06.component";
import { Sitart06aComponent } from "./sitart06/sitart06a.component";

import { Sitart07Resolve } from "./sitart07/sitart07.resolve";
import { Sitart07Service } from "./sitart07/sitart07.service";
import { Sitart07Component } from "./sitart07/sitart07.component";
import { Sitart07aComponent } from "./sitart07/sitart07a.component";

import { Sitart08Resolve } from "./sitart08/sitart08.resolve";
import { Sitart08Service } from "./sitart08/sitart08.service";
import { Sitart08Component } from "./sitart08/sitart08.component";
import { Sitart08aComponent } from "./sitart08/sitart08a.component";

import { Sitadt01Resolve } from "./sitadt01/sitadt01.resolve";
import { Sitadt01Service } from "./sitadt01/sitadt01.service";
import { Sitadt01Component } from "./sitadt01/sitadt01.component";
import { Sitadt01aComponent } from "./sitadt01/sitadt01a.component";

import { Sitadt02Resolve } from "./sitadt02/sitadt02.resolve";
import { Sitadt02Service } from "./sitadt02/sitadt02.service";
import { Sitadt02Component } from "./sitadt02/sitadt02.component";
import { Sitadt02aComponent } from "./sitadt02/sitadt02a.component";

import { Sitadt04Resolve } from "./sitadt04/sitadt04.resolve";
import { Sitadt04Service } from "./sitadt04/sitadt04.service";
import { Sitadt04Component } from "./sitadt04/sitadt04.component";
import { Sitadt04aComponent } from "./sitadt04/sitadt04a.component";

import { Sitadt05Resolve } from "./sitadt05/sitadt05.resolve";
import { Sitadt05Service } from "./sitadt05/sitadt05.service";
import { Sitadt05Component } from "./sitadt05/sitadt05.component";

import { Sitadt06Resolve } from "./sitadt06/sitadt06.resolve";
import { Sitadt06Service } from "./sitadt06/sitadt06.service";
import { Sitadt06Component } from "./sitadt06/sitadt06.component";


const routes: Routes = [{
  path: '',
  component: AppLayoutComponent,
  canActivate: [AuthGuard],
  children: [{
    path: '',
    data: { title: 'TA' },
    component: SitaComponent,
    canActivateChild: [AuthGuard]
  }, {
    path: 'sitart01',
    data: {
      title: 'SITART01'
    },
    component: Sitart01Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart01Resolve
    }
  }, {
    path: 'sitart02',
    data: {
      title: 'SITART02'
    },
    component: Sitart02Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart02Resolve
    }
  }, {
    path: 'sitart03',
    data: {
      title: 'SITART03'
    },
    component: Sitart03Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart03Resolve
    }
  }
  , {
    path: 'sitart04',
    data: {
      title: 'SITART04'
    },
    component: Sitart04Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart04Resolve
    }
  }
  , {
    path: 'sitart05',
    data: {
      title: 'SITART05'
    },
    component: Sitart05Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart05Resolve
    }
  }, {
    path: 'sitart06',
    data: {
      title: 'SITART06'
    },
    component: Sitart06Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart06Resolve
    }
  }, {
    path: 'sitart07',
    data: {
      title: 'SITART07'
    },
    component: Sitart07Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart07Resolve
    }
  }, {
    path: 'sitart08',
    data: {
      title: 'SITART08'
    },
    component: Sitart08Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart08Resolve
    }
  }, {
    path: 'sitadt01',
    data: {
      title: 'SITADT01'
    },
    component: Sitadt01Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitadt01Resolve
    }
  }, {
    path: 'sitadt02',
    data: {
      title: 'SITADT02'
    },
    component: Sitadt02Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitadt02Resolve
    }
  }
  , {
    path: 'sitadt04',
    data: {
      title: 'SITADT04'
    },
    component: Sitadt04Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitadt04Resolve
    }
  }
  , {
    path: 'sitadt05',
    data: {
      title: 'SITADT05'
    },
    component: Sitadt05Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitadt05Resolve
    }
  }, {
    path: 'sitadt06',
    data: {
      title: 'SITADT06'
    },
    component: Sitadt06Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitadt06Resolve
    }
  },]
}, {
  path: '',
  component: SimpleLayoutComponent,
  canActivate: [AuthGuard],
  children: [{
    path: 'sitart01a/:leaveGroupId',
    data: { title: 'SITART01A' },
    component: Sitart01aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart01Resolve
    }
  }, {
    path: 'sitart01A/:leaveGroupId',
    component: Sitart01aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart01Resolve
    }
  }, {
    path: 'sitart02a/:leaveTypeId',
    data: { title: 'SITART02A' },
    component: Sitart02aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart02Resolve
    }
  }, {
    path: 'sitart02A/:leaveTypeId',
    component: Sitart02aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart02Resolve
    }
  }, {
    path: 'sitart03a/:shiftId',
    data: { title: 'SITART03A' },
    component: Sitart03aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart03Resolve
    }
  }
  , {
    path: 'sitart04a/:workingId',
    data: { title: 'SITART04A' },
    component: Sitart04aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart04Resolve
    }
  }
  , {
    path: 'sitart05a/:holidayId',
    data: { title: 'SITART05A' },
    component: Sitart05aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart05Resolve
    }
  }, {
    path: 'sitart06a/:periodId',
    data: { title: 'SITART06A' },
    component: Sitart06aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart06Resolve
    }
  }, {
    path: 'sitart07a/:shiftDepId',
    data: { title: 'SITART06A' },
    component: Sitart07aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart07Resolve
    }
  }, {
    path: 'sitart08a/:shiftEmpId',
    data: { title: 'SITART08A' },
    component: Sitart08aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitart08Resolve
    }
     }, {
    path: 'sitadt01a/:leaveId',
    data: { title: 'SITADT01A' },
    component: Sitadt01aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitadt01Resolve
    }
  }, {
    path: 'sitadt02a/:overTimeId',
    data: { title: 'SITADT02A' },
    component: Sitadt02aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitadt02Resolve
    }
  }
  , {
    path: 'sitadt04a/:clockId',
    data: { title: 'SITADT04A' },
    component: Sitadt04aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: Sitadt04Resolve
    }
  }
  ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  providers: [CommonSelectService,
    Sitart01Resolve, Sitart02Resolve, Sitart03Resolve, Sitart04Resolve, Sitart05Resolve, Sitart06Resolve, Sitart07Resolve, Sitart08Resolve
    , Sitart01Service, Sitart02Service, Sitart03Service, Sitart04Service, Sitart05Service, Sitart06Service, Sitart07Service, Sitart08Service

    , Sitadt01Resolve, Sitadt02Resolve, , Sitadt04Resolve, Sitadt05Resolve, Sitadt06Resolve
    , Sitadt01Service, Sitadt02Service, , Sitadt04Service, Sitadt05Service, Sitadt06Service


  ]

})
export class SitaRoutingModule {
}
