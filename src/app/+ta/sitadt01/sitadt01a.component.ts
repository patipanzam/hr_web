import { BeanUtils } from '../../core/bean-utils.service';
import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit, } from '@angular/core';
import { DateUtils } from '../../core/date-utils.service';
import { EmployeeSelectDirective } from '../shared/employee-select.directive';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { ModalDirective } from 'ng2-bootstrap';
import { OuSelectDirective } from '../shared/ou-select.directive';
import { Page } from '../../shared/pagination.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ShiftSelectDirective } from '../shared/shift-select.directive';
import { Sitadt01SaveLeave } from '../sitadt01/sitadt01.interface';
import { Sitadt01Service } from './sitadt01.service';
import { SSDatePicker } from '../../shared/ss-datepicker.component';
import { LeaveTypeSelectDirective } from '../shared/leave-type-select.directive';
import { SSTimePicker } from '../../shared/ss-timepicker.component';


@Component({
  selector: 'sitadt01a',
  template: require('./sitadt01a.component.html'),
  providers: [Page, Sitadt01Service, Sitadt01SaveLeave]
})
export class Sitadt01aComponent implements AfterViewInit {

  @Input('info') private info: any;
  @Output('closeModal') private closeEvent = new EventEmitter();
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('backModal') public backModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;
  @ViewChild('successModal') public successModal: ModalDirective;
  @ViewChild('employeeCode') public employeeCode: EmployeeSelectDirective;
  @ViewChild('employee') public employee: EmployeeSelectDirective;
  @ViewChild('leaveType') public leaveType: LeaveTypeSelectDirective;
  @ViewChild('leaveTypeCode') public leaveTypeCode: LeaveTypeSelectDirective;
  @ViewChild('startDate') public startDate: SSDatePicker;
  @ViewChild('endDate') public endDate: SSDatePicker;
  @ViewChild('startTime') private startTime: SSTimePicker;
  @ViewChild('endTime') private endTime: SSTimePicker;
  @ViewChild('ouCode') public ouCode: OuSelectDirective;


  private form: FormGroup = this.fb.group({
    leaveId: this.fb.control(null)
    , ouCode: this.fb.control(null, Validators.required)
    , empCode: this.fb.control(null, Validators.required)
    , empThaiName: this.fb.control(null)
    , leaveTypeCode: this.fb.control(null, Validators.required)
    , leaveTypeDesc: this.fb.control(null)
    , startDate: this.fb.control(null, Validators.required)
    , endDate: this.fb.control(null, Validators.required)
    , startTime: this.fb.control(null, Validators.required)
    , endTime: this.fb.control(null, Validators.required)
    , allDay: this.fb.control("")
    , day: this.fb.control(null)
    , hours: this.fb.control(null)
    , mins: this.fb.control(null)
    , employeeCode: this.fb.control(null)
    , leaveDay: this.fb.control(null)
  });

  private leaveId: number = null;
  private allDay: number = null;
  private alert = { title: '', msg: '', isRedirect: false };
  private currentDate: Date = new Date();
  private data: any;
  private dirty: boolean;
  private flagForLoad: boolean = false;
  private fromLoadObject: any;
  private initMsg: string;
  private objCriteria: Sitadt01SaveLeave = new Sitadt01SaveLeave();
  private objDelete: Sitadt01SaveLeave = new Sitadt01SaveLeave();
  private records: any[];
  private showMsg = false;
  private shows: any[] = [];

  constructor(private fb: FormBuilder
    , private page: Page
    , private pageContext: PageContext
    , private router: Router
    , private routerActive: ActivatedRoute
    , private service: Sitadt01Service) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      this.setData(this.fromLoadObject.searchData);
    }
  }

  ngAfterViewInit() {
    if (BeanUtils.isNotEmpty(this.leaveId)) {
      this.flagForLoad = true;
      this.ouCode.setValue(this.form.controls["ouCode"].value);
      this.ouCode.disabled(true);
      this.employee.setValue(this.form.controls["empCode"].value);
      this.employee.disabled(true);
      this.leaveType.setValue(this.form.controls["leaveTypeCode"].value);
      this.leaveType.disabled(true);
      this.startTime.setValue(this.form.controls["startTime"].value);
      this.endTime.setValue(this.form.controls["endTime"].value);
      this.startDate.setValue(this.fromLoadObject.searchData.records.data.startDate);
      this.endDate.setValue(this.fromLoadObject.searchData.records.data.endDate);
      this.flagForLoad = false;
      if (this.startDate.getValue() < this.currentDate && this.endDate.getValue() < this.currentDate) {
        this.startDate.readOnly = true;
        this.endDate.readOnly = true;
      }
      if (this.startDate.getValue() < this.currentDate && this.endDate.getValue() > this.currentDate) {
        this.startDate.readOnly = true;
      }
      if (this.startDate.getValue() < this.currentDate && this.endDate.getValue() < this.currentDate) {
        $('#checkedMoreSendN').prop("disabled", true);

      }
      if (this.startDate.getValue() < this.currentDate && this.endDate.getValue() < this.currentDate) {
        this.startTime.readOnly = true;
        this.endTime.readOnly = true;
      }

    }else{
      this.form.controls["allDay"].setValue(1);
      this.startTime.readOnly = true;
      this.endTime.readOnly = true;

    }
    if(this.form.controls["allDay"].value == 1) {
      this.startTime.readOnly = true;
      this.endTime.readOnly = true;
    }
  }

  private changeOu(valueField: any) {
    this.form.controls["ouCode"].setValue(valueField.ouCode);
    this.objCriteria.ouCode = valueField.ouCode;
  }

  private changeEmployee(valueField: any) {
    this.form.controls["empCode"].setValue(valueField.empCode);
    this.form.controls["empThaiName"].setValue(valueField.empThaiName);
  }

  private changeLeaveType(valueField: any) {
    this.form.controls["leaveTypeCode"].setValue(valueField.leaveTypeCode);
    this.objCriteria.leaveTypeCode = valueField.leaveTypeCode;
    this.getLeave();

  }

  private changCheckboxAllDay(event: any) {
    if (event.target.checked) {
      this.form.controls["allDay"].setValue(1);
      this.startTime.clearValue();
      this.endTime.clearValue();
      this.startTime.readOnly = true;
      this.endTime.readOnly = true;

    } else {
      this.form.controls["allDay"].setValue(0);
      this.startTime.readOnly = false;
      this.endTime.readOnly = false;
    }
  }

  private changeStardDate(valueField: any) {
    if (valueField) {
      this.form.controls["startDate"].setValue(this.startDate.getMilliSecond());
      this.objCriteria.startDate = this.startDate.getMilliSecond();
      this.objCriteria.obligationsHolidayDate = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsHolidayDateShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
    }
  }

    private changeEndDate(valueField: any) {
    if (valueField) {
      this.form.controls["endDate"].setValue(this.endDate.getMilliSecond());
      this.objCriteria.endDate = this.endDate.getMilliSecond();
      this.objCriteria.obligationsHolidayDate = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsHolidayDateShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
    }
  }

  private changeStartTime(valueField: any) {
    if (valueField) {
      this.form.controls["startTime"].setValue(this.startTime.getValue());
      this.objCriteria.startTime = this.startTime.getValue();
      if (this.flagForLoad == false) {
        this.form.controls["startTime"].markAsDirty(true);
        this.form.pristine;
      }
    }
  }

    private changeEndTime(valueField: any) {
    if (valueField) {
      this.form.controls["endTime"].setValue(this.endTime.getValue());
      this.objCriteria.endTime = this.endTime.getValue();
      if (this.flagForLoad == false) {
        this.form.controls["endTime"].markAsDirty(true);
        this.form.pristine;
      }
    }
  }



  public checkDirty() {
    if (this.form.dirty == true) {
      this.dirty = true;
    } else {
      this.dirty = false;
    }
    return this.dirty;
  }

  private alertModalToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.alertModal.show();
  }

  private alertModalSuccessToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.successModal.show();
  }

  private alertModalClose() {
    if (this.records == null) {
      (this.router.url.split("/")[2] == "sitadt01a")
    }
    this.alertModal.toggle();
  }

  private alertModalSuccessClose() {
    if (this.records != null) {
      if (this.router.url.split("/")[2] == "sitadt01a") {
      }
    }
    if (this.records == null) {
      this.pageContext.backPage();
    }
    this.successModal.toggle();
  }

  private onSave() {
    if (this.form.valid == true) {
      if (this.form.dirty) {
        LoadMask.show();
        if (BeanUtils.isNotEmpty(this.leaveId)) {
          this.objCriteria.leaveId = this.leaveId;
        }
        this.objCriteria.ouCode = this.form.controls["ouCode"].value;
        this.objCriteria.empCode = this.form.controls["empCode"].value;
        this.objCriteria.leaveTypeCode = this.form.controls["leaveTypeCode"].value;
        this.objCriteria.allDay = this.form.controls["allDay"].value;
        this.objCriteria.startDate = this.form.controls["startDate"].value;
        this.objCriteria.endDate = this.form.controls["endDate"].value;
        this.objCriteria.startTime = this.form.controls["startTime"].value;
        this.objCriteria.endTime = this.form.controls["endTime"].value;
        this.objCriteria.day = this.form.controls["day"].value;
        this.objCriteria.hours = this.form.controls["hours"].value;
        this.objCriteria.mins = this.form.controls["mins"].value;
        this.showMsg = false;
        this.service.saveLeave(this.page, this.objCriteria).subscribe(response => {
          this.records = response.records.data.leaveId;
          this.leaveId = response.records.data.leaveId;
          if (response.records.data.success == 0) {
            LoadMask.hide();
            this.alertModalToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ', true);
            this.ouCode.disabled(true);
            this.employee.disabled(true);
            this.leaveType.disabled(true);
            this.form.controls["startDate"].disable();
            this.form.controls["endDate"].disable();
            this.form.controls["startTime"].disable();
            this.form.controls["endTime"].disable();
            this.form.markAsDirty(false);
          } else if (response.records.data.success == 1) {
            LoadMask.hide();
            this.alertModalToggle('แจ้งเตือน', 'รหัสกะการทำงานซ้ำ', false);
          } else {
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้', false);
          }
        },
          error => {
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
          }
        );
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข', false);
      }
    } else {
      LoadMask.hide();
      this.alertModalToggle('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล', false);
    }
  }

  private setData(object: any) {
    console.log(object)
    if (BeanUtils.isNotEmpty(object)) {
      this.leaveId = object.records.data.leaveId;
      this.form.controls["ouCode"].setValue(object.records.data.ouCode);
      this.objCriteria.ouCode = object.records.data.ouCode;
      this.form.controls["empCode"].setValue(object.records.data.empCode);
      this.objCriteria.employeeCode = object.records.data.empCode;
      this.form.controls["empThaiName"].setValue(object.records.data.empThaiName);
      this.form.controls["leaveTypeCode"].setValue(object.records.data.leaveTypeCode);
      this.objCriteria.leaveTypeCode = object.records.data.leaveTypeCode;
      this.form.controls["allDay"].setValue(object.records.data.allDay);

      this.form.controls["leaveDay"].setValue(object.records.data.leaveDay);

      this.form.controls["startTime"].setValue(object.records.data.startTime);

      this.form.controls["endTime"].setValue(object.records.data.endTime);

      this.form.controls["startDate"].setValue(object.records.data.startDate);

      this.form.controls["endDate"].setValue(object.records.data.endDate);

      this.form.controls["day"].setValue(object.records.data.day);

      this.form.controls["hours"].setValue(object.records.data.hours);

      this.form.controls["mins"].setValue(object.records.data.mins);

      this.objCriteria.startDate = this.fromLoadObject.searchData.records.data.startDate;
      this.objCriteria.endDate = this.fromLoadObject.searchData.records.data.endDate;
    }
  }

  public onBack() {
    if (this.form.dirty) {
      this.backModalBack();
    } else {
      this.backModalToggle('แจ้งเตือน', 'ข้อมูลถูกแก้ไข ต้องการเปลี่ยนหน้าใช่หรือไม่', false);
    }
  }

  private backModalClose() {
    this.backModal.hide();
  }

  private backModalBack() {
    this.pageContext.backPage();
  }

  private backModalToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.backModal.show();
  }

  public onCancel() {
    this.closeEvent.emit();
  }

  private deleteModalClose() {
    this.deleteModal.hide();
  }

  private deleteModalConfirm() {
    this.onDelete();
    this.deleteModal.hide();
  }

  private alertModalDeleteToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.deleteModal.toggle();
  }

  public onBeforeDelete(record: any) {
    this.alertModalDeleteToggle('ยืนยันการลบ', 'ต้องการลบข้อมูลใช่หรือไม่', true);
  }

  public onDelete() {
    LoadMask.show();
    this.objDelete.leaveId = this.leaveId
    this.service.deleteLeave(this.page, this.objDelete).subscribe(response => {
      this.alertModalSuccessToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ', true);
      LoadMask.hide();
      this.pageContext.backPage();
    },
      error => {
        console.error(error);
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
      }
    );
  }
  private getLeave() {
    if (this.form.controls["leaveTypeCode"].value != null) {
      LoadMask.show();
      this.service.loadLeave(this.form.controls["leaveTypeCode"].value)
        .subscribe(response => {
          this.form.controls["leaveDay"].setValue(response.leaveDay);
          LoadMask.hide();
        }, error => {
          this.form.controls["leaveDay"].setValue(null);
          LoadMask.hide();
          this.alertModalToggle('ผิดพลาด', 'ไม่พบข้อมูลที่ระบุ', false);
        });
    }
    else {
      this.form.controls["leaveDay"].setValue(null);
    }
  }
}
