import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { Sitadt01Criteria, Sitadt01SaveLeave } from '../sitadt01/sitadt01.interface';
import { BeanUtils } from '../../core/bean-utils.service';

const querystring = require('querystring');

@Injectable()
export class Sitadt01Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

 //searchShiftEmp
  public getData(page: Page, searchValue: Sitadt01Criteria): Observable<any> {
let startDate = searchValue.startDate!=null?searchValue.startDate:"";
let endDate = searchValue.endDate!=null?searchValue.endDate:"";
let departmentId = searchValue.departmentId!=null?searchValue.departmentId:"";
let leaveTypeCode = searchValue.leaveTypeCode!=null?searchValue.leaveTypeCode:"";
let employeeCodeStart = searchValue.employeeCodeStart!=null?searchValue.employeeCodeStart:"";
let employeeCodeEnd = searchValue.employeeCodeEnd!=null?searchValue.employeeCodeEnd:"";
    let params = '?' + querystring.stringify(page) + "&startDate=" +  startDate+"&endDate="+endDate+"&departmentId=" +  departmentId+"&leaveTypeCode=" +  leaveTypeCode+"&employeeCodeStart=" +  employeeCodeStart+"&employeeCodeEnd=" +  employeeCodeEnd;
    return this.authHttp.get(this.restServer.getAPI('/ta/sitadt01/searchLeave') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });
  }

  //saveShiftEmp
  public saveLeave(page: Page, params: Sitadt01SaveLeave): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitadt01/saveLeave'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  //selectShiftEmp
  public searchData(leaveId: number): Observable<any> {
    let params = '?leaveId=' + leaveId;
    return this.authHttp.get(this.restServer.getAPI('ta/sitadt01/selectLeave') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  //deleteShiftEmp
  public deleteLeave(page: Page, params: Sitadt01SaveLeave): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitadt01/deleteLeave'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }
  public loadLeave(leaveTypeCode: string): Observable<any> {
    let params = '?leaveTypeCode=' + leaveTypeCode;
    return this.authHttp.get(this.restServer.getAPI('ta/sitadt01/loadLeave') + params)
      .map(val => { return val.json() });
  }
}

