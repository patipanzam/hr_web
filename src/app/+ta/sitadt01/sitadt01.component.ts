import { BeanUtils } from '../../core/bean-utils.service';
import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { DepartmentSelectDirective } from '../shared/department-select.directive';
import { EmployeeSelectDirective } from '../shared/employee-select.directive';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchStatus } from '../../shared/constant/common.interface';
import { ShiftSelectDirective } from '../shared/shift-select.directive';
import { Sitadt01Service } from './sitadt01.service';
import { Sitadt01Criteria } from '../sitadt01/sitadt01.interface';
import { LeaveTypeSelectDirective } from '../shared/leave-type-select.directive';
import { SSDatePicker } from '../../shared/ss-datepicker.component';
import { DateUtils } from '../../core/date-utils.service';

@Component({
  selector: 'sitadt01',
  template: require('./sitadt01.component.html'),
  providers: [Page, Sitadt01Service, Sitadt01Criteria]
})

export class Sitadt01Component implements OnInit {

  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('department') public department: DepartmentSelectDirective;
  @ViewChild('employeeStart') public employeeStart: EmployeeSelectDirective;
  @ViewChild('employeeEnd') public employeeEnd: EmployeeSelectDirective;
  @ViewChild('shiftGroup') public shiftGroup: ShiftSelectDirective;
  @ViewChild('leaveType') public leaveType: LeaveTypeSelectDirective;
  @ViewChild('startDate') public startDate: SSDatePicker;
  @ViewChild('endDate') public endDate: SSDatePicker;

  private form: FormGroup = this.fb.group({
    leaveId: this.fb.control(null)
    , ouCode: this.fb.control("")
    , empCode: this.fb.control("")
    , empThaiName: this.fb.control("")
    , shiftCode: this.fb.control("")
    , shiftIn: this.fb.control("")
    , shiftOut: this.fb.control("")
    , startDate: this.fb.control(null)
    , endDate: this.fb.control(null)
    , leaveTypeCode: this.fb.control("")
    , leaveTypeDesc: this.fb.control("")
    , leaveGroupCode: this.fb.control("")
    , leaveGroup: this.fb.control("")
    , leaveType: this.fb.control("")
    , departmentId: this.fb.control("")

  });

  private leaveId: any;
  private allday: number = null;
  private startTime: number = null;
  private endTime: number = null;
  private alert = { title: '', msg: '' };
  private checkDelete: number = 0;
  private fromLoadObject: any;
  private initMsg: string;
  private objCriteria: Sitadt01Criteria = new Sitadt01Criteria();
  private showMsg = false;
  private records: any[];

  constructor(private fb: FormBuilder
    , private page: Page
    , private pageContext: PageContext
    , private service: Sitadt01Service
    , private router: Router
    , private routerActive: ActivatedRoute) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (this.objCriteria.startDate) {
        this.startDate.setValue(this.objCriteria.startDate);
      }
      if (this.objCriteria.endDate) {
        this.endDate.setValue(this.objCriteria.endDate);
      }
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
  }

  public childModalShow(record: any) {
    this.pageContext.nextPage(['/ta/sitadt01a', record.leaveId], this.objCriteria);
  }

  public createLeave() {
    let leaveId: any = "";
    this.pageContext.nextPage(['/ta/sitadt01a', leaveId], this.objCriteria);
  }

  private changeDepartment(valueField: any) {
    this.objCriteria.departmentId = valueField.departmentId;
    this.objCriteria.departmentCode = valueField.departmentCode;
    // this.loadEmployee();
  }

   private changeLeaveType(valueField: any) {
    this.objCriteria.leaveTypeCode = valueField.leaveTypeCode;
  }

  private changeStartEmployee(valueField: any) {
    this.objCriteria.employeeCodeStart = valueField.empCode;
  }

  private changeEndEmployee(valueField: any) {
    this.objCriteria.employeeCodeEnd = valueField.empCode;
  }

    private changeStartDate(valueField: any) {
    if (valueField) {
      // this.form.controls["startDate"].setValue(this.startDate.getMilliSecond());
      this.objCriteria.startDate = this.startDate.getMilliSecond();
      // this.objCriteria.obligationsHolidayDate = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      // this.objCriteria.obligationsHolidayDateShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
    }
  }

    private changeEndDate(valueField: any) {
    if (valueField) {
      // this.form.controls["endDate"].setValue(this.endDate.getMilliSecond());
      this.objCriteria.endDate = this.endDate.getMilliSecond();
      // this.objCriteria.obligationsHolidayDate = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      // this.objCriteria.obligationsHolidayDateShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
    }
  }


  // About Search
  public onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.showMsg = false;
    this.callServiceSearch(changeEvent, this.objCriteria);
  }

  private callServiceSearch(page: Page, mapping: Sitadt01Criteria) {
    console.log(mapping);
    this.service.getData(page, mapping)
      .subscribe(response => {
        this.page = response.page;
        this.records = response.records;
        if (BeanUtils.isNotEmpty(this.records)) {
          LoadMask.hide();
        } else {
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
        }
      },
      error => {
        console.error(error);
        LoadMask.hide();
      }
      );
  }

  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  public onChangePage(changeEvent: Page) {
    this.page = changeEvent;
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private onSearch(modeSearch: number) {
    if (this.form.valid == true) {
      if (BeanUtils.isNotEmpty(modeSearch)) {
        this.objCriteria.flagForPaging = modeSearch
        this.page.start = 0;
        if (modeSearch == SearchStatus.normalSearch) {
          this.onNormalSearch(this.page);
        }
      }
    }
  }

  public closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }
}

//   private loadEmployee() {
//     let params = {
//       departmentId: this.objCriteria.departmentId,
//       departmentCode: this.objCriteria.departmentCode || ''
//     }
//     this.employeeStart.load(params);
//     this.employeeEnd.load(params);
//   }
// }
