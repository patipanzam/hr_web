import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { LoadMask } from '../../load-mask.component';
import { Sitadt01Service } from './sitadt01.service';
import { BeanUtils } from '../../core/bean-utils.service';

@Injectable()
export class Sitadt01Resolve implements Resolve<any> {
  constructor(private selectService: CommonSelectService, private sitadt01Service: Sitadt01Service) {
    this.selectService.setModuleName('ta');
  }
  resolve(route: ActivatedRouteSnapshot): any {
    LoadMask.show();
    if (route.url[0].path == 'sitadt01') {
      var department = this.selectService.getDepartment();
      var employee = this.selectService.getEmployee();
      var leaveType = this.selectService.getLeaveType();
      return Observable.forkJoin([department, employee, leaveType]).map((response) => {

        LoadMask.hide();
        return {
          department: response[0],
          employee: response[1],
          leaveType: response[2]
        };
      }).first();
    } else if (route.url[0].path == 'sitadt01a') {

            if (BeanUtils.isNotEmpty(route.params['leaveId'])) {
              var searchData = this.sitadt01Service.searchData(route.params['leaveId']);
              var employee = this.selectService.getEmployee();
              var leaveType = this.selectService.getLeaveType();
              var ouCode = this.selectService.getOu();
              return Observable.forkJoin([searchData, leaveType, employee,ouCode ]).map((response) => {
                LoadMask.hide();
                return {
                  searchData: response[0],
                  leaveType: response[1],
                  employee: response[2],
                  ouCode: response[3]
                };
              }).first();
            } else {
              var leaveType = this.selectService.getLeaveType();
              var employee = this.selectService.getEmployee();
              var ouCode = this.selectService.getOu();
              return Observable.forkJoin([leaveType, employee,ouCode ]).map((response) => {
                LoadMask.hide();
                return {
                  leaveType: response[0],
                  employee: response[1],
                  ouCode: response[2]
                };
              }).first();
            }
          }
        }
      }


