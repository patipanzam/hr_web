import { Component, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitart04Service } from './sitart04.service';
import { Sitart04Criteria, Sitart04SaveTaWorking } from '../sitart04/sitart04.interface';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BeanUtils } from '../../core/bean-utils.service';
import { LoadMask } from '../../load-mask.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { OuSelectDirective } from '../shared/ou-select.directive';
@Component({
  selector: 'sitart04a',
  template: require('./sitart04a.component.html'),
  providers: [Page, Sitart04Service]
})
export class Sitart04aComponent implements AfterViewInit {

  @Output('closeModal') private closeEvent = new EventEmitter();
  @Input('info') private info: any;
  @ViewChild('alertModal') private alertModal: ModalDirective;
  @ViewChild('backModal') private backModal: ModalDirective;
  @ViewChild('ouCode') private ouCode: OuSelectDirective;

  private form: FormGroup = this.fb.group({
    workingId: this.fb.control(null)
    , workingCode: this.fb.control(null, Validators.required)
    , workingDesc: this.fb.control("", Validators.required)
    , workingStatus: this.fb.control(null)
    , ouCode: this.fb.control("", Validators.required)
  });
  private workingId: number = null;
  private workingCode: string = "";
  private workingDesc: string = "";
  private workingStatus: number = null;
  private fromLoadObject: any;
  private initMsg: string;
  private showMsg = false;
  private objCriteria: Sitart04SaveTaWorking = new Sitart04SaveTaWorking();
  private alert = { title: '', msg: '' };
  private records: any[];

  constructor(private page: Page
    , private fb: FormBuilder
    , private service: Sitart04Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private pageContext: PageContext
  ) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      this.setData(this.fromLoadObject.searchData);
    }
  }

  ngAfterViewInit() {
    if (BeanUtils.isEmpty(this.workingId)) {
      this.form.controls["workingStatus"].setValue(1);
    }
    this.ouCode.setValue(this.form.controls["ouCode"].value);
  }

  private alertModalToggle(title: string, msg: string): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  private alertModalClose(): void {
    if (null != this.records) {
      if (this.router.url.split("/")[2] == "sitart04a") {
        this.router.navigate(['/ta/sitart04a', this.workingId], { skipLocationChange: true });
      }
    }
    this.alertModal.toggle();
  }

  private onSave() {
    if (this.form.dirty) {
      if (this.form.valid == true) {
        LoadMask.show();
        if (BeanUtils.isNotEmpty(this.workingId)) {
          this.objCriteria.workingId = this.workingId;
        }
        this.objCriteria.ouCode = this.form.controls["ouCode"].value;
        this.objCriteria.workingCode = this.form.controls["workingCode"].value;
        this.objCriteria.workingDesc = this.form.controls["workingDesc"].value;
        this.objCriteria.workingStatus = this.form.controls["workingStatus"].value;
        this.showMsg = false;
        this.service.saveTaWorking(this.page, this.objCriteria).subscribe(response => {
          this.records = response.records.data.workingId;
          this.workingId = response.records.data.workingId;
          if (response.records.data.checkWorkingCode == 0) {
            LoadMask.hide();
            this.alertModalToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ');
            this.form.markAsPristine();
          } else {
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้');
          }
        },
          error => {
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ');
          }
        );
      } else {
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
      }
    } else {
      LoadMask.hide();
      this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข');
    }
  }

  private setData(object: any) {
    if (BeanUtils.isNotEmpty(object)) {
      this.form.controls["ouCode"].setValue(object.records.data[0].ouCode);
      this.form.controls["workingCode"].setValue(object.records.data[0].workingCode);
      this.form.controls["workingDesc"].setValue(object.records.data[0].workingDesc);
      this.form.controls["workingStatus"].setValue(object.records.data[0].workingStatus);
      this.workingId = object.records.data[0].workingId
    }
  }

  private onBack() {
    if (this.form.dirty) {
      this.backModalToggle('แจ้งเตือน', 'ข้อมูลได้รับการแก้ไข ต้องการเปลี่ยนหน้าหรือไม่ ?');
    } else {
      this.pageContext.backPage();
    }
  }

  private backModalClose(): void {
    this.backModal.hide();
  }

  private backModalBack(): void {
    this.pageContext.backPage();
  }

  private backModalToggle(title: string, msg: string): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.backModal.toggle();
  }

  private onCancel() {
    this.closeEvent.emit();
  }

  private changCheckbox(event: any) {
    if (event.target.checked) {
      this.form.controls["workingStatus"].setValue(1);
    } else {
      this.form.controls["workingStatus"].setValue(0);
    }
  }

  private changeOu(valueField: any) {
    this.form.controls["ouCode"].setValue(this.ouCode.getValue());
    this.objCriteria.ouCode = this.ouCode.getValue();
    this.fromLoadObject.ouCode = this.ouCode.getValue();
  }
}
