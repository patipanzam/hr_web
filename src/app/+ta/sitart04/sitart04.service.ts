import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { Sitart04Criteria, Sitart04SaveTaWorking } from '../sitart04/sitart04.interface';
import { BeanUtils } from '../../core/bean-utils.service';

const querystring = require('querystring');

@Injectable()
export class Sitart04Service {
  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

  public getData(page: Page, searchValue: Sitart04Criteria): Observable<any> {
    let params = '?' + querystring.stringify(page) + "&" + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('ta/sitart04/searchWorking') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });
  }

  public saveTaWorking(page: Page, params: Sitart04SaveTaWorking): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('ta/sitart04/saveWorking'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  public searchData(workingId: number): Observable<any> {
    let params = '?workingId=' + workingId;
    return this.authHttp.get(this.restServer.getAPI('ta/sitart04/selectWorking') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }
}
