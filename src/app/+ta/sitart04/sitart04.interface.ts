export class Sitart04Criteria {
  public inputSearch: string = '';
  public workingCode: string;
  public workingDesc: string;
  public workingStatus: number;
  public ouCode: string;
  public flagForPaging: number = 0;
}
export class Sitart04SaveTaWorking {
  public workingId: number;
  public workingCode: string;
  public workingDesc: string;
  public workingStatus: number;
  public ouCode: string;
  public flagForPaging: number = 0;
}
