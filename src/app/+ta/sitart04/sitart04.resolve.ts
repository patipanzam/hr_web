import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { LoadMask } from '../../load-mask.component';
import { Sitart04Service } from './sitart04.service';
import { BeanUtils } from '../../core/bean-utils.service';

@Injectable()
export class Sitart04Resolve implements Resolve<any> {
  constructor(private selectService: CommonSelectService, private Sitart04Service: Sitart04Service) {
    this.selectService.setModuleName('ta');
  }

  resolve(route: ActivatedRouteSnapshot): any {
    LoadMask.show();
    if (route.url[0].path == 'sitart04') {
      LoadMask.hide();
      return {
      };

    } else if (route.url[0].path == 'sitart04a') {
      if (BeanUtils.isNotEmpty(route.params['workingId'])) {
        var searchData = this.Sitart04Service.searchData(route.params['workingId']);
        var ouCode = this.selectService.getOu();
        return Observable.forkJoin([searchData,ouCode]).map((response) => {
          LoadMask.hide();
          return {
            searchData: response[0],
            ouCode: response[1]
          };
        }).first();

      } else {
        var ouCode = this.selectService.getOu();
        return Observable.forkJoin([ouCode]).map((response) => {
          LoadMask.hide();
          return {
            ouCode: response[0]
          };
        }).first();
      }
    }
  }
}
