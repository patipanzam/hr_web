import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitart04Service } from './sitart04.service';
import { Sitart04Criteria, Sitart04SaveTaWorking } from '../sitart04/sitart04.interface';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DateUtils } from '../../core/date-utils.service';
import { LoadMask } from '../../load-mask.component';
import { BeanUtils } from '../../core/bean-utils.service';
import { SearchStatus } from '../../shared/constant/common.interface';
import { PageContext } from '../../shared/service/pageContext.service';
@Component({
  selector: 'sitart04',
  template: require('./sitart04.component.html'),
  providers: [Page, Sitart04Service]
})
export class Sitart04Component {

  @ViewChild('alertModal') private alertModal: ModalDirective;

  private form: FormGroup = this.formBuilder.group({
    workingCode: this.formBuilder.control("")
    , workingDesc: this.formBuilder.control("")
    , workingStatus: this.formBuilder.control("")
  });

  private initMsg: string;
  private showMsg = false;
  private records: any[];
  private fromLoadObject: any;
  private objCriteria: Sitart04Criteria = new Sitart04Criteria();
  private alert = { title: '', msg: '' };
  private workingId: any;

  constructor(private page: Page
    , private service: Sitart04Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private formBuilder: FormBuilder
    , private pageContext: PageContext
  ) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
  }

  private childModalShow(record: any) {
    this.pageContext.nextPage(['/ta/sitart04a', record.workingId], this.objCriteria);
  }

  private onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.showMsg = false;
    if (BeanUtils.isNotEmpty(changeEvent)) {
      this.page = changeEvent;
    }
    this.callServiceSearch(changeEvent, this.objCriteria);
  }

  private alertModalToggle(title: string, msg: string): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  private callServiceSearch(page: Page, mapping: Sitart04Criteria) {
    this.service.getData(page, mapping).subscribe(response => {
      this.page = response.page;
      this.records = response.records;
      if (BeanUtils.isNotEmpty(this.records)) {
        LoadMask.hide();
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
      }
    },
      error => {
        LoadMask.hide();
      }
    );
  }

  private onChangePage(changeEvent: Page) {
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private onSearch(modeSearch: number) {
    if (BeanUtils.isNotEmpty(modeSearch)) {
      this.objCriteria.flagForPaging = modeSearch
      this.page.start = 0;
      if (modeSearch == SearchStatus.normalSearch) {
        this.onNormalSearch(this.page);
      }
    }
  }

  private closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }
}

