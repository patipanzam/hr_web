import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { LoadMask } from '../../load-mask.component';
import { Sitart02Service } from './sitart02.service';
import { BeanUtils } from '../../core/bean-utils.service';

@Injectable()
export class Sitart02Resolve implements Resolve<any> {

  constructor(private selectService: CommonSelectService, private sitart02Service: Sitart02Service) {
    this.selectService.setModuleName('ta');
  }

  resolve(route: ActivatedRouteSnapshot): any {
    LoadMask.show();
    if (route.url[0].path == 'sitart02') {

      var leaveType = this.selectService.getLeaveType();
      var leaveGroup = this.selectService.getLeaveGroup();
      var empType = this.selectService.getEmpType();
      var sex = this.selectService.getGender();
      var ouCode = this.selectService.getOu();

      return Observable.forkJoin([leaveType, leaveGroup, empType, sex, ouCode]).map((response) => {
        LoadMask.hide();
        return {
          leaveType: response[0],
          leaveGroup: response[1],
          empType: response[2],
          sex: response[3],
          ouCode: response[4]
        };

      }).first();
    }
    else if (route.url[0].path == 'sitart02a') {

      if (BeanUtils.isNotEmpty(route.params['leaveTypeId'])) {
        let searchData = this.sitart02Service.searchData(route.params['leaveTypeId']);
        let leaveType = this.selectService.getLeaveType();
        let leaveGroup = this.selectService.getLeaveGroup();
        let empType = this.selectService.getEmpType();
        let sex = this.selectService.getGender();
        let ouCode = this.selectService.getOu();
        let incomDeduct = this.selectService.getincome();

        return Observable.forkJoin([leaveType, leaveGroup, empType, sex, ouCode, incomDeduct, searchData]).map((response) => {
          LoadMask.hide();
          return {
            leaveType: response[0],
            leaveGroup: response[1],
            empType: response[2],
            sex: response[3],
            ouCode: response[4],
            incomDeduct: response[5],
            searchData: response[6]
          };

        }).first();
      }
      else {
        let leaveType = this.selectService.getLeaveType();
        let leaveGroup = this.selectService.getLeaveGroup();
        let empType = this.selectService.getEmpType();
        let sex = this.selectService.getGender();
        let ouCode = this.selectService.getOu();
        let incomDeduct = this.selectService.getincome();

        return Observable.forkJoin([leaveType, leaveGroup, empType, sex, ouCode, incomDeduct]).map((response) => {
          LoadMask.hide();
          return {
            leaveType: response[0],
            leaveGroup: response[1],
            empType: response[2],
            sex: response[3],
            ouCode: response[4],
            incomDeduct: response[5],
          };
        }).first();
      }
    }
  }
}



