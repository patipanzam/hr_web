export class Sitart02Criteria {
  public inputSearch: string = '';
  public leaveTypeId: string = '';
  public leaveTypeCode: string = '';
  public leaveGroupId: number;
  public leaveGroupCode: string = '';
  public inChol: number;
  public oneTime: number;
  public sex: string = '';
  public workAge: number;
  public leaveDay: number;
  public leaveAllDay: number;
  public status: number;
  public rate: number;
  public incomeDepartmentId: number;
  public flagForPaging: number = 0;
}

export class Sitart02SaveLeaveType {
  public leaveTypeId: number;
  public ouCode: string;
  public leaveTypeCode: string;
  public leaveTypeDesc: string;
  public leaveGroupId: number;
  public leaveGroupCode: string;
  public incomDeduct: string;
  public leaveEmpId: string;
  public inChol: number;
  public oneTime: number;
  public sex: string;
  public workAge: number;
  public leaveDay: number;
  public leaveAllDay: number;
  public status: number;
  public flagForPaging: number = 0;
  public incomeDepartmentId: number;
  public incomeDeductId: string;
  public incomeDeductCode: string;
  public incomeDeductName: string;
  public rate: number;

  public grid: Sitart02IncomeDeduct[];
}

export class Sitart02IncomeDeduct {
  public leaveTypeId: number;
  public incomeDepartmentId: number;
  public incomeDeductId: number;
  public incomeDescLocal: string;
  public rate: number;
  public ouCode: string;
  rowStatus: string = '';
  rowAdded: boolean = true;

};
