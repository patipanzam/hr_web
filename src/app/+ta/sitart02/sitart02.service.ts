import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { Sitart02Criteria, Sitart02SaveLeaveType } from '../sitart02/sitart02.interface';

const querystring = require('querystring');

@Injectable()
export class Sitart02Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

  //Sitart02 search
  public getData(page: Page, searchValue: Sitart02Criteria): Observable<any> {
    let params = '?' + querystring.stringify(page) + "&" + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('ta/sitart02/searchLeaveType') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });
  }

  //saveLeaveType
  public saveLeaveType(page: Page, params: Sitart02SaveLeaveType): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('ta/sitart02/saveLeaveType'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  //selectLeaveType
  public searchData(leaveTypeId: number): Observable<any> {
    let params = '?leaveTypeId=' + leaveTypeId;
    return this.authHttp.get(this.restServer.getAPI('ta/sitart02/selectLeaveType') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  //deleteLeaveType
  public deleteLeaveType(page: Page, params: Sitart02SaveLeaveType): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('ta/sitart02/deleteLeaveType'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  public getMoney(incomeDeductId: number, leaveTypeId: number): Observable<any> {
    let param = '?incomeDeductId=' + incomeDeductId + '&leaveTypeId=' + leaveTypeId;
    return this.authHttp.get(this.restServer.getAPI('/ta/sitart02/load/payment/money') + param)
      .map(val => { return val.json() });
  }

  public loadHistoryDetail(leaveTypeId: number): Observable<any> {
    let params = `?leaveTypeId=` + leaveTypeId;
    return this.authHttp.get(this.restServer.getAPI('ta/sitart02/load/historyDetail') + params)
      .map(val => { return val.json() });
  }

  private loadHistory(): Observable<any> {
    return this.authHttp.get(this.restServer.getAPI('ta/sitart02/load/history'))
      .map(val => { return val.json() });
  }

}
