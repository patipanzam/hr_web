import { LocalSelectDirective } from '../shared/local-select.directive';
import { Component, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitart02Service } from './sitart02.service';
import { Sitart02SaveLeaveType, Sitart02IncomeDeduct } from '../sitart02/sitart02.interface';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BeanUtils } from '../../core/bean-utils.service';
import { LoadMask } from '../../load-mask.component';
import { LeaveGroupSelectDirective } from '../shared/leave-group-select.directive';
import { LeaveTypeSelectDirective } from '../shared/leave-type-select.directive';
import { EmpTypeSelectDirective } from '../shared/emp-type-select.directive';
import { OuSelectDirective } from '../shared/ou-select.directive';
import { IncomeTypeSelectDirective } from '../shared/income-type-select.directive';
import { PaymentType } from '../../shared/system-type.enum';
import { PageContext } from '../../shared/service/pageContext.service';

@Component({
  selector: 'sitart02a',
  template: require('./sitart02a.component.html'),
  providers: [Page, Sitart02Service]
})
export class Sitart02aComponent implements AfterViewInit {

  @Output('closeModal') private closeEvent = new EventEmitter();
  @ViewChild('deleteModal') private deleteModal: ModalDirective;
  @Input('info') private info: any;
  @ViewChild('alertModal') private alertModal: ModalDirective;
  @ViewChild('backModal') private backModal: ModalDirective;
  @ViewChild('leaveGroupCode') private leaveGroupCode: LeaveGroupSelectDirective;
  @ViewChild('leaveType') private leaveType: LeaveTypeSelectDirective;
  @ViewChild('leaveEmpId') private leaveEmpId: EmpTypeSelectDirective;
  @ViewChild('sex') private sex: LocalSelectDirective;
  @ViewChild('ouCode') private ouCode: OuSelectDirective;
  @ViewChild('packageDataModalGetHistory') private packageDataModalGetHistory: ModalDirective;

  private form: FormGroup = this.fb.group({
    leaveTypeId: this.fb.control("")
    , leaveTypeCode: this.fb.control(null, Validators.required)
    , leaveTypeDesc: this.fb.control("", Validators.required)
    , leaveGroupCode: this.fb.control("", Validators.required)
    , leaveEmpId: this.fb.control("", Validators.required)
    , ouCode: this.fb.control("", Validators.required)
    , inChol: this.fb.control("")
    , oneTime: this.fb.control("")
    , sex: this.fb.control("")
    , workAge: this.fb.control("")
    , leaveDay: this.fb.control("", Validators.required)
    , leaveAllDay: this.fb.control("")
    , status: this.fb.control(null)

  });

  private leaveTypeId: number = null;
  private leaveTypeCode: string;
  private leaveTypeDesc: string;
  private inChol: number;
  private oneTime: number;
  private workAge: string = null;
  private leaveDay: string = null;
  private leaveAllDay: number;
  private status: number = null;
  private ou: string = "";
  private dirty: boolean;
  private routerActiveSubscribe: any;
  private paymentType: PaymentType;
  private nameTitleForm: string;
  private flagForload = 0;

  private fromLoadObject: any;
  private initMsg: string;
  private showMsg = false;
  private objCriteria: Sitart02SaveLeaveType = new Sitart02SaveLeaveType();
  private objDelete: Sitart02SaveLeaveType = new Sitart02SaveLeaveType();
  private income: Sitart02IncomeDeduct = new Sitart02IncomeDeduct();
  private alert = { title: '', msg: '' };
  private records: any[];
  private rows: any[] = [];
  private flagForLoad: boolean = false;
  private data: any;
  private totalAmount: number = 0;
  private radioOneTime: any;
  private radioLeaveAllDay: any;
  private radioInChol: any;
  private historyData: any;

  constructor(private page: Page
    , private fb: FormBuilder
    , private service: Sitart02Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private pageContext: PageContext) {

    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      this.setData(this.fromLoadObject.searchData);
      this.leaveTypeId = this.fromLoadObject.searchData.records.data.leaveTypeId;
    }
  }

  ngAfterViewInit() {
    if (!this.info) {
      this.flagForload = 1;
      if (BeanUtils.isNotEmpty(this.leaveTypeId)) {
        this.ouCode.setValue(this.form.controls["ouCode"].value);
        this.ouCode.disabled(true);
        this.leaveGroupCode.setValue(this.form.controls["leaveGroupCode"].value);
        this.leaveGroupCode.disabled(true);
        this.leaveEmpId.setValue(this.form.controls["leaveEmpId"].value);
        this.leaveEmpId.disabled(false);
        this.sex.setValue(this.form.controls["sex"].value);
        this.sex.disabled(false);
        this.workAge = this.form.controls["workAge"].value;
        this.leaveDay = this.form.controls["leaveDay"].value;
        this.form.controls["oneTime"].setValue(1);
        this.form.controls["leaveAllDay"].setValue(1);
        this.form.controls["inChol"].setValue(1);

        this.flagForload = 0;

        this.service.loadHistoryDetail(this.leaveTypeId).subscribe(
          (response) => {
            console.log(response);
            this.setRow(response.data);
          }, (error) => {
          }
        );

      }
      this.form.controls["status"].setValue(1);
    }
  }

  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  private alertModalClose() {
    if (null != this.records) {
      if (this.router.url.split("/")[2] == "sitart02a") {
        this.pageContext.backPage();
      }
    }
    this.alertModal.toggle();
  }

  private onSave() {
    if (this.form.valid == true) {
      if (this.form.dirty) {
        LoadMask.show();
        if (BeanUtils.isNotEmpty(this.leaveTypeId)) {
          this.objCriteria.leaveTypeId = this.leaveTypeId;
        }

        this.objCriteria.leaveTypeCode = this.form.controls["leaveTypeCode"].value;
        this.objCriteria.leaveTypeDesc = this.form.controls["leaveTypeDesc"].value;
        this.objCriteria.leaveGroupCode = this.form.controls["leaveGroupCode"].value;
        this.objCriteria.leaveEmpId = this.form.controls["leaveEmpId"].value;
        this.objCriteria.leaveDay = this.form.controls["leaveDay"].value;
        this.objCriteria.sex = this.form.controls["sex"].value;
        this.objCriteria.workAge = this.form.controls["workAge"].value;
        this.objCriteria.inChol = this.inChol;
        this.objCriteria.oneTime = this.oneTime;
        this.objCriteria.leaveAllDay = this.leaveAllDay;
        this.objCriteria.ouCode = this.form.controls["ouCode"].value;
        this.objCriteria.status = this.form.controls["status"].value;
        this.showMsg = false;

        this.objCriteria.grid = this.rows;
        this.objCriteria.leaveTypeId = this.leaveTypeId;

        this.service.saveLeaveType(this.page, this.objCriteria).subscribe(response => {
          this.records = response.records.data.leaveTypeId;
          this.leaveTypeId = response.records.data.leaveTypeId;
          if (response.records.data.checkLeaveTypeCode == 0) {
            LoadMask.hide();
            this.alertModalToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ');
            this.ouCode.disabled(true);
            this.form.markAsPristine();
          } else if (response.records.data.checkLeaveTypeCode == 1) {
            LoadMask.hide();
            this.alertModalToggle('แจ้งเตือน', 'รหัสประเภทพนักงานซ้ำ');
          } else {
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้');
          }
        },
          error => {
            console.error(error);
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ');
          }
        );
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข');
      }
    } else {
      LoadMask.hide();
      this.alertModalToggle('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
    }
  }

  private setData(object: any) {
    if (BeanUtils.isNotEmpty(object)) {
      object.records.data = object.records.data || {};
      this.oneTime = (object.records.data.oneTime == null) ? null : object.records.data.oneTime.toString();
      this.leaveAllDay = (object.records.data.leaveAllDay == null) ? null : object.records.data.leaveAllDay.toString();
      this.inChol = (object.records.data.inChol == null) ? null : object.records.data.inChol.toString();
      this.form.controls["leaveTypeCode"].setValue(object.records.data.leaveTypeCode);
      this.form.controls["leaveTypeDesc"].setValue(object.records.data.leaveTypeDesc);
      this.form.controls["leaveGroupCode"].setValue(object.records.data.leaveGroupCode);
      this.form.controls["leaveEmpId"].setValue(object.records.data.leaveEmpId);
      this.form.controls["leaveDay"].setValue(object.records.data.leaveDay);
      this.form.controls["sex"].setValue(object.records.data.sex);
      this.form.controls["workAge"].setValue(object.records.data.workAge);
      this.form.controls["inChol"].setValue(object.records.data.inChol);
      this.form.controls["oneTime"].setValue(object.records.data.oneTime);
      this.form.controls["leaveAllDay"].setValue(object.records.data.leaveAllDay);
      this.form.controls["ouCode"].setValue(object.records.data.ouCode);
      this.form.controls["status"].setValue(object.records.data.status);
      this.form.controls["leaveTypeId"].setValue(object.records.data.leaveTypeId);
      this.radioOneTime = this.oneTime;
      this.radioLeaveAllDay = this.leaveAllDay;
      this.radioInChol = this.inChol;
    }
  }

  private onBack() {
    this.checkDirty();
    if (this.form.dirty == true) {
      this.backModalToggle('แจ้งเตือน', 'ข้อมูลได้รับการแก้ไข ต้องการเปลี่ยนหน้าหรือไม่ ?');
    } else {
      this.pageContext.backPage();
    }
  }

  private backModalClose() {
    this.backModal.hide();
  }

  private backModalBack() {
    this.pageContext.backPage();
  }

  private backModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;

    this.backModal.toggle();
  }
  private onCancel() {
    this.closeEvent.emit();
  }

  private deleteModalClose() {
    this.deleteModal.hide();
  }

  private deleteModalConfirm() {
    this.onDelete();
    this.deleteModal.hide();
  }

  private alertModalDeleteToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.deleteModal.toggle();
  }

  private onBeforeDelete(record: any) {
    this.alertModalDeleteToggle('ยืนยันการลบ', 'คุณยืนยันการลบหรือไม่');
  }

  private checkDirty() {
    if (this.form.dirty == true) {
      this.dirty = true;
    } else {
      this.dirty = false;
    }
    return this.dirty;
  }

  private onDelete() {
    LoadMask.show();
    this.objDelete.leaveTypeId = this.leaveTypeId
    this.service.deleteLeaveType(this.page, this.objDelete).subscribe(response => {
      this.alertModalToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ');
      LoadMask.hide();
      this.pageContext.backPage();
    },
      error => {
        console.error(error);
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ');
      }
    );
  }

  private changCheckbox(event: any) {
    if (event.target.checked) {
      this.form.controls["status"].setValue(1);
    } else {
      this.form.controls["status"].setValue(0);
    }
  }

  private changeLeaveGroup(valueField: any) {
    this.form.controls["leaveGroupCode"].setValue(valueField.leaveGroupCode);
  }

  private changeEmpType(valueField: any) {
    this.form.controls["leaveEmpId"].setValue(valueField.empTypeCode);
  }

  private changeSex(valueField: any) {
    this.form.controls["sex"].setValue(valueField.code);
    if (this.flagForload != 1) {
      this.form.controls["sex"].markAsDirty();
    }
  }

  private changeOuCode(valueField: any) {
    this.form.controls["ouCode"].setValue(valueField.ouCode);
  }

  private changOneTimeNot(valueField: any) {
    this.oneTime = 0;
    if (this.oneTime == this.radioOneTime) {
      this.form.controls["oneTime"].reset();
    } else {
      this.form.controls["oneTime"].markAsDirty(true);
    }
  }

  private changOneTimeYes(valueField: any) {
    this.oneTime = 1;
    if (this.oneTime == this.radioOneTime) {
      this.form.controls["oneTime"].reset();
    } else {
      this.form.controls["oneTime"].markAsDirty(true);
    }
  }

  private changLeaveAllDayNot(valueField: any) {
    this.leaveAllDay = 0;
    if (this.leaveAllDay == this.radioLeaveAllDay) {
      this.form.controls["leaveAllDay"].reset();
    } else {
      this.form.controls["leaveAllDay"].markAsDirty(true);
    }
  }

  private changLeaveAllDayYes(valueField: any) {
    this.leaveAllDay = 1;
    if (this.leaveAllDay == this.radioLeaveAllDay) {
      this.form.controls["leaveAllDay"].reset();
    } else {
      this.form.controls["leaveAllDay"].markAsDirty(true);
    }
  }

  private changInCholNot(valueField: any) {
    this.inChol = 0;
    if (this.inChol == this.radioInChol) {
      this.form.controls["inChol"].reset();
    } else {
      this.form.controls["inChol"].markAsDirty(true);
    }
  }

  private changInCholYes(valueField: any) {
    this.inChol = 1;
    if (this.inChol == this.radioInChol) {
      this.form.controls["inChol"].reset();
    } else {
      this.form.controls["inChol"].markAsDirty(true);
    }
  }

  private addRow() {
    this.rows.push(new Sitart02IncomeDeduct());
  }

  private delRow(record: Sitart02IncomeDeduct) {
    if (!record.incomeDeductId) {
      this.rows.splice(this.rows.indexOf(record), 1);
    } else if (record.rowStatus != 'D') {
      record.rowStatus = 'D';
    } else if (record.incomeDepartmentId) {
      record.rowStatus = 'M';
    } else {
      record.rowStatus = 'A';
    }
    this.calcurate();
  }

  private changeRate(valueField: any, record: Sitart02IncomeDeduct) {
    if (record.incomeDepartmentId) {
      record.rowStatus = 'M';
    }

    record.rate = +valueField;
    this.calcurate();
  }

  private calcurate() {
    this.totalAmount = 0;
    for (let record of this.rows) {
      if (record.rowStatus != 'D') {
        this.totalAmount += (+record.rate) || 0;
      }
    }
  }

  private setRow(datas: any[]) {
    let row: Sitart02IncomeDeduct[] = [];
    if (datas.length === 0) {
      row.push(new Sitart02IncomeDeduct());
    } else {
      for (let data of datas) {
        row.push({
          incomeDepartmentId: data.incomeDepartmentId,
          incomeDeductId: data.incomeDeductId,
          leaveTypeId: data.leaveTypeId,
          incomeDescLocal: data.incomeDescLocal,
          ouCode: data.ouCode,
          rate: data.rate || 0.00,
          rowStatus: (!data.incomeDepartmentId) ? 'A' : (data.incomeDepartmentId) ? 'M' : '',
          rowAdded: false
        });
      }
    }
    this.rows = row;
    this.calcurate();
  }

  private findIncomeDeduct(data: any) {
    this.fromLoadObject.income.find((obj: any) => {
      if (obj.incomDeductId == data.incomDeductId) {
        data.incomDeductId = obj.incomDeductId;
        data.incomeDescLocal = obj.incomeDescLocal
        data.ouCode = obj.from.controls["ouCode"].value
        data.incomeDepartmentId = obj.incomeDepartmentId
        data.rate = obj.rate
      }
      return;
    });
  }

  private getHistoryDetail(leaveTypeId: number) {
    this.packageDataModalGetHistory.hide();
    this.service.loadHistoryDetail(leaveTypeId).subscribe(
      (response) => {
        this.addRow();
      }, (error) => {
      }
    );
  }

  private getHistory(leaveTypeId: number) {
    this.service.loadHistoryDetail(leaveTypeId).subscribe(
      (response) => {
        this.setRow(response.data);
      }, (error) => {
      }
    );
  }

  private changeIncomDeductId(valueField: any, record: Sitart02IncomeDeduct) {
    if (!this.rows.find((obj) => { return (obj.incomeDeductId) && (obj.incomeDeductId == valueField.incomeDeductId) })) {

      this.income.incomeDeductId = valueField.incomeDeductId;
      this.income.incomeDescLocal = valueField.incomeDescLocal;
      record.ouCode = this.form.controls["ouCode"].value;
      record.incomeDeductId = valueField.incomeDeductId;
      record.incomeDescLocal = valueField.incomeDescLocal;
      record.rate = valueField.rate;
      record.leaveTypeId = this.leaveTypeId;
      record.rowStatus = (this.leaveTypeId) ? '' : 'A';

      this.service.getMoney(+this.income.incomeDeductId, +this.leaveTypeId)
        .subscribe(response => {
          console.log(response);
          this.changeRate(response.data != null ? response.data : 0, record);
        }, (error) => {
          console.log('error, ', error);
          LoadMask.hide();
        });
    } else {
      this.alertModalToggle('ผิดพลาด', 'ข้อมูลซ้ำ');
      this.rows.splice(this.rows.indexOf(record), 1, new Sitart02IncomeDeduct());
      this.changeRate(String(record.rate), record);
    }
  }
}

