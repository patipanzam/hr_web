import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitart02Service } from './sitart02.service';
import { Sitart02Criteria } from '../sitart02/sitart02.interface';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { BeanUtils } from '../../core/bean-utils.service';
import { SearchStatus } from '../../shared/constant/common.interface';
import { PageContext } from '../../shared/service/pageContext.service';
import { LeaveGroupSelectDirective } from '../shared/leave-group-select.directive';
import { LeaveTypeSelectDirective } from '../shared/leave-type-select.directive';

@Component({
  selector: 'sitart02',
  template: require('./sitart02.component.html'),
  providers: [Page, Sitart02Service]
})
export class Sitart02Component implements OnInit {

  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('leaveType') public leaveType: LeaveTypeSelectDirective;
  @ViewChild('leaveGroup') public leaveGroup: LeaveGroupSelectDirective;

  private form: FormGroup = this.formBuilder.group({

    leaveTypeCode: this.formBuilder.control("")
    , leaveTypeDesc: this.formBuilder.control("")
    , leaveGroupCode: this.formBuilder.control("")
    , leaveGroup: this.formBuilder.control("")
    , leaveType: this.formBuilder.control("")

  });

  private initMsg: string;
  private showMsg = false;
  private records: any[];
  private fromLoadObject: any;
  private objCriteria: Sitart02Criteria = new Sitart02Criteria();
  private alert = { title: '', msg: '' };

  private checkDelete: number = 0;
  private leaveTypeId: any;

  constructor(private page: Page
    , private service: Sitart02Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private formBuilder: FormBuilder
    , private pageContext: PageContext
  ) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  private onActive() {

  }

  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
  }

  public childModalShow(record: any) {
    this.pageContext.nextPage(['/ta/sitart02a', record.leaveTypeId], this.objCriteria);
  }

  public createLeaveType() {
    let leaveTypeId: any = "";
    this.pageContext.nextPage(['/ta/sitart02a', leaveTypeId], this.objCriteria);
  }

  public onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.showMsg = false;
    this.callServiceSearch(changeEvent, this.objCriteria);
  }
  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }
  private callServiceSearch(page: Page, mapping: Sitart02Criteria) {
    this.service.getData(page, mapping).subscribe(response => {
      this.page = response.page;
      this.records = response.records;
      if (BeanUtils.isNotEmpty(this.records)) {
        LoadMask.hide();
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
      }
    },
      error => {
        console.error(error);
        LoadMask.hide();
      }
    );
  }

  public onChangePage(changeEvent: Page) {
    this.page = changeEvent;
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private changeLeaveGroup(valueField: any) {
    this.objCriteria.leaveGroupCode = valueField.leaveGroupCode;
  }

  private changeLeaveType(valueField: any) {
    this.objCriteria.leaveTypeCode = valueField.leaveTypeCode;
  }

  private onSearch(modeSearch: number) {
    if (this.form.valid == true) {
      if (BeanUtils.isNotEmpty(modeSearch)) {
        this.objCriteria.flagForPaging = modeSearch
        this.page.start = 0;
        if (modeSearch == SearchStatus.normalSearch) {
          this.onNormalSearch(this.page);
        }
      }
    }
  }

  public closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }
}

