import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { Sitart05Criteria, Sitart05SaveHoliday } from '../sitart05/sitart05.interface';
import { RestServerService } from '../../core/rest-server.service';

const querystring = require('querystring');

@Injectable()
export class Sitart05Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

  //searchHoliday
  public getData(page: Page, searchValue: Sitart05Criteria): Observable<any> {
    let params = '?' + querystring.stringify(page) + "&" + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('/ta/sitart05/searchHoliday') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });
  }

  //selectHoliday
  public searchData(holidayId: number): Observable<any> {
    let params = '?holidayId=' + holidayId;
    return this.authHttp.get(this.restServer.getAPI('/ta/sitart05/selectHoliday') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  //saveHoliday
  public saveHoliday(page: Page, params: Sitart05SaveHoliday): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitart05/saveHoliday'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  //deleteHoliday
  public deleteHoliday(page: Page, params: Sitart05SaveHoliday): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitart05/deleteHoliday'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }
}
