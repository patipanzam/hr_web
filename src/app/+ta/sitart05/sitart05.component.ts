import { BeanUtils } from '../../core/bean-utils.service';
import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { DateUtils } from '../../core/date-utils.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchStatus } from '../../shared/constant/common.interface';
import { Sitart05Criteria, Sitart05SaveHoliday } from '../sitart05/sitart05.interface';
import { Sitart05Service } from './sitart05.service';
import { SSDatePicker } from '../../shared/ss-datepicker.component';

@Component({
  selector: 'sitart05',
  template: require('./sitart05.component.html'),
  providers: [Page, Sitart05Service]
})

export class Sitart05Component implements OnInit {

  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('holidayDate') public holidayDate: SSDatePicker;

  private form: FormGroup = this.formBuilder.group({
    ouCode: this.formBuilder.control("")
    , holidayDate: this.formBuilder.control("")
    , holidayDesc: this.formBuilder.control("")
  });

  private holidayId: any;
  private alert = { title: '', msg: '' };
  private checkDelete: number = 0;
  private fromLoadObject: any;
  private initMsg: string;
  private objCriteria: Sitart05Criteria = new Sitart05Criteria();
  private showMsg = false;
  private records: any[];

  constructor(private formBuilder: FormBuilder
    , private page: Page
    , private pageContext: PageContext
    , private service: Sitart05Service
    , private router: Router
    , private routerActive: ActivatedRoute) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (this.objCriteria.holidayDate) {
        this.holidayDate.setValue(this.objCriteria.holidayDate);
      }
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
  }

  private childModalShow(record: any) {
    this.pageContext.nextPage(['/ta/sitart05a', record.holidayId], this.objCriteria);
  }

  private createHoliday() {
    let holidayId: any = "";
    this.pageContext.nextPage(['/ta/sitart05a', holidayId], this.objCriteria);
  }

  private changeHolidayDate(valueField: any) {
    if (valueField) {
      this.form.controls["holidayDate"].setValue(this.holidayDate.getMilliSecond());
      this.objCriteria.obligationsHolidayDate = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsHolidayDateShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
    }
  }

  // About Search
  private onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.objCriteria.holidayDate = this.holidayDate.getMilliSecond();
    this.showMsg = false;
    this.callServiceSearch(changeEvent, this.objCriteria);
  }

  private callServiceSearch(page: Page, mapping: Sitart05Criteria) {
    this.service.getData(page, mapping)
      .subscribe(response => {
        this.page = response.page;
        this.records = response.records;
        if (BeanUtils.isNotEmpty(this.records)) {
          LoadMask.hide();
        } else {
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
        }
      },
      error => {
        console.error(error);
        LoadMask.hide();
      }
      );
  }

  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  private onChangePage(changeEvent: Page) {
    this.page = changeEvent;
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private onSearch(modeSearch: number) {
    if (this.form.valid == true) {
      if (BeanUtils.isNotEmpty(modeSearch)) {
        this.objCriteria.flagForPaging = modeSearch
        this.page.start = 0;
        if (modeSearch == SearchStatus.normalSearch) {
          this.onNormalSearch(this.page);
        }
      }
    }
  }

  private closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }
}
