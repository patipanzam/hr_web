import { BeanUtils } from '../../core/bean-utils.service';
import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit, } from '@angular/core';
import { DateUtils } from '../../core/date-utils.service';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { LocalSelectDirective } from '../shared/local-select.directive';
import { ModalDirective } from 'ng2-bootstrap';
import { OuSelectDirective } from '../shared/ou-select.directive';
import { Page } from '../../shared/pagination.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Sitart05Criteria, Sitart05SaveHoliday } from '../sitart05/sitart05.interface';
import { Sitart05Service } from './sitart05.service';
import { SSDatePicker } from '../../shared/ss-datepicker.component';

@Component({
  selector: 'sitart05a',
  template: require('./sitart05a.component.html'),
  providers: [Page, Sitart05Service]
})

export class Sitart05aComponent implements AfterViewInit {

  @Input('info') private info: any;
  @Output('closeModal') private closeEvent = new EventEmitter();
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('backModal') public backModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;
  @ViewChild('holidayDate') public holidayDate: SSDatePicker;
  @ViewChild('ouCode') public ouCode: OuSelectDirective;

  private form: FormGroup = this.fb.group({
    holidayId: this.fb.control(null)
    , ouCode: this.fb.control("", Validators.required)
    , holidayDate: this.fb.control(null, Validators.required)
    , holidayDesc: this.fb.control("", Validators.required)
  });

  private holidayId: number = null;
  private alert = { title: '', msg: '', isRedirect: false };
  private data: any;
  private dirty: boolean;
  private flagForLoad: boolean = false;
  private fromLoadObject: any;
  private initMsg: string;
  private objCriteria: Sitart05SaveHoliday = new Sitart05SaveHoliday();
  private objDelete: Sitart05SaveHoliday = new Sitart05SaveHoliday();
  private records: any[];
  private showMsg = false;

  constructor(private fb: FormBuilder
    , private page: Page
    , private pageContext: PageContext
    , private router: Router
    , private routerActive: ActivatedRoute
    , private service: Sitart05Service) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      this.setData(this.fromLoadObject.searchData);
    }
  }

  ngAfterViewInit() {
    if (BeanUtils.isNotEmpty(this.holidayId)) {
      this.flagForLoad = true;
      this.ouCode.setValue(this.form.controls["ouCode"].value);
      this.ouCode.disabled(true);
      this.holidayDate.setValue(this.fromLoadObject.searchData.records.data.holidayDate);
      this.holidayDate.readOnly = true;
      this.flagForLoad = false;
    }
  }

  private changeOuCode(valueField: any) {
    this.form.controls["ouCode"].setValue(valueField.ouCode);
    if (this.flagForLoad == false) {
      this.form.controls["ouCode"].markAsDirty(true);
    }
  }

  private changeHolidayDateA(valueField: any) {
    if (valueField) {
      this.form.controls["holidayDate"].setValue(this.holidayDate.getMilliSecond());
      this.objCriteria.obligationsHolidayDateA = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsHolidayDateShowA = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
      if (this.flagForLoad == false) {
        this.form.controls["holidayDate"].markAsDirty(true);
      }
    }
  }

  private checkDirty() {
    if (this.form.dirty == true) {
      this.dirty = true;
    } else {
      this.dirty = false;
    }
    return this.dirty;
  }

  private alertModalToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.alertModal.show();
  }

  private alertModalClose() {
    if (this.records != null) {
      if (this.router.url.split("/")[2] == "sitart05a") {
        this.pageContext.backPage();
      }
    }
    this.alertModal.toggle();
  }

  private onSave() {
    if (this.form.valid == true) {
      if (this.form.dirty) {
        LoadMask.show();
        if (BeanUtils.isNotEmpty(this.holidayId)) {
          this.objCriteria.holidayId = this.holidayId;
        }
        this.objCriteria.ouCode = this.form.controls["ouCode"].value;
        this.objCriteria.holidayDate = this.form.controls["holidayDate"].value;
        this.objCriteria.holidayDesc = this.form.controls["holidayDesc"].value;
        this.showMsg = false;
        this.service.saveHoliday(this.page, this.objCriteria).subscribe(response => {
          this.records = response.records.data.holidayId;
          this.holidayId = response.records.data.holidayId;
          if (response.records.data.checkHolidayDate == 0) {
            LoadMask.hide();
            this.alertModalToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ', true);
            this.ouCode.disabled(true);
            this.holidayDate.readOnly = true;
            this.form.markAsPristine();
          } else if (response.records.data.checkHolidayDate == 1) {
            LoadMask.hide();
            this.alertModalToggle('แจ้งเตือน', 'วันที่ซ้ำ', false);
          } else {
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้', false);
          }
        },
          error => {
            console.error(error);
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
          }
        );
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข', false);
      }
    } else {
      LoadMask.hide();
      this.alertModalToggle('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล', false);
    }
  }

  private setData(object: any) {
    if (BeanUtils.isNotEmpty(object)) {
      this.form.controls["ouCode"].setValue(object.records.data.ouCode);
      this.form.controls["holidayDate"].setValue(object.records.data.holidayDate);
      this.form.controls["holidayDesc"].setValue(object.records.data.holidayDesc);
      this.holidayId = object.records.data.holidayId;
      this.objCriteria.holidayDate = this.fromLoadObject.searchData.records.data.holidayDate;
    }
  }

  private onBack() {
    this.checkDirty();
    if (this.form.dirty == false) {
      this.backModalBack();
    } else {
      this.backModalToggle('แจ้งเตือน', 'ข้อมูลถูกแก้ไข ต้องการเปลี่ยนหน้าใช่หรือไม่', false);
    }
  }

  private backModalClose() {
    this.backModal.hide();
  }

  private backModalBack() {
    this.pageContext.backPage();
  }

  private backModalToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.backModal.toggle();
  }

  private onCancel() {
    this.closeEvent.emit();
  }

  private deleteModalClose() {
    this.deleteModal.hide();
  }

  private deleteModalConfirm() {
    this.onDelete();
    this.deleteModal.hide();
  }

  private alertModalDeleteToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.deleteModal.toggle();
  }

  private onBeforeDelete(record: any) {
    this.alertModalDeleteToggle('ยืนยันการลบ', 'ต้องการลบข้อมูลใช่หรือไม่', true);
    this.holidayId = record.holidayId
  }

  private onDelete() {
    LoadMask.show();
    this.objDelete.holidayId = this.holidayId
    this.service.deleteHoliday(this.page, this.objDelete).subscribe(response => {
      this.alertModalToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ', true);
      LoadMask.hide();
      this.pageContext.backPage();
    },
      error => {
        console.error(error);
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
      }
    );
  }
}
