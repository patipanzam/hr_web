import { BeanUtils } from '../../core/bean-utils.service';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { Injectable } from '@angular/core';
import { LoadMask } from '../../load-mask.component';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Sitart05Service } from './sitart05.service';

@Injectable()
export class Sitart05Resolve implements Resolve<any> {

  constructor(private selectService: CommonSelectService, private sitart05Service: Sitart05Service) {
    this.selectService.setModuleName('ta');
  }

  resolve(route: ActivatedRouteSnapshot): any {
    LoadMask.show();
    if (route.url[0].path == 'sitart05') {
      LoadMask.hide();
      return {

      };
    } else if (route.url[0].path == 'sitart05a') {

      if (BeanUtils.isNotEmpty(route.params['holidayId'])) {
        var searchData = this.sitart05Service.searchData(route.params['holidayId']);
        var ouCode = this.selectService.getOu();
        return Observable.forkJoin([searchData, ouCode]).map((response) => {
          LoadMask.hide();
          return {
            searchData: response[0],
            ouCode: response[1]
          };
        }).first();
      } else {
        var ouCode = this.selectService.getOu();
        return Observable.forkJoin([ouCode]).map((response) => {
          LoadMask.hide();
          return {
            ouCode: response[0]
          };
        }).first();
      }
    }
  }
}
