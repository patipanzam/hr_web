export class Sitart05Criteria {
  public inputSearch: string = '';
  public ouCode: string;
  public holidayDate: number;
  public holidayDesc: string;
  public flagForPaging: number = 0;
  public obligationsHolidayDate: any = '';
  public obligationsHolidayDateShow: any = '';
}

export class Sitart05SaveHoliday {
  public holidayId: number;
  public ouCode: string;
  public holidayDate: number;
  public holidayDesc: string;
  public flagForPaging: number = 0;
  public obligationsHolidayDateA: any = '';
  public obligationsHolidayDateShowA: any = '';
}
