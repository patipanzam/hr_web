import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { Sitart01Criteria, Sitart01SaveLeaveGroup } from '../sitart01/sitart01.interface';

const querystring = require('querystring');
@Injectable()
export class Sitart01Service {
  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }
  public getData(page: Page, searchValue: Sitart01Criteria): Observable<any> {
    let params = '?' + querystring.stringify(page) + "&" + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('ta/sitart01/searchLeaveGroup') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total
            ,start: page.start
            ,limit: page.limit
          } ,records: val.json().data
        }
      });
  }
  public saveLeaveGroup(page: Page, params: Sitart01SaveLeaveGroup): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('ta/sitart01/saveLeaveGroup'), params)
      .map(val => {
        return {
          page: {
            total: 100
            ,start: page.start
            ,limit: page.limit
          } ,records: val.json()
        }
      });
  }
  public searchData(leaveGroupId: number): Observable<any> {
    let params = '?leaveGroupId=' + leaveGroupId;
    return this.authHttp.get(this.restServer.getAPI('ta/sitart01/selectLeaveGroup') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }
  public deleteLeaveGroupe(page: Page, params: Sitart01SaveLeaveGroup): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('ta/sitart01/deleteLeaveGroup'), params)
      .map(val => {
        return {
          page: {
            total: 100
            ,start: page.start
            ,limit: page.limit
          } ,records: val.json()
        }
      });
  }
}
