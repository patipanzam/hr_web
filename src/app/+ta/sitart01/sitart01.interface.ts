export class Sitart01Criteria {
  public ouCode: number;
  public inputSearch: string = '';
  public leaveGroupCode: string;
  public leaveGroupDesc: string;
  public status: number;
  public flagForPaging: number = 0;
}
export class Sitart01SaveLeaveGroup {
  public ouCode: number;
  public leaveGroupId: number;
  public leaveGroupCode: string;
  public leaveGroupDesc: string;
  public status: number;
  public flagForPaging: number = 0;
}
