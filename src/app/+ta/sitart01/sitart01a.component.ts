import { Component, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitart01Service } from './sitart01.service';
import { Sitart01SaveLeaveGroup } from '../sitart01/sitart01.interface';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BeanUtils } from '../../core/bean-utils.service';
import { LoadMask } from '../../load-mask.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { OuSelectDirective } from "../../+ta/shared/ou-select.directive";

@Component({
  selector: 'sitart01a',
  template: require('./sitart01a.component.html'),
  providers: [Page, Sitart01Service]
})
export class Sitart01aComponent implements AfterViewInit {
  @Output('closeModal') private closeEvent = new EventEmitter();
  @Input('info') private info: any;
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;
  @ViewChild('backModal') public backModal: ModalDirective;
  @ViewChild('ouCode') public ouCode: OuSelectDirective;
  private form: FormGroup = this.fb.group({
    ouCode: this.fb.control(""),
    leaveGroupId: this.fb.control(null),
    leaveGroupCode: this.fb.control(null, Validators.required),
    leaveGroupDesc: this.fb.control("", Validators.required),
    status: this.fb.control(null)
  });
  private leaveGroupId: number = null;
  private leaveGroupCode: string = "";
  private leaveGroupDesc: string = "";
  private status: number = null;
  private fromLoadObject: any;
  private initMsg: string;
  private showMsg = false;
  private objCriteria: Sitart01SaveLeaveGroup = new Sitart01SaveLeaveGroup();
  private objDelete: Sitart01SaveLeaveGroup = new Sitart01SaveLeaveGroup();
  private alert = { title: '', msg: '' };
  private records: any[];

  constructor(private page: Page
    , private fb: FormBuilder
    , private service: Sitart01Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private pageContext: PageContext) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      this.setData(this.fromLoadObject.searchData);
    }
  }
  ngAfterViewInit() {
    if (BeanUtils.isNotEmpty(this.leaveGroupId)) {
      this.ouCode.setValue(this.form.controls["ouCode"].value);
      this.ouCode.disabled(true);
    }
    this.form.controls["status"].setValue(1);
  }
  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }
  private alertModalClose() {
    if (this.records != null) {
      if (this.router.url.split("/")[2] == "sitart01a") {
        this.pageContext.backPage();
      }
    }
    this.alertModal.hide();
  }
  private onSave() {
    if (this.form.valid == true) {
      if (this.form.dirty) {
        LoadMask.show();
        if (BeanUtils.isNotEmpty(this.leaveGroupId)) {
          this.objCriteria.leaveGroupId = this.leaveGroupId;
        }
        this.objCriteria.ouCode = this.form.controls["ouCode"].value;
        this.objCriteria.leaveGroupCode = this.form.controls["leaveGroupCode"].value;
        this.objCriteria.leaveGroupDesc = this.form.controls["leaveGroupDesc"].value;
        this.objCriteria.status = this.form.controls["status"].value;
        this.showMsg = false;
        this.service.saveLeaveGroup(this.page, this.objCriteria).subscribe(response => {
          this.records = response.records.data.leaveGroupId;
          this.leaveGroupId = response.records.data.leaveGroupId;
          if (response.records.data.checkLeaveGroupCode == 0) {
            LoadMask.hide();
            this.alertModalToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ');
            this.ouCode.disabled(true);
            this.form.markAsPristine();
          } else if (response.records.data.checkLeaveGroupCode == 1) {
            LoadMask.hide();
            this.alertModalToggle('แจ้งเตือน', 'รหัสกลุ่มการลาซ้ำ');
          } else {
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้');
          }
        },
          error => {
            console.error(error);
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ');
          }
        );
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข');
      }
    } else {
      LoadMask.hide();
      this.alertModalToggle('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
    }
  }
  private setData(object: any) {
    if (BeanUtils.isNotEmpty(object)) {
      this.form.controls["ouCode"].setValue(object.records.data.ouCode);
      this.form.controls["leaveGroupCode"].setValue(object.records.data.leaveGroupCode);
      this.form.controls["leaveGroupDesc"].setValue(object.records.data.leaveGroupDesc);
      this.form.controls["status"].setValue(object.records.data.status);
      this.leaveGroupId = object.records.data.leaveGroupId
    }
  }
  private onBack() {
    if (this.form.dirty == true) {
      this.backModalToggle('แจ้งเตือน', 'ข้อมูลได้รับการแก้ไข ต้องการเปลี่ยนหน้าหรือไม่ ?');
    } else {
      this.pageContext.backPage();
    }
  }
  private backModalClose() {
    this.backModal.hide();
  }
  private backModalBack() {
    this.pageContext.backPage();
  }
  private backModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.backModal.toggle();
  }
  private onCancel() {
    this.closeEvent.emit();
  }
  private deleteModalClose() {
    this.deleteModal.hide();
  }
  private deleteModalConfirm() {
    this.onDelete();
    this.deleteModal.hide();
  }
  private alertModalDeleteToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.deleteModal.toggle();
  }
  private onBeforeDelete(record: any) {
    this.alertModalDeleteToggle('ยืนยันการลบ', 'คุณยืนยันการลบหรือไม่');
  }
  private onDelete() {
    LoadMask.show();
    this.objDelete.leaveGroupId = this.leaveGroupId
    this.service.deleteLeaveGroupe(this.page, this.objDelete).subscribe(response => {
      this.alertModalToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ');
      LoadMask.hide();
      this.pageContext.backPage();
    }
      , error => {
        console.error(error);
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'เนื่องจากข้อมูลถูกใช้งานอยู่กรุณาตรวจสอบข้อมูลที่ต้องการลบ');
      }
    );
  }
  private changeOuCode(valueField: any) {
    this.form.controls["ouCode"].setValue(valueField.ouCode);
  }
  private changCheckbox(event: any) {
    if (event.target.checked) {
      this.form.controls["status"].setValue(1);
    } else {
      this.form.controls["status"].setValue(0);
    }
  }
}
