import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { Sitart01Service } from './sitart01.service';
import { Sitart01Criteria} from '../sitart01/sitart01.interface';
import { FormBuilder,FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { BeanUtils } from '../../core/bean-utils.service';
import { SearchStatus } from '../../shared/constant/common.interface';
import { PageContext } from '../../shared/service/pageContext.service';

@Component({
  selector: 'sitart01',
  template: require('./sitart01.component.html'),
  providers: [Page, Sitart01Service]
})
export class Sitart01Component implements OnInit {
  @ViewChild('alertModal') public alertModal: ModalDirective;
  private form: FormGroup = this.formBuilder.group({
    ouCode: this.formBuilder.control(""),
    leaveGroupCode: this.formBuilder.control(""),
    leaveGroupDesc: this.formBuilder.control(""),
    status: this.formBuilder.control("")
  });

  private initMsg: string;
  private showMsg = false;
  private records: any[];
  private fromLoadObject: any;
  private objCriteria: Sitart01Criteria = new Sitart01Criteria();
  private alert = { title: '', msg: '' };
  private checkDelete: number = 0;
  private leaveGroupId: any;
  private createLeaveGroup() {
    let leaveGroupId: any = "";
    this.pageContext.nextPage(['/ta/sitart01a', leaveGroupId], this.objCriteria);
  }
  constructor(private page: Page,
    private service: Sitart01Service,
    private router: Router,
    private routerActive: ActivatedRoute,
    private formBuilder: FormBuilder,
    private pageContext: PageContext) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }
  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
  }
   private onActive() {
    this.onSearch(0);
  }
  private childModalShow(record: any) {
    this.pageContext.nextPage(['/ta/sitart01a', record.leaveGroupId], this.objCriteria);
  }
  private onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.showMsg = false;
    this.callServiceSearch(changeEvent, this.objCriteria);
  }
  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }
  private callServiceSearch(page: Page, mapping: Sitart01Criteria) {
    this.service.getData(page, mapping).subscribe(response => {
      this.page = response.page;
      this.records = response.records;
      if (BeanUtils.isNotEmpty(this.records)) {
        LoadMask.hide();
      } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
      }
    },
      error => {
        console.error(error);
        LoadMask.hide();
      }
    );
  }
  private onChangePage(changeEvent: Page) {
    this.page = changeEvent;
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }
  private onSearch(modeSearch: number) {
    if (this.form.valid == true) {
      if (BeanUtils.isNotEmpty(modeSearch)) {
        this.objCriteria.flagForPaging = modeSearch
        this.page.start = 0;
        if (modeSearch == SearchStatus.normalSearch) {
          this.onNormalSearch(this.page);
        }
      }
    }
  }
  private closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }
}
