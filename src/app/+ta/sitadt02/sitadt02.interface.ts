export class Sitadt02Criteria {
  public inputSearch: string = '';
  public ouCode: string = '';
  public empId: number;
  public empCode: string = '';
  public empThaiName: string = '';

  public startDate: number;
  public endDate: number;
  public departmentIdStart: number;
  public departmentIdEnd: number;
  public departmentCodeStart: string = '';
  public departmentCodeEnd: string = '';
  public employeeStart: string = '';
  public employeeEnd: string = '';

  public flagForPaging: number = 0;
  public obligationsOverTimeDate: any = '';
  public obligationsOverTimeDateShow: any = '';
  public overTimeStartDate: number;
  public overTimeEndDate: number;
  public overTimeStartDateA: number;
  public overTimeEndDateA: number;

public otBeforeWorkStart: string;
 public otBeforeWorkEnd: string;
 public otBeforeBreakStart: string;
 public otBeforeBreakEnd: string;
 public otAfterBreakStart: string;
 public otAfterBreakEnd: string;
 public otAfterWorkStart: string;
 public otAfterWorkEnd: string;




}

export class Sitadt02SaveOverTime {
  public employeeStart: string ;
  public employeeEnd: string ;
  public overTimeId: number;
  public ouCode: string = '';
  public empId: number;
  public empCode: string = '';
  public empThaiName: string = '';

  public startDateA: number;
  public endDateA: number;

  public flagForPaging: number = 0;
  public departmentIdStartA: number;
  public departmentIdEndA: number;
  public departmentCodeStartA: string = '';
  public departmentCodeEndA: string = '';
  public obligationsoverTimeStartDateA : any = '';
  public obligationsoverTimeStartDateShowA: any = '';
  public obligationsoverTimeEndDateA: any = '';
  public obligationsoverTimeEndDateShowA: any = '';
  public overTimeStartDateA: number;
  public overTimeEndDateA: number;


 public otBeforeWorkStartA: string;
 public otBeforeWorkEndA: string;
 public otBeforeBreakStartA: string;
 public otBeforeBreakEndA: string;
 public otAfterBreakStartA: string;
 public otAfterBreakEndA: string;
 public otAfterWorkStartA: string;
 public otAfterWorkEndA: string;
  public hours: number
  public mins: number
  public grid: Sitadt02OverTime[];
}

export class Sitadt02OverTime {

  public ouCode: string = '';
  public empId: number;
  public empCode: string = '';
  public empThaiName: string = '';
  public startDate: Date;
  public endDate: Date;
  public departmentIdStart: number;
  public departmentIdEnd: number;
  public employeeStart: string = '';
  public employeeEnd: string = '';
  public otBeforeWorkStart: string;
  public otBeforeWorkEnd: string;
  public otBeforeBreakStart: string;
  public otBeforeBreakEnd: string;
  public otAfterBreakStart: string;
  public otAfterBreakEnd: string;
  public otAfterWorkStart: string;
  public otAfterWorkEnd: string;
}
