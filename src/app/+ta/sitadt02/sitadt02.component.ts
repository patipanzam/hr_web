import { BeanUtils } from '../../core/bean-utils.service';
import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { DepartmentSelectDirective } from '../shared/department-select.directive';
import { EmployeeSelectDirective } from '../shared/employee-select.directive';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchStatus } from '../../shared/constant/common.interface';
import { ShiftSelectDirective } from '../shared/shift-select.directive';
import { Sitadt02Service } from './sitadt02.service';
import { Sitadt02Criteria } from '../sitadt02/sitadt02.interface';
import { SSDatePicker } from '../../shared/ss-datepicker.component';
import { DateUtils } from '../../core/date-utils.service';
import { SSTimePicker } from '../../shared/ss-timepicker.component';
@Component({
  selector: 'sitadt02',
  template: require('./sitadt02.component.html'),
  providers: [Page, Sitadt02Service, Sitadt02Criteria]
})

export class Sitadt02Component implements OnInit {

  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('departmentStart') public departmentStart: DepartmentSelectDirective;
  @ViewChild('departmentEnd') public departmentEnd: DepartmentSelectDirective;
  @ViewChild('employeeStart') public employeeStart: EmployeeSelectDirective;
  @ViewChild('employeeEnd') public employeeEnd: EmployeeSelectDirective;

  @ViewChild('overTimeStartDate') public overTimeStartDate: SSDatePicker;
  @ViewChild('overTimeEndDate') public overTimeEndDate: SSDatePicker;
  @ViewChild('searchModal') public searchModal: ModalDirective;

  @ViewChild('otBeforeWorkStart') private otBeforeWorkStart: SSTimePicker;
  @ViewChild('otBeforeWorkEnd') private otBeforeWorkEnd: SSTimePicker;
  @ViewChild('otBeforeBreakStart') private otBeforeBreakStart: SSTimePicker;
  @ViewChild('otBeforeBreakEnd') private otBeforeBreakEnd: SSTimePicker;
  @ViewChild('otAfterBreakStart') private otAfterBreakStart: SSTimePicker;
  @ViewChild('otAfterBreakEnd') private otAfterBreakEnd: SSTimePicker;
  @ViewChild('otAfterWorkStart') private otAfterWorkStart: SSTimePicker;
  @ViewChild('otAfterWorkEnd') private otAfterWorkEnd: SSTimePicker;

  private form: FormGroup = this.fb.group({
    overTimeId: this.fb.control(null)
    , ouCode: this.fb.control("")
    , empCode: this.fb.control("")
    , empThaiName: this.fb.control("")

    , startDate: this.fb.control(null)
    , endDate: this.fb.control(null)
    , overTimeStartDate: this.fb.control("")
    , overTimeEndDate: this.fb.control("")

    , otBeforeWorkStart: this.fb.control("")
    , otBeforeWorkEnd: this.fb.control("")
    , otBeforeBreakStart: this.fb.control("")
    , otBeforeBreakEnd: this.fb.control("")
    , otAfterBreakStart: this.fb.control("")
    , otAfterBreakEnd: this.fb.control("")
    , otAfterWorkStart: this.fb.control("")
    , otAfterWorkEnd: this.fb.control("")
  });

  private overTimeId: number = null;
  private alert = { title: '', msg: '' };
  private checkDelete: number = 0;
  private fromLoadObject: any;
  private initMsg: string;
  private objCriteria: Sitadt02Criteria = new Sitadt02Criteria();
  private showMsg = false;
  private records: any[];

  constructor(private fb: FormBuilder
    , private page: Page
    , private pageContext: PageContext
    , private service: Sitadt02Service
    , private router: Router
    , private routerActive: ActivatedRoute) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (this.objCriteria.overTimeStartDate) {
        this.overTimeStartDate.setValue(this.objCriteria.overTimeStartDate);
      }
      if (this.objCriteria.overTimeEndDate) {
        this.overTimeEndDate.setValue(this.objCriteria.overTimeEndDate);
      }
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }

    if (BeanUtils.isNotEmpty(this.overTimeId)) {
      this.otBeforeWorkStart.setValue(this.form.controls["otBeforeWorkStart"].value);
      this.otBeforeWorkEnd.setValue(this.form.controls["otBeforeWorkEnd"].value);
      this.otBeforeBreakStart.setValue(this.form.controls["otBeforeBreakStart"].value);
      this.otBeforeBreakEnd.setValue(this.form.controls["otBeforeBreakEnd"].value);
      this.otAfterBreakStart.setValue(this.form.controls["otAfterBreakStart"].value);
      this.otAfterBreakEnd.setValue(this.form.controls["otAfterBreakEnd"].value);
      this.otAfterWorkStart.setValue(this.form.controls["otAfterWorkStart"].value);
      this.otAfterWorkEnd.setValue(this.form.controls["otAfterWorkEnd"].value);

    }

  }

  // public childModalShow(record: any) {
  //   this.pageContext.nextPage(['/ta/sitadt02a', record.overTimeId], this.objCriteria);
  // }

  public createOverTime() {
    this.pageContext.nextPage(['/ta/sitadt02a', ""], this.objCriteria);
  }

  private changeStartDepartment(valueField: any) {
    this.objCriteria.departmentIdStart = valueField.departmentId;
    this.objCriteria.departmentCodeStart = valueField.departmentCode;
    this.loadEmployee();
  }

  private changeEndDepartment(valueField: any) {
    this.objCriteria.departmentIdEnd = valueField.departmentId;
    this.objCriteria.departmentCodeEnd = valueField.departmentCode;
    this.loadEmployee();
  }

  private changeStartEmployee(valueField: any) {
    this.objCriteria.employeeStart = valueField.empCode;
  }

  private changeEndEmployee(valueField: any) {
    this.objCriteria.employeeEnd = valueField.empCode;
  }

  // private changeShiftGroup(valueField: any) {
  //   this.objCriteria.shiftGroup = valueField.shiftCode;
  // }
  private changeOverTimeStartDate(valueField: any) {
    if (valueField) {
      this.form.controls["overTimeStartDate"].setValue(this.overTimeStartDate.getMilliSecond());
      this.objCriteria.obligationsOverTimeDate = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsOverTimeDateShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
    }
  }
  private changeOverTimeEndDate(valueField: any) {
    if (valueField) {
      this.form.controls["overTimeEndDate"].setValue(this.overTimeEndDate.getMilliSecond());
      this.objCriteria.obligationsOverTimeDate = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsOverTimeDateShow = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
    }
  }
  private OtBeforeWorkStart(valueField: any) {
    if (valueField) {
      this.form.controls["breakIn"].setValue(this.otBeforeWorkStart.getValue());
      this.objCriteria.otBeforeWorkStart = this.otBeforeWorkStart.getValue();
    }
  }
  private changeOtBeforeWorkStart(valueField: any) {
    if (valueField) {
      this.form.controls["otBeforeWorkStart"].setValue(this.otBeforeWorkStart.getValue());
      this.objCriteria.otBeforeWorkStart = this.otBeforeWorkStart.getValue();
    }
  }
  private changeOtBeforeWorkEnd(valueField: any) {
    if (valueField) {
      this.form.controls["otBeforeWorkEnd"].setValue(this.otBeforeWorkEnd.getValue());
      this.objCriteria.otBeforeWorkEnd = this.otBeforeWorkEnd.getValue();
    }
  }
  private changeOtBeforeBreakStart(valueField: any) {
    if (valueField) {
      this.form.controls["otBeforeBreakStart"].setValue(this.otBeforeBreakStart.getValue());
      this.objCriteria.otBeforeBreakStart = this.otBeforeBreakStart.getValue();
    }
  }
  private changeOtBeforeBreakEnd(valueField: any) {
    if (valueField) {
      this.form.controls["otBeforeBreakEnd"].setValue(this.otBeforeBreakEnd.getValue());
      this.objCriteria.otBeforeBreakEnd = this.otBeforeBreakEnd.getValue();
    }
  }
  private changeOtAfterBreakStart(valueField: any) {
    if (valueField) {
      this.form.controls["otAfterBreakStart"].setValue(this.otAfterBreakStart.getValue());
      this.objCriteria.otAfterBreakStart = this.otAfterBreakStart.getValue();
    }
  }
  private changeOtAfterBreakEnd(valueField: any) {
    if (valueField) {
      this.form.controls["otAfterBreakEnd"].setValue(this.otAfterBreakEnd.getValue());
      this.objCriteria.otAfterBreakEnd = this.otAfterBreakEnd.getValue();
    }
  }
  private changeOtAfterWorkStart(valueField: any) {
    if (valueField) {
      this.form.controls["otAfterWorkStart"].setValue(this.otAfterWorkStart.getValue());
      this.objCriteria.otAfterWorkStart = this.otAfterWorkStart.getValue();
    }
  }
  private changeOtAfterWorkEnd(valueField: any) {
    if (valueField) {
      this.form.controls["otAfterWorkEnd"].setValue(this.otAfterWorkEnd.getValue());
      this.objCriteria.otAfterWorkEnd = this.otAfterWorkEnd.getValue();
    }
  }
  // About Search
  public onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [changeEvent]);
    LoadMask.show();
    this.showMsg = false;
    this.callServiceSearch(changeEvent, this.objCriteria);
  }

  private callServiceSearch(page: Page, mapping: Sitadt02Criteria) {
    this.service.getData(page, mapping)
      .subscribe(response => {
        this.page = response.page;
        this.records = response.records;
        if (BeanUtils.isNotEmpty(this.records)) {
          LoadMask.hide();
        } else {
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
        }
      },
      error => {
        console.error(error);
        LoadMask.hide();
      }
      );
  }

  private alertModalToggle(title: string, msg: string) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  public onChangePage(changeEvent: Page) {
    this.page = changeEvent;
    if (BeanUtils.isNotEmpty(this.objCriteria.flagForPaging)) {
      if (this.objCriteria.flagForPaging == SearchStatus.normalSearch) {
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private onSearch(modeSearch: number) {
    if (this.form.valid == true) {
      if (BeanUtils.isNotEmpty(modeSearch)) {
        this.objCriteria.flagForPaging = modeSearch
        this.page.start = 0;
        if (modeSearch == SearchStatus.normalSearch) {
          this.onNormalSearch(this.page);
        }
      }
    }
  }

  public closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }

  private loadEmployee() {
    let params = {
      departmentIdStart: this.objCriteria.departmentIdStart || '',
      departmentIdEnd: this.objCriteria.departmentIdEnd || '',
      departmentCodeStart: this.objCriteria.departmentCodeStart || '',
      departmentCodeEnd: this.objCriteria.departmentCodeEnd || ''
    }
    this.employeeStart.load(params);
    this.employeeEnd.load(params);
  }
  private onAdvSearch(page: Page) {

    // this.pageContext.setLastSearch('onAdvSearch',this.page);
    this.pageContext.setLastSearch('onAdvSearch', [page]);
    console.log('adv search');
    LoadMask.show();
    this.service.getDataAdvSearch(page, this.objCriteria)
      .subscribe(response => {
        this.page = response.page;
        this.records = response.records;
        LoadMask.hide();
      },
      error => {
        console.error(error);
        LoadMask.hide();
      }
      );
    this.searchModal.hide();
  }
  private setData(object: any) {
    if (BeanUtils.isNotEmpty(object))
      this.form.controls["otBeforeWorkStart"].setValue(object.records.data.otBeforeWorkStart);
    this.form.controls["otBeforeWorkEnd"].setValue(object.records.data.otBeforeWorkEnd);
    this.form.controls["otBeforeBreakStart"].setValue(object.records.data.otBeforeBreakStart);
    this.form.controls["otBeforeBreakEnd"].setValue(object.records.data.otBeforeBreakEnd);
    this.form.controls["otAfterBreakStart"].setValue(object.records.data.otAfterBreakStart);
    this.form.controls["otAfterBreakEnd"].setValue(object.records.data.otAfterBreakEnd);
    this.form.controls["otAfterWorkStart"].setValue(object.records.data.otAfterWorkStart);
    this.form.controls["otAfterWorkEnd"].setValue(object.records.data.otAfterWorkEnd);

    this.form.controls["startDate"].setValue(object.records.data.startDate);
    this.form.controls["endDate"].setValue(object.records.data.endDate);

    this.form.controls["employeeStart"].setValue(object.records.data.employeeStart);
    this.form.controls["employeeEnd"].setValue(object.records.data.employeeEnd);

    this.form.controls["empCode"].setValue(object.records.data.empCode);




  }
}

