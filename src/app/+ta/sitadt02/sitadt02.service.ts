import { AuthHttp } from 'angular2-jwt';
import { BeanUtils } from '../../core/bean-utils.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { Sitadt02Criteria, Sitadt02SaveOverTime } from '../sitadt02/sitadt02.interface';

const querystring = require('querystring');

@Injectable()
export class Sitadt02Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

  //searchOverTime
  public getData(page: Page, searchValue: Sitadt02Criteria): Observable<any> {
    let params = '?' + querystring.stringify(page) + "&" + querystring.stringify(searchValue);
    return this.authHttp.get(this.restServer.getAPI('/ta/sitadt02/searchOverTime') + params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });
  }

  //processOverTime
  public processOverTime(page: Page, params: Sitadt02SaveOverTime): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitadt02/processOverTime'), params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  //saveOverTime
  public saveOverTime(page: Page, params: Sitadt02SaveOverTime): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitadt02/saveOverTime'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  //selectOverTime
  public searchData(OverTimeId: number): Observable<any> {
    let params = '?OverTimeId=' + OverTimeId;
    return this.authHttp.get(this.restServer.getAPI('/ta/sitadt02/selectOverTime') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  //deleteOverTime
  public deleteOverTime(page: Page, params: Sitadt02SaveOverTime): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/ta/sitadt02/deleteOverTime'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }
   public getDataAdvSearch(page: Page, params: Sitadt02Criteria): Observable<any> {
    // let params = '?' + querystring.stringify(page) + '&ouCode=' + ouCode+ '&empTypeCode=' + empTypeCode+ '&employTypeCode=' + employTypeCode+ '&year=' + year;
    return this.authHttp.post(this.restServer.getAPI('ta/sitadt02/editOverTime'), params)
      .map(val => {
        return {
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }, records: val.json().data
        }
      });
  }
}
//   public loadShift(shiftCode: string): Observable<any> {
//     let params = '?shiftCode=' + shiftCode;
//     return this.authHttp.get(this.restServer.getAPI('ta/sitadt02/loadShift') + params)
//       .map(val => { return val.json() });
//   }

