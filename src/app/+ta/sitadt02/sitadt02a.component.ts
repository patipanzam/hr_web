import { BeanUtils } from '../../core/bean-utils.service';
import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit, } from '@angular/core';
import { DateUtils } from '../../core/date-utils.service';
import { EmployeeSelectDirective } from '../shared/employee-select.directive';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoadMask } from '../../load-mask.component';
import { ModalDirective } from 'ng2-bootstrap';
import { OuSelectDirective } from '../shared/ou-select.directive';
import { Page } from '../../shared/pagination.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ShiftSelectDirective } from '../shared/shift-select.directive';
import { Sitadt02SaveOverTime } from '../sitadt02/sitadt02.interface';
import { Sitadt02Service } from './sitadt02.service';
import { SSDatePicker } from '../../shared/ss-datepicker.component';
import { DepartmentSelectDirective } from '../shared/department-select.directive';
import { SSTimePicker } from '../../shared/ss-timepicker.component';
@Component({
  selector: 'sitadt02a',
  template: require('./sitadt02a.component.html'),
  providers: [Page, Sitadt02Service, Sitadt02SaveOverTime]
})
export class Sitadt02aComponent implements AfterViewInit {

  @Input('info') private info: any;
  @Output('closeModal') private closeEvent = new EventEmitter();
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('backModal') public backModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;
  @ViewChild('successModal') public successModal: ModalDirective;
  @ViewChild('employeeAdd') public employeeAdd: EmployeeSelectDirective;
  // @ViewChild('ouCode') public ouCode: OuSelectDirective;
  // @ViewChild('shiftGroup') public shiftGroup: ShiftSelectDirective;

  @ViewChild('employeeStartA') public employeeStartA: EmployeeSelectDirective;
  @ViewChild('employeeEndA') public employeeEndA: EmployeeSelectDirective;
  @ViewChild('departmentStartA') public departmentStartA: DepartmentSelectDirective;
  @ViewChild('departmentEndA') public departmentEndA: DepartmentSelectDirective;


  @ViewChild('startDateA') public startDateA: SSDatePicker;
  @ViewChild('endDateA') public endDateA: SSDatePicker;
  @ViewChild('otBeforeWorkStartA') private otBeforeWorkStartA: SSTimePicker;
  @ViewChild('otBeforeWorkEndA') private otBeforeWorkEndA: SSTimePicker;
  @ViewChild('otBeforeBreakStartA') private otBeforeBreakStartA: SSTimePicker;
  @ViewChild('otBeforeBreakEndA') private otBeforeBreakEndA: SSTimePicker;
  @ViewChild('otAfterBreakStartA') private otAfterBreakStartA: SSTimePicker;
  @ViewChild('otAfterBreakEndA') private otAfterBreakEndA: SSTimePicker;
  @ViewChild('otAfterWorkStartA') private otAfterWorkStartA: SSTimePicker;
  @ViewChild('otAfterWorkEndA') private otAfterWorkEndA: SSTimePicker;

  private form: FormGroup = this.fb.group({
    overTimeId: this.fb.control(null)
    , empId: this.fb.control("")
    , ouCode: this.fb.control("", Validators.required)
    , empCode: this.fb.control("", Validators.required)
    , employeeStartA: this.fb.control("")
    , employeeEndA: this.fb.control("")
    , empThaiName: this.fb.control("", Validators.required)
    , startDateA: this.fb.control(null, Validators.required)
    , endDateA: this.fb.control(null, Validators.required)



    , departmentStartA:this.fb.control("")
    , departmentEndA:this.fb.control("")


    , otBeforeWorkStartA: this.fb.control("")
    , otBeforeWorkEndA: this.fb.control("")
    , otBeforeBreakStartA: this.fb.control("")
    , otBeforeBreakEndA: this.fb.control("")
    , otAfterBreakStartA: this.fb.control("")
    , otAfterBreakEndA: this.fb.control("")
    , otAfterWorkStartA: this.fb.control("")
    , otAfterWorkEndA: this.fb.control("")
    ,hours:this.fb.control("")
    ,mins:this.fb.control("")
  });

  private overTimeId: number = null;
  private alert = { title: '', msg: '', isRedirect: false };
  private currentDate: Date = new Date();
  private data: any;
  private dirty: boolean;
  private flagForLoad: boolean = false;
  private fromLoadObject: any;
  private initMsg: string;
  private objCriteria: Sitadt02SaveOverTime = new Sitadt02SaveOverTime();
  private objDelete: Sitadt02SaveOverTime = new Sitadt02SaveOverTime();
  private records: any[];
  private showMsg = false;
  private shows: any[] = [];



  constructor(private fb: FormBuilder
    , private page: Page
    , private pageContext: PageContext
    , private router: Router
    , private routerActive: ActivatedRoute
    , private service: Sitadt02Service) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      this.setData(this.fromLoadObject.searchData);
    }
  }

  ngAfterViewInit() {
    if (BeanUtils.isNotEmpty(this.overTimeId)) {
      this.flagForLoad = true;
      // this.ouCode.setValue(this.form.controls["ouCode"].value);
      // this.ouCode.disabled(true);
      // this.shiftGroup.setValue(this.form.controls["shiftCode"].value);
      // this.shiftGroup.disabled(true);
      this.employeeStartA.setValue(this.form.controls["employeeStartA"].value);
      this.employeeEndA.setValue(this.form.controls["employeeEndA"].value);
      // this.employeeAdd.disabled(true);
      this.startDateA.setValue(this.fromLoadObject.searchData.records.data.startDate);
      this.endDateA.setValue(this.fromLoadObject.searchData.records.data.endDate);
      this.flagForLoad = false;

      this.otBeforeWorkStartA.setValue(this.form.controls["otBeforeWorkStartA"].value);
      this.otBeforeWorkEndA.setValue(this.form.controls["otBeforeWorkEndA"].value);
      this.otBeforeBreakStartA.setValue(this.form.controls["otBeforeBreakStartA"].value);
      this.otBeforeBreakEndA.setValue(this.form.controls["otBeforeBreakEndA"].value);
      this.otAfterBreakStartA.setValue(this.form.controls["otAfterBreakStarAt"].value);
      this.otAfterBreakEndA.setValue(this.form.controls["otAfterBreakEndA"].value);
      this.otAfterWorkStartA.setValue(this.form.controls["otAfterWorkStartA"].value);
      this.otAfterWorkEndA.setValue(this.form.controls["otAfterWorkEndA"].value);

      if (this.startDateA.getValue() < this.currentDate && this.endDateA.getValue() < this.currentDate) {
        this.startDateA.readOnly = true;
        this.endDateA.readOnly = true;
      }
      if (this.startDateA.getValue() < this.currentDate && this.endDateA.getValue() > this.currentDate) {
        this.startDateA.readOnly = true;
      }
    }
  }
  private onSave() {
    if (this.form.valid == true || this.shows.length != 0) {
      if (this.shows.length != 0 || this.overTimeId != null) {
        if (this.form.dirty) {
          console.log("dirty"+this.form.dirty);
          LoadMask.show();
          // this.objCriteria = this.form.value;
          if (BeanUtils.isNotEmpty(this.overTimeId)) {
            this.objCriteria.overTimeId = this.overTimeId;
          }

          // this.objCriteria.employeeCodeStart=this.form.controls["employeeCodeStart"].value;
          // this.objCriteria.employeeCodeEnd=this.form.controls["employeeCodeEnd"].value;
          // this.objCriteria.ouCode=this.form.controls["ouCode"].value;
          // this.objCriteria.empId=this.form.controls["empId"].value;
          // this.objCriteria.endDate=this.form.controls["endDate"].value;
          // this.objCriteria.startDate=this.form.controls["startDate"].value;
          // this.objCriteria.otBeforeWorkStart = this.form.controls["otBeforeWorkStart"].value;
          // this.objCriteria.otBeforeWorkEnd = this.form.controls["otBeforeWorkEnd"].value;
          // this.objCriteria.otBeforeBreakStart = this.form.controls["otBeforeBreakEnd"].value;
          // this.objCriteria.otBeforeBreakEnd = this.form.controls["otBeforeBreakEnd"].value;
          // this.objCriteria.otAfterBreakStart = this.form.controls["otAfterBreakStart"].value;
          // this.objCriteria.otAfterBreakEnd = this.form.controls["otAfterBreakEnd"].value;
          // this.objCriteria.otAfterWorkStart = this.form.controls["otAfterWorkStart"].value;
          // this.objCriteria.otAfterWorkEnd = this.form.controls["otAfterWorkEnd"].value;
          // this.objCriteria.hours = this.form.controls["hours"].value;
          // this.objCriteria.mins = this.form.controls["mins"].value;
          this.objCriteria.grid = this.shows;
          this.showMsg = false;
          this.service.saveOverTime(this.page, this.objCriteria).subscribe(response => {
            if (response.records.data.success) {
              LoadMask.hide();
              this.alertModalSuccessToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ', true);
              this.form.markAsPristine();
            } else {
              LoadMask.hide();
              this.alertModalToggle('แจ้งเตือน', 'วันที่ผิดพลาด', false);
            }
          },
            error => {
              console.error(error);
              LoadMask.hide();
              this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
            }
          );
        } else {
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข', false);
        }
       } else {
        LoadMask.hide();
        this.alertModalToggle('แจ้งเตือน', 'กรุณากดปุ่มประมวลผล', false);
      }
     } else {
      LoadMask.hide();
      this.alertModalToggle('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล', false);
    }
  }

  private onProcess(): void {
    let data = this.shows.filter(row => {
      return row.ouCode == this.form.controls["ouCode"].value
        && row.empCode == this.form.controls["empCode"].value
        && row.empThaiName == this.form.controls["empThaiName"].value
        && row.startDateA == this.form.controls["startDateA"].value
        && row.endDateA == this.form.controls["endDateA"].value

        && row.employeeStartA == this.form.controls["employeeStartA"].value
        && row.employeeEndA == this.form.controls["employeeEndA"].value

        && row.otBeforeWorkStartA == this.form.controls["otBeforeWorkStartA"].value
        && row.otBeforeWorkEndA == this.form.controls["otBeforeWorkEndA"].value
        && row.otBeforeBreakStartA == this.form.controls["otBeforeBreakStartA"].value
        && row.otBeforeBreakEndA == this.form.controls["otBeforeBreakEndA"].value
        && row.otAfterBreakStartA == this.form.controls["otAfterBreakStartA"].value
        && row.otAfterBreakEndA == this.form.controls["otAfterBreakEndA"].value
        && row.otAfterWorkStartA == this.form.controls["otAfterWorkStartA"].value
        && row.otAfterWorkEndA == this.form.controls["otAfterWorkEndA"].value;

    });
    if (data.length > 0) {
      this.alertModalToggle('แจ้งเตือน', 'ข้อมูลนี้ถูกประมวลผลแล้ว', false);
    } else {
      if ((this.form.controls["ouCode"].value == null) || (this.form.controls["empCode"].value == null) ||
        (this.form.controls["startDateA"].value == null) || (this.form.controls["endDateA"].value == null) ||
        (this.form.controls["otBeforeWorkStartA"].value == null) || (this.form.controls["otBeforeWorkEndA"].value == null) ||

        (this.form.controls["employeeStartA"].value == null) || (this.form.controls["employeeEndA"].value == null) ||

        (this.form.controls["otBeforeBreakStartA"].value == null) || (this.form.controls["otBeforeBreakEndA"].value == null) ||
        (this.form.controls["otAfterBreakStartA"].value == null) || (this.form.controls["otAfterBreakEndA"].value == null) ||
       (this.form.controls["otAfterWorkStartA"].value == null) || (this.form.controls["otAfterWorkEndA"].value == null))  {
        this.alertModalToggle('ผิดพลาด', 'กรุณากรอกข้อมูลให้ครบถ้วน', false);
      } else {
        this.objCriteria.ouCode = this.form.controls["ouCode"].value;
        this.objCriteria.empCode = this.form.controls["empCode"].value;
        this.objCriteria.empThaiName = this.form.controls["empThaiName"].value;
        this.objCriteria.startDateA = this.form.controls["startDateA"].value;
        this.objCriteria.endDateA = this.form.controls["endDateA"].value;

        this.objCriteria.endDateA = this.form.controls["employeeStartA"].value;
        this.objCriteria.endDateA = this.form.controls["otBeforeBreakEndA"].value;

        this.objCriteria.otBeforeWorkStartA = this.form.controls["otBeforeWorkStartA"].value;
        this.objCriteria.otBeforeWorkEndA = this.form.controls["otBeforeWorkEndA"].value;
        this.objCriteria.otBeforeBreakStartA = this.form.controls["otBeforeBreakStartA"].value;
        this.objCriteria.otBeforeBreakEndA = this.form.controls["otBeforeBreakEndA"].value;
        this.objCriteria.otAfterBreakStartA = this.form.controls["otAfterBreakStartA"].value;
        this.objCriteria.otAfterBreakEndA = this.form.controls["otAfterBreakEndA"].value;
        this.objCriteria.otAfterWorkStartA = this.form.controls["otAfterWorkStartA"].value;
        this.objCriteria.otAfterWorkEndA = this.form.controls["otAfterWorkEndA"].value;

        this.showMsg = false;
        this.service.processOverTime(this.page, this.objCriteria).subscribe(response => {
          if (response.records.data.success) {
            LoadMask.hide();
            let row: {};
            row = {
              ouCode: this.form.controls["ouCode"].value,
              empCode: this.form.controls["empCode"].value,
              empThaiName: this.form.controls["empThaiName"].value,
              startDateA: this.form.controls["startDateA"].value,
              endDateA: this.form.controls["endDateA"].value,

              employeeStartA: this.form.controls["employeeStartA"].value,
              employeeEndA: this.form.controls["employeeEndA"].value,

              otBeforeWorkStartA: this.form.controls["otBeforeWorkStartA"].value,
              otBeforeWorkEndA: this.form.controls["otBeforeWorkEndA"].value,
              otBeforeBreakStartA: this.form.controls["otBeforeBreakStartA"].value,
              otBeforeBreakEndA: this.form.controls["otBeforeBreakEndA"].value,
              otAfterBreakStartA: this.form.controls["otAfterBreakStartA"].value,
              otAfterBreakEndA: this.form.controls["otAfterBreakEndA"].value,
              otAfterWorkStartA: this.form.controls["otAfterWorkStartA"].value,
              otAfterWorkEndA: this.form.controls["otAfterWorkEndA"].value


            };
            this.shows.push(row);

            this.employeeStartA.clearValue() ;
            this.employeeEndA.clearValue() ;
            this.departmentStartA.clearValue();
            this.departmentEndA.clearValue();
            this.startDateA.clearValue();
            this.endDateA.clearValue();
            this.otBeforeWorkStartA.clearValue();
            this.otBeforeWorkEndA.clearValue();
            this.otBeforeBreakStartA.clearValue();
            this.otBeforeBreakEndA.clearValue();
            this.otAfterBreakStartA.clearValue();
            this.otAfterBreakEndA.clearValue();
            this.otAfterWorkStartA.clearValue();
            this.otAfterWorkEndA.clearValue();


          } else {
            LoadMask.hide();
            this.alertModalToggle('แจ้งเตือน', 'วันที่ผิดพลาด', false);
          }
        });
      }
    }
  }
  private setData(object: any) {
    if (BeanUtils.isNotEmpty(object)) {
      this.form.controls["ouCode"].setValue(object.records.data.ouCode);
      this.form.controls["empCode"].setValue(object.records.data.empCode);
      this.form.controls["empThaiName"].setValue(object.records.data.empThaiName);

      this.form.controls["startDateA"].setValue(object.records.data.startDate);
      this.form.controls["endDateA"].setValue(object.records.data.endDate);

      this.form.controls["otBeforeWorkStartA"].setValue(object.records.data.otBeforeWorkStart);
      this.form.controls["otBeforeWorkEndA"].setValue(object.records.data.otBeforeWorkEnd);
      this.form.controls["otBeforeBreakStartA"].setValue(object.records.data.otBeforeBreakStart);
      this.form.controls["otBeforeBreakEndA"].setValue(object.records.data.otBeforeBreakEnd);
      this.form.controls["otAfterBreakStartA"].setValue(object.records.data.otAfterBreakStart);
      this.form.controls["otAfterBreakEndA"].setValue(object.records.data.otAfterBreakEnd);
      this.form.controls["otAfterWorkStartA"].setValue(object.records.data.otAfterWorkStart);
      this.form.controls["otAfterWorkEndA"].setValue(object.records.data.otAfterWorkEnd);

      this.overTimeId = object.records.dataoverTimeId;
      this.objCriteria.startDateA = this.fromLoadObject.searchData.records.data.startDate;
      this.objCriteria.endDateA = this.fromLoadObject.searchData.records.data.endDate;
    }
  }

  public onBack() {
    this.checkDirty();
    if (this.form.dirty == false) {
      this.backModalBack();
    } else {
      this.backModalToggle('แจ้งเตือน', 'ข้อมูลถูกแก้ไข ต้องการเปลี่ยนหน้าใช่หรือไม่', false);
    }
  }

  private backModalClose() {
    this.backModal.hide();
  }

  private backModalBack() {
    this.pageContext.backPage();
  }

  private backModalToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.backModal.show();
  }

  public onCancel() {
    this.closeEvent.emit();
  }

  private deleteModalClose() {
    this.deleteModal.hide();
  }

  private deleteModalConfirm() {
    this.onDelete();
    this.deleteModal.hide();
  }

  private alertModalDeleteToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.deleteModal.toggle();
  }

  public onBeforeDelete(record: any) {
    this.alertModalDeleteToggle('ยืนยันการลบ', 'ต้องการลบข้อมูลใช่หรือไม่', true);
  }

  public onDelete() {
    LoadMask.show();
    this.objDelete.overTimeId = this.overTimeId
    this.service.deleteOverTime(this.page, this.objDelete).subscribe(response => {
      this.alertModalSuccessToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ', true);
      LoadMask.hide();
      this.pageContext.backPage();
    },
      error => {
        console.error(error);
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
      }
    );
  }

  // private getShift() {
  //   if (this.form.controls["shiftCode"].value != null) {
  //     LoadMask.show();
  //     this.service.loadShift(this.form.controls["shiftCode"].value)
  //       .subscribe(response => {
  //         this.form.controls["shiftIn"].setValue(response.shiftIn);
  //         this.form.controls["shiftOut"].setValue(response.shiftOut);
  //         LoadMask.hide();
  //       }, error => {
  //         this.form.controls["shiftIn"].setValue(null);
  //         this.form.controls["shiftOut"].setValue(null);
  //         LoadMask.hide();
  //         this.alertModalToggle('ผิดพลาด', 'ไม่พบข้อมูลที่ระบุ', false);
  //       });
  //   }
  //   else {
  //     this.form.controls["shiftIn"].setValue(null);
  //     this.form.controls["shiftOut"].setValue(null);
  //   }
  // }

  private loadEmployee() {
    let params = {
      departmentStartA: this.objCriteria.departmentIdStartA || '',
      departmentIdEndA: this.objCriteria.departmentIdEndA || '',
      departmentCodeStartA: this.objCriteria.departmentCodeStartA || '',
      departmentCodeEndA: this.objCriteria.departmentCodeEndA || ''
    }
    this.employeeStartA.load(params);
    this.employeeEndA.load(params);

  }
   private changeOuCode(valueField: any) {
    this.form.controls["ouCode"].setValue(valueField.ouCode);
    this.objCriteria.ouCode = valueField.ouCode
    if (this.flagForLoad == false) {
      this.form.controls["ouCode"].markAsDirty(true);
    }
  }



  // private changeShiftGroupA(valueField: any) {
  //   this.form.controls["shiftCode"].setValue(valueField.shiftCode);
  //   this.getShift();
  //   if (this.flagForLoad == false) {
  //     this.form.controls["shiftCode"].markAsDirty(true);
  //   }
  // }

  // private changeEmployeeAdd(valueField: any) {
  //   this.form.controls["empCode"].setValue(valueField.empCode);
  //   this.form.controls["empThaiName"].setValue(valueField.empThaiName);
  //   this.objCriteria.empCode = valueField.empCode
  //   this.objCriteria.empThaiName = valueField.empThaiName
  //   if (this.flagForLoad == false) {
  //     this.form.controls["empCode"].markAsDirty(true);
  //   }

  // }

  // private changeStartEmployee(valueField: any) {
  //   this.form.controls["empCode"].setValue(valueField.empCode);
  //   this.form.controls["empThaiName"].setValue(valueField.empThaiName);
  //   this.objCriteria.employeeCodeStart = valueField.empCode;
  //   if (this.flagForLoad == false) {
  //     this.form.controls["empCode"].markAsDirty(true);
  //   }

  // }

  // private changeEndEmployee(valueField: any) {
  //   this.form.controls["empCode"].setValue(valueField.empCode);
  //   this.form.controls["empThaiName"].setValue(valueField.empThaiName);
  //    this.objCriteria.employeeCodeEnd = valueField.empCode;
  //   if (this.flagForLoad == false) {
  //     this.form.controls["empCode"].markAsDirty(true);
  //   }

  // }



  private changeEmployeeStartA(valueField: any) {
    this.form.controls["employeeStartA"].setValue(valueField.empCode);
    // this.form.controls["empThaiName"].setValue(valueField.empThaiName);
    //  this.form.controls["empId"].setValue(valueField.empId);
      this.objCriteria.empId = valueField.empId;
     this.objCriteria.employeeStart = valueField.empCode;
    if (this.flagForLoad == false) {
      this.form.controls["employeeStartA"].markAsDirty(true);
    }

  }

  private changeEmployeeEndA(valueField: any) {
    this.form.controls["employeeEndA"].setValue(valueField.empCode);
    // this.form.controls["empThaiName"].setValue(valueField.empThaiName);
      this.objCriteria.employeeEnd = valueField.empCode;
    if (this.flagForLoad == false) {
      this.form.controls["employeeEndA"].markAsDirty(true);
    }

  }


private changeDepartmentStartA(valueField: any) {
  //  this.form.controls["departmentIdStart"].setValue(valueField.departmentCode);
    this.form.controls["departmentStartA"].setValue(valueField.departmentCode);
      this.objCriteria.departmentCodeStartA = valueField.departmentCode;

     if (this.flagForLoad == false) {
      this.form.controls["departmentStartA"].markAsDirty(true);

    }

  }

  private changeDepartmentEndA(valueField: any) {
  //  this.form.controls["departmentIdEnd"].setValue(valueField.departmentId);
    this.form.controls["departmentEndA"].setValue(valueField.departmentCode);
     this.objCriteria.departmentCodeEndA = valueField.departmentCode;
     if (this.flagForLoad == false) {
      this.form.controls["departmentEndA"].markAsDirty(true);



    }

  }


  private changeOverTimeStartDateA(valueField: any) {
    if (valueField) {
      this.form.controls["startDateA"].setValue(this.startDateA.getMilliSecond());
      this.objCriteria.obligationsoverTimeStartDateA = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsoverTimeStartDateShowA = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
      if (this.flagForLoad == false) {
        this.form.controls["startDateA"].markAsDirty(true);
      }
    }
  }

  private changeOverTimeEndDateA(valueField: any) {
    if (valueField) {
      this.form.controls["endDateA"].setValue(this.endDateA.getMilliSecond());
      this.objCriteria.obligationsoverTimeEndDateA = DateUtils.setFormatDateDD_MM_YYYY(valueField) || null;
      this.objCriteria.obligationsoverTimeEndDateShowA = DateUtils.setFormatDateDD_MM_YYYY_TH(valueField);
      if (this.flagForLoad == false) {
        this.form.controls["endDateA"].markAsDirty(true);
      }
    }
  }
private changeOtBeforeWorkStartA(valueField: any) {
  if (valueField) {
    this.form.controls["otBeforeWorkStartA"].setValue(this.otBeforeWorkStartA.getValue());
    this.objCriteria.otBeforeWorkStartA = this.otBeforeWorkStartA.getValue();
  }
}
private changeOtBeforeWorkEndA(valueField: any) {
  if (valueField) {
    this.form.controls["otBeforeWorkEndA"].setValue(this.otBeforeWorkEndA.getValue());
    this.objCriteria.otBeforeWorkEndA = this.otBeforeWorkEndA.getValue();
  }
}
private changeOtBeforeBreakStartA(valueField: any) {
  if (valueField) {
    this.form.controls["otBeforeBreakStartA"].setValue(this.otBeforeBreakStartA.getValue());
    this.objCriteria.otBeforeBreakStartA = this.otBeforeBreakStartA.getValue();
  }
}
private changeOtBeforeBreakEndA(valueField: any) {
  if (valueField) {
    this.form.controls["otBeforeBreakEndA"].setValue(this.otBeforeBreakEndA.getValue());
    this.objCriteria.otBeforeBreakEndA = this.otBeforeBreakEndA.getValue();
  }
}
private changeOtAfterBreakStartA(valueField: any) {
  if (valueField) {
    this.form.controls["otAfterBreakStartA"].setValue(this.otAfterBreakStartA.getValue());
    this.objCriteria.otAfterBreakStartA = this.otAfterBreakStartA.getValue();
  }
}
private changeOtAfterBreakEndA(valueField: any) {
  if (valueField) {
    this.form.controls["otAfterBreakEndA"].setValue(this.otAfterBreakEndA.getValue());
    this.objCriteria.otAfterBreakEndA = this.otAfterBreakEndA.getValue();
  }
}
private changeOtAfterWorkStartA(valueField: any) {
  if (valueField) {
    this.form.controls["otAfterWorkStartA"].setValue(this.otAfterWorkStartA.getValue());
    this.objCriteria.otAfterWorkStartA = this.otAfterWorkStartA.getValue();
  }
}
private changeOtAfterWorkEndA(valueField: any) {
  if (valueField) {
    this.form.controls["otAfterWorkEndA"].setValue(this.otAfterWorkEndA.getValue());
    this.objCriteria.otAfterWorkEndA = this.otAfterWorkEndA.getValue();
  }
}
  public checkDirty() {
    if (this.form.dirty == true) {
      this.dirty = true;
    } else {
      this.dirty = false;
    }
    return this.dirty;
  }

  private alertModalToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.alertModal.show();
  }

  private alertModalSuccessToggle(title: string, msg: string, isRedirect: boolean) {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.successModal.show();
  }

  private alertModalClose() {
    if (this.records == null) {
      (this.router.url.split("/")[2] == "sitadt02a")
    }
    this.alertModal.toggle();
  }

  private alertModalSuccessClose() {
    if (this.records != null) {
      if (this.router.url.split("/")[2] == "sitadt02a") {
      }
    }
    if (this.records == null) {
      this.pageContext.backPage();
    }
    this.successModal.toggle();
  }

}

