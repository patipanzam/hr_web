import { BeanUtils } from '../../core/bean-utils.service';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { Injectable } from '@angular/core';
import { LoadMask } from '../../load-mask.component';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Sitadt02Service } from './sitadt02.service';

@Injectable()
export class Sitadt02Resolve implements Resolve<any> {

  constructor(private selectService: CommonSelectService, private sitadt02Service: Sitadt02Service) {
    this.selectService.setModuleName('ta');
  }

  resolve(route: ActivatedRouteSnapshot): any {
    LoadMask.show();
    if (route.url[0].path == 'sitadt02') {
      var department = this.selectService.getDepartment();
      var employee = this.selectService.getEmployee();

      return Observable.forkJoin([department, employee]).map((response) => {

        LoadMask.hide();
        return {
          department: response[0],
          employee: response[1],

        };
      }).first();
    } else if (route.url[0].path == 'sitadt02a') {

      if (BeanUtils.isNotEmpty(route.params['overTimeId'])) {
        var searchData = this.sitadt02Service.searchData(route.params['overTimeId']);
        var ouCode = this.selectService.getOu();

        var employee = this.selectService.getEmployee();
        var department = this.selectService.getDepartment();
        return Observable.forkJoin([searchData, ouCode, employee,department]).map((response) => {
          LoadMask.hide();
          return {
            searchData: response[0],
            ouCode: response[1],

            employee: response[2],
            department: response[3]
          };
        }).first();
      } else {
        var ouCode = this.selectService.getOu();

        var employee = this.selectService.getEmployee();
        var department = this.selectService.getDepartment();
        return Observable.forkJoin([ouCode, employee,department]).map((response) => {
          LoadMask.hide();
          return {
            ouCode: response[0],

            employee: response[1],
            department: response[2]
          };
        }).first();
      }
    }
  }
}
