import { Component } from '@angular/core';
import { LoadPage } from './load-page.component';

@Component({
  selector: 'load-mask',
  template: `
    <div class="bg-load-mask" style="display:none" >
      <div class="load-mask">
        <div class="spinner">
          <div class="bounce1"></div>
          <div class="bounce2"></div>
          <div class="bounce3"></div>
        </div>
      </div>
    </div>
  `
})
export class LoadMaskComponent {
}

export class LoadMask {
  public static show() {
    if (!LoadPage.isShowing()) {
      (<HTMLScriptElement[]><any>document.getElementsByClassName('bg-load-mask'))[0].style.display = '';
    }
  }
  public static hide() {
    (<HTMLScriptElement[]><any>document.getElementsByClassName('bg-load-mask'))[0].style.display = 'none';
  }
}
