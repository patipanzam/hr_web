import { Component } from '@angular/core';

@Component({
  selector: 'body',
  template: `<router-outlet></router-outlet>
             <loag-page></loag-page>
             <load-mask></load-mask>
  `
})
export class AppComponent {

  constructor() {
  }
 
}
