import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../auth-guard.service';
import { CanDeactivateGuard } from '../can-deactivate-guard.service';
import { AppLayoutComponent } from './../shared/layouts/app-layout.component';
import { SimpleLayoutComponent } from './../shared/layouts/simple-layout.component';
import { CommonSelectService } from '../shared/service/common-select.service';

import { SipmComponent } from './sipm.component';

import { sipmrt01Component } from './sipmrt01/sipmrt01.component';
import { sipmrt01aComponent } from './sipmrt01/sipmrt01a.component';
import { sipmrt01Resolve } from './sipmrt01/sipmrt01.resolve';
import { sipmrt01Service } from "./sipmrt01/sipmrt01.service";

import { sipmrt02Component } from './sipmrt02/sipmrt02.component';
import { sipmrt02aComponent } from './sipmrt02/sipmrt02a.component';
import { sipmrt02Resolve } from './sipmrt02/sipmrt02.resolve';
import { sipmrt02Service } from "./sipmrt02/sipmrt02.service";


const routes: Routes = [{
  path: '',
  component: AppLayoutComponent,
  canActivate: [AuthGuard],
  children: [{
    path: '',
    data: { title: 'PM' },
    component: SipmComponent,
    canActivateChild: [AuthGuard]
  }, {
    path: 'sipmrt01',
    data: { title: 'sipmrt01' },
    component: sipmrt01Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: sipmrt01Resolve
    }
   }, {
    path: 'sipmrt02',
    data: { title: 'sipmrt02' },
    component: sipmrt02Component,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: sipmrt02Resolve
    }
   }]
}, {
  path: '',
  component: SimpleLayoutComponent,
  canActivate: [AuthGuard],
  children: [{
    path: 'sipmrt01a/:emp_id',
    data: { title: 'sipmrt01A' },
    component: sipmrt01aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: sipmrt01Resolve
    }
  }, {
    path: 'sipmrt01A/:emp_id',
    component: sipmrt01aComponent,
    canActivateChild: [AuthGuard],
    resolve: {
      fromLoadObject: sipmrt01Resolve
    }
},{
  path: 'sipmrt02a/:resign_reason_id',
  data: { title: 'sipmrt02A' },
  component: sipmrt02aComponent,
  canActivateChild: [AuthGuard],
  resolve: {
    fromLoadObject: sipmrt02Resolve
  }
}, {
  path: 'sipmrt02A/:resign_reason_id',
  component: sipmrt02aComponent,
  canActivateChild: [AuthGuard],
  resolve: {
    fromLoadObject: sipmrt02Resolve
  }
}
]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  providers: [CommonSelectService, 
    sipmrt01Resolve, sipmrt01Service,sipmrt02Resolve, sipmrt02Service  
  ]

})
export class SipmRoutingModule {
}
