import { Component, ViewContainerRef, OnInit } from '@angular/core';
import { MenuService } from './../core/menu.service';

@Component({
  selector: 'sipm',
    styleUrls: ['./sipm.component.scss'],
  template: `
   <div class="container">
    <div class="row" style="margin-top:20px">
      <div class="col-sm-4" style="padding-left:4%" style="padding-right:4%" *ngFor="let menu of menus | async">
        <div class="card  bounce-in">
          <a routerLinkActive="active" [routerLink]="[menu.path]" class="underline-link">
            <div class="card-block" style="padding: 1rem;">
               <h4 class="module-title" style="color:#FFFFFF">{{menu.name}}</h4>
            </div>
            <div class="card-block" style="background-color:#FFFFFF">
                <h5 class="module-title">{{menu.desc}}</h5>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
  `
}) 
export class SipmComponent implements OnInit{
  viewContainerRef: ViewContainerRef;
  public menus: any;

  constructor(viewContainerRef: ViewContainerRef, private menuService: MenuService) {
    this.viewContainerRef = viewContainerRef;
  }

  ngOnInit() {
    this.menus = this.menuService.menus;
  }
}
