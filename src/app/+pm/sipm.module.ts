import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownModule, ModalModule, TabsModule, TypeaheadModule, AlertModule, TooltipModule } from 'ng2-bootstrap';

import { SharedModule } from '../shared/shared.module';

import { SipmComponent } from './sipm.component';
import { SipmRoutingModule } from './sipm-routing.module';

import { sipmrt01Component } from './sipmrt01/sipmrt01.component';
import { sipmrt01aComponent } from './sipmrt01/sipmrt01a.component';

import { sipmrt02Component } from './sipmrt02/sipmrt02.component';
import { sipmrt02aComponent } from './sipmrt02/sipmrt02a.component';

import { ProvinceSelectDirective } from './shared/province-select.directive';
import { CompanySelectDirective } from './shared/company-select.directive';
import { BranchSelectDirective } from './shared/branch-select.directive';
import { ResignSelectDirective } from './shared/resign-select.directive';

import { FileUploadModule } from 'ng2-file-upload';
import { UploadFileComponent } from './../shared/layouts/upload-file.component';

@NgModule({
  imports: [SharedModule, ReactiveFormsModule, SipmRoutingModule, DropdownModule, ModalModule, TabsModule, TypeaheadModule, AlertModule, TooltipModule, FileUploadModule],
  declarations: [
    SipmComponent,
   

    sipmrt01Component,
    sipmrt01aComponent,
    sipmrt02Component,
    sipmrt02aComponent,
   
    ProvinceSelectDirective,
    CompanySelectDirective,
    BranchSelectDirective,
    ResignSelectDirective,
    UploadFileComponent,

  ],
  providers: []
})
export class SipmModule {
}
