import { Component, OnInit, ViewChild , OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { sipmrt01Service } from './sipmrt01.service';
import { sipmrt01Criteria, sipmrt01SaveEmpStatus} from '../sipmrt01/sipmrt01.interface';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { DateUtils } from '../../core/date-utils.service';
import { LoadMask } from '../../load-mask.component';
import { AppLayoutComponent } from '../../shared/layouts/app-layout.component';
import { BeanUtils } from '../../core/bean-utils.service';
import { SearchStatus } from '../../shared/constant/common.interface';
import { PageContext } from '../../shared/service/pageContext.service';

@Component({
  selector: 'sipmrt01',
  template: require('./sipmrt01.component.html'),
  providers: [Page,sipmrt01Service]
})
export class sipmrt01Component {

  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('deleteModals') public deleteModals: ModalDirective;

    private form: FormGroup = this.formBuilder.group({
      empCode:  this.formBuilder.control("")
    , empName:  this.formBuilder.control("")
});

  private emp_id: number = null;
  private initMsg: string;
  private showMsg = false;
  private records: any[];
  private fromLoadObject: any;
  private objCriteria: sipmrt01Criteria = new sipmrt01Criteria();
  private objDelete: sipmrt01SaveEmpStatus = new sipmrt01SaveEmpStatus();
  private flagForPaging: number = 0;
  private alert = { title: '', msg: ''};
  private inputSearch: string;
  private checkDelete: number = 0;

   public childModalShow(record: any) {
     console.log(record)
    this.pageContext.nextPage(['/pm/sipmrt01a', record.emp_id], this.objCriteria);
  }

   public createEmpStatus() {
     let emp_id : any = "";
    this.pageContext.nextPage(['/pm/sipmrt01a' , emp_id], this.objCriteria);
  }

  constructor(private page: Page
    , private service: sipmrt01Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private pageContext: PageContext
    , private formBuilder : FormBuilder ) {
      this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

    ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
    this.onNormalSearch(this.page)
  }

  // about search
  public onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [this.page]);
    LoadMask.show();
    this.showMsg = false;
    if(BeanUtils.isNotEmpty(changeEvent)){
      this.page = changeEvent;
    }
    this.callServiceSearch(this.page, this.objCriteria);
  }

  private alertModalToggle(title: string, msg: string): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alertModal.toggle();
  }

  private callServiceSearch(page: Page, inputSearch: sipmrt01Criteria){
    console.log(inputSearch)
     this.service.getData(page, inputSearch).subscribe(response => {
        this.page = response.page;
        this.records = response.records;
        console.log(this.records)
        console.log(this.page)
        if(BeanUtils.isNotEmpty(this.records)){
        LoadMask.hide();
        }else{
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล');
       }
      },
      error => {
        console.error(error);
        LoadMask.hide();
      }
    );
  }

 public onChangePage(changeEvent: Page){
    if(BeanUtils.isNotEmpty(this.flagForPaging)){
      if(this.flagForPaging == SearchStatus.normalSearch){
        this.onNormalSearch(changeEvent);
      }
    }
  }

  private onSearch(modeSearch: number){
    if(BeanUtils.isNotEmpty(modeSearch)){
      this.flagForPaging = modeSearch
      this.page.start = 0;
      if(modeSearch == SearchStatus.normalSearch){
        this.onNormalSearch(this.page);
      }
    }
  }  
  public closeAlert() {
    console.log('---0')
    this.initMsg = "";
    this.showMsg = false;
  }
}

