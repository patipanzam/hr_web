import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { sipmrt01Criteria ,sipmrt01SaveEmpStatus} from '../sipmrt01/sipmrt01.interface';
import { Http, RequestOptions, Response, Headers } from '@angular/http';
import { BeanUtils } from '../../core/bean-utils.service';

const querystring = require('querystring');

@Injectable()
export class sipmrt01Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService,private http: Http) {}

  //sipmrt01 search
  public getData(page: Page,inputSearch:sipmrt01Criteria): Observable<any> {
    var inputSearchs = inputSearch.inputSearch;
    let params = '?' + querystring.stringify(page) +'&inputSearch=' + inputSearchs;
    console.log('===>')
    console.log(params)
    return this.http.get(this.restServer.getAPI('Employee/getemployee') + params)
      .map(val => {
        return { 
          page: {
            total: val.json().length,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
  }

  //saveEmpStatus
    public saveEmpStatus(page: Page, params: sipmrt01SaveEmpStatus): Observable<any> {
      return this.http.post(this.restServer.getAPI('Employee/saveemployee'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
    }

    //updateEmpStatus
    public updateEmpStatus(page: Page, params: sipmrt01SaveEmpStatus): Observable<any> {
      return this.http.put(this.restServer.getAPI('Employee/updateemployee'), params)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
    }

  //sipmrt01a
  public searchData(emp_id: number): Observable<any> {
    console.log('aaaa')
    let params = '?emp_id=' + emp_id;
    return this.http.get(this.restServer.getAPI('Employee/searchemployee') + params)
      .map(val => {
        console.log('==>>l')
        console.log(val)
        return {
          records: val.json()
        }
      });
  }

  //deleteEmpStatus
    public deleteEmpStatus(page: Page, params: sipmrt01SaveEmpStatus): Observable<any> {
      console.log('delete')
      let param = '?&emp_id=' + params.emp_id;
      return this.http.delete(this.restServer.getAPI('Employee/deleteemployee')+param)
      .map(val => {
        return {
          page: {
            total: 100,
            start: page.start,
            limit: page.limit
          }
          , records: val.json()
        }
      });
    }

  
}
