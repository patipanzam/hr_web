import { SSDatePicker } from '../../shared/ss-datepicker.component';
import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { sipmrt01Service } from './sipmrt01.service';
import { sipmrt01Criteria, sipmrt01SaveEmpStatus} from '../sipmrt01/sipmrt01.interface';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { BeanUtils } from '../../core/bean-utils.service';
import { LoadMask } from '../../load-mask.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { ProvinceSelectDirective } from "../shared/province-select.directive";
import { CompanySelectDirective } from "../shared/company-select.directive";
import { BranchSelectDirective } from "../shared/branch-select.directive";
import { DateUtils } from '../../core/date-utils.service';


@Component({
  selector: 'sipmrt01a',
  template: require('./sipmrt01a.component.html'),
  providers: [Page, sipmrt01Service]
})
export class sipmrt01aComponent implements AfterViewInit {

    @Output('closeModal') private closeEvent = new EventEmitter();
    @Input('info') private info: any;
    @ViewChild('alertModal') public alertModal: ModalDirective;
    @ViewChild('backModal') public backModal: ModalDirective;
    @ViewChild('deleteModal') public deleteModal: ModalDirective;
    @ViewChild('alertModalSave') public alertModalSave: ModalDirective;
    @ViewChild('province') public province: ProvinceSelectDirective;
    @ViewChild('company') public company: CompanySelectDirective;
    @ViewChild('branch') public branch: BranchSelectDirective;
    @ViewChild('brithDate') public brithDate: SSDatePicker;
    @ViewChild('startDate') public startDate: SSDatePicker;
    


     private form: FormGroup = this.fb.group({
      empCode:  this.fb.control(null, Validators.required)
    , empName:  this.fb.control("", Validators.required)
    , empSername:  this.fb.control("", Validators.required)
    , empAddress:  this.fb.control("", Validators.required)
    , empSaraly:  this.fb.control("", Validators.required)
    , brithDate: this.fb.control("", Validators.required)
   
    //combobox province
     , province: this.fb.control("")    
     , provinceId: this.fb.control("", Validators.required)
     , provinceCode: this.fb.control("")

      //combobox company
      , company: this.fb.control("")    
      , company_id: this.fb.control("", Validators.required)
      , company_name: this.fb.control("")

      //combobox company
      , branch: this.fb.control("")    
      , branch_id: this.fb.control("", Validators.required)
      , branch_name: this.fb.control("")
     
  });

    private emp_id: number = null;
    private PROVINCE_CODE: string = "";
    private branch_id :number = null;
    private company_id :number = null;
    private brithdate :number = null;
    public location = {};

    private fromLoadObject: any;
    private initMsg: string;
    private showMsg = false;
    private objDelete: sipmrt01SaveEmpStatus = new sipmrt01SaveEmpStatus();
    private objCriteria: sipmrt01SaveEmpStatus = new sipmrt01SaveEmpStatus();
    private alert = { title: '', msg: '', isRedirect: false };
    private records: any[];

  constructor(private page: Page
                , private fb: FormBuilder
                , private service: sipmrt01Service
                , private router: Router
                , private routerActive: ActivatedRoute
                , private pageContext: PageContext
                ) { this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
        if(BeanUtils.isNotEmpty(this.fromLoadObject.searchData)){
          this.setData(this.fromLoadObject.searchData);
        }else {
          this.fromLoadObject.searchData = { records: { data: {} } };
        }
    }

     ngAfterViewInit(){
       if(BeanUtils.isEmpty(this.emp_id)){
       }
       this.province.setValue(this.PROVINCE_CODE);
       this.company.setValue(this.branch_id);
       this.branch.setValue(this.company_id);
       this.brithDate.setValue(this.fromLoadObject.searchData.records.emp_birth_date);
       this.startDate.setValue(this.fromLoadObject.searchData.records.emp_work_date_start);
     }

     ngOnInit(){
      if(navigator.geolocation){
         navigator.geolocation.getCurrentPosition(position => {
         this.location = position.coords;
         });
      }
    }

  private alertModalToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.alertModal.show();
  }

    private alertModalClose(): void {
      if(null != this.records){
        if(this.router.url.split("/")[2]=="sipmrt01a"){
        this.pageContext.backPage();
        }
        if(this.router.url.split("/")[2]=="sipmrt01A"){
        this.pageContext.backPage();
        }
      }
      this.alertModal.toggle();
  }

  public onSave() {
    if(this.form.dirty){
    if(BeanUtils.isNotEmpty(this.emp_id)){
      this.objCriteria.emp_id = this.emp_id;
    }
    this.objCriteria.emp_code = this.form.controls["empCode"].value;
    this.objCriteria.emp_first_name = this.form.controls["empName"].value;
    this.objCriteria.emp_last_name = this.form.controls["empSername"].value;
    this.objCriteria.emp_address = this.form.controls["empAddress"].value;
    this.objCriteria.emp_salary = this.form.controls["empSaraly"].value;
    this.objCriteria.emp_address_province_id =this.form.controls["provinceId"].value;
    this.objCriteria.emp_company_id = this.form.controls["company_id"].value;
    this.objCriteria.emp_branch_id = this.form.controls["branch_id"].value;

    console.log( this.objCriteria)
    this.showMsg = false;
    this.service.saveEmpStatus(this.page, this.objCriteria).subscribe(response => {
      console.log('response.records')
        console.log(response.records)
        this.records = response.records.emp_id;
        this.emp_id = response.records.emp_id;
        if (response.records.checkEmpStatusCode == 0) {
                LoadMask.hide();
                this.alertModalToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ', true);
              }else if (response.records.checkEmpStatusCode == 1) {
                LoadMask.hide();
                this.alertModalToggle('แจ้งเตือน', 'รหัสพนักงานซ้ำ', false);
              }else{
                LoadMask.hide();
                this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้', false);
              }
      },
      error => {
        console.error(error);
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
      }
      );
    }else{
          LoadMask.hide();
          this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข', false);
    }
  }

  onUpdate(){
    if(this.form.dirty){
      if(BeanUtils.isNotEmpty(this.emp_id)){
        this.objCriteria.emp_id = this.emp_id;
        this.objCriteria.emp_code = this.form.controls["empCode"].value;
        this.objCriteria.emp_first_name = this.form.controls["empName"].value;
        this.objCriteria.emp_last_name = this.form.controls["empSername"].value;
        this.objCriteria.emp_address = this.form.controls["empAddress"].value;
        this.objCriteria.emp_salary = this.form.controls["empSaraly"].value;
        this.objCriteria.emp_address_province_id =this.form.controls["provinceId"].value;
        this.objCriteria.emp_company_id = this.form.controls["company_id"].value;
        this.objCriteria.emp_branch_id = this.form.controls["branch_id"].value;
        this.showMsg = false;
        this.service.updateEmpStatus(this.page, this.objCriteria).subscribe(response => {
            console.log(response.records)
            this.records = response.records.emp_id;
            this.emp_id = response.records.emp_id;
            if (response.records.checkEmpStatusCode == 0) {
                    LoadMask.hide();
                    this.alertModalToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ', true);
                  }else if (response.records.checkEmpStatusCode == 1) {
                    LoadMask.hide();
                    this.alertModalToggle('แจ้งเตือน', 'รหัสพนักงานซ้ำ', false);
                  }else{
                    LoadMask.hide();
                    this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้', false);
                  }
          },
          error => {
            console.error(error);
            LoadMask.hide();
            this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
          }
        );
      }
    }else{
      LoadMask.hide();
      this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข', false);
}
  }

  private setData(Object: any){
       if(BeanUtils.isNotEmpty(Object)){
        this.form.controls["empCode"].setValue(Object.records.emp_code);
        this.form.controls["empName"].setValue(Object.records.emp_first_name);
        this.form.controls["empSername"].setValue(Object.records.emp_last_name);
        this.form.controls["empAddress"].setValue(Object.records.emp_address);
        this.form.controls["empSaraly"].setValue(Object.records.emp_salary);
        this.form.controls["company_id"].setValue(Object.records.emp_company_id);
        this.form.controls["brithDate"].setValue(Object.records.emp_birth_date);
        this.emp_id = Object.records.emp_id;
        this.PROVINCE_CODE = Object.records.emp_address_province_id;
        this.branch_id = Object.records.emp_branch_id;
        this.company_id = Object.records.emp_company_id;
        this.objCriteria.emp_birth_date = this.fromLoadObject.searchData.records.emp_birth_date;
        this.objCriteria.emp_work_date_start = this.fromLoadObject.searchData.records.emp_work_date_start;
       }
    }

 private backModalToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.backModal.show();
  }

  private onBack(): void {
    if (this.form.dirty == false) {
      this.backPage();
    } else {
      this.backModalToggle('แจ้งเตือน', 'ข้อมูลได้รับการแก้ไข ต้องการเปลี่ยนหน้าหรือไม่ ?', false);
    }
  }

  private backPage(): void {
    this.pageContext.backPage();
  }

  private back() {
    this.alertModal.hide();
    if (this.alert.isRedirect) {
      this.backPage();
    }
  }

    public onCancel() {
        this.closeEvent.emit();
    }

     private changCheckbox(event: any) {
    if (event.target.checked) {
        this.form.controls["empStatusStatus"].setValue(1);
    }else{
    this.form.controls["empStatusStatus"].setValue(0);
    }
    
  }

   private deleteModalClose(): void {
    this.deleteModal.hide();
  }

  private deleteModalConfirm(): void {
    this.onDelete();
    this.deleteModal.hide();
  }

  private alertModalDeleteToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.deleteModal.toggle();
  }

  public onBeforeDelete(record: any) {
    this.alertModalDeleteToggle('ยืนยันการลบ', 'คุณยืนยันการลบหรือไม่', true);
  }

  public onDelete() {
    LoadMask.show();
    this.objDelete.emp_id = this.emp_id
    this.service.deleteEmpStatus(this.page, this.objDelete).subscribe(response => {
      this.alertModalToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ', true);
      LoadMask.hide();
      this.pageContext.backPage();
    },
      error => {
        console.error(error);
        LoadMask.hide();
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
      }
    );
  }

  private changeProvince(valueField: any) {
    console.log(valueField)
    this.form.controls["provinceId"].setValue(valueField.PROVINCE_ID);
  }

  private changeCompany(valueField: any) {
    console.log(valueField)
    this.form.controls["company_id"].setValue(valueField.company_id);
  }

  private changeBranch(valueField: any) {
    console.log(valueField)
    this.form.controls["branch_id"].setValue(valueField.branch_id);
  }

  private changeBrithDate(valueField: any) {
    console.log(valueField)
    if (valueField) {
      this.objCriteria.emp_birth_date = valueField.toJSON();
    }
  }

  private changeStartDate(valueField: any) {
    console.log(valueField)
    if (valueField) {
      this.objCriteria.emp_work_date_start = valueField.toJSON();
    }
  }
}

  









    
    

   
    
    
       



    



  

   


