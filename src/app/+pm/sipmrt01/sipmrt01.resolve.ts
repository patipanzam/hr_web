import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { LoadMask } from '../../load-mask.component';
import { sipmrt01Service } from './sipmrt01.service';
import { BeanUtils } from '../../core/bean-utils.service';

@Injectable()
export class sipmrt01Resolve implements Resolve<any> {

  constructor(private selectService: CommonSelectService,  private sipmrt01Service: sipmrt01Service) {
    this.selectService.setModuleName('Main');
  }

  resolve(route: ActivatedRouteSnapshot): any {
        LoadMask.show();
  if (route.url[0].path == 'sipmrt01') {
    LoadMask.hide();
            return {
        
            };
  } else if (route.url[0].path == 'sipmrt01a') {
    if (BeanUtils.isNotEmpty(route.params.emp_id)) {
      var searchData = this.sipmrt01Service.searchData(route.params.emp_id);
      let province = this.selectService.getProvince();
      let company = this.selectService.getCompany();
      let branch = this.selectService.getBranch();
      return Observable.forkJoin([searchData,province,company,branch]).map((response) => {
        LoadMask.hide();
        return {
          searchData: response[0],
          province: response[1],
          company: response[2],
          branch: response[3],
        };
      }).first();
    } else {
      let province = this.selectService.getProvince();
      let company = this.selectService.getCompany();
      let branch = this.selectService.getBranch();
      return Observable.forkJoin([province,company,branch]).map((response) => {
        LoadMask.hide();
        return {
          province: response[0],
          company: response[1],
          branch: response[2],
        };
      }).first();
    }    
  } else if (route.url[0].path == 'sipmrt01A') {
    if (BeanUtils.isNotEmpty(route.params.emp_id)) {
      let province = this.selectService.getProvince();
      let company = this.selectService.getCompany();
      let branch = this.selectService.getBranch();
      return Observable.forkJoin([province,company,branch]).map((response) => {
        LoadMask.hide();
        return {
          province: response[0],
          company: response[1],
          branch: response[2],
        };
      }).first();
    } else {
      LoadMask.hide();
      return {

      };
    }
  }
  }
}
