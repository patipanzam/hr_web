export class sipmrt01Criteria {
    public inputSearch: string = '';
    public emp_id:number;
    public emp_code: string;
    public emp_first_name: string;
    public emp_last_name : string;
    public emp_birth_date : any;
    public emp_address : string;
    public emp_address_province_id : number;
    public emp_company_id : number;
    public emp_branch_id : number;
    public emp_work_date_start : any;
    public emp_salary : number;
     //Combobox
     public province: string = '';
     public provinceCode: string;
     public provinceId: number;
     public company: string = '';
     public company_id: number;
     public brithDate: any = '';
     public startDate: any = '';
}

export class sipmrt01SaveEmpStatus {
    public emp_id:number;
    public emp_code: string;
    public emp_first_name: string;
    public emp_last_name : string;
    public emp_birth_date : any;
    public emp_address : string;
    public emp_address_province_id : number;
    public emp_company_id : number;
    public emp_branch_id : number;
    public emp_work_date_start : any;
    public emp_salary : number;
      //Combobox
      public provinceCode: string;
      public provinceId: number;
      public company: string = '';
      public company_id: number;
      public brithDate: any = '';
      public startDate: any = '';
}