import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CommonSelectService } from '../../shared/service/common-select.service';
import { LoadMask } from '../../load-mask.component';
import { sipmrt02Service } from './sipmrt02.service';
import { BeanUtils } from '../../core/bean-utils.service';

@Injectable()
export class sipmrt02Resolve implements Resolve<any> {

    constructor(private selectService: CommonSelectService,  private sipmrt01Service: sipmrt02Service) {
        this.selectService.setModuleName('Main');
      }
    
      resolve(route: ActivatedRouteSnapshot): any {
            LoadMask.show();
      if (route.url[0].path == 'sipmrt02') {
        LoadMask.hide();
                return {
            
                };
      } else if (route.url[0].path == 'sipmrt02a') {
        if (BeanUtils.isNotEmpty(route.params.resign_reason_id)) {
        //   var searchData = this.sipmrt01Service.searchData(route.params.emp_id);
          let resign = this.selectService.getResign();
          return Observable.forkJoin([resign]).map((response) => {
            LoadMask.hide();
            return {
             // searchData: response[0],
             resign: response[0],
            };
          }).first();
        } else {
            let resign = this.selectService.getResign();
          return Observable.forkJoin([resign]).map((response) => {
            LoadMask.hide();
            return {
                resign: response[0],
            };
          }).first();
        }    
      } else if (route.url[0].path == 'sipmrt01A') {
        if (BeanUtils.isNotEmpty(route.params.resign_reason_id)) {
          let resign = this.selectService.getResign();
          return Observable.forkJoin([resign]).map((response) => {
            LoadMask.hide();
            return {
                resign: response[0],
            };
          }).first();
        } else {
          LoadMask.hide();
          return {
    
          };
        }
      }
      }
    }
    