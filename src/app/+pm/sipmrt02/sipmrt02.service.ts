import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/rest-server.service';
import { sipmrt02Criteria ,sipmrt02SaveResignSetting} from '../sipmrt02/sipmrt02.interface';
import { Http, RequestOptions, Response, Headers } from '@angular/http';
import { BeanUtils } from '../../core/bean-utils.service';

const querystring = require('querystring');

@Injectable()
export class sipmrt02Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService,private http: Http) {}





//saveResignSetting
public saveResignSetting(page: Page, params: sipmrt02SaveResignSetting): Observable<any> {
  return this.http.post(this.restServer.getAPI('Employee/saveResignSetting'), params)
  .map(val => {
    return {
      page: {
        total: 100,
        start: page.start,
        limit: page.limit
      }
      , records: val.json()
    }
  });
}




}


