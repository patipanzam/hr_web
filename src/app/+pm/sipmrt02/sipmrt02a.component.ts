import { SSDatePicker } from '../../shared/ss-datepicker.component';
import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ng2-bootstrap';
import { Page } from '../../shared/pagination.component';
import { sipmrt02Service } from './sipmrt02.service';
import { sipmrt02Criteria, sipmrt02SaveResignSetting} from '../sipmrt02/sipmrt02.interface';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { BeanUtils } from '../../core/bean-utils.service';
import { LoadMask } from '../../load-mask.component';
import { PageContext } from '../../shared/service/pageContext.service';
import { ResignSelectDirective } from "../shared/resign-select.directive";
import { DateUtils } from '../../core/date-utils.service';


@Component({
  selector: 'sipmrt02a',
  template: require('./sipmrt02a.component.html'),
  providers: [Page, sipmrt02Service]
})
export class sipmrt02aComponent implements AfterViewInit {

  @Output('closeModal') private closeEvent = new EventEmitter();
  @Input('info') private info: any;
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('backModal') public backModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;
  @ViewChild('alertModalSave') public alertModalSave: ModalDirective;
  @ViewChild('resign') public resign: ResignSelectDirective;

  private form: FormGroup = this.fb.group({
    amount:  this.fb.control(null, Validators.required)
  
  //combobox province
   , resign: this.fb.control("")    
   , resign_reason_id: this.fb.control("", Validators.required)
   , resign_reason_code: this.fb.control("")
   , resign_reason_name: this.fb.control("")

  
   
});

private idsetting_resign_reason_id: number = null;
private resign_reason_code: string = "";
public location = {};

private fromLoadObject: any;
private initMsg: string;
private showMsg = false;
private objDelete: sipmrt02SaveResignSetting = new sipmrt02SaveResignSetting();
private objCriteria: sipmrt02SaveResignSetting = new sipmrt02SaveResignSetting();
private alert = { title: '', msg: '', isRedirect: false };
private records: any[];

constructor(private page: Page
            , private fb: FormBuilder
            , private service: sipmrt02Service
            , private router: Router
            , private routerActive: ActivatedRoute
            , private pageContext: PageContext
            ) { this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if(BeanUtils.isNotEmpty(this.fromLoadObject.searchData)){
      // this.setData(this.fromLoadObject.searchData);
    }else {
      this.fromLoadObject.searchData = { records: { data: {} } };
    }
}

 ngAfterViewInit(){
   if(BeanUtils.isEmpty(this.idsetting_resign_reason_id)){
   }
   this.resign.setValue(this.resign_reason_code);
 }

 ngOnInit(){
  if(navigator.geolocation){
     navigator.geolocation.getCurrentPosition(position => {
     this.location = position.coords;
     });
  }
}

private alertModalToggle(title: string, msg: string, isRedirect: boolean): void {
this.alert.title = title;
this.alert.msg = msg;
this.alert.isRedirect = isRedirect;
this.alertModal.show();
}

private alertModalClose(): void {
  if(null != this.records){
    if(this.router.url.split("/")[2]=="sipmrt01a"){
    this.pageContext.backPage();
    }
    if(this.router.url.split("/")[2]=="sipmrt01A"){
    this.pageContext.backPage();
    }
  }
  this.alertModal.toggle();
}

public onSave() {
if(this.form.dirty){
if(BeanUtils.isNotEmpty(this.idsetting_resign_reason_id)){
  this.objCriteria.resign_reason_id = this.idsetting_resign_reason_id;
}
this.objCriteria.amount = this.form.controls["amount"].value;
this.objCriteria.resign_reason_id = this.form.controls["resign_reason_id"].value;

console.log( this.objCriteria)
this.showMsg = false;
this.service.saveResignSetting(this.page, this.objCriteria).subscribe(response => {
    this.idsetting_resign_reason_id = response.records.emp_id;
            LoadMask.hide();
            this.alertModalToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ', true);
  },
  error => {
    console.error(error);
    LoadMask.hide();
    this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
  }
  );
}else{
      LoadMask.hide();
      this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข', false);
}
}



// private setData(Object: any){
//    if(BeanUtils.isNotEmpty(Object)){
//     this.form.controls["empCode"].setValue(Object.records.emp_code);
//     this.form.controls["empName"].setValue(Object.records.emp_first_name);
//     this.form.controls["empSername"].setValue(Object.records.emp_last_name);
//     this.form.controls["empAddress"].setValue(Object.records.emp_address);
//     this.form.controls["empSaraly"].setValue(Object.records.emp_salary);
//     this.form.controls["company_id"].setValue(Object.records.emp_company_id);
//     this.form.controls["brithDate"].setValue(Object.records.emp_birth_date);
//     this.emp_id = Object.records.emp_id;
//     this.PROVINCE_CODE = Object.records.emp_address_province_id;
//     this.branch_id = Object.records.emp_branch_id;
//     this.company_id = Object.records.emp_company_id;
//     this.objCriteria.emp_birth_date = this.fromLoadObject.searchData.records.emp_birth_date;
//     this.objCriteria.emp_work_date_start = this.fromLoadObject.searchData.records.emp_work_date_start;
//    }
// }

private backModalToggle(title: string, msg: string, isRedirect: boolean): void {
this.alert.title = title;
this.alert.msg = msg;
this.alert.isRedirect = isRedirect;
this.backModal.show();
}

private onBack(): void {
if (this.form.dirty == false) {
  this.backPage();
} else {
  this.backModalToggle('แจ้งเตือน', 'ข้อมูลได้รับการแก้ไข ต้องการเปลี่ยนหน้าหรือไม่ ?', false);
}
}

private backPage(): void {
this.pageContext.backPage();
}

private back() {
this.alertModal.hide();
if (this.alert.isRedirect) {
  this.backPage();
}
}

public onCancel() {
    this.closeEvent.emit();
}



private deleteModalClose(): void {
this.deleteModal.hide();
}

private deleteModalConfirm(): void {
this.onDelete();
this.deleteModal.hide();
}

private alertModalDeleteToggle(title: string, msg: string, isRedirect: boolean): void {
this.alert.title = title;
this.alert.msg = msg;
this.alert.isRedirect = isRedirect;
this.deleteModal.toggle();
}

public onBeforeDelete(record: any) {
this.alertModalDeleteToggle('ยืนยันการลบ', 'คุณยืนยันการลบหรือไม่', true);
}

public onDelete() {
// LoadMask.show();
// this.objDelete.emp_id = this.emp_id
// this.service.deleteEmpStatus(this.page, this.objDelete).subscribe(response => {
//   this.alertModalToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ', true);
//   LoadMask.hide();
//   this.pageContext.backPage();
// },
//   error => {
//     console.error(error);
//     LoadMask.hide();
//     this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
//   }
// );
}

private changeResign(valueField: any) {
console.log(valueField)
this.form.controls["resign_reason_id"].setValue(valueField.resign_reason_id);
}


}
