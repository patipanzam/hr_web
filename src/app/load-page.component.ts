import { Component } from '@angular/core';

declare var $: any;

@Component({
  selector: 'loag-page',
  template: `
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
  `
})
export class LoadPageComponent {
}

export class LoadPage {
  public static isShow: boolean = true;
  public static show() {
    this.isShow = true;
    $('loag-page').removeClass('loaded');
  }
  public static hide() {
    this.isShow = false;
    setTimeout(function () { $('loag-page').addClass('loaded'); }, 200);
  }
  public static isShowing(): boolean {
    return this.isShow;
  }
}
