import { Component, OnInit } from '@angular/core';
import { MenuService } from './../core/menu.service';
import { AuthService } from './../auth.service';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private menuService: MenuService, private authService: AuthService) {}

  ngOnInit() {
    this.menuService.clearMenu();
  }

  logout() {
    this.authService.logout();
  }

  show() {
    if ( $(".collapse").css('display') == 'none' ){
      $(".collapse").show();

    } else {
      $(".collapse").hide();
    }
  }
}
