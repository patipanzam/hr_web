import { Injectable, Component } from '@angular/core';
import { Http, RequestOptions, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { CookieService } from 'angular2-cookie/core';
import { JwtHelper } from 'angular2-jwt';

import { LoadPage } from './load-page.component';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';

const http_basic: string = 'aWhyLXdlYjpzNHQyR1JUU0hXR0FSZDd6YlFteDR1SFQ=';
const base_authserver_url = 'http://localhost:4002';
const csrf_token_uri: string = `${base_authserver_url}/csrf`;
const login_uri: string = `${base_authserver_url}/Login/getprovince`;
const logout_uri: string = `${base_authserver_url}/logout`;
const ACCESS_TOKEN_KEY: string = 'id_token';

@Injectable()
export class AuthService {
  // store the URL so we can redirect after logging in
  private jwt: any;
  private csrfToken: any;
  private jwtHelper: JwtHelper = new JwtHelper();
  private _loginAnnounceSource = new Subject<any>();
  redirectUrl: string;
  username: string;
  isLoggedIn: boolean = false;
  isAuthened = this._loginAnnounceSource.asObservable();

  constructor(private http: Http, private cookieService:CookieService) {
  }

  getCsrfToken() {
    // let body = '';
    // let options = new RequestOptions({ withCredentials: true });
    // this.http.get(csrf_token_uri, options)
    //   .map((res, err) => res.json())
    //   .subscribe((csrf) => {
    //     // console.log('csrf token: ' + csrf.token);
    //     if (csrf.delegate) {
    //       this.csrfToken = csrf.delegate;
    //     } else {
    //       this.csrfToken = csrf;
    //     }
    //     //XSRF-TOKEN Angular2 spec.
    //     this.cookieService.put('XSRF-TOKEN', this.csrfToken.token);
    //     LoadPage.hide();
    //   },
    //   error => {
    //     console.log('error: ' + error);
    //     LoadPage.hide();
    //   });
    LoadPage.hide();
  }

  login(u: string, p: string): Observable<any> {

    if (this.isLoggedIn) {
      console.log('already authenticated');
      this._loginAnnounceSource.next(this.isLoggedIn);
      return
    }
    let headers = new Headers();
    headers.append('Authorization', `Basic ${http_basic}`);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    // headers.append(this.csrfToken.headerName, this.csrfToken.token);
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let body = `username=${u}&password=${p}`;

    return this.http.get(login_uri).map(response => {
      let result = response.json();
      console.log(result);
      this.isLoggedIn = true;
      // if (result.access_token) {
      //   localStorage.setItem(ACCESS_TOKEN_KEY, result.access_token);
      
      //   this.jwt = this.jwtHelper.decodeToken(result.access_token);
      //   this.isLoggedIn = true;
      //   this.username = this.jwt.user_name;

      //   return this.isLoggedIn;
      // } else {
      //   //{"error":"invalid_grant","error_description":"Bad credentials"}
      //   throw Observable.throw(result.error_description);
      // }
    }).catch(error => {
      this.isLoggedIn = true;
      // let err = error.json();
      console.log(error)
      return Observable.throw(error);
    });
  }

  logout() {
    localStorage.removeItem(ACCESS_TOKEN_KEY);
    this.isLoggedIn = false;
  }

  checkAuthen() {
    let token = localStorage.getItem(ACCESS_TOKEN_KEY);
    if (token) {
      this.jwt = this.jwtHelper.decodeToken(token);
      this.isLoggedIn = !this.jwtHelper.isTokenExpired(token);
      console.log('token expired? '+this.isLoggedIn);
      this.username = this.jwt.user_name;
    }
    this._loginAnnounceSource.next(this.isLoggedIn);
  }

}
