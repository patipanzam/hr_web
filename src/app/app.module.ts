import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { ANGULAR2_COOKIE_PROVIDERS,CookieService } from 'angular2-cookie/core';
// import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { Ng2BootstrapModule } from 'ng2-bootstrap';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { LoginRoutingModule } from './login-routing.module';
import { AUTH_PROVIDERS } from 'angular2-jwt';

/* App Root */
import { AppComponent } from './app.component';
import { LoginComponent } from './login.component';
import { PageNotFoundComponent } from './page-not-found.component';

import { LoadPageComponent } from './load-page.component';
import { LoadMaskComponent } from './load-mask.component';

/* Feature Root */
import { CoreModule } from './core/core.module';

import { PageContext } from './shared/service/pageContext.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    Ng2BootstrapModule.forRoot(),
    ChartsModule,
    LoginRoutingModule,
    AppRoutingModule,
    CoreModule.forRoot({ userName: 'Miss Marble' })
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotFoundComponent,
    LoadPageComponent,
    LoadMaskComponent
  ],
  providers: [
    CookieService,
    ANGULAR2_COOKIE_PROVIDERS,
    AUTH_PROVIDERS,
    PageContext
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
