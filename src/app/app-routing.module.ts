import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactivateGuard } from './can-deactivate-guard.service';
import { AuthGuard } from './auth-guard.service';

//Layouts
import { LoginComponent } from './login.component';

import { PageNotFoundComponent } from './page-not-found.component';

export const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  {

      path: 'dashboard',
      loadChildren: './dashboard/dashboard.module#DashboardModule'

  },
  {
      path: 'pm',
      loadChildren: './+pm/sipm.module#SipmModule'
  },
  
  { path: 'ta', loadChildren: './+ta/sita.module#SitaModule'},
 

  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [CanDeactivateGuard]
})
export class AppRoutingModule { }
