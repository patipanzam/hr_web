// Angular 2
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';
// angular2 cookie
import 'angular2-cookie/core';
// RxJS
import 'rxjs';

// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...

// import 'bootstrap-sass/assets/javascripts/bootstrap';
// import 'admin-lte/dist/js/app';

// import 'tether/dist/css/tether.css';
// import 'bootstrap/dist/js/bootstrap.js';

import './scss/preloader.scss';

import 'selectize/dist/js/selectize.js';
import 'selectize/dist/css/selectize.css';
import 'selectize/dist/css/selectize.bootstrap3.css';

import './scss/style.scss';

import 'bootstrap-datepicker/dist/css/bootstrap-datepicker3.css';
import 'bootstrap-datepicker/dist/js/bootstrap-datepicker.js';

import 'bootstrap-timepicker/css/bootstrap-timepicker.css';
import 'bootstrap-timepicker/js/bootstrap-timepicker.js';

import './assets/css/font-awesome.css';
import './assets/css/simple-line-icons.css';

import './vendor.scss';

import './scss/ihrStyle.scss';
